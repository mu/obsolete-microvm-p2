from microvm import (mtypes as t, snippet as s, builder)

class ConciseBuilder(builder.Builder):
    """ A builder merely more concise than Builder. """

    def insert_bb(self, name=None, old_bb=None):
        """ Create a new basic block and insert after another bb. """
        if old_bb == None:
            old_bb = self.cur_bb

        new_bb = s.BasicBlock(self.snp, name)
        for ix, bb in enumerate(self.snp.basic_blocks):
            if bb is old_bb:
                self.snp.basic_blocks.insert(ix+1, new_bb)
                break
        else:
            raise ValueError("old_bb not found")
        
        return new_bb

    def const(self, ty, data, *a, **ka):
        return s.Constant(ty, data, *a, **ka)

    def binop(self, op, lhs, rhs, *a, **ka):
        ty = lhs.value_type
        return self.BinOp(ty, op, lhs, rhs, *a, **ka)

    def cmp(self, cond, lhs, rhs, *a, **ka):
        ty = lhs.value_type
        return self.Cmp(ty, cond, lhs, rhs, *a, **ka)

    def select(self, cond, if_true, if_false, *a, **ka):
        ty = if_true.value_type
        return self.Select(ty, cond, if_true, if_false, *a, **ka)
    
    def trunc(self, opnd, to_len, *a, **ka):
        from_len = opnd.value_type.length
        if isinstance(to_len, t.Int):
            to_len = to_len.length
        return self.Trunc(from_len, to_len, opnd, *a, **ka)

    def zext(self, opnd, to_len, *a, **ka):
        from_len = opnd.value_type.length
        if isinstance(to_len, t.Int):
            to_len = to_len.length
        return self.ZExt(from_len, to_len, opnd, *a, **ka)

    def sext(self, opnd, to_len, *a, **ka):
        from_len = opnd.value_type.length
        if isinstance(to_len, t.Int):
            to_len = to_len.length
        return self.SExt(from_len, to_len, opnd, *a, **ka)

    def uitofp(self, opnd, to_ty, *a, **ka):
        from_ty = opnd.value_type
        return self.UIToFP(from_ty, to_ty, opnd, *a, **ka)

    def sitofp(self, opnd, to_ty, *a, **ka):
        from_ty = opnd.value_type
        return self.SIToFP(from_ty, to_ty, opnd, *a, **ka)

    def fptoui(self, opnd, to_ty, *a, **ka):
        from_ty = opnd.value_type
        return self.FPToUI(from_ty, to_ty, opnd, *a, **ka)

    def fptosi(self, opnd, to_ty, *a, **ka):
        from_ty = opnd.value_type
        return self.FPToSI(from_ty, to_ty, opnd, *a, **ka)

    def bitcast(self, opnd, to_ty, *a, **ka):
        from_ty = opnd.value_type
        return self.BitCast(from_ty, to_ty, opnd, *a, **ka)

    def refcast(self, opnd, to_ty, *a, **ka):
        from_ty = opnd.value_type.ty
        return self.RefCast(from_ty, to_ty, opnd, *a, **ka)

    def isint(self, opnd, *a, **ka):
        return self.IsInt(opnd, *a, **ka)

    def isfp(self, opnd, *a, **ka):
        return self.IsFP(opnd, *a, **ka)
    
    def isref(self, opnd, *a, **ka):
        return self.IsRef(opnd, *a, **ka)

    def inttotr64(self, ty, opnd, *a, **ka):
        return self.IntToTR64(ty, opnd, *a, **ka)

    def fptotr64(self, ty, opnd, *a, **ka):
        return self.FPToTR64(ty, opnd, *a, **ka)

    def reftotr64(self, opnd, tag, *a, **ka):
        ty = opnd.value_type.ty
        return self.RefToTR64(ty, opnd, tag, *a, **ka)

    def tr64toint(self, opnd, *a, **ka):
        ty = opnd.value_type.ty
        return self.TR64ToInt(ty, opnd, *a, **ka)

    def tr64tofp(self, opnd, *a, **ka):
        ty = opnd.value_type.ty
        return self.TR64ToFP(ty, opnd, *a, **ka)

    def tr64toref(self, opnd, *a, **ka):
        ty = opnd.value_type.ty
        return self.TR64ToRef(ty, opnd, *a, **ka)

    def tr64totag(self, opnd, *a, **ka):
        ty = opnd.value_type.ty
        return self.TR64ToTag(ty, opnd, *a, **ka)

    def br(self, bb, *a, **ka):
        if isinstance(bb, str):
            bb = self.new_bb(name=bb)

        self.Branch(bb, *a, **ka)
        return bb

    def br2(self, cond, if_true, if_false, *a, **ka):
        if isinstance(if_true, str):
            if_true = self.new_bb(name=if_true)
        if isinstance(if_false, str):
            if_false = self.new_bb(name=if_false)
        self.Branch2(cond, if_true, if_false, *a, **ka)
        return if_true, if_false

    def jt(self, cond, if_true, *a, **ka):
        """
        A pseudo instruction emulating the "fall through" behaviour. It
        translates to a Branch2, but the builder creates an anonymous basic
        block for the case when the cond is false and uses it.
        """
        new_bb = self.insert_bb(self.cur_bb.name+">jt")
        t,f = self.br2(cond, if_true, new_bb, *a, **ka)
        assert(f==new_bb)
        self.use(f)
        assert(self.cur_bb is new_bb)
        return t

    def jf(self, cond, if_false, *a, **ka):
        """
        A pseudo instruction emulating the "fall through" behaviour. It
        translates to a Branch2, but the builder creates an anonymous basic
        block for the case when the cond is true and uses it.
        """
        new_bb = self.insert_bb(self.cur_bb.name+">jf")
        t,f = self.br2(cond, new_bb, if_false, *a, **ka)
        self.use(t)
        return f

    def switch(self, val, def_target, cases, *a, **ka):
        ty = val.value_type
        return self.Switch(ty, val, def_target, cases, *a, **ka)

    def phi(self, ty, cases, *a, **ka):
        return self.Phi(ty, cases, *a, **ka)

    def call(self, target, args, *a, **ka):
        sig = target.value_type.sig
        return self.Call(sig, target, args, *a, **ka)

    def invoke(self, target, args, normal, exceptional, *a, **ka):
        sig = target.value_type.sig
        return self.Invoke(sig, target, args, normal, exceptional, *a, **ka)

    def tailcall(self, target, args, *a, **ka):
        sig = target.value_type.sig
        return self.TailCall(sig, target, args, *a, **ka)

    def ret(self, val, *a, **ka):
        ty = val.value_type
        return self.Ret(ty, val, *a, **ka)

    def throw(self, val, *a, **ka):
        return self.Throw(val, *a, **ka)

    def landingpad(self, *a, **ka):
        return self.LandingPad(*a, **ka)

    def alloc(self, td, aid=0, *a, **ka):
        return self.Alloc(td, aid, *a, **ka)

    def allocha(self, td, length, aid=0, *a, **ka):
        return self.AllocHybridArray(td, length, aid, *a, **ka)

    def getscalarfield(self, obj, field_indices:"[int]", *a, **ka):
        obj_ty = obj.value_type.ty
        assert(not isinstance(obj_ty, t.HybridArray))
        field_ty = t.field_type(obj_ty, field_indices)
        return self.GetField(obj_ty, field_ty, field_indices, t.SCALAR, None,
                obj, *a, **ka)

    def setscalarfield(self, obj, field_indices:"[int]", val, *a, **ka):
        obj_ty = obj.value_type.ty
        assert(not isinstance(obj_ty, t.HybridArray))
        field_ty = t.field_type(obj_ty, field_indices)
        return self.SetField(obj_ty, field_ty, field_indices, t.SCALAR, None,
                obj, val, *a, **ka)

    def getfixedfield(self, obj, field_indices:"[int]", *a, **ka):
        obj_ty = obj.value_type.ty
        assert(isinstance(obj_ty, t.HybridArray))
        field_ty = t.field_type(obj_ty.fixed_ty, field_indices)
        return self.GetField(obj_ty, field_ty, field_indices, t.FIXED_PART, None,
                obj, *a, **ka)

    def setfixedfield(self, obj, field_indices:"[int]", val, *a, **ka):
        obj_ty = obj.value_type.ty
        assert(isinstance(obj_ty, t.HybridArray))
        field_ty = t.field_type(obj_ty.fixed_ty, field_indices)
        return self.SetField(obj_ty, field_ty, field_indices, t.FIXED_PART, None,
                obj, val, *a, **ka)

    def getvarfield(self, obj, index, field_indices:"[int]", *a, **ka):
        obj_ty = obj.value_type.ty
        assert(isinstance(obj_ty, t.HybridArray))
        field_ty = t.field_type(obj_ty.var_ty, field_indices)
        return self.GetField(obj_ty, field_ty, field_indices, t.VAR_PART, index,
                obj, *a, **ka)

    def setvarfield(self, obj, index, field_indices:"[int]", val, *a, **ka):
        obj_ty = obj.value_type.ty
        assert(isinstance(obj_ty, t.HybridArray))
        field_ty = t.field_type(obj_ty.var_ty, field_indices)
        return self.SetField(obj_ty, field_ty, field_indices, t.VAR_PART, index,
                obj, val, *a, **ka)

    def getlength(self, obj, *a, **ka):
        obj_ty = obj.value_type.ty
        return self.GetLength(obj_ty, obj, *a, **ka)

    def trap(self, handler, args, *a, **ka):
        sig = t.Signature([inst.value_type for inst in args])
        return self.Trap(sig, handler, args, *a, **ka)

    def trapcall(self, sig, handler, args, *a, **ka):
        return self.TrapCall(sig, handler, args, *a, **ka)

    def intrinsiccall(self, funcname, args, *a, **ka):
        return self.IntrinsicCall(funcname, args, *a, **ka)

