"""
This module translates Lua code into MicroVM IR.

There are two main objects. LuaCompiler and LuaBuilder. LuaCompiler is a
companion object of the LuaClient and it is responsible for walking the parse
tree of Lua. LuaBuilder, on the other hand, is an extended version of
microvm.Builder. It is responsible for constructing the MicroVM IR in a Lua-
specific way and is capable of generating MicroVM instructions for basic Lua
operations.

There only needs to be one instance of LuaCompiler per LuaClient, but there
must be one LuaBuilder per Lua function.

The line between LuaCompiler and LuaBuilder is a bit blur. A rule of thumb is
that if an operation generates MicroVM Values but does not touch the parse tree
or the scope, then it belongs to LuaBuilder. If it works on the Lua syntax,
then it belongs to LuaCompiler.
"""

from .thirdparty import lua52grammar, lua52precompiled, lrparsing

grammar = lua52grammar.Lua52Grammar
grammar.pre_compile_grammar(lua52precompiled.LUA52_PRECOMPILED)

from .luaconstants import *
from . import concisebuilder

from collections import namedtuple

import os
_DEBUG = "LUABUILDER_DEBUG" in os.environ

# Some helper functions

def is_token(pt, *ms):
    return isinstance(pt[0], lrparsing.Token) and pt[1] in ms

# A binding associates a local variable name with the scope it is defined in and
# the current SSA value it is bound to.
Binding = namedtuple("Binding", ["scope", "value"])

def pthandler(ptnode):
    """
    An annotation on a LuaCompiler method. It will check the "pt" parameter and
    verify it is passed the correct parse tree node.
    """
    def decl(f):
        def wrapper(self, b, pt, scope, *args, **kwargs):
            if not isinstance(b, LuaBuilder):
                raise ValueError("Argument b has type {}, but LuaBuilder is "
                        "expected.".format(type(b).__name__))
            if not isinstance(scope, Scope):
                raise ValueError("Argument scope has type {}, but Scope is "
                        "expected.".format(type(scope).__name__))
            if pt[0] != ptnode:
                raise ValueError("Method {} handles parse tree node {}, but "
                        "node {} is supplied.".format(f.__name__, ptnode,
                            pt[0]))
            return f(self, b, pt, scope, *args, **kwargs)
        wrapper.__name__ = f.__name__
        return wrapper
    return decl

class Scope(object):
    def __init__(self, parent=None):
        self.bindings = {}      # str -> Binding
        self.parent=parent
        self.level = 0 if parent == None else parent.level+1
        # note: bindings may contain variables of the parent scopes, which
        # means those variables are re-assigned in the current or nested
        # scopes. The parent scope's translator function should merge the
        # current scope properly acording to specific control structures
        # (while, repeat, for)

class LuaClientFriend(object):
    """
    The base class of some classes which are too tightly coupled with LuaClient.

    Including LuaCompiler and LuaBuilder.
    """

    def __init__(self, lc):
        self.lc = lc

    # Associated objects

    mvm  = property(lambda b: b.lc.mvm)
    de   = property(lambda b: b.lc.de)
    itpr = property(lambda b: b.lc.itpr)

    # TypeDescriptors

    number_td     = property(lambda b: b.lc.number_td)
    string_td     = property(lambda b: b.lc.string_td)
    table_td      = property(lambda b: b.lc.table_td)
    hash_table_td = property(lambda b: b.lc.hash_table_td)
    function_td   = property(lambda b: b.lc.function_td)
    params_td     = property(lambda b: b.lc.params_td)
    error_td      = property(lambda b: b.lc.error_td)

    # SSA Constants

    nil   = property(lambda b: s.Constant(lv_r, b.lc.make_nil()))
    false = property(lambda b: s.Constant(lv_r, b.lc.make_boolean(False)))
    true  = property(lambda b: s.Constant(lv_r, b.lc.make_boolean(True)))

    def number(self, number:float):
        return s.Constant(lv_r, self.lc.make_number(number))

    def string(self, string:str):
        return s.Constant(lv_r, self.lc.make_string(string))

    empty_params = property(lambda b: s.Constant(p_r, b.lc.make_params()))

    env = property(lambda b: s.Constant(lv_r, b.lc.get_env()))
    #env = property(lambda b: 42)

    def primfunc(self, func_name:str) -> "V:lv_r":
        sid, sig = self.lc.prim_funcs[func_name]
        return s.Constant(t.FuncHandle(sig), sid)

    def builtinfunc(self, func_name:str) -> "V:lv_r":
        func_addr = self.lc.builtin_funcs[func_name].func_gr.obj_addr
        func_tr = self.de.ref_to_tr64(func_addr, LUA_TFUNCTION)
        return s.Constant(lv_r, func_tr)

class LuaCompiler(LuaClientFriend):
    """
    Compiles Lua code to MicroVM IR. This is a stateless object. Create only one
    per LuaClient.
    """

    def build_from_code(self, code:str) -> 'addr:f_t':
        lines = code.splitlines()

        # Detect "she-bang"
        if len(lines)>0 and lines[0].startswith("#!"):
            skip = len(lines[0])+1
        else:
            skip = 0

        lua_prog = "\n".join(lines[skip:])

        parse_tree = grammar.parse(lua_prog)

        if _DEBUG:
            print(grammar.repr_parse_tree(parse_tree))

        func = self.chunk(parse_tree)

        return func

    # Visitors of the parse tree

    ## Helpers

    def dispatch(self, b:'LuaBuilder', pt:'ParseTree', scope:Scope):
        if isinstance(pt[0], lrparsing.Token):
            method_name = pt[0].name.split(".")[-1]
        else:
            method_name = pt[0].name

        method = getattr(self, method_name)
        return method(b, pt, scope)

    ## Tokens

    def name(self, b, pt, scope):
        raise Exception("The semantic of name is context-dependent."
                "See variable_ref, field, function_call, name_list, "
                "_parameter_list, function_name, for_step and "
                "local_function_decl_st." )

    @pthandler(grammar.T.short_string)
    def short_string(self, b, pt, scope) -> 'V:lv_r':
        "returns: String constant"
        RE = lua52grammar.LUA_SHORT_STRING_RE
        ESC = lua52grammar.LUA_SHORT_STRING_ESC
        def lua_sub(match):
            data = match.group()
            c = data[1]
            result = ESC.get(c, c)
            if not callable(result):
                return result
            return result(data[1:])
        str_content = RE.sub(lua_sub, pt[1])[1:-1]
        luastr = b.string(str_content)
        return luastr

    @pthandler(grammar.T.long_string)
    def long_string(self, b, pt, scope) -> 'V:lv_r':
        "returns: String constant"
        marker_len = pt[1].find('[', 1) + 1
        if pt[1][marker_len] == '\n':
            str_content = pt[1][marker_len + 1:-marker_len]
        else:
            str_content = pt[1][marker_len:-marker_len]
        luastr = b.string(str_content)
        return luastr

    @pthandler(grammar.T.decimal_number)
    def decimal_number(self, b, pt, scope) -> 'V:lv_r':
        "returns: Number constant"
        num = float(pt[1])
        luanum = b.number(num)
        return luanum

    @pthandler(grammar.T.hex_number)
    def hex_number(self, b, pt, scope) -> 'V:lv_r':
        "returns: Number constant"
        toks = pt[1].split("p")
        if len(toks) == 1:
            mult = 1.0
        else:
            mult = 2.0 ** int(toks[1])
        toks = toks[0].split(".")
        if len(toks) == 1:
            num = float(int(toks[0], 16)) * mult
        else:
            num = int(toks[0] + toks[1], 16) * mult / 16.0 ** len(toks[1])
        luanum = b.number(num)
        return luanum

    @pthandler(grammar.T.var_args)
    def var_args(self, b, pt, scope):
        raise NotImplementedError(pt)

    ## Non-terminals

    @pthandler(grammar.string)
    def string(self, b, pt, scope) -> 'V:lv_r':
        "returns: String constant"
        return self.dispatch(b, pt[1], scope)

    @pthandler(grammar.number)
    def number(self, b, pt, scope) -> 'V:lv_r':
        "returns: Number constant"
        return self.dispatch(b, pt[1], scope)

    @pthandler(grammar.variable_ref)
    def variable_ref(self, b, pt, scope) -> 'V:lv_r':
        var_name = pt[1][1]
        var_val = self.rvalue_search_scope(b, var_name, scope)

        for next_vr in pt[2:]:
            if not is_token(next_vr, "."):
                next_var_name = next_vr[1]
                var_val = b.lua_index(var_val, b.string(next_var_name))

        return var_val

    @pthandler(grammar.subscript_exp)
    def subscript_exp(self, b, pt, scope) -> 'V:lv_r':
        obj = self.prefix_exp(b, pt[1], scope, single_value=True)
        sub = self.exp(b, pt[3], scope, single_value=True)
        return b.lua_index(obj, sub)

    @pthandler(grammar.var)
    def var(self, b, pt, scope) -> 'V:lv_r':
        return self.dispatch(b, pt[1], scope)

    # NOTE: LValue is one of:
    #     | ("local", Scope, str)       A local variable
    #     | ("global", str)             A global variable
    #     | ("subscr", V:lv_r, V:lv_r)  A subscription

    @pthandler(grammar.variable_ref)
    def variable_ref_lval(self, b, pt, scope) -> 'LValue':
        valid_vrs = [vr for vr in pt[1:] if not is_token(vr, ".")]
        if len(valid_vrs) == 1:
            var_name = valid_vrs[0][1]
            return self.lvalue_search_scope(var_name, scope)
        else:
            var_name = valid_vrs[0][1]
            var_val = self.rvalue_search_scope(b, var_name, scope)

            for next_vr in pt[1:-1]:
                next_var_name = next_vr[1]
                var_val = b.lua_index(var_val, b.string(next_var_name))

            return ("subscr", var_val, b.string(pt[-1][1]))

    @pthandler(grammar.subscript_exp)
    def subscript_exp_lval(self, b, pt, scope) -> 'LValue':
        obj = self.prefix_exp(b, pt[1], scope, single_value=True)
        sub = self.exp(b, pt[3], scope, single_value=True)
        return ("subscr", obj, sub)

    @pthandler(grammar.var)
    def var_lval(self, b, pt, scope) -> 'LValue':
        if pt[1][0] == grammar.variable_ref:
            return self.variable_ref_lval(b, pt[1], scope)
        else:
            raise NotImplementedError("LValue: {}".format(repr(pt)))

    @pthandler(grammar.var_list)
    def var_list(self, b, pt, scope) -> '[LValue]':
        lvals = []
        for var in pt[1:]:
            if not is_token(var, ","):
                lval = self.var_lval(b, var, scope)
                lvals.append(lval)
        return lvals

    @pthandler(grammar.exp_list)
    def exp_list(self, b, pt, scope) -> '[V:lv_t|p_r]':
        "returns: A list of lv_t, but the last one may be p_r"
        vals = []
        valid_exps = [exp for exp in pt[1:] if not is_token(exp, ",")]
        for ix, exp in enumerate(valid_exps):
            val = self.exp(b, exp, scope, single_value=(ix!=len(valid_exps)-1))
            vals.append(val)
        return vals

    @pthandler(grammar.field)
    def field(self, b, pt, scope):
        raise NotImplementedError(pt)

    @pthandler(grammar.table_constructor)
    def table_constructor(self, b, pt, scope):
        raise NotImplementedError(pt)

    @pthandler(grammar.function_args)
    def function_args(self, b, pt, scope) -> '[V:lv_t|p_r]':
        if is_token(pt[1], "("):
            if pt[2][0] == grammar.exp_list:
                return self.exp_list(b, pt[2], scope)
            else:
                return []
        else:
            return [self.dispatch(b, pt[1], scope)]

    @pthandler(grammar.function_call)
    def function_call(self, b, pt, scope, single_value) -> 'V:lv_t|p_r':
        """ Returns lv_t if single_value==True. Otherwise returns p_r """
        if is_token(pt[2], ":"):
            raise NotImplementedError("Method call")
        else:
            func = self.prefix_exp(b, pt[1], scope, single_value=True)
            func_args = self.function_args(b, pt[2], scope)
            largs = b.params_pack(func_args)

            rv = b.lua_call(func, largs)

        if single_value:
            [rv] = b.params_unpack(rv, 1)

        return rv

    @pthandler(grammar.block)
    def block(self, b, pt, scope) -> bool:
        "returns: True if it has a last_statement. False otherwise"
        for x in pt[1:]:
            if x[0] == grammar.statement:
                self.statement(b, x, scope)
            elif x[0] == grammar.last_statement:
                self.last_statement(b, x, scope)
                new_bb = b.new_bb("unreachable bb after block")
                b.use(new_bb)
                return True

        return False

    @pthandler(grammar.name_list)
    def name_list(self, b, pt, scope) -> '[LValue]':
        lvals = []
        for name in pt[1:]:
            if not is_token(name, ","):
                lval = ("local", scope, name[1])
                lvals.append(lval)
        return lvals

    @pthandler(grammar.parameter_list)
    def parameter_list(self, b, pt, scope) -> '[str]':
        params = []
        for p in pt[1:]:
            if p[0] == grammar.T.name:
                params.append(p[1])
            elif p[0] == grammar.T.var_args:
                raise NotImplementedError(p)

        return params

    @pthandler(grammar.function_body)
    def function_body(self, b, pt, scope, name:str) -> 'V:lv_r':
        " Returns a lv_r, which contains a f_r "

        nb = LuaBuilder(self.lc, luafunc_sig, name)
        snp = nb.snp
        [lua_args] = snp.params

        new_scope = Scope()

        params = self.parameter_list(nb, pt[3], new_scope)
        lvals = [("local", new_scope, name) for name in params]
        rvals = [lua_args]
        self.assign_many(nb, lvals, rvals, new_scope)

        self.block(nb, pt[5], new_scope)

        # Guard in case the body does not return.
        nb.ret(nb.empty_params)

        if _DEBUG:
            print(LuaSnippetPrettyPrinter(self.lc).lua_aware_ppsnippet(snp))

        sc.check_snippet(snp)
        sid = self.itpr.declare(luafunc_sig)
        self.itpr.define(sid, snp)

        func = self.de.alloc(self.function_td)
        self.lc._lua_function_set_mvm_func(func, sid)
        func_lv = self.de.ref_to_tr64(func, LUA_TFUNCTION)

        return s.Constant(lv_r, func_lv)

    @pthandler(grammar.anon_function)
    def anon_function(self, b, pt, scope) -> 'V:lv_r':
        return self.function_body(b, pt[2], scope, "<lua_anon_func_at_({},{})>"
                .format(pt[1][3], pt[1][4]))

    @pthandler(grammar.constant)
    def constant(self, b, pt, scope) -> 'V:lv_r':
        if is_token(pt[1], "nil"):
            return b.nil
        elif is_token(pt[1], "false"):
            return b.false
        elif is_token(pt[1], "true"):
            return b.true
        else:
            raise ValueError(pt)

    @pthandler(grammar.adjusted_exp)
    def adjusted_exp(self, b, pt, scope) -> 'V:lv_r':
        return self.exp(b, pt[2], scope, single_value=True)

    @pthandler(grammar.prefix_exp)
    def prefix_exp(self, b, pt, scope, single_value) -> 'V:[lv_r|p_r]':
        if pt[1][0] == grammar.function_call:
            return self.function_call(b, pt[1], scope, single_value)
        else:
            return self.dispatch(b, pt[1], scope)

    @pthandler(grammar.exp)
    def exp(self, b, pt, scope, single_value) -> 'V:[lv_r|p_r]':
        if len(pt) == 2:
            if pt[1][0] == grammar.prefix_exp:
                return self.prefix_exp(b, pt[1], scope, single_value)
            else:
                return self.dispatch(b, pt[1], scope)
        elif (len(pt) == 4 and pt[1][0] == grammar.exp and
                pt[3][0] == grammar.exp and
                isinstance(pt[2][0], lrparsing.Token)):
            op = pt[2][1]
            if pt[2][1] in ["and", "or"]:
                return self.binop_exp_lazy(b, pt[1], op, pt[3], scope)
            else:
                lhs = self.exp(b, pt[1], scope, single_value=True)
                rhs = self.exp(b, pt[3], scope, single_value=True)
                if   op == "+":  return b.lua_add(lhs, rhs)
                elif op == "-":  return b.lua_sub(lhs, rhs)
                elif op == "*":  return b.lua_mul(lhs, rhs)
                elif op == "/":  return b.lua_div(lhs, rhs)
                elif op == "%":  return b.lua_mod(lhs, rhs)
                elif op == "^":  return b.lua_pow(lhs, rhs)
                elif op == "==": return b.lua_eq(lhs, rhs)
                elif op == "~=": return b.lua_ne(lhs, rhs)
                elif op == "<":  return b.lua_lt(lhs, rhs)
                elif op == "<=": return b.lua_le(lhs, rhs)
                elif op == ">":  return b.lua_lt(rhs, lhs)
                elif op == ">=": return b.lua_le(rhs, lhs)
                elif op == "..": return b.lua_concat(lhs, rhs)
                else:
                    raise ValueError("Unrecognised binop {}".format(op))
        elif (len(pt) == 3 and isinstance(pt[1][0], lrparsing.Token) and
                pt[2][0] == grammar.exp):
            opnd = self.exp(b, pt[2], scope, single_value=True)
            op = pt[1][1]
            if   op == "not": return b.lua_not(opnd)
            elif op == "-":   return b.lua_unm(opnd)
            elif op == "#":   return b.lua_len(opnd)
            else:
                raise ValueError("Unrecognised unop {}".format(op))
        else:
            raise ValueError("Unexpected expression {}".format(pt))

    @pthandler(grammar.function_name)
    def function_name(self, b, pt, scope) -> 'LValue':
        lval = self.variable_ref_lval(b, pt[1], scope)

        if len(pt)==4:
            raise NotImplementedError("Method definition")

        return lval

    @pthandler(grammar.assign_st)
    def assign_st(self, b, pt, scope) -> None:
        lvals = self.var_list(b, pt[1], scope)
        rvals = self.exp_list(b, pt[3], scope)
        self.assign_many(b, lvals, rvals, scope)

    @pthandler(grammar.do_st)
    def do_st(self, b, pt, scope) -> None:
        nested_scope = Scope(scope)
        self.block(b, pt[2][2], nested_scope)

        # Copy re-defined variable bindings
        for var_name, binding in nested_scope.bindings.items():
            if binding.scope.level <= scope.level:
                scope.bindings[var_name] = binding

    @pthandler(grammar.while_st)
    def while_st(self, b, pt, scope) -> None:
        cond_pt, body_pt = pt[2], pt[4][2]
        
        prev_bb = b.cur_bb

        while_head = b.new_bb("while_head")
        while_body = b.new_bb("while_body")
        while_next = b.new_bb("while_next")
        while_exit = b.new_bb("while_exit")

        b.br(while_head)

        rebinds = self.inspect_for_variable_rebindings(body_pt, scope)

        b.use(while_head)

        phi_nodes = {}

        for rebind_name in rebinds:
            old_scope, old_rval = self.binding_search_scope(rebind_name, scope)

            phi_node = b.phi(lv_r, {prev_bb: old_rval})
            phi_nodes[rebind_name] = phi_node
            scope.bindings[rebind_name] = Binding(old_scope, phi_node)

        cond = self.exp(b, cond_pt, scope, single_value=True)
        b.br2(b.is_lua_true(cond), while_body, while_exit)

        b.use(while_body)
        new_scope = Scope(scope)
        self.block(b, body_pt, new_scope)
        b.br(while_next)
        
        b.use(while_next)
        b.br(while_head)

        for rebind_name, phi_node in phi_nodes.items():
            new_rval = new_scope.bindings[rebind_name].value
            phi_node.cases[while_next] = new_rval

        b.use(while_exit)

    @pthandler(grammar.repeat_st)
    def repeat_st(self, b, pt, scope) -> None:
        body_pt, cond_pt = pt[2][2], pt[4]
        
        prev_bb = b.cur_bb

        repeat_body = b.new_bb("repeat_body")
        repeat_tail = b.new_bb("repeat_tail")
        repeat_next = b.new_bb("repeat_next")
        repeat_exit = b.new_bb("repeat_exit")

        b.br(repeat_body)

        rebinds = self.inspect_for_variable_rebindings(body_pt, scope)
        
        b.use(repeat_body)

        phi_nodes = {}

        for rebind_name in rebinds:
            old_scope, old_rval = self.binding_search_scope(rebind_name, scope)

            phi_node = b.phi(lv_r, {prev_bb: old_rval})
            phi_nodes[rebind_name] = phi_node
            scope.bindings[rebind_name] = Binding(old_scope, phi_node)

        new_scope = Scope(scope)
        self.block(b, body_pt, new_scope)

        for rebind_name, phi_node in phi_nodes.items():
            new_rval = new_scope.bindings[rebind_name].value
            phi_node.cases[repeat_next] = new_rval

        b.br(repeat_tail)

        b.use(repeat_tail)
        # Copy re-defined variable bindings.
        # Those phi_nodes are for the repeat_body, but not repeat_exit
        for var_name, binding in new_scope.bindings.items():
            if binding.scope.level <= scope.level:
                scope.bindings[var_name] = binding

        cond = self.exp(b, cond_pt, scope, single_value=True)
        b.br2(b.is_lua_true(cond), repeat_exit, repeat_next)

        b.use(repeat_next)
        b.br(repeat_body)
        
        b.use(repeat_exit)

    @pthandler(grammar.if_st)
    def if_st(self, b, pt, scope) -> None:
        cond_body_pairs = [(pt[2], pt[4][2])]
        body_else = None

        for ix in range(5, len(pt), 4):
            if is_token(pt[ix], "elseif"):
                cond_body_pairs.append((pt[ix+1], pt[ix+3][2]))
            elif is_token(pt[ix], "else"):
                body_else = pt[ix+1][2]
                break
            elif is_token(pt[ix], "end"):
                break
            else:
                raise ValueError(pt[ix])

        branch_hbcs = [(
            b.new_bb("if_case{}_head".format(ix)),
            b.new_bb("if_case{}_body".format(ix)),
            b.new_bb("if_case{}_cont".format(ix)),
            Scope(scope),
            ) for ix in range(len(cond_body_pairs))]

        if body_else != None:
            bb_else = b.new_bb("bb_else")
            bb_else_cont = b.new_bb("bb_else_cont")
        else:
            bb_empty_cont = b.new_bb("bb_empty_cont")

        if_exit = b.new_bb("if_exit")

        b.br(branch_hbcs[0][0])

        prev_bb = b.cur_bb

        for ix, (cond_pt, body_pt) in enumerate(cond_body_pairs):
            bb_head, bb_body, bb_cont, nested_scope = branch_hbcs[ix]

            b.use(bb_head)
            cond = self.exp(b, cond_pt, scope, single_value=True)
            if ix == len(cond_body_pairs)-1:
                if body_else != None:
                    fail_to = bb_else
                else:
                    fail_to = bb_empty_cont
            else:
                fail_to = branch_hbcs[ix+1][0]
            b.br2(b.is_lua_true(cond), bb_body, fail_to)

            b.use(bb_body)
            self.block(b, body_pt, nested_scope)
            b.br(bb_cont)

            b.use(bb_cont)
            b.br(if_exit)

        if body_else != None:
            else_scope = Scope(scope)
            b.use(bb_else)
            self.block(b, body_else, else_scope)
            b.br(bb_else_cont)
            
            b.use(bb_else_cont)
            b.br(if_exit)
        else:
            b.use(bb_empty_cont)
            b.br(if_exit)

        rebinds = set()

        branch_scopes = [x[3] for x in branch_hbcs]
        else_scopes = [else_scope] if body_else != None else []

        for nested_scope in branch_scopes + else_scopes:
            for var_name, (def_scope, _) in nested_scope.bindings.items():
                if def_scope.level <= scope.level:
                    rebinds.add(var_name)

        b.use(if_exit)
        for var_name in rebinds:
            old_scope, old_rval = self.binding_search_scope(var_name, scope)
            phi_node = b.phi(lv_r, {})
            for _,_,bb_cont,ns in branch_hbcs:
                binding = ns.bindings.get(var_name, None)
                if (binding != None and binding.scope.level <= scope.level):
                    phi_node.cases[bb_cont] = binding.value
                else:
                    phi_node.cases[bb_cont] = old_rval

            if body_else != None:
                binding = else_scope.bindings.get(var_name, None)
                if (binding != None and binding.scope.level <= scope.level):
                    phi_node.cases[bb_else_cont] = binding.value
                else:
                    phi_node.cases[bb_else_cont] = old_rval
            else:
                phi_node.cases[bb_empty_cont] = old_rval
            scope.bindings[var_name] = Binding(old_scope, phi_node)

    @pthandler(grammar.for_steps)
    def for_steps(self, b, pt, scope) -> '(V:lv_r)*3':
        name = pt[1][1]

        var = self.exp(b, pt[3], scope, single_value=True)
        limit = self.exp(b, pt[5], scope, single_value=True)
        if len(pt)==8:
            step = self.exp(b, pt[7], scope, single_value=True)
        else:
            step = b.number(1.0)

        return (name, var, limit, step)

    @pthandler(grammar.for_step_st)
    def for_step_st(self, b, pt, scope) -> None:
        for_steps_pt, body_pt = pt[3], pt[5]

        v_name, var, limit, step = self.for_steps(b, for_steps_pt, scope)
        
        for_head = b.new_bb("for_head")
        for_body = b.new_bb("for_body")
        for_next = b.new_bb("for_next")
        for_exit = b.new_bb("for_exit")

        prev_bb = b.cur_bb
        b.br(for_head)

        new_scope = Scope(scope)
        new_scope.bindings[v_name] = Binding(new_scope, None)

        rebinds = self.inspect_for_variable_rebindings(body_pt, new_scope)

        b.use(for_head)

        var_phi = b.phi(lv_r, { prev_bb: var })

        phi_nodes = {}

        for rebind_name in rebinds:
            old_scope, old_rval = self.binding_search_scope(rebind_name, scope)

            phi_node = b.phi(lv_r, {prev_bb: old_rval})
            phi_nodes[rebind_name] = phi_node
            scope.bindings[rebind_name] = Binding(old_scope, phi_node)

        step_is_pos = b.lua_lt(b.number(0.0), step)
        bb_check_le, bb_check_ge = b.br2(b.is_lua_true(step_is_pos),
                "bb_check_le", "bb_check_ge")

        bb_check_merge = b.new_bb("bb_check_merge")

        b.use(bb_check_le)
        le_limit = b.lua_le(var_phi, limit)
        bb_check_le2 = b.cur_bb
        b.br(bb_check_merge)

        b.use(bb_check_ge)
        ge_limit = b.lua_le(limit, var_phi)
        bb_check_ge2 = b.cur_bb
        b.br(bb_check_merge)

        b.use(bb_check_merge)
        cond = b.phi(lv_r, {bb_check_le2: le_limit, bb_check_ge2: ge_limit})

        b.br2(b.is_lua_true(cond), for_body, for_exit)

        b.use(for_body)
        new_scope.bindings[v_name] = Binding(new_scope, var_phi)
        self.block(b, body_pt, new_scope)

        new_var = b.lua_add(var_phi, step)

        b.br(for_next)
        
        b.use(for_next)
        b.br(for_head)

        var_phi.cases[for_next] = new_var

        for rebind_name, phi_node in phi_nodes.items():
            new_rval = new_scope.bindings[rebind_name].value
            phi_node.cases[for_next] = new_rval

        b.use(for_exit)

    @pthandler(grammar.for_name_list)
    def for_name_list(self, b, pt, scope):
        raise NotImplementedError(pt)

    @pthandler(grammar.for_in_st)
    def for_in_st(self, b, pt, scope):
        raise NotImplementedError(pt)

    @pthandler(grammar.function_call_st)
    def function_call_st(self, b, pt, scope) -> None:
        self.function_call(b, pt[1], scope, single_value=False)

    @pthandler(grammar.function_decl_st)
    def function_decl_st(self, b, pt, scope) -> None:
        lval = self.function_name(b, pt[2], scope)
        func = self.function_body(b, pt[3], scope, pt[2][1])
        self.assign(b, lval, func, scope)

    @pthandler(grammar.local_function_decl_st)
    def local_function_decl_st(self, b, pt, scope) -> None:
        func_name = pt[3][1]
        func = self.function_body(b, pt[4], scope, name="<lua_func>:"+func_name)
        lval = ("local", scope, func_name)
        self.assign(b, lval, func, scope)

    @pthandler(grammar.local_assign_st)
    def local_assign_st(self, b, pt, scope) -> None:
        lvals = self.name_list(b, pt[2], scope)
        if len(pt)==5:
            rvals = self.exp_list(b, pt[4], scope)
        else:
            rvals = [b.nil]*len(lvals)
        self.assign_many(b, lvals, rvals, scope)

    @pthandler(grammar.statement)
    def statement(self, b, pt, scope) -> None:
        return self.dispatch(b, pt[1], scope)

    @pthandler(grammar.return_st)
    def return_st(self, b, pt, scope) -> None:
        exps = self.exp_list(b, pt[2], scope)
        rv = b.params_pack(exps)
        b.ret(rv)

    @pthandler(grammar.break_st)
    def break_st(self, b, pt, scope):
        raise NotImplementedError(pt)

    @pthandler(grammar.last_statement)
    def last_statement(self, b, pt, scope) -> None:
        return self.dispatch(b, pt[1], scope)

    def chunk(self, pt:'ParseTree') -> 'addr:f_t':

        b = LuaBuilder(self.lc, luafunc_sig, "<lua_main_chunk>")
        snp = b.snp

        new_scope = Scope()

        self.block(b, pt[2], new_scope)

        # Guard in case the body does not return.
        b.ret(b.empty_params)

        if _DEBUG:
            print(LuaSnippetPrettyPrinter(self.lc).lua_aware_ppsnippet(snp))

        sc.check_snippet(snp)
        sid = self.itpr.declare(luafunc_sig)
        self.itpr.define(sid, snp)

        func = self.de.alloc(self.function_td)
        self.lc._lua_function_set_mvm_func(func, sid)

        return func

    ## Special code structures

    def binop_exp_lazy(self, b, lhs_unevaled:'ParserTree', op:'and|or',
            rhs_unevaled:'parserTree', scope) -> 'V:lv_r':
        """ Handle the "and" and "or" binary operator """
        if op not in ["and", "or"]:
            raise ValueError("Unexpected op {}".format(op))

        lhs = self.exp(b, lhs_unevaled, scope, single_value=True)
        lhs_is_true = b.is_lua_true(lhs)

        bb_eval_rhs = b.new_bb(op+"_eval_rhs")
        bb_merge = b.new_bb(op+"_merge")

        prev_bb = b.cur_bb

        if op == "and":
            b.br2(lhs_is_true, bb_eval_rhs, bb_merge)
        else:
            b.br2(lhs_is_true, bb_merge, bb_eval_rhs)

        b.use(bb_eval_rhs)
        rhs = self.exp(b, rhs_unevaled, scope, single_value=True)
        b.br(bb_merge)

        b.use(bb_merge)
        result = b.phi(lv_r, {prev_bb: lhs, bb_eval_rhs: rhs})

        return result

    def lvalue_search_scope(self, var_name:str, scope) -> "LValue":
        """ Search the scope for the LValue of a variable """
        if var_name == "_ENV":
            raise ValueError("_ENV cannot be LValue")
        else:
            binding = self.binding_search_scope(var_name, scope)
            if binding != None:
                return ("local", binding.scope, var_name)
            else:
                return ("global", var_name)

    def rvalue_search_scope(self, b, var_name:str, scope) -> 'V:lv_r':
        """ Search the scope for the RValue (SSA Value) of a variable """
        if var_name == "_ENV":
            return b.env
        else:
            binding = self.binding_search_scope(var_name, scope)
            if binding != None:
                return binding.value
            else:
                return b.global_get(var_name)

    def binding_search_scope(self, var_name:str, scope) -> Binding:
        """ Search the scope for the Binding entry of a variable """
        cur_scope = scope
        while cur_scope != None:
            if var_name in cur_scope.bindings:
                return cur_scope.bindings[var_name]
            cur_scope = cur_scope.parent
        else:
            return None

    def assign_many(self, b, lvals:'[LValue]', rvals:'[V:lv_r|p_r]', scope):
        """
        Assign many variables to their corresponding LValues. Usable for the
        assignment statement n1,n2,n3 = v1,v2,v3,func() and also function
        parameter lists.

        The last element of rvals may be a Value of params_r. In that case, it
        will be unpacked if the number of lvalues is more than or equal to the
        number of rvalues.
        """
        if len(lvals) >= len(rvals):
            last = rvals[-1]
            last_ty = last.value_type
            if t.type_equals(last_ty, p_r):
                elems_to_unpack = len(lvals) - len(rvals) + 1
                extra_values = b.params_unpack(last, elems_to_unpack)
                rvals = rvals[:-1] + extra_values
            else:
                # A scalar value. Fill the rest with nils
                rvals = rvals + [b.nil]*(len(lvals) - len(rvals))
        elif len(lvals) < len(rvals):
            rvals = rvals[:len(lvals)]

        # Now there are as many lvalues as rvalues.

        for lv,rv in zip(lvals,rvals):
            self.assign(b, lv, rv, scope)

    def assign(self, b, lvalue:'LValue', rvalue:'V:lv_r', scope):
        """ Assign one rvalue to its lvalue. """

        if lvalue[0] == 'local':
            def_scope, var_name = lvalue[1], lvalue[2]
            scope.bindings[var_name] = Binding(def_scope, rvalue)
            # There is no instructions for local assignment since it is SSA.
        elif lvalue[0] == 'global':
            var_name = lvalue[1]
            b.global_set(var_name, rvalue)
        elif lvalue[0] == 'subscr':
            table, sub = lvalue[1], lvalue[2]
            b.lua_newindex(table, sub, rvalue)
        else:
            raise ValueError("Invalid LValue {}".format(lvalue))
    
    def inspect_for_variable_rebindings(self, block:"pt:block", par_scope:Scope,
            mock_scope:Scope=None, rebinds:"set:str"=None) -> "set:str":
        """
        Inspect a block to find all variables which were defined in the parent
        scope (the scope which contains that block) but are re-assigned within
        the block. This is important for implementing loops.

        par_scope       The scope outside the "while" or similar stmts.
        shadows         A set of variables defined in the blocks.
        mock_scope      The "current" scope as seen in recursive calls.
        rebinds         The set of all re-bound variables shared between
                        recursive calls.
        return: A set of variables which are re-assigned.
        """

        if mock_scope == None:
            mock_scope = Scope(par_scope)

        if rebinds == None:
            rebinds = set()

        stmts = [s for s in block[1:] if s[0] == grammar.statement]
        for stmt in stmts:
            if stmt[1][0] == grammar.assign_st:
                var_s = [v for v in stmt[1][1][1:] if v[0] == grammar.var]
                var_names = [v[1][1][1] for v in var_s
                    if v[1][0] == grammar.variable_ref
                    and len(v[1]) == 2]
                for var_name in var_names:
                    lval = self.lvalue_search_scope(var_name, mock_scope)
                    if lval[0] == "local":
                        lv_scope = lval[1]
                        if lv_scope.level <= par_scope.level:
                            rebinds.add(var_name)

            elif stmt[1][0] == grammar.local_assign_st:
                var_names = [n[1] for n in stmt[1][2][1:]
                        if n[0] == grammar.T.name]

                for var_name in var_names:
                    mock_scope.bindings[var_name] = Binding(mock_scope, None)

            elif stmt[1][0] == grammar.do_st:
                inner_block = stmt[1][2][2]
                self.inspect_for_variable_rebindings(inner_block, par_scope,
                        Scope(mock_scope), rebinds)

            elif stmt[1][0] == grammar.while_st:
                inner_block = stmt[1][4][2]
                self.inspect_for_variable_rebindings(inner_block, par_scope,
                        Scope(mock_scope), rebinds)

            elif stmt[1][0] == grammar.repeat_st:
                inner_block = stmt[1][2][2]
                self.inspect_for_variable_rebindings(inner_block, par_scope,
                        Scope(mock_scope), rebinds)

            elif stmt[1][0] == grammar.if_st:
                inner_blocks = [node[2] for node in stmt[1][1:]
                        if node[0] == grammar.scope]
                for inner_block in inner_blocks:
                    self.inspect_for_variable_rebindings(inner_block, par_scope,
                            Scope(mock_scope), rebinds)

        return rebinds

class LuaBuilder(concisebuilder.ConciseBuilder, LuaClientFriend):
    """
    A convenient tool for building Lua-specific snippets.
    
    This object keeps the current snippet and a cursor of the current basic
    block and instruction to insert new MicroVM instructions.

    Create one LuaBuilder instance per Lua function. Do not re-use.
    """

    def __init__(self, lc, sig:"t.Signature"=luafunc_sig, func_name=None):
        concisebuilder.ConciseBuilder.__init__(self, sig, func_name)
        LuaClientFriend.__init__(self, lc)

    # Primitive actions
    # These *_new functions are for dynamically creating objects.
    # They are not for constants.

    ## For string_t

    def string_new(self, length:int) -> 'V:s_r':
        return self.allocha(self.string_td, length)

    def string_get_char(self, string:'V:s_r', ix:'V:w_t') -> 'V:c_t':
        return self.getvarfield(string, ix, [])

    def string_set_char(self, string:'V:s_r', ix:'V:w_t', ch:'V:c_t'):
        self.setvarfield(string, ix, [], ch)

    def string_length(self, string:'V:s_r'):
        return self.getlength(string)

    ## For table_t

    def table_get_ht(self, table:'V:t_r') -> 'V:ht_r':
        return self.getscalarfield(table, [0])

    def table_set_ht(self, table:'V:t_r', ht:'V:ht_r'):
        self.setscalarfield(table, [0], ht)

    def table_get_size(self, table:'V:t_r') -> 'V:w_t':
        return self.getscalarfield(table, [1])

    def table_set_size(self, table:'V:t_r', new_len:'V:w_t'):
        self.setscalarfield(table, [1], new_len)

    ## For hash_table_t

    def ht_new(self, length:'V:w_t') -> 'V:ht_r':
        return self.allocha(self.hash_table_td, length)

    def ht_get_key(self, ht:'V:ht_r', ix:'V:w_t') -> 'V:lv_r':
        return self.getvarfield(ht, ix, [0])

    def ht_set_key(self, ht:'V:ht_r', ix:'V:w_t', k:'V:lv_r'):
        self.setvarfield(ht, ix, [0], k)

    def ht_get_value(self, ht:'V:ht_r', ix:'V:w_t') -> 'V:lv_r':
        return self.getvarfield(ht, ix, [1])

    def ht_set_value(self, ht:'V:ht_r', ix:'V:w_t', v:'V:lv_r'):
        self.setvarfield(ht, ix, [1], v)

    def ht_get_entry(self, ht:'V:ht_r', ix:'V:w_t') -> '(V:lv_r)*2':
        k = self.getvarfield(ht, ix, [0])
        v = self.getvarfield(ht, ix, [1])
        return (k, v)

    def ht_set_entry(self, ht:'V:ht_r', ix:'V:w_t', k:'V:lv_r', v:'V:lv_r'):
        self.setvarfield(ht, ix, [0], k)
        self.setvarfield(ht, ix, [1], v)

    def ht_get_capacity(self, ht:'V:ht_r') -> 'V:w_t':
        return self.getlength(ht)

    ## For params_t

    def params_new(self, length:'V:w_t') -> 'V:p_r':
        return self.allocha(self.params_td, length)

    def params_get_item(self, params:'V:p_r', ix:'V:w_t') ->'V:lv_r':
        return self.getvarfield(params, ix, [])
        
    def params_set_item(self, params:'V:p_r', ix:'V:w_t', v:'V:lv_r'):
        self.setvarfield(params, ix, [], v)

    def params_get_length(self, params:'V:p_r') -> 'V:w_t':
        return self.getlength(params)

    ## For function_t

    def function_new(self) -> 'V:f_r':
        return self.alloc(self.function_td)

    def function_get_mvm_func(self, func:'V:f_r') -> 'V:f_t':
        return self.getscalarfield(func, [])

    def function_set_mvm_func(self, func:'V:f_r', mfunc:'V:f_t'):
        self.setscalarfield(fun, [], mfunc)

    ## For error_t

    def error_new(self) -> 'V:e_r':
        return self.alloc(self.error_td)

    def error_get_value(self, error:'V:e_r') -> 'V:lv_r':
        return self.getscalarfield(error, [0])

    def error_set_value(self, error:'V:e_r', value:'V:lv_r'):
        self.setscalarfield(error, [0], value)

    # Shorthand for typecast
    # WARNING: Check type before casting!

    def lv2b(self, luaval:'V:l_r') -> 'V:b_t':
        return self.tr64toint(luaval)

    def lv2n(self, luaval:'V:l_r') -> 'V:n_t':
        return self.tr64tofp(luaval)

    def lv2s(self, luaval:'V:l_r') -> 'V:s_r':
        ref = self.tr64toref(luaval)
        return self.refcast(ref, s_t)

    def lv2t(self, luaval:'V:l_r') -> 'V:t_r':
        ref = self.tr64toref(luaval)
        return self.refcast(ref, t_t)

    def lv2f(self, luaval:'V:l_r') -> 'V:f_r':
        ref = self.tr64toref(luaval)
        return self.refcast(ref, f_t)

    def lv2e(self, luaval:'V:l_r') -> 'V:e_r':
        ref = self.tr64toref(luaval)
        return self.refcast(ref, e_t)

    def b2lv(self, boolean:'V:b_t') -> 'V:lv_r':
        return self.inttotr64(void_t, boolean)

    def n2lv(self, number:'V:n_t') -> 'V:lv_r':
        return self.fptotr64(void_t, number)

    def s2lv(self, string:'V:s_r') -> 'V:lv_r':
        voidref = self.refcast(string, void_t)
        return self.reftotr64(voidref, consttid(LUA_TSTRING))

    def t2lv(self, table:'V:t_r') -> 'V:lv_r':
        voidref = self.refcast(table, void_t)
        return self.reftotr64(voidref, consttid(LUA_TTABLE))

    def f2lv(self, function:'V:f_r') -> 'V:lv_r':
        voidref = self.refcast(function, void_t)
        return self.reftotr64(voidref, consttid(LUA_TFUNCTION))

    def e2lv(self, error:'V:e_r') -> 'V:lv_r':
        voidref = self.refcast(error, void_t)
        return self.reftotr64(voidref, consttid(LUACLIENT_TERROR))

    # Primitive and built-in function wrappers
    ## Primitive functions calls can be generated by directly invoking the
    ## function name on the LuaBuilder object. Built-in functions can be called
    ## similarly by prepending "builtin_" before the function name.

    def __getattr__(self, name):
        if name in self.lc.prim_funcs:
            def wrapper(*args):
                return self.call(self.primfunc(name), list(args))
            return wrapper
        elif name.startswith("builtin_") and name[8:] in self.lc.builtin_funcs:
                def wrapper(largs):
                    return self.lua_call(self.builtinfunc(name[8:]), largs)
                return wrapper
        else:
            return super(LuaBuilder, self).__getattr__(name)

    # Advanced control flow

    def for_loop(self, init:'V:w_t', bound:'V:w_t', incr:'V:w_t',
            name:str="(unnanmed)") -> "(V:w_t) + (BB)*3":
        """
        return: (loopvar, loopbody, loopnext, loopexit)

        Generate SSA values and basicblocks for a for-loop like:
            for (int loopvar=init; loopvar<bound; loopvar+=incr) { loopbody }

        It returns one SSA value and three BasicBlocks.
        - loopvar is the SSA value for the loop variable.
        - loopbody is a blank basicblock to fill loop body code.
        - loopnext contains the code for incrementing the loopvar and jumping
          back to the loop head (an unreturned block)
        - loopexit is a blank basicblock which will be branched to if the loop
          condition no longer holds.

        The user of this function usually add a Branch instruction to the
        loopnext basicblock when finished the loop body.
        """

        loophead = self.new_bb(name=name+"_loophead")
        loopbody = self.new_bb(name=name+"_loopbody")
        loopnext = self.new_bb(name=name+"_loopnext")
        loopexit = self.new_bb(name=name+"_loopexit")

        prev_bb = self.cur_bb
        self.br(loophead)

        self.use(loophead)
        loopvar = self.phi(w_t, {prev_bb: init})
        less_than_bound = self.cmp("slt", loopvar, bound)
        self.br2(less_than_bound, loopbody, loopexit)
        
        self.use(loopnext)
        next_loopvar = self.binop("add", loopvar, incr)
        loopvar.cases[loopnext] = next_loopvar
        self.br(loophead)

        self.use(loopexit)

        return loopvar, loopbody, loopnext, loopexit

    def lua_call(self, lua_func:'V:lv_r', lua_args:'V:p_r') -> 'V:p_r':
        """ Check if the lua_func is really callable and call it.  """
        func = self.lua_prepare_call(lua_func)
        return self.call(func, [lua_args])

    def lua_tailcall(self, lua_func:'V:lv_r', lua_args:'V:p_r') -> 'V:p_r':
        """ Check if the lua_func is really callable and tail-call it.  """
        func = self.lua_prepare_call(lua_func)
        return self.tailcall(func, [lua_args])

    def error(self, luaval:'V:lv_r'):
        """
        Create an error and throw. This is a lower-level counterpart of the
        error() built-in function.
        """
        err = self.error_new()
        self.error_set_value(err, luaval)
        self.throw(err)

    # Lua parameter facilities

    def params_pack(self, exp_list:'V:lv_r|p_r') -> 'V:p_r':
        """
        Pack a parameter list into a single params_r object.

        The last element of the exp_list may be a params_r while all others must
        be lv_r. If the last one is params_r, it will be unpacked and its elems
        will be transferred to the new params_t object.
        """
        if len(exp_list) == 0:
            return self.empty_params

        last = exp_list[-1]
        last_ty = last.value_type
        if t.type_equals(last_ty, p_r):
            valen = self.params_get_length(last)
            selems = len(exp_list) - 1
            nelems = self.binop("add", constiw(selems), valen)
            has_va = True
        else:
            # A scalar value. Just create a simple params_ty
            selems = len(exp_list)
            nelems = constiw(selems)
            has_va = False

        params = self.params_new(nelems)

        for ix in range(selems):
            self.params_set_item(params, constiw(ix), exp_list[ix])

        if has_va:
            self.params_append(params, last, constiw(selems))

        return params

    def params_unpack(self, params:'V:p_r', n:int, va:bool=False
            ) -> "[V:lv_r] | ([V:lv_r], V:p_r)":
        """
        Adjust a params_t object into n values. Typically used for arguments,
        return, assignments, etc. This is the reverse of params_pack.

        If va==False, return a list of SSA Values for each left-hand-side
        variables.

        If va==True, return a tuple. The first element is the same as va==False.
        The second element is a Value of params_r which contains all
        un-unpacked values if va==True. Otherwise the second element is None)
        """

        vals = []
        for ix in range(n):
            vals.append(self.params_get(params, constiw(ix)))

        if va:
            va_val = self.params_slice(params, constiw(n))
            return vals, va_val
        else:
            return vals

    # Global variable helpers

    def global_get(self, var_name:str) -> 'V:lv_r':
        """ Get a global variable.  """
        return self.lua_index(self.env, self.string(var_name))

    def global_set(self, var_name:str, val:'V:lv_r'):
        """ Set a global variable.  """
        return self.lua_newindex(self.env, self.string(var_name), val)

class LuaSnippetPrettyPrinter(LuaClientFriend):
    def lua_aware_pptype(self, ty):
        if t.type_equals(ty, lv_r):
            return "luaval_r"
        elif t.type_equals(ty, b_t):
            return "boolean_t"
        elif t.type_equals(ty, n_t):
            return "number_t"
        elif t.type_equals(ty, c_t):
            return "char_t"
        elif t.type_equals(ty, s_t):
            return "string_t"
        elif t.type_equals(ty, s_r):
            return "string_r"
        elif t.type_equals(ty, hash_entry_t):
            return "hash_entry_t"
        elif t.type_equals(ty, ht_t):
            return "hash_table_t"
        elif t.type_equals(ty, ht_r):
            return "hash_table_r"
        elif t.type_equals(ty, t_t):
            return "table_t"
        elif t.type_equals(ty, t_r):
            return "table_r"
        elif t.type_equals(ty, p_t):
            return "params_t"
        elif t.type_equals(ty, p_r):
            return "params_r"
        elif t.type_equals(ty, f_t):
            return "function_t"
        elif t.type_equals(ty, f_r):
            return "function_r"
        elif t.type_equals(ty, e_t):
            return "error_t"
        elif t.type_equals(ty, e_r):
            return "error_r"
        else:
            return None

    def lua_aware_ppconst(self, const):
        if isinstance(const.ty, t.TagRef64):
            if self.de.is_ref(const.data):
                ref = self.de.tr64_to_ref(const.data)
                if ref == self.lc.env_gr.obj_addr:
                    return "_ENV"
                else:
                    return "{}".format(self.lc.lua_repr(const.data))
            else:
                return None
        elif isinstance(const.ty, t.FuncHandle):
            sid = const.data
            snp = self.itpr.id_to_snippet(sid)
            return "Snippet({})".format(snp.name or "(undef:{})".format(sid))
        elif t.type_equals(const.ty, w_t):
            return "word:{}".format(const.data)
        else:
            return None

    def lua_aware_ppsnippet(self, snp):
        return s.ppsnippet(snp, self.lua_aware_pptype, self.lua_aware_ppconst)
