try:
    from microvm import (
            microvm as mvm,
            mtypes as t,
            snippet as s,
            snippetcheck as sc,
            interpreter as i,
            directexecutor as de,
            builder,
            intrinsicfuncs,
            )
    from microvm.mconstants import *
    from microvm.commontypes import *
except ImportError:
    print(
u"""Package `microvm' (the `microvm' directory) needs to be on PYTHONPATH.
This is the preferred way of executing:
    $ cd /path/to/microvm-p1/src
    $ python -m luaclient.luaclient <filename.lua>
""")
    raise

LUA_TNIL = 0
LUA_TBOOLEAN = 1
LUA_TLIGHTUSERDATA = 2
LUA_TNUMBER = 3
LUA_TSTRING = 4
LUA_TTABLE = 5
LUA_TFUNCTION = 6
LUA_TUSERDATA = 7
LUA_TTHREAD = 8

LUACLIENT_TPARAMS = 9
LUACLIENT_TERROR = 10

luaval_r = t.TagRef64(void_t)
boolean_t = int52_t
number_t = double_t

char_t = int16_t
string_t = t.HybridArray(void_t, char_t)
string_r = t.Ref(string_t)

hash_entry_t = t.Struct([luaval_r, luaval_r])
hash_table_t = t.HybridArray(void_t, hash_entry_t)
hash_table_r = t.Ref(hash_table_t)

table_t = t.Struct([
    hash_table_r,
    word_t,
    ])
table_r = t.Ref(table_t)

TABLE_INIT_SIZE = 16

params_t = t.HybridArray(void_t, luaval_r)
params_r = t.Ref(params_t)

luafunc_sig = t.Signature([params_r], params_r)

function_t = t.FuncHandle(luafunc_sig)
function_r = t.Ref(function_t)

error_t = t.Struct([luaval_r])
error_r = t.Ref(error_t)

# Convenient shorthands

tid_t = int6_t
w_t = word_t
lv_r = luaval_r
b_t = boolean_t
n_t = number_t
c_t = char_t
s_t = string_t
s_r = string_r
ht_t = hash_table_t
ht_r = hash_table_r
t_t = table_t
t_r = table_r
p_t = params_t
p_r = params_r
f_t = function_t
f_r = function_r
e_t = error_t
e_r = error_r

# SSA Constants
def consttid(n): return s.Constant(tid_t   , n)
def constiw (n): return s.Constant(word_t  , n)
def consti1 (n): return s.Constant(int1_t  , n)
def consti8 (n): return s.Constant(int8_t  , n)
def consti16(n): return s.Constant(int16_t , n)
def consti32(n): return s.Constant(int32_t , n)
def consti64(n): return s.Constant(int64_t , n)
def constd  (n): return s.Constant(double_t, n)
def constlv (n): return s.Constant(luaval_r, n)
constvoid = s.Constant(void_t, None)

