from . import luabuilder
from .luaconstants import *

def primitive_function(sig:t.Signature, fname=None):
    def wrapped(f):
        f.__lua_primitive__ = True
        f.__lua_sig__ = sig
        f.__lua_funcname__ = fname if fname is not None else f.__name__
        return f
    return wrapped

def builtin_function(fname=None):
    def wrapped(f):
        f.__lua_builtin__ = True
        f.__lua_funcname__ = fname if fname is not None else f.__name__
        return f
    return wrapped

def trap_stop_thread(thread):
    thread.running = False

class LuaPrimitives(object):
    """
    This class defines primitive functions and built-in functions.

    Both primitive functions and built-in functions are implemented directly in
    the MicroVM IR and both are supposed to do things which are impossible or
    inefficient to do in Lua code.

    Primitive functions are not callable from Lua. They are used in translating
    primitive Lua expressions and statements (e.g. accessing elements from a Lua
    table, or concatenating two strings). Since they are not callable from Lua,
    it may have arbitrary parameters and return values.

    Built-in functions are directly callable from Lua code. They must have the
    function_t type.

    The methods of this class are all static methods. LuaClient, on
    initialisation, scans the functions of this class and reads the
    annotations. It creates a LuaBuilder object for each primitive function and
    builtin function and the corresponding method of this class is called with
    the builder as its first parameter and the parameters (Snippet.params) are
    passed as the rest of the positional parameters for convenience.  Before
    calling, SIDs for all functions are already assigned, so cyclic function
    calls between primitive or built-in functions are possible. Methods should
    just start building.  They are not required to return any value.
    """

    # Primitive functions

    ## Fundamentals

    @primitive_function(t.Signature([lv_r, lv_r], int1_t))
    def raw_equals(b, lhs, rhs):
        """ Check for equality, ignoring metatable. """

        bb_int = b.new_bb("bb_int")
        bb_str = b.new_bb("bb_str")
        bb_ref = b.new_bb("bb_ref")

        bb_ne  = b.new_bb("bb_ne")

        cur_bb = b.cur_bb
        b.jf(b.isfp(lhs), bb_int)
        b.jf(b.isfp(rhs), bb_ne)
        b.ret(b.cmp("oeq", b.lv2n(lhs), b.lv2n(rhs)))

        b.use(bb_int)
        b.jf(b.isint(lhs), bb_str)
        b.jf(b.isint(rhs), bb_ne)
        b.ret(b.cmp("eq", b.lv2b(lhs), b.lv2b(rhs)))

        b.use(bb_str)
        b.jf(b.isref(rhs), bb_ne)

        ltid = b.tr64totag(lhs)
        rtid = b.tr64totag(lhs)

        b.jf(b.cmp("eq", ltid, consttid(LUA_TSTRING)), bb_ref)
        b.jf(b.cmp("eq", rtid, consttid(LUA_TSTRING)), bb_ne)

        lref_str = b.lv2s(lhs)
        rref_str = b.lv2s(rhs)
        strcmp = b.call(b.primfunc("string_cmp"), [lref_str, rref_str])
        b.ret(b.cmp("eq", strcmp, constiw(0)))

        b.use(bb_ref)
        b.jf(b.cmp("eq", ltid, rtid), bb_ne)
        lref = b.tr64toref(lhs)
        rref = b.tr64toref(rhs)
        b.ret(b.cmp("req", lref, rref))

        b.use(bb_ne)
        b.ret(consti1(0))

    @primitive_function(t.Signature([f_r], void_t))
    def bottom_frame_func(b, lua_func):
        """
        This snippet is for handling the stack underflow. Returning from the
        main chunk and exceptions thrown from the main chunk will be handled
        here.
        """

        mvm_func = b.function_get_mvm_func(lua_func)

        bb_normal = b.new_bb(name="bb_normal")
        bb_landingpad = b.new_bb(name="bb_landingpad")
        
        ret_val = b.invoke(mvm_func, [b.empty_params],
                bb_normal, bb_landingpad)

        b.use(bb_normal)
        trap_inst1 = b.trap(trap_stop_thread, [])

        b.use(bb_landingpad)
        
        exc_untyped = b.landingpad()
        exc = b.refcast(exc_untyped, e_t)
        message = b.error_get_value(exc)

        def show_message(thread, msg):
            print("error:", b.lc.lua_print_repr(msg))

        trapcall_inst = b.trapcall(t.Signature([lv_r], void_t),
                show_message, [message])
        trap_inst2 = b.trap(trap_stop_thread, [])

    @primitive_function(t.Signature([], t_r))
    def table_new(b):
        """ Create a new table.  """

        table = b.alloc(b.table_td)
        ht = b.ht_new(constiw(TABLE_INIT_SIZE))
        b.table_set_ht(table, ht)
        b.table_set_size(table, constiw(0))
        b.ret(table)

    @primitive_function(t.Signature([t_r, lv_r], lv_r))
    def table_get(b, table, key):
        """ Get the value corresponding to a specific key from a Lua table. """

        ht = b.table_get_ht(table)
        table_len = b.table_get_size(table)

        ix, fbody, fnext, fexit = b.for_loop(constiw(0), table_len, constiw(1),
                "search_loop")

        b.use(fbody)
        entry_k = b.ht_get_key(ht, ix)
        keq = b.raw_equals(entry_k, key)
        bb_found, _ = b.br2(keq, "bb_found", fnext)

        b.use(bb_found)
        entry_v = b.ht_get_value(ht, ix)
        b.ret(entry_v)

        b.use(fexit)
        b.ret(b.nil)

    @primitive_function(t.Signature([t_r, lv_r, lv_r], void_t))
    def table_set(b, table, key, value):
        """ Set the value corresponding to a specific key in a Lua table. """

        ht = b.table_get_ht(table)
        table_len = b.table_get_size(table)

        ix, fbody, fnext, fexit = b.for_loop(constiw(0), table_len, constiw(1),
                "search_loop")

        b.use(fbody)    # Search for the entry
        entry_k = b.ht_get_key(ht, ix)
        keq = b.raw_equals(entry_k, key)
        bb_found, _ = b.br2(keq, "bb_found", fnext)

        b.use(bb_found)
        is_nil = b.cmp("treq", value, b.nil)
        bb_f_nil, bb_f_non_nil = b.br2(is_nil, "bb_f_nil", "bb_f_non_nil")

        b.use(bb_f_nil) # Found, new value is nil, so remove the entry
        last_ix = b.binop("sub", table_len, constiw(1))
        is_last_elem = b.cmp("eq", ix, last_ix)
        bb_cl_last, bb_repl = b.br2(is_last_elem, "bb_cl_last", "bb_repl")

        b.use(bb_cl_last)  # Clear the last entry
        b.ht_set_entry(ht, last_ix, b.nil, b.nil)
        new_len = b.binop("sub", table_len, constiw(1))
        b.table_set_size(table, new_len)
        b.ret(constvoid)

        b.use(bb_repl)  # Replace current entry with the last entry.
        last_k, last_v = b.ht_get_entry(ht, last_ix)
        b.ht_set_entry(ht, ix, last_k, last_v)
        b.br(bb_cl_last)    # Still need to clear the last entry

        b.use(bb_f_non_nil) # Found, new value is not nil, so replace the entry
        b.ht_set_value(ht, ix, value)
        b.ret(constvoid)

        b.use(fexit)    # Not found
        is_nil2 = b.cmp("treq", value, b.nil)
        bb_donothing, bb_insert = b.br2(is_nil2, "bb_donothing", "bb_insert")

        b.use(bb_donothing)
        b.ret(constvoid)

        b.use(bb_insert)    # Actually insert element
        ht_len = b.ht_get_capacity(ht)
        is_full = b.cmp("eq", table_len, ht_len)
        bb_extend, bb_add = b.br2(is_full, "bb_extend", "bb_add")

        b.use(bb_extend)    # Need to extend hash table
        new_ht_len = b.binop("mul", ht_len, constiw(2))
        new_ht = b.ht_new(new_ht_len)

        ix2, cp_body, cp_next, cp_exit = b.for_loop(constiw(0), ht_len,
                constiw(1), "copy_loop")

        b.use(cp_body)  # Copy elements
        old_k, old_v = b.ht_get_entry(ht, ix2)
        b.ht_set_entry(new_ht, ix2, old_k, old_v)
        b.br(cp_next)

        b.use(cp_exit)  # Update the table's hashtable field
        b.table_set_ht(table, new_ht)
        b.br(bb_add)

        b.use(bb_add)   # Insert the new key-value pair
        cur_ht = b.phi(ht_r, {
            bb_insert: ht,
            cp_exit: new_ht
            })
        b.ht_set_entry(cur_ht, table_len, key, value)
        new_len2 = b.binop("add", table_len, constiw(1))
        b.table_set_size(table, new_len2)
        b.ret(constvoid)

    @primitive_function(t.Signature([lv_r], f_t))
    def lua_prepare_call(b, lua_func):
        """
        Prepare to call a lua function.
        
        This function checks whether the argument is callable. If so, this
        function returns a MicroVM Function type value. Otherwise an error is
        thrown.
        """
        bb_notfunc = b.new_bb("bb_notfunc")

        b.jf(b.isref(lua_func), bb_notfunc)
        b.jf(b.cmp("eq", b.tr64totag(lua_func), consttid(LUA_TFUNCTION)),
                bb_notfunc)
        func = b.function_get_mvm_func(b.lv2f(lua_func))
        b.ret(func)
        
        b.use(bb_notfunc)
        def not_func_trap(thread, func_luaval):
            print("OOPS! func_luaval={}. it is not a function!".format(
                    b.lc.from_lua(func_luaval)))

            # TODO: Generalise the stack-trace generation.
            stack = thread.stack
            frame = stack.top.prev
            while frame is not None:
                bb, ix = frame.cur_call
                snp = bb.snippet
                inst = bb.instructions[ix]
                sm = s.SnippetMarker(snp)
                print("Snippet: {}, bb: {}, inst: {}".format(
                    snp.name or "(noname)", sm.bb_numbered(bb),
                    sm.inst_numbered(inst)))
                frame = frame.prev

        b.trapcall(t.Signature([lv_r], void_t), not_func_trap, [lua_func])
        b.error(b.string("Attempted to call a non-function"))

    @primitive_function(t.Signature([s_r, s_r], w_t))
    def string_cmp(b, lhs, rhs):
        """
        Compare two lua strings lexicographically.
        Return -1, 0, 1 for lt, eq and gt.
        """
        lhs_len = b.string_length(lhs)
        rhs_len = b.string_length(rhs)

        len_cmp = b.cmp("sle", lhs_len, rhs_len)
        min_len = b.select(len_cmp, lhs_len, rhs_len)

        ind, fbody, fnext, fexit = b.for_loop(constiw(0), min_len, constiw(1),
                "for_over_common_length")
        is_lt = b.new_bb("is_lt")
        is_gt = b.new_bb("is_gt")
        is_eq = b.new_bb("is_eq")

        b.use(fbody)
        lhs_ch = b.string_get_char(lhs, ind)
        rhs_ch = b.string_get_char(rhs, ind)

        b.jt(b.cmp("slt", lhs_ch, rhs_ch), is_lt)
        b.br2(b.cmp("sgt", lhs_ch, rhs_ch), is_gt, fnext)

        b.use(fexit)
        b.jt(b.cmp("eq", lhs_len, rhs_len), is_eq)
        b.br2(len_cmp, is_lt, is_gt)

        b.use(is_lt)
        b.ret(constiw(-1))
        b.use(is_gt)
        b.ret(constiw(1))
        b.use(is_eq)
        b.ret(constiw(0))

    @primitive_function(t.Signature([s_r, s_r], s_r))
    def string_cat(b, lhs, rhs):
        """ concatenate two lua strings. Implementing the ".." operator.  """
        lhs_len = b.string_length(lhs)
        rhs_len = b.string_length(rhs)

        result_len = b.binop("add", lhs_len, rhs_len)

        new_str = b.string_new(result_len)

        ind, fbody, fnext, fexit = b.for_loop(constiw(0), lhs_len, constiw(1),
                name="copy_lhs_string")

        b.use(fbody)
        old_ch = b.string_get_char(lhs, ind)
        b.string_set_char(new_str, ind, old_ch)
        b.br(fnext)

        b.use(fexit)

        ind2, fbody2, fnext2, fexit2 = b.for_loop(constiw(0), rhs_len, constiw(1),
                name="copy_rhs_string")

        b.use(fbody2)
        new_ind = b.binop("add", ind2, lhs_len)
        old_ch2 = b.string_get_char(rhs, ind2)
        b.string_set_char(new_str, new_ind, old_ch2)
        b.br(fnext2)

        b.use(fexit2)

        b.ret(new_str)

    ## Params operations

    @primitive_function(t.Signature([p_r, w_t], lv_r))
    def params_get(b, params, ind):
        """
        Get a parameter from a parameter list (params_r). Return nil if the
        index is out of bound.
        """
        params_len = b.params_get_length(params)
        bb_oob = b.jf(b.cmp("slt", ind, params_len), "bb_oob")
        param = b.params_get_item(params, ind)
        b.ret(param)

        b.use(bb_oob)
        b.ret(b.nil)

    @primitive_function(t.Signature([p_r, w_t], p_r))
    def params_slice(b, params, start):
        """
        Get a sublist of parameters from a given params. Return a new params
        object containing the elements of params starting from `start`.

        Assume `start` is strictly less than the length of params.
        """
        params_len = b.params_get_length(params)
        necessary = b.cmp("sgt", params_len, start)
        make_new, use_empty = b.br2(necessary, "make_new", "use_empty")

        b.use(make_new)
        new_len = b.binop("sub", params_len, start)
        new_params = b.params_new(new_len)

        ind, fbody, fnext, fexit = b.for_loop(constiw(0), new_len,
                constiw(1), "params_unpack")
        b.use(fbody)
        old_ind = b.binop("add", ind, start)
        old_val = b.params_get_item(params, old_ind)# we know the length.
        b.params_set_item(new_params, ind, old_val)
        b.br(fnext)

        b.use(fexit)
        params_unpack_merge = b.br("params_unpack_merge")

        b.use(use_empty)
        b.br(params_unpack_merge)

        b.use(params_unpack_merge)
        va_val = b.phi(p_r, {
            fexit: new_params,
            use_empty: b.empty_params,
            })
        
        b.ret(va_val)

    @primitive_function(t.Signature([p_r, p_r, w_t], void_t))
    def params_append(b, params1, params2, start):
        """
        Copy the contents in params2 into params1[start:].

        Assume params1 is large enough to contain all elements from params2
        after the start index.
        """
        valen = b.params_get_length(params2)

        ind, fbody, fnext, fexit = b.for_loop(constiw(0), valen,
                constiw(1), "params_append")

        b.use(fbody)
        pind = b.binop("add", ind, start)
        param = b.params_get_item(params2, ind)
        b.params_set_item(params1, pind, param)
        b.br(fnext)

        b.use(fexit)
        b.ret(constvoid)

    ## Eager binary operations

    @staticmethod
    def lua_prepare_arith(b, lhs:'V:lv_r', rhs:'V:lv_r') -> '(V:n_t)*2':
        """ Check if both operands are numbers """
        not_num = b.new_bb("not_num")

        b.jf(b.isfp(lhs), not_num)
        b.jf(b.isfp(rhs), not_num)

        prev_bb = b.cur_bb

        b.use(not_num)
        b.error(b.string("attempt to perform arithmetic on non-number values."))

        b.use(prev_bb)
        lhs_num = b.lv2n(lhs)
        rhs_num = b.lv2n(rhs)
        return (lhs_num, rhs_num)

    @primitive_function(t.Signature([lv_r, lv_r], lv_r))
    def lua_add(b, lhs, rhs):
        """ Binary operator + """
        lhs_num, rhs_num = LuaPrimitives.lua_prepare_arith(b, lhs, rhs)
        b.ret(b.n2lv(b.binop("fadd", lhs_num, rhs_num)))

    @primitive_function(t.Signature([lv_r, lv_r], lv_r))
    def lua_sub(b, lhs, rhs):
        """ Binary operator - """
        lhs_num, rhs_num = LuaPrimitives.lua_prepare_arith(b, lhs, rhs)
        b.ret(b.n2lv(b.binop("fsub", lhs_num, rhs_num)))

    @primitive_function(t.Signature([lv_r, lv_r], lv_r))
    def lua_mul(b, lhs, rhs):
        """ Binary operator * """
        lhs_num, rhs_num = LuaPrimitives.lua_prepare_arith(b, lhs, rhs)
        b.ret(b.n2lv(b.binop("fmul", lhs_num, rhs_num)))

    @primitive_function(t.Signature([lv_r, lv_r], lv_r))
    def lua_div(b, lhs, rhs):
        """ Binary operator / """
        lhs_num, rhs_num = LuaPrimitives.lua_prepare_arith(b, lhs, rhs)
        b.ret(b.n2lv(b.binop("fdiv", lhs_num, rhs_num)))

    @primitive_function(t.Signature([lv_r, lv_r], lv_r))
    def lua_mod(b, lhs, rhs):
        """ Binary operator % """
        lhs_num, rhs_num = LuaPrimitives.lua_prepare_arith(b, lhs, rhs)
        ldr = b.binop("fdiv", lhs_num, rhs_num)
        flr = b.intrinsiccall("floor", [ldr])
        flrmb = b.binop("fmul", flr, rhs_num)
        result_num = b.binop("fsub", lhs_num, flrmb)
        b.ret(b.n2lv(result_num))

    @primitive_function(t.Signature([lv_r, lv_r], lv_r))
    def lua_pow(b, lhs, rhs):
        """ Binary operator ^ """
        lhs_num, rhs_num = LuaPrimitives.lua_prepare_arith(b, lhs, rhs)
        b.ret(b.n2lv(b.intrinsiccall("pow", [lhs_num, rhs_num])))

    @primitive_function(t.Signature([lv_r, lv_r], lv_r))
    def lua_eq(b, lhs, rhs):
        """ Binary operator == """
        raw_eq = b.raw_equals(lhs, rhs)
        b.ret(b.select(raw_eq, b.true, b.false))

    @primitive_function(t.Signature([lv_r, lv_r], lv_r))
    def lua_ne(b, lhs, rhs):
        """ Binary operator ~= """
        raw_eq = b.raw_equals(lhs, rhs)
        b.ret(b.select(raw_eq, b.false, b.true))

    @staticmethod
    def make_lt_le(b, lhs, rhs, op):
        """ Put lt and le together for similarity """
        check_str = b.new_bb("check_str")
        mismatch = b.new_bb("mismatch")

        b.jf(b.isfp(lhs), check_str)
        b.jf(b.isfp(rhs), mismatch)
        lhs_num = b.lv2n(lhs)
        rhs_num = b.lv2n(rhs)
        lt = b.cmp("olt" if op == "lt" else "ole", lhs_num, rhs_num)
        b.ret(b.select(lt, b.true, b.false))
        
        b.use(check_str)
        b.jf(b.isref(lhs), mismatch)
        b.jf(b.cmp("eq", b.tr64totag(lhs), consttid(LUA_TSTRING)), mismatch)
        b.jf(b.isref(rhs), mismatch)
        b.jf(b.cmp("eq", b.tr64totag(rhs), consttid(LUA_TSTRING)), mismatch)
        lhs_str = b.lv2s(lhs)
        rhs_str = b.lv2s(rhs)
        str_cmp = b.string_cmp(lhs_str, rhs_str)
        if op == "lt":
            str_lt = b.cmp("eq", str_cmp, constiw(-1))
        else:
            str_lt = b.cmp("ne", str_cmp, constiw(1))
        b.ret(b.select(str_lt, b.true, b.false))

        b.use(mismatch)
        b.error(b.string("attempt to compare values of different types"))

    @primitive_function(t.Signature([lv_r, lv_r], lv_r))
    def lua_lt(b, lhs, rhs):
        """ Binary operator < """
        LuaPrimitives.make_lt_le(b, lhs, rhs, "lt")

    @primitive_function(t.Signature([lv_r, lv_r], lv_r))
    def lua_le(b, lhs, rhs):
        """ Binary operator <= """
        LuaPrimitives.make_lt_le(b, lhs, rhs, "le")

    @primitive_function(t.Signature([lv_r, lv_r], lv_r))
    def lua_concat(b, lhs, rhs):
        """ Binary operator .. """
        not_str = b.new_bb("not_str")

        b.jf(b.isref(lhs), not_str)
        b.jf(b.cmp("eq", b.tr64totag(lhs), consttid(LUA_TSTRING)), not_str)
        b.jf(b.isref(rhs), not_str)
        b.jf(b.cmp("eq", b.tr64totag(rhs), consttid(LUA_TSTRING)), not_str)
        lhs_str = b.lv2s(lhs)
        rhs_str = b.lv2s(rhs)
        str_cat = b.string_cat(lhs_str, rhs_str)
        b.ret(b.s2lv(str_cat))

        b.use(not_str)
        b.error(b.string("attempt to concatenate non-string values."))

    ## Unary operators

    @primitive_function(t.Signature([lv_r], lv_r))
    def lua_not(b, opnd):
        """ Unary operator not """
        is_true = b.is_lua_true(opnd)
        b.ret(b.select(is_true, b.false, b.true))

    @primitive_function(t.Signature([lv_r], lv_r))
    def lua_unm(b, opnd):
        """ Unary operator - """
        not_num = b.new_bb("not_num")

        b.jf(b.isfp(opnd), not_num)
        opnd_num = b.lv2n(opnd)
        result = b.binop("fsub", constd(0.0), opnd_num)
        b.ret(b.n2lv(result))

        b.use(not_num)
        b.error(b.string("attempt to negate non-number values."))

    @primitive_function(t.Signature([lv_r], lv_r))
    def lua_len(b, opnd):
        """ Unary operator # NOTE: Not the same semantic """
        check_table = b.new_bb("check_table")
        not_seq = b.new_bb("not_seq")

        b.jf(b.isref(opnd), check_table)
        b.jf(b.cmp("eq", b.tr64totag(opnd), consttid(LUA_TSTRING)), check_table)
        opnd_str = b.lv2s(opnd)
        string_len = b.string_length(opnd_str)
        string_len_d = b.sitofp(string_len, n_t)
        b.ret(b.n2lv(string_len_d))

        b.use(check_table)
        b.jf(b.isref(opnd), not_seq)
        b.jf(b.cmp("eq", b.tr64totag(opnd), consttid(LUA_TTABLE)), not_seq)
        opnd_table = b.lv2t(opnd)
        table_len = b.table_get_size(opnd_table)
        table_len_d = b.sitofp(table_len, n_t)
        b.ret(b.n2lv(table_len_d))

        b.use(not_seq)
        b.error(b.string("attempt to get the length of non-sequence values."))

    ## Other operators

    @primitive_function(t.Signature([lv_r, lv_r], lv_r))
    def lua_index(b, table, key):
        """ The indexing access table[key] """
        not_table = b.new_bb("not_table")

        b.jf(b.isref(table), not_table)
        b.jf(b.cmp("eq", b.tr64totag(table), consttid(LUA_TTABLE)), not_table)
        lhs_table = b.lv2t(table)
        result = b.table_get(lhs_table, key)
        b.ret(result)

        b.use(not_table)
        b.error(b.string("attempt to index non-table values."))

    @primitive_function(t.Signature([lv_r, lv_r, lv_r], void_t))
    def lua_newindex(b, table, key, value):
        """ The indexing assignment table[key] = value """
        not_table = b.new_bb("not_table")

        b.jf(b.isref(table), not_table)
        b.jf(b.cmp("eq", b.tr64totag(table), consttid(LUA_TTABLE)), not_table)
        lhs_table = b.lv2t(table)
        b.table_set(lhs_table, key, value)
        b.ret(constvoid)

        b.use(not_table)
        b.error(b.string("attempt to index non-table values."))

    @primitive_function(t.Signature([lv_r], int1_t))
    def is_lua_true(b, opnd):
        is_false = b.new_bb("is_false")

        b.jt(b.cmp("treq", opnd, b.nil), is_false)
        b.jt(b.cmp("treq", opnd, b.false), is_false)
        b.ret(consti1(1))

        b.use(is_false)
        b.ret(consti1(0))


    # Builtin-functions

    @staticmethod
    def python_wrapper(b, sig:t.Signature, pyfunc:callable):
        """
        Build a wrapper snippet for primitive or built-in functions implemented
        in Python.  This will use TrapCall to call the Python function.
        """
        trap_call_inst = b.trapcall(sig, pyfunc, b.snp.params)
        ret_inst = b.ret(trap_call_inst)

    @builtin_function("print")
    def builtin_print(b, lparams):
        LuaPrimitives.python_wrapper(b, luafunc_sig, b.lc.builtin_print)

    @builtin_function("error")
    def builtin_error(b, lua_args):
        """
        Build the built-in error() function, the only way Lua can throw
        exceptions.
        """
        message = b.params_get(lua_args, constiw(0))
        b.error(message)

