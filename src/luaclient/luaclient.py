"""
This module implements a subset of the Lua programming language.

Because of the current implementation of MicroVM which does not support mutable
variables on the stack, this Lua client does not support Lua closures.

Upvalues are not supported. The only two kinds of variables are global
variables and local variables. Functions are still first class in the sense
that they can still be used as arguments and return values.

Local variables are implemented as SSA values. Global variables are implemented
as _ENV[XXXX] where _ENV is a per-client table. Unlike the official Lua, _ENV
cannot be overridden by local variables because of the lack of upvalues.
"""

from . import luabuilder

from . import luaprimitives

from .luaconstants import *

import collections
from abc import abstractmethod

PrimFuncRec = collections.namedtuple("PrimFuncRec", ["sid", "sig"])
BuiltinFuncRec = collections.namedtuple("BuiltinFuncRec", ["sid", "func_gr"])

class AbstractLuaClient(object):
    """
    This abstract client is shared between LuaClient and LuLuClient.
    """
    def __init__(self):
        # Create MicroVM and friends

        self.mvm = mvm.MicroVM()
        self.de = de.DirectExecutor(self.mvm)
        self.itpr = i.Interpreter(self.mvm, self.de)

        # Type desciptors

        self.string_td = self.mvm.define_type(s_t)
        self.hash_table_td = self.mvm.define_type(ht_t)
        self.table_td = self.mvm.define_type(t_t)
        self.function_td = self.mvm.define_type(f_t)
        self.params_td = self.mvm.define_type(p_t)
        self.error_td = self.mvm.define_type(e_t)

        # Create singleton values (nil, false, true)
        # All in TagRef64 format

        self.nil_data = self.de.ref_to_tr64(0, LUA_TNIL)
        self.false_data = self.de.int_to_tr64(0)
        self.true_data = self.de.int_to_tr64(1)

        empty_params_addr = self.de.allochybridarray(self.params_td, 0)
        self.empty_params_gr = self.mvm.make_global_ref(empty_params_addr)

        # Create _ENV

        env = self.make_table()
        self.env_gr = self.mvm.make_global_ref(env)

        # Primitive functions and built-in functions

        self.prim_funcs = {}
        self.builtin_funcs = {}

        funcs_nursery = []

        for meth in luaprimitives.LuaPrimitives.__dict__.values():
            is_primitive = getattr(meth, "__lua_primitive__", False)
            is_builtin = getattr(meth, "__lua_builtin__", False)
            if is_primitive or is_builtin:
                func_name = meth.__lua_funcname__
                sig = meth.__lua_sig__ if is_primitive else luafunc_sig
                sid = self.itpr.declare(sig)
                funcs_nursery.append((meth, func_name, sig, sid))

                if is_primitive:
                    self.prim_funcs[func_name] = PrimFuncRec(sid, sig)
                else:
                    # Create a function object on the heap so Lua can call it.
                    func_addr = self.de.alloc(self.function_td)
                    self._lua_function_set_mvm_func(func_addr, sid)
                    func_gr = self.mvm.make_global_ref(func_addr)
                    self.builtin_funcs[func_name] = BuiltinFuncRec(sid, func_gr)

                    # Add to the _ENV table.
                    func_name_lua = self.make_string(func_name)
                    func_lv = self.de.ref_to_tr64(func_addr, LUA_TFUNCTION)
                    self.lua_table_set(self.env_gr.obj_addr, func_name_lua,
                            func_lv)

        for meth, func_name, sig, sid in funcs_nursery:
            try:
                func_builder = luabuilder.LuaBuilder(self, sig, func_name)
                meth(func_builder, *func_builder.snp.params)
                sc.check_snippet(func_builder.snp)
                self.itpr.define(sid, func_builder.snp)
            except:
                print("Error while building snippet")
                print(luabuilder.LuaSnippetPrettyPrinter(self)
                        .lua_aware_ppsnippet(func_builder.snp))
                raise

    # Some helper functions

    ## For string_t

    def _lua_string_get_char(self, string_addr:'addr:s_t', ix:int) -> int:
        return self.de.getfield(s_t, c_t, [], t.VAR_PART, ix, string_addr)

    def _lua_string_set_char(self, string_addr:'addr:s_t', ix:int, ch:int):
        self.de.setfield(s_t, c_t, [], t.VAR_PART, ix, string_addr, ch)

    def _lua_string_get_length(self, string_addr:'addr:s_t') -> int:
        return self.de.getlength(s_t, string_addr)

    ## For table_t

    def _lua_table_get_ht(self, table_addr:'addr:t_t') -> 'addr:ht_r':
        return self.de.getfield(t_t, ht_r, [0], t.SCALAR, None, table_addr)

    def _lua_table_set_ht(self, table_addr:'addr:t_t', new_ht:'addr:ht_t'):
        self.de.setfield(t_t, ht_r, [0], t.SCALAR, None, table_addr, new_ht)

    def _lua_table_get_size(self, table_addr:'addr:t_t') -> int:
        return self.de.getfield(t_t, w_t, [1], t.SCALAR, None, table_addr)

    def _lua_table_set_size(self, table_addr:'addr:t_t', new_len:int):
        self.de.setfield(t_t, w_t, [1], t.SCALAR, None, table_addr, new_len)

    ## For hash_table_t

    def _lua_ht_get_key(self, ht_addr:'addr:ht_t', ix:int) -> "tagref64":
        return self.de.getfield(ht_t, lv_r, [0], t.VAR_PART, ix, ht_addr)

    def _lua_ht_set_key(self, ht_addr:'addr:ht_t', ix:int, k:"tagref64"):
        self.de.setfield(ht_t, lv_r, [0], t.VAR_PART, ix, ht_addr, k)

    def _lua_ht_get_value(self, ht_addr:'addr:ht_t', ix:int) -> "tagref64":
        return self.de.getfield(ht_t, lv_r, [1], t.VAR_PART, ix, ht_addr)

    def _lua_ht_set_value(self, ht_addr:'addr:ht_t', ix:int, v:"tagref64"):
        self.de.setfield(ht_t, lv_r, [1], t.VAR_PART, ix, ht_addr, v)

    def _lua_ht_get_entry(self, ht_addr:'addr:ht_t', ix:int) -> "(tagref64)*2":
        k = self.de.getfield(ht_t, lv_r, [0], t.VAR_PART, ix, ht_addr)
        v = self.de.getfield(ht_t, lv_r, [1], t.VAR_PART, ix, ht_addr)
        return (k, v)

    def _lua_ht_set_entry(self, ht_addr:'addr:ht_t', ix:int,
            k:"tagref64", v:"tagref64"):
        self.de.setfield(ht_t, lv_r, [0], t.VAR_PART, ix, ht_addr, k)
        self.de.setfield(ht_t, lv_r, [1], t.VAR_PART, ix, ht_addr, v)

    def _lua_ht_get_capacity(self, ht_addr:'addr:ht_t') -> int:
        return self.de.getlength(ht_t, ht_addr)

    ## For params_t

    def _lua_params_get_item(self, params_addr:'addr:p_t', ix:int) ->"tagref64":
        return self.de.getfield(p_t, lv_r, [], t.VAR_PART, ix, params_addr)
        
    def _lua_params_set_item(self, params_addr:'addr:p_t', ix:int, v:"tagref64"):
        self.de.setfield(p_t, lv_r, [], t.VAR_PART, ix, params_addr, v)

    def _lua_params_get_length(self, params_addr:'addr:p_t') -> int:
        return self.de.getlength(p_t, params_addr)

    ## For function_t

    def _lua_function_get_mvm_func(self, func_addr:'addr:f_t') -> int:
        return self.de.getfield(f_t, f_t, [], t.SCALAR, None, func_addr)

    def _lua_function_set_mvm_func(self, func_addr:'addr:f_t', sid:int):
        self.de.setfield(f_t, f_t, [], t.SCALAR, None, func_addr, sid)

    ## For error_t

    def _lua_error_get_value(self, error_addr:'addr:e_t') -> "tagref64":
        return self.de.getfield(e_t, lv_r, [0], t.SCALAR, None, error_addr)

    def _lua_error_set_value(self, error_addr:'addr:e_t', value:"tagref64"):
        self.de.setfield(e_t, lv_r, [0], t.SCALAR, None, error_addr, value)

    # Methods for creating simple Lua objects

    def make_nil(self) -> "tagref64":
        """ Return the tagref64 data for Lua nil. """
        return self.nil_data

    def make_boolean(self, pyval:bool) -> "tagref64":
        """ Return the tagref64 data of a Lua boolean for pyval.  """
        return self.true_data if pyval else self.false_data

    def make_number(self, pyval:float) -> "tagref64":
        """ Return the tagref64 value for pyval.  """
        return self.de.fp_to_tr64(pyval)

    def make_string(self, s:str) -> "tagref64":
        """ Allocate a Lua string object and return its tagref64 data. """
        # NOTE: Make sure you put it into a global reference if needed.

        if any(ord(ch)>=65536 for ch in s):
            raise ValueError("Restricted to double_byte characters")

        string_addr = self.de.allochybridarray(self.string_td, len(s))
        for ix,ch in enumerate(s):
            self._lua_string_set_char(string_addr, ix, ord(ch))

        return self.de.ref_to_tr64(string_addr, LUA_TSTRING)

    def make_table(self, pyval:"[(tagref64,tagref64)]"=None) -> "tagref64":
        """ Allocate and initialise a new table and return its tagref64. """

        table_addr = self.de.alloc(self.table_td)
        ht_addr = self.de.allochybridarray(self.hash_table_td, TABLE_INIT_SIZE)
        self._lua_table_set_ht(table_addr, ht_addr)
        self._lua_table_set_size(table_addr, 0)

        if pyval is not None:
            for k,v in pyval:
                self.lua_table_set(table_addr, k, v)

        return self.de.ref_to_tr64(table_addr, LUA_TTABLE)

    def make_params(self, *luavals:"tagref64*") -> "addr:p_t":
        """
        Allocate and initialise a new parameter list. Reuse the existing empty
        parameter list when possible.
        """

        length = len(luavals)
        if length == 0:
            return self.empty_params_gr.obj_addr
        else:
            params_addr = self.de.allochybridarray(self.params_td, length)
            for ix in range(length):
                self._lua_params_set_item(params_addr, ix, luavals[ix])
            return params_addr

    def get_env(self) -> "tagref64":
        """ Get the _ENV table.  """
        return self.de.ref_to_tr64(self.env_gr.obj_addr, LUA_TTABLE)
        

    # Simple lua object queries and operations.

    def lua_get_typeid(self, luaval:"tagref64") -> int:
        """ A convenient function to get the type ID of a given Lua value.  """
        if self.de.is_fp(luaval):
            return LUA_TNUMBER
        elif self.de.is_int(luaval):
            return LUA_TBOOLEAN
        else:
            return self.de.tr64_to_tag(luaval)

    def from_lua(self, luaval:"tagref64") -> object:
        """
        Convert a lua value to a suitable Python representation.

        Conversion rule:
        nil     -> None
        boolean -> bool
        number  -> float
        string  -> str
        table   -> [(kaddr,vaddr)]
        """

        if self.de.is_fp(luaval):
            return self.de.tr64_to_fp(luaval)
        elif self.de.is_int(luaval):
            return self.de.tr64_to_int(luaval) == 1
        else:
            typeid = self.de.tr64_to_tag(luaval)
            addr = self.de.tr64_to_ref(luaval)
            if typeid == LUA_TNIL:
                return None
            elif typeid == LUA_TSTRING:
                length = self._lua_string_get_length(addr)
                ords = []
                for ix in range(length):
                    ch = self._lua_string_get_char(addr, ix)
                    ords.append(ch)

                return "".join(chr(ch) for ch in ords)
            elif typeid == LUA_TTABLE:
                ht_addr = self._lua_table_get_ht(addr)
                n_elems = self._lua_table_get_size(addr)
                result = []

                for ix in range(n_elems):
                    hk, hv = self._lua_ht_get_entry(ix)
                    result.append((hk,hv))

                return result
            else:
                raise NotImplementedError(
                        "Cannot convert type {} to Python yet".format(typeid))

    def params_dump(self, params:'addr:p_t') -> '[tagref64]':
        """ Unpack the contents of a params_t object.  """
        n_elems = self._lua_params_get_length(params)
        result = []

        for ix in range(n_elems):
            v = self._lua_params_get_item(params, ix)
            result.append(v)

        return result


    def lua_raw_equals(self, x:"tagref64", y:"tagref64"):
        """
        Test whether two lua values are equal. Only support simple types:
        nil, boolean, number and string.

        x, y :: int     the addresses of the two values.
        """

        tx, ty = self.lua_get_typeid(x), self.lua_get_typeid(y)
        if tx != ty:
            return False    # Different types result in False
        elif tx in [LUA_TNUMBER, LUA_TSTRING]:
            return self.from_lua(x) == self.from_lua(y)     # By value
        else:
            return x == y   # By reference (and also tag)

    ## Table access

    def lua_table_set(self, table:"tagref64", k:"tagref64", v:"tagref64"):
        table_addr = self.de.tr64_to_ref(table)
        return self._lua_table_set(table_addr, k, v)
    
    def _lua_table_set(self, table_addr:'addr', k:"tagref64", v:"tagref64"):
        """ Set an entry of a Lua table.  """
        ht_addr = self._lua_table_get_ht(table_addr)
        n_elems = self._lua_table_get_size(table_addr)

        for ix in range(n_elems):
            hk = self._lua_ht_get_key(ht_addr, ix)
            if self.lua_raw_equals(hk, k):
                found = True
                break
        else:
            found = False

        v_type = self.lua_get_typeid(v)
        if v_type == LUA_TNIL:      # Setting to nil. i.e. Removing entry.
            if found:
                nl = n_elems - 1    # New length
                if ix < nl:         # Not the last element
                    # Move the last element to the front
                    lk, lv = self._lua_ht_get_entry(ht_addr, nl)
                    self._lua_ht_set_entry(ht_addr, ix, lk, lv)

                # Set the last entry to zeros to prevent dangling references
                self._lua_ht_set_entry(ht_addr, nl, 0, 0)
                # Update table size
                self._lua_table_set_size(table_addr, nl)
        else:                       # Actually inserting entry
            if found:
                # Update value
                self._lua_ht_set_value(ht_addr, ix, v)
            else:
                nl = n_elems + 1    # New length
                hash_size = self._lua_ht_get_capacity(ht_addr)
                if n_elems == hash_size:    # Full. Extend hash table.
                    ht_new_addr = self.de.allochybridarray(self.hash_table_td,
                            hash_size * 2)  # Double the size
                    for ix2 in range(n_elems):  # Copy entries
                        old_k, old_v = self._lua_ht_get_entry(ht_addr, ix2)
                        self._lua_ht_set_entry(ht_new_addr, ix2, old_k, old_v)

                    self._lua_table_set_ht(table_addr, ht_new_addr)
                    ht_addr = ht_new_addr
                # Insert new element
                self._lua_ht_set_entry(ht_addr, n_elems, k, v)
                # Update table size
                self._lua_table_set_size(table_addr, nl)

    
    def lua_table_get(self, table:"tagref64", k:"tagref64") -> "tagref64":
        table_addr = self.de.tr64_to_ref(table)
        return self._lua_table_get(table_addr, k)

    def _lua_table_get(self, table_addr:'addr', k:"tagref64") -> "tagref64":
        """ Get an entry from a Lua table. Return the value as tagref64. """
        ht_addr = self._lua_table_get_ht(table_addr)
        n_elems = self._lua_table_get_size(table_addr)

        for ix in range(n_elems):
            hk = self._lua_ht_get_key(ht_addr, ix)
            if self.lua_raw_equals(hk, k):
                hv = self._lua_ht_get_value(ht_addr, ix)
                return hv
        else:
            return self.make_nil()

    def lua_func_get_sid(self, func:"tagref64") -> int:
        """ Get the corresponding snippet ID from a Lua function value. """
        func_addr = self.de.tr64_to_ref(func)
        return self._lua_function_get_mvm_func(func_addr)
    
    # Lua code execution
    @abstractmethod
    def run_lua(self, prog:str):
        """ Load a Lua program from its text form and execute it.  """

    # Built-in functions
    # They follows the "TrapCall" calling convention.

    def builtin_print(self, thread:'i.Thread', largs:'addr:p_t') -> 'addr:p_r':
        """ Print its parameters """
        lalen = self._lua_params_get_length(largs)
        for ix in range(lalen):
            lval = self._lua_params_get_item(largs, ix)
            s = self.lua_print_repr(lval)
            if ix > 0:
                print("\t", sep="", end="")
            print(s, end="")
        print()

        return self.make_params()

    def lua_print_repr(self, lval:"tagref64"):
        if self.de.is_fp(lval):
            s = self.from_lua(lval)
        elif self.de.is_int(lval):
            s = "true" if self.de.tr64_to_int(lval)==1 else "false"
        else:
            lval_tid = self.de.tr64_to_tag(lval)
            lval_ref = self.de.tr64_to_ref(lval)
            if lval_tid == LUA_TNIL:
                s = "nil"
            elif lval_tid == LUA_TSTRING:
                s = self.from_lua(lval)
            elif lval_tid == LUA_TTABLE:
                s = "table: 0x{:x}".format(self.de.tr64_to_ref(lval_ref))
            elif lval_tid == LUA_TFUNCTION:
                func_sid = self._lua_function_get_mvm_func(lval_ref)
                s = "function: 0x{:x}".format(func_sid)
            else:
                s = "other type: 0x{:x}; tid={}".format(lval_ref, lval_tid)

        return s

    def lua_repr(self, lval:"tagref64"):
        if self.de.is_fp(lval):
            s = self.from_lua(lval)
        elif self.de.is_int(lval):
            s = "true" if self.de.tr64_to_int(lval)==1 else "false"
        else:
            lval_tid = self.de.tr64_to_tag(lval)
            lval_ref = self.de.tr64_to_ref(lval)
            if lval_tid == LUA_TNIL:
                s = "nil"
            elif lval_tid == LUA_TSTRING:
                s = self.from_lua(lval)
            elif lval_tid == LUA_TTABLE:
                s = "table: 0x{:x}".format(self.de.tr64_to_ref(lval_ref))
            elif lval_tid == LUA_TFUNCTION:
                func_sid = self._lua_function_get_mvm_func(lval_ref)
                s = "function: 0x{:x}".format(func_sid)
            elif lval_tid == LUACLIENT_TPARAMS:
                if lval_ref == self.empty_params_gr.obj_addr:
                    s = "empty_params"
                else:
                    s = "params@0x{:x]".format(lval_ref)
            else:
                s = "other type: 0x{:x}; tid={}".format(lval_ref, lval_tid)

        return s

class LuaClient(AbstractLuaClient):
    def __init__(self):
        super().__init__()

        # A LuaCompiler object to compile lua code
        self.lcmp = luabuilder.LuaCompiler(self)

    def build_lua(self, prog:str) -> "addr:f_t":
        """ Build a lua program to a snippet.  """
        
        func = self.lcmp.build_from_code(prog)

        return func
    
    # Lua code execution
    def run_lua(self, prog:str):
        """ Load a Lua program from its text form and execute it.  """

        func = self.build_lua(prog)
        bottom_frame_func_sid = self.prim_funcs["bottom_frame_func"].sid

        th = self.itpr.run_snippet(bottom_frame_func_sid, [func])
        
if __name__=='__main__':
    import sys
    if len(sys.argv) != 2:
        print(
"""USAGE: python -m luaclient.luaclient <filename.lua>
If <filename.lua> is -, then it is read from STDIN
Use the Lua test suite for testing.
Available at http://www.lua.org/tests/5.2/""")
    else:
        lua_prog_fn = sys.argv[1]
        if lua_prog_fn == "-":
            lua_prog_raw = sys.stdin.read()
        else:
            with open(lua_prog_fn) as f:
                lua_prog_raw = f.read()

        #print("This is the program:", lua_prog_raw)

        lua_client = LuaClient()
        lua_client.run_lua(lua_prog_raw)


