from . import (mtypes as t, layout)
from .mconstants import *
from .commontypes import *

class MicroVM():

    def __init__(self, gc_strategy="NO_GC"):
        """
        Create a new instance of the MicroVM.

        The client may choose a different GC strategy via the gc_strategy
        parameter (defaults to "NO_GC"). This is currently ignored.

        """

        self.mem_sz = 2097152
        self.mem = bytearray(self.mem_sz)

        self.reserve_size = 524288
        self.obj_space_size = self.mem_sz - self.reserve_size
        self.obj_space = Space(self.mem, self.reserve_size, self.obj_space_size)

        self.global_refs = set()

    def define_type(self, ty:t.Type, is_prolific:bool=False):
        """ Create a type descriptor.  """

        return t.TypeDescriptor(ty)

    def make_global_ref(self, obj_addr:int):
        " Create a global reference and add to the MicroVM's global_refs set. "

        global_ref = GlobalRef(obj_addr)
        self.global_refs.add(global_ref)
        return global_ref

    def release_global_ref(self, global_ref:"GlobalRef"):
        """ Release a global reference. """

        self.global_refs.remove(global_ref)

class GlobalRef(object):
    def __init__(self, obj_addr:int):
        self.obj_addr = obj_addr

class Space(object):
    def __init__(self, mem, begin, size):
        self.mem = mem
        self.begin = begin
        self.size = size
        self.end = begin + size
        self.cur_ptr = layout.align_up(self.begin, WORD_SIZE_BYTES)

    def allocate(self, obj_size:int) -> int:
        """
        Allocate a piece of memory. obj_size is in bytes.

        Return the BEGINNING of the allocated object. This is not the objref.
        """

        if self.cur_ptr + obj_size > self.end:
            raise MemoryError("Insufficient MicroVM object space")

        offset = self.cur_ptr
        self.cur_ptr = layout.align_up(self.cur_ptr + obj_size, WORD_SIZE_BYTES)

        return offset

