#!/usr/bin/env python3

"""
A utility class for a somehow static type model.

A supertuple may contain fields or annotations.

A field is a formal member of the type. It is defined with the grammar:

    field_name = Field(ty, desc)

where ty is an object for type checking and desc is a human-readable description
of this field. Fields may be used for automatic type checking (see snippetcheck)

An annotation is an assignable filed, but is less "officially" a part of a
SuperTuple object. It is defined with a class attribute:

    __annotations__ = ["anno1", "anno2", ...]

Fields and annotations can be initialised with the class's __init__ method, but
only Fields can be initialised with positional arguments whild annotations can
only be initialised with keyword arguments.

Example:

    class Foo(SuperTuple):
        __annotations__ = ["anno1", "anno2"]

        fld1 = Field(int)
        fld2 = Field(double, "This is dangerous! Set with care!")
        fld3 = Field(["some", "machine", "readable", "object"], "hahahah!)

    # okay
    foo = Foo(42, 3.14, "whatever", anno1="Safe", anno2=True)
    print(foo.fld1, foo.fld2, foo.fld3, foo.anno1, foo.anno2)

    # same as above
    foo = Foo(42, fld3="whatever", fld2=3.14, anno1="Safe", anno2=True)

    # error: Too many fields
    foo = Foo(1,2,3,4)

    # error: Unknown field or annotation
    foo = Foo(1,2,3, blahblah=42)
    
    # automatically assign to None
    foo = Foo(1, anno1=2)
    print(foo.fld1, foo.fld2, foo.fld3, foo.anno1, foo.anno2)
    # prints: 1 None None 2 None

A SuperTuple may "mix-in" other supertuples. This differs from inheritence in
the order of fields. Mixed-in fields will be inserted at the mix-in point.
MixIns are also considered "parents" of the current type just like being
inherited.

Example:

    class Bar(SuperTuple):

        bar1 = Field(int)
        bar2 = Field(double)
        foo_mixin = MixIn(Foo)
        bar3 = Field(int)

Bar's fields will be [bar1, bar2, fld1, fld2, fld3, bar3].

"""

__all__ = ["Field", "MixIn", "SuperTuple"]

import collections

class Field():
    def __init__(self, ty, description=None):
        self.ty = ty
        self.description = description

class MixIn():
    def __init__(self, cls):
        self.cls = cls

class SupertupleMeta(type):
    def __prepare__(name, bases, **kwargs):
        return collections.OrderedDict()

    def __new__(mcs, name, bases, dic):
        fields = collections.OrderedDict()
        annotations = []

        def inherit(cls):
            if hasattr(cls, "__fields__"):
                fields.update(cls.__fields__)
            if hasattr(base, "__annotations__"):
                annotations.extend(cls.__annotations__)

        # Inherit from parents
        for base in bases:
            inherit(base)

        # Local attributes
        local_fields = []
        extra_bases = []

        for k,v in dic.items():
            if isinstance(v, Field):
                fields[k] = v
                local_fields.append(k)
            elif isinstance(v, MixIn):
                inherit(v.cls)
                extra_bases.append(v.cls)

        for fn in local_fields:
            del dic[fn]

        if len(extra_bases)>0:
            bases = bases+tuple(extra_bases)

        if "__annotations__" in dic:
            annotations.extend(dic["__annotations__"])

        def __init__(s, *args, **kwargs):
            if len(args) > len(fields):
                raise AttributeError("SuperTuple {} has {} fields. {} supplied"
                        .format(name, len(fields), len(args)))
            for k in kwargs:
                if k not in fields and k not in annotations:
                    raise AttributeError("SuperTuple {} has no field '{}'"
                            .format(name, k))

            for fn in fields.keys():
                setattr(s, fn, None)

            for fn in annotations:
                setattr(s, fn, None)

            for fn, fv in zip(fields.keys(), args):
                setattr(s, fn, fv)

            for fn, fv in kwargs.items():
                setattr(s, fn, fv)

        def __setattr__(s, attrname, value):
            if attrname not in fields and attrname not in annotations:
                raise AttributeError("SuperTuple {} has no field '{}'"
                        .format(name, attrname))

            object.__setattr__(s, attrname, value)

        dic.update({
                "__init__": __init__,
                "__annotations__": annotations,
                "__setattr__": __setattr__,
                "__fields__": fields,
                })

        return type.__new__(mcs, name, bases, dic)

class SuperTuple(metaclass=SupertupleMeta):
    pass
