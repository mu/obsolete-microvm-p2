from . import mtypes as t
from .mconstants import *
from .commontypes import *
    
"""
####### Object layout #######

Fixed (atomic or Struct) object layout:
+--------+--------+--------+-----------+----------+
| field0 | field1 | field2 | ......... | fieldN-1 |
+--------+--------+--------+-----------+----------+
0           w
^objref

HybridArray object layout:
+------------+--------+-------+-------+-----+---------+
+ fixed part | length | elem0 | elem1 | ... | elemN-1 |
+------------+--------+-------+-------+-----+---------+
0   .....    f        f+w
^objref

w = word size
f = fixed part size (word-aligned)

####### Size and Alignment #######

An object's address (objref) is always word-aligned (on 64-bit systems, it is 8
bytes). This also implies that the last 3 bits of any valid references are
always zeroes.

For atom types (not struct or array) of 1, 2, 4 and 8 bytes, their
addresses will remain 0 when divided by 1, 2, 4 and 8, respectively.

Atom types of other sizes less than 8 bytes are rounded up to the next power of
two in both size and alignment.

Atom types of sizes larget than 8 bytes will occupy the space of a multiple of
WORD_SIZE_BYTES bytes and are aligned to WORD_SIZE_BYTES bytes.

Structs are always word-aligned and stored in multiple of words even if they
consist of smaller components.

The fixed part of a HybridArray may be any fixed type (Struct or atom types),
but that part is always word-aligned (or more precisely, the variable part
always starts in a word-aligned address).  """

def field_offset(ty:t.Type, field_indices:[int], part:'PART'=t.SCALAR,
        index:int=None) -> int:
    """
    Get the offset of a field relative to the object address.

    Usable for both fixed objects and HybridArray. See mtypes.field_type for the
    documentation about the field_indices and part parameters.

    The index parameter is used for accessing the variable part of a HybridArray
    type.
    """
    if isinstance(ty, t.HybridArray):
        if part == t.FIXED_PART:
            cur_ty = ty.fixed_ty
            cur_off = HYBRIDARRAY_FIELDS_OFFSET
        elif part == t.VAR_PART:
            if index == None:
                raise ValueError("The 'index' parameter must be specified "
                        "when calculating the offset of a VAR_PART.")
            cur_ty = ty.var_ty
            array_begin_off = elements_offset(ty)
            elem_off = align_up(sizeof(cur_ty), alignof(cur_ty)) * index
            cur_off = array_begin_off + elem_off
        else:
            raise TypeError("Must select either the fixed part or the var part "
                    "using the 'part' parameter")
    else:
        cur_ty = ty
        cur_off = FIELDS_OFFSET

    for f in field_indices:
        if isinstance(cur_ty, t.Struct):
            if f >= len(cur_ty.fields):
                raise TypeError("Struct {} has ony {} fields, "
                        "but the {}-th field is requested".format(
                            cur_ty, len(cur_ty.fields), f))

            prefix_sz = type_seq_size(cur_ty.fields[:f])
            field_align = alignof(cur_ty.fields[f])
            cur_off = align_up(cur_off + prefix_sz, field_align)

            cur_ty = cur_ty.fields[f]

        else:
            raise TypeError("Type {} is a primitive type, "
                        "but the {}-th field is requested".format(cur_ty, f))

    return cur_off

# General object offsets
## the offset of the object reference relative to the beginning of the object
OBJREF_FROM_BEGIN = 0

# Fixed object offsets

## the offset of the first field of a fixed object
FIELDS_OFFSET = 0

# HybridArray object offsets

## the offset of the fixed part of a HybridArray
HYBRIDARRAY_FIELDS_OFFSET = 0

def elements_offset(ty):
    " Return the offset of the first element of a HybridArray "
    return align_up(sizeof(ty.fixed_ty), WORD_SIZE_BYTES) + WORD_SIZE_BYTES

def length_offset(ty):
    " Return the offset of the length field of a HybridArray "
    return align_up(sizeof(ty.fixed_ty), WORD_SIZE_BYTES)

# Object size queries. These are used for allocating memory.

def fixed_obj_size(ty):
    " Return the total size (including headers) of a fixed-size object. "
    return align_up(sizeof(ty), WORD_SIZE_BYTES)

def hybridarray_obj_size(ty, length):
    " Return the total size (including headers) of a HybridArray object. "
    fixed_sz = align_up(sizeof(ty.fixed_ty), WORD_SIZE_BYTES)
    var_head_sz = WORD_SIZE_BYTES
    var_body_sz = align_up(sizeof(ty.var_ty), alignof(ty.var_ty)) * length
    return fixed_sz + var_head_sz + align_up(var_body_sz, WORD_SIZE_BYTES)

# General object size query

def sizeof(ty):
    """
    Size of the space a fixed type occupies in bytes.
    """
    if isinstance(ty, t.Void):
        return 0
    elif isinstance(ty, t.AtomType):
        return bytes_size(bits_to_bytes(ty.bitlength))
    elif isinstance(ty, t.Struct):
        return align_up(type_seq_size(ty.fields), WORD_SIZE_BYTES)
    elif isinstance(ty, t.HybridArray):
        raise TypeError("HybridArray's size is not statically known")
    else:
        raise TypeError("Unsupported type {}".format(ty))

def alignof(ty):
    """
    Returns the required alignment for a specific fixed type in bytes.
    """
    if isinstance(ty, t.Void):
        return 1
    elif isinstance(ty, t.AtomType):
        return bytes_alignment(bits_to_bytes(ty.bitlength))
    elif isinstance(ty, t.Struct):
        return WORD_SIZE_BYTES
    elif isinstance(ty, t.HybridArray):
        raise TypeError("HybridArray cannot be embedded in an object, hence "
                "alignment is unapplicable for HybridArray.")
    else:
        raise TypeError("Unsupported type {}".format(ty))

def type_seq_size(seq:[t.Type]) -> int:
    """
    Calculate the size of a sequence of many types, assuming the initial
    address is aligned to word. The end is not aligned.
    """
    sz = 0
    for ty in seq:
        tysz = sizeof(ty)
        tyal = alignof(ty)
        sz = align_up(sz, tyal)
        sz = sz + tysz

    return sz

def bits_to_bytes(n:int) -> int:
    return align_up(n, 8) >> 3

def next_pow_of_two(n:int) -> int:
    """ Convert n (in bytes) to the next power of 2. """
    sz = 1
    while sz < n:
        sz <<= 1
    return sz

def bytes_size(n:int) -> int:
    """ Calculate the size (in bytes) of atom types of n bytes. """
    if n >= 8:
        return align_up(n, WORD_SIZE_BYTES)
    else:
        return next_pow_of_two(n)

def bytes_alignment(n:int) -> int:
    """ Calculate the alignment (in bytes) of atom types of n bytes. """
    if n >= 8:
        return WORD_SIZE_BYTES
    else:
        return next_pow_of_two(n)

def align_up(addr:int, align:int) -> int:
    return (addr + align - 1) & ~(align-1)

def align_down(addr:int, align:int):
    return addr & ~(align - 1)
