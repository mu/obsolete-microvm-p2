"""
Constants used in the MicroVM Project.

Values with the _log suffix means yyy_yyy_sz==2**x wher x==yyy_yyy_log.
"""

def _bits(l): return 1<<l
def _bytes(l): return 1<<(l-3)

WORD_SIZE_LOG = 6
WORD_SIZE_BITS = _bits(WORD_SIZE_LOG)
WORD_SIZE_BYTES = _bytes(WORD_SIZE_LOG)

ADDRESS_SIZE_LOG = WORD_SIZE_LOG
ADDRESS_SIZE_BITS = _bits(ADDRESS_SIZE_LOG)
ADDRESS_SIZE_BYTES = _bytes(ADDRESS_SIZE_LOG)
