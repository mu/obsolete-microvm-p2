from .utils.supertuple import Field, SuperTuple

from .mconstants import *

from abc import abstractmethod

def abstracttype(ty):
    """ A no-op annotation to mark that a type is abstract. """
    return ty

@abstracttype
class Type(SuperTuple):
    def __repr__(self):
        """ Pretty print this type. """
        return pptype(self)

class Signature(SuperTuple):
    """
    A function signature. Not a MicroVM IR type.
    """
    param_ty = Field([Type])        # Parameter types
    ret_ty = Field(Type)            # Return value type

    def __repr__(self):
        """ Pretty print this type. """
        return "Signature<({}): {}>".format(
                ", ".join(map(pptype, self.param_ty)),
                pptype(self.ret_ty))

@abstracttype
class AtomType(Type):
    """
    A type whose value is stored alone, not a combination of multiple values.
    
    A type which is not an atom type is a compound type.
    """

    @property
    @abstractmethod
    def bitlength(self):
        return

class Int(AtomType):
    length = Field(int)             # Length in bits

    @property
    def bitlength(self):
        return self.length

@abstracttype
class FloatingPointType(AtomType):
    pass

class Float(FloatingPointType):
    @property
    def bitlength(self):
        return 32

class Double(FloatingPointType):
    @property
    def bitlength(self):
        return 64

class Ref(AtomType):
    ty = Field(Type)                # The referred type

    @property
    def bitlength(self):
        return ADDRESS_SIZE_BITS

class WeakRef(AtomType):
    ty = Field(Type)                # The referred type
    weakness = Field(int, ">0")     # The higher, the weaker.

    @property
    def bitlength(self):
        return ADDRESS_SIZE_BITS

class InternalRef(AtomType):
    ty = Field(Type)

    @property
    def bitlength(self):
        return ADDRESS_SIZE_BITS

class TagRef64(AtomType):
    """
    A TagRef64 is a tagged union of Int(52), Double() and a Ref(ty). Its length
    is 64 bits.
    
    Only usable when ADDRESS_SIZE_BITS==64. Using on other platforms is a static
    error.
    """
    ty = Field(Type)                # The referered type

    @property
    def bitlength(self):
        return ADDRESS_SIZE_BITS

class Struct(Type):
    fields = Field([Type])          # Fields of the struct

    @property
    def bitlength(self):
        return ADDRESS_SIZE_BITS

# These three constants are for selecting different parts of a HybridArray. The
# SCALAR constant is for non-HybridArray types.
SCALAR = 0
FIXED_PART = 1
VAR_PART = 2

class HybridArray(Type):
    fixed_ty = Field(Type)
    var_ty = Field(Type)

class Void(AtomType):
    @property
    def bitlength(self):
        return 0

class FuncHandle(AtomType):
    sig = Field(Signature)    # Function signature

    @property
    def bitlength(self):
        return ADDRESS_SIZE_BITS

def mkfunchandle(param_ty, ret_ty):
    """ An easier way to create function handle types.  """
    return FuncHandle(Signature(param_ty, ret_ty))

class Thread(AtomType):
    @property
    def bitlength(self):
        return ADDRESS_SIZE_BITS

class Stack(AtomType):
    @property
    def bitlength(self):
        return ADDRESS_SIZE_BITS

class Frame(AtomType):
    @property
    def bitlength(self):
        return ADDRESS_SIZE_BITS

# Memory management objects

class TypeDescriptor():
    """
    A type descriptor contains all information needed to allocate a type and
    perform garbage collection. This is an abstract class. The memory manager
    should extend this type to contain more information.
    """

    def __init__(self, ty):
        """
        Every TypeDescriptor should at least know the type it represents.
        """
        self.ty = ty

# Helper functions

def pptype(ty, custom=None, visited=None):
    """
    Pretty print type. Alias of Type.repr()
    """
    if visited==None:
        visited = []

    visited_par = -1
    for i in range(len(visited)-1, -1, -1):
        if visited[i] == ty:
            visited_par = i
            break
    if visited_par != -1:
        return "..{}".format(len(visited)-visited_par)

    if custom is not None:
        custom_result = custom(ty)
        if custom_result is not None:
            return custom_result

    nv = visited + [ty]

    if isinstance(ty, Int):
        return "Int({})".format(ty.length)
    elif isinstance(ty, Ref):
        return "Ref({})".format(pptype(ty.ty, custom, nv))
    elif isinstance(ty, WeakRef):
        return "WeakRef_{}({})".format(ty.weakness, pptype(ty.ty, custom, nv))
    elif isinstance(ty, InternalRef):
        return "InternalRef({})".format(pptype(ty.ty, custom, nv))
    elif isinstance(ty, TagRef64):
        return "TagRef64({})".format(pptype(ty.ty, custom, nv))
    elif isinstance(ty, Struct):
        return "Struct({})".format(
                ", ".join([pptype(tyy, custom, nv) for tyy in ty.fields]))
    elif isinstance(ty, HybridArray):
        return "HybridArray(fixed_ty={}, var_ty={})".format(
                pptype(ty.fixed_ty, custom, nv), pptype(ty.var_ty, custom, nv))
    elif isinstance(ty, FuncHandle):
        sig = ty.sig
        return "FuncHandle(<{}>: {})".format(
                ",".join([
                    pptype(tyy, custom, nv) for tyy in sig.param_ty]),
                    pptype(sig.ret_ty, custom, nv))
    else:
        return type(ty).__name__

def type_equals(ty1, ty2):
    """
    Test whether two types are equal. Alias of ty1==ty2.
    """

    visited = set()
    look_stack = [(ty1,ty2)]

    while len(look_stack) > 0:
        l,r = look_stack.pop()

        if (l,r) in visited:
            return True

        visited.add((l,r))

        if type(l) != type(r):
            return False
        
        tty = type(l)

        # Test current node

        if issubclass(tty, Int):
            ceq = l.length==r.length
        elif issubclass(tty, WeakRef):
            ceq = l.weakness==r.weakness
        elif issubclass(tty, Struct):
            ceq = len(l.fields)==len(r.fields)
        elif issubclass(tty, Signature):
            ceq = len(l.param_ty)==len(r.param_ty)
        else:
            ceq = True

        if not ceq:
            return False

        # Push related nodes

        ## With ".ty" field
        if any(issubclass(tty, ty) for ty in (Ref, WeakRef, InternalRef,
            TagRef64)):
            look_stack.append((l.ty, r.ty))
        elif issubclass(tty, HybridArray):
            look_stack.append((l.fixed_ty, r.fixed_ty))
            look_stack.append((l.var_ty, r.var_ty))
        elif issubclass(tty, Struct):
            for nty1, nty2 in zip(l.fields, r.fields):
                look_stack.append((nty1, nty2))
        elif issubclass(tty, FuncHandle):
            look_stack.append((l.sig, r.sig))
        elif issubclass(tty, Signature):
            for nty1, nty2 in zip(l.param_ty, r.param_ty):
                look_stack.append((nty1, nty2))
            look_stack.append((l.ret_ty, r.ret_ty))

    return True


def is_prefix(ty1, ty2):
    """
    Check if ty1 is a prefix of ty2. Alias of ty1 < ty2.
    
    The "isprefix" relation is required for typecasting. If ty1 is a prefix of
    ty2, then ty1 and ty2 can be cast to each other. Casting ty1 to ty2 is
    called "downcast" and casting t2 to t1 is called "upcast".

    ty1 is a prefix of ty2 if either
    - ty1 is Void (which is a prefix of all types including itself)
    - or both ty1 and ty2 are Structs and ty1.fields and
      ty2.fields[:len(ty1.fields)] are identical element-wise.
    - ty1 and ty2 are equal
    """

    return (
            isinstance(ty1, Void)
            or
            isinstance(ty1, Struct) and isinstance(ty2, Struct) and
            len(ty1.fields) <= len(ty2.fields) and
            all(type_equals(f1, f2) for f1,f2 in zip(ty1.fields, ty2.fields))
            or
            type_equals(ty1, ty2)
            )

def field_type(ty:Type, field_indices:[int], part:'PART'=SCALAR) -> Type:
    """
    Get the type of a field.

    This function can find the type of a field in a Struct or the type itself
    for any scalar. If ty is a HybridArray, the "part" parameter is used to
    select its fixed part type or its variable part type, either of which must
    be a scalar type. Otherwise the "part" parameter is ignored.

    field_indices is an array of indices into nested Structs. Each element is
    an index of the Struct fields. When field_indices is an empty list, this
    function returns the type itself.

    For example, t1 = Struct([Double(), Struct([int8_t, int16_t, int32_t])),
    then field_type(t1, [1,2]) is int32_t. It goes through the 1st field of the
    outer Struct and the 2nd field of the inner Struct.
    """

    if isinstance(ty, HybridArray):
        if part == FIXED_PART:
            cur_ty = ty.fixed_ty
        elif part == VAR_PART:
            cur_ty = ty.var_ty
        else:
            raise TypeError("Must select either the fixed part or the var part "
                    "using the 'part' parameter")
    else:
        cur_ty = ty

    for f in field_indices:
        if isinstance(cur_ty, Struct):
            if f >= len(cur_ty.fields):
                raise TypeError("Struct {} has ony {} fields, "
                        "but the {}-th field is requested".format(
                            cur_ty, len(cur_ty.fields), f))

            cur_ty = cur_ty.fields[f]

        else:
            raise TypeError("Type {} is a primitive type, "
                        "but the {}-th field is requested".format(
                            cur_ty, f))

    return cur_ty

