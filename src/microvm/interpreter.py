"""
This module interprets MicroVM snippets.

The interpreter creates an environment for executing a snippet. The environment
contains:

- a Thread: a thread of execution, the unit of CPU scheduling.
            It refers to one Stack at a time.
- a Stack: the execution status of nested function calls.
           It refers to the top stack frames.
- many Frames: each frame is the execution context of a function. Stack frames
               are linked in a singly-linked list structure. Each frame contains
               its local variables as registers and its current program counter.

To avoid confusion, this module uses the word "value" for SSA values themselves
(instances of s.Value) and the word "data" for the actual value an SSA value is
currently holding (as defined in the directexecutor.py).
"""

import os
_PRINT_STACK_TRACE_SNIPPETS = (
        os.environ.get("MVM_PRINT_STACK_TRACE_SNIPPETS", None)
        in ("on", "yes", "1"))

from . import (mtypes as t, snippet as s, snippetcheck as sc, microvm as mvm,
        directexecutor)
from .mconstants import *
from .commontypes import *

de = directexecutor

import collections
import math

# PC is the program counter type.
# PC.bb :: BasicBlock
# PC.ix :: int          The index within the BasicBlock.
PC = collections.namedtuple("PC", ["bb", "ix"])

class Register(object):
    " The storage of an SSA value's data. "

    def __init__(self, ty:t.Type):
        self.ty = ty
        self.data = None    # Current data. See directexecutor

class Frame(object):
    def __init__(self, stack, prev:"Frame"=None, regs:"{Value:Register}"=None,
            snp:"s.Snippet"=None):
        self.stack = stack
        self.prev = prev        # Previous frame
        self.regs = {}          # Maps SSA values to registers
        self.snp = snp          # The snippet this frame is for
        self.cur_call = None    # The PC to the current Call instruction

    @staticmethod
    def for_snippet(snippet:s.Snippet, args:list, stack:"Stack"):
        """
        Cretae a new frame for a snippet.

        This method will create a register for every instruction.
        """
        frame = Frame(stack)

        frame.snp = snippet

        if len(snippet.params) != len(args):
            raise TypeError("Snippet {} accepts {} arguments, "
                    "but {} supplied.".format(
                        getattr(snippet, "name", "(unnamed)"),
                        len(snippet.params), len(args)))

        for val,data in zip(snippet.params, args):
            reg = Register(val.ty)
            reg.data = data
            
            frame.regs[val] = reg

        for bb in snippet.basic_blocks:
            for val in bb.instructions:
                frame.regs[val] = Register(val.value_type)

        return frame

class Stack(object):
    def __init__(self):
        self.top = None         # The top frame

    @staticmethod
    def for_snippet(snippet:s.Snippet, args:list):
        """
        Create a new stack for executing a snippet. Also create the bottom
        frame.
        """
        stack = Stack()
        bottom = Frame.for_snippet(snippet, args, stack)
        stack.top = bottom
        return stack

class Thread(object):
    """ The bare data structure for a thread """
    def __init__(self, stack=None):
        self.stack = stack
        self.pc = None          # The current program counter
        self.running = True     # Set to False in a trap will stop the thread.

    def print_stack_trace(self):
        cur_pc = self.pc
        cur_frame = self.stack.top

        if _PRINT_STACK_TRACE_SNIPPETS:
            printed = set()

        print("Stack trace:")
        while cur_frame != None and cur_pc != None:
            sm = s.SnippetMarker(cur_frame.snp)
            print("    Snippet: {}, bb: {}, inst: {}".format(
                cur_frame.snp.name, sm.bb_numbered(cur_pc.bb),
                sm.inst_numbered(cur_pc.bb.instructions[cur_pc.ix])))

            if _PRINT_STACK_TRACE_SNIPPETS:
                if not cur_frame.snp in printed:
                    printed.add(cur_frame.snp)
                    print(s.ppsnippet(cur_frame.snp))

            cur_frame = cur_frame.prev
            if cur_frame != None:
                cur_pc = cur_frame.cur_call

class InterpreterException(Exception):
    """ A general interpreter exception """

class InterpreterStaticError(InterpreterException):
    """ Thrown when an abnormal condition is caused by malformed snippet """

class InterpreterBug(InterpreterException):
    """ An impossible case which indicates a bug """

class InterpreterRuntimeException(InterpreterException):
    """
    Thrown when the interpreter encounters a run-time error. It is usually
    triggered by "undefined behaviours".
    """
        
class InterpreterThread(object):
    """ An interpreter thread which executes Instructions on a Thread """

    def __init__(self, itpr:"Interpreter", thread:Thread):
        self.itpr = itpr
        self.thread = thread

    @property
    def de(self) -> de.DirectExecutor:
        """ The DirectExecutor associated to the interpreter. """
        return self.itpr.de

    @property
    def stack(self) -> Stack:
        """ The current stack """
        return self.thread.stack
    
    @stack.setter
    def stack(self, v):
        self.thread.stack = v

    @property
    def top(self) -> Frame:
        """ The current top frame """
        return self.stack.top

    @top.setter
    def top(self, v):
        self.stack.top = v

    @property
    def pc(self) -> PC:
        """ The current PC """
        return self.thread.pc

    @pc.setter
    def pc(self, v):
        self.thread.pc = v

    @property
    def inst(self) -> s.Instruction:
        """ The current instruction """
        pc = self.pc
        return pc.bb.instructions[pc.ix]

    @property
    def running(self) -> bool:
        """ Whether the current thread should continue running """
        return self.thread.running

    @running.setter
    def running(self, v):
        self.thread.running = v

    def data(self, value:s.Value):
        """ Get the current data of an SSA value in the top frame.  """
        if isinstance(value, s.Constant):
            return value.data
        else:
            rv = self.top.regs[value].data
            if rv == None:
                raise self._error_with_status(InterpreterStaticError, lambda sm:
                        "SSA Value {} either does not generate any"
                        " data or it is read before evaluating.".format(
                            sm.value_numbered(value)))
            return rv

    def write_back(self, data):
        """ Write the data to the register for the current PC """
        self.top.regs[self.inst].data = data

    def inc_pc(self):
        """ Increment the current PC """
        self.pc = PC(self.pc.bb, self.pc.ix+1)

    def jump(self, new_pc:PC):
        """ Jump to a new PC """
        self.pc = new_pc

    def push_pc(self):
        """ Push the current pc to the top frame's cur_call field """
        self.top.cur_call = self.pc

    def pop_pc(self):
        """ Restore the pc to the saved cur_call field in the top frame """
        self.jump(self.top.cur_call)
        self.top.cur_call = None

    def run(self):
        """ Run until running==False """
        self.running = True
        while self.running:
            self.step()

    def step(self):
        """ Run for one step """
        inst_ty = type(self.inst)
        handler = getattr(self, "run_"+inst_ty.__name__, None)

        if handler is None:
            raise self._error_with_status(InterpreterStaticError, lambda sm:
                    "Unsupported instruction '{}'".format(inst_ty.__name__))

        try:
            handler()
        except:
            self.thread.print_stack_trace()
            raise

    def run_BinOp(self):
        ty, op = self.inst.ty, self.inst.op
        lhs, rhs = self.data(self.inst.lhs), self.data(self.inst.rhs)
        if isinstance(ty, t.Int):
            length = ty.length
            result = (
                self.de.binop_add (lhs, rhs, length) if op == "add"  else
                self.de.binop_sub (lhs, rhs, length) if op == "sub"  else
                self.de.binop_mul (lhs, rhs, length) if op == "mul"  else
                self.de.binop_udiv(lhs, rhs, length) if op == "udiv" else
                self.de.binop_sdiv(lhs, rhs, length) if op == "sdiv" else
                self.de.binop_urem(lhs, rhs, length) if op == "urem" else
                self.de.binop_srem(lhs, rhs, length) if op == "srem" else
                self.de.binop_shl (lhs, rhs, length) if op == "shl"  else
                self.de.binop_lshr(lhs, rhs, length) if op == "lshr" else
                self.de.binop_ashr(lhs, rhs, length) if op == "ashr" else
                self.de.binop_and (lhs, rhs, length) if op == "and"  else
                self.de.binop_or  (lhs, rhs, length) if op == "or"   else
                self.de.binop_xor (lhs, rhs, length) if op == "xor"  else
                None)
        elif isinstance(ty, t.FloatingPointType):
            result = (
                self.de.binop_fadd(lhs, rhs) if op == "fadd" else
                self.de.binop_fsub(lhs, rhs) if op == "fsub" else
                self.de.binop_fmul(lhs, rhs) if op == "fmul" else
                self.de.binop_fdiv(lhs, rhs) if op == "fdiv" else
                self.de.binop_frem(lhs, rhs) if op == "frem" else
                None)
        else:
            raise self._error_with_status(InterpreterStaticError, lambda sm:
                    "Unknown binop operand ty '{}'".format(ty))
            
        if result == None:
            raise self._error_with_status(InterpreterStaticError, lambda sm:
                    "Unknown op '{}'".format(op))

        self.write_back(result)
        self.inc_pc()

    def run_Cmp(self):
        ty, cond = self.inst.ty, self.inst.cond
        lhs, rhs = self.data(self.inst.lhs), self.data(self.inst.rhs)
        if isinstance(ty, t.Int):
            length = ty.length
            result = (
                self.de.cmp_ieq (lhs, rhs, length) if cond == "eq"  else
                self.de.cmp_ine (lhs, rhs, length) if cond == "ne"  else
                self.de.cmp_iugt(lhs, rhs, length) if cond == "ugt" else
                self.de.cmp_iuge(lhs, rhs, length) if cond == "uge" else
                self.de.cmp_iult(lhs, rhs, length) if cond == "ult" else
                self.de.cmp_iule(lhs, rhs, length) if cond == "ule" else
                self.de.cmp_isgt(lhs, rhs, length) if cond == "sgt" else
                self.de.cmp_isge(lhs, rhs, length) if cond == "sge" else
                self.de.cmp_islt(lhs, rhs, length) if cond == "slt" else
                self.de.cmp_isle(lhs, rhs, length) if cond == "sle" else
                None)
        elif isinstance(ty, t.FloatingPointType):
            result = (
                self.de.cmp_ffalse(lhs, rhs) if cond == "false" else
                self.de.cmp_foeq(lhs, rhs) if cond == "oeq" else
                self.de.cmp_fogt(lhs, rhs) if cond == "ogt" else
                self.de.cmp_foge(lhs, rhs) if cond == "oge" else
                self.de.cmp_folt(lhs, rhs) if cond == "olt" else
                self.de.cmp_fole(lhs, rhs) if cond == "ole" else
                self.de.cmp_fone(lhs, rhs) if cond == "one" else
                self.de.cmp_fueq(lhs, rhs) if cond == "ueq" else
                self.de.cmp_fugt(lhs, rhs) if cond == "ugt" else
                self.de.cmp_fuge(lhs, rhs) if cond == "uge" else
                self.de.cmp_fult(lhs, rhs) if cond == "ult" else
                self.de.cmp_fule(lhs, rhs) if cond == "ule" else
                self.de.cmp_fune(lhs, rhs) if cond == "une" else
                self.de.cmp_ftrue(lhs, rhs) if cond == "true" else
                None)
        elif isinstance(ty, t.Ref):
            result = (
                self.de.cmp_req(lhs, rhs) if cond == "req" else
                self.de.cmp_rne(lhs, rhs) if cond == "rne" else
                None)
        elif isinstance(ty, t.TagRef64):
            result = (
                self.de.cmp_treq(lhs, rhs) if cond == "treq" else
                self.de.cmp_trne(lhs, rhs) if cond == "trne" else
                None)
        else:
            raise self._error_with_status(InterpreterStaticError, lambda sm:
                    "Unknown cmp operand ty '{}'".format(ty))
            
        if result == None:
            raise self._error_with_status(InterpreterStaticError, lambda sm:
                    "Unknown cond '{}'".format(cond))

        self.write_back(result)
        self.inc_pc()

    def run_Select(self):
        cond = self.data(self.inst.cond)
        if_true = self.data(self.inst.if_true)
        if_false = self.data(self.inst.if_false)
        result = self.de.select(cond, if_true, if_false)

        self.write_back(result)
        self.inc_pc()

    def _run_Resize(self, defunc):
        opnd = self.data(self.inst.opnd)
        from_len, to_len = self.inst.from_len, self.inst.to_len
        result = defunc(opnd, from_len, to_len)

        self.write_back(result)
        self.inc_pc()

    def run_Trunc(self):
        self._run_Resize(self.de.trunc)
                    
    def run_ZExt(self):
        self._run_Resize(self.de.zext)

    def run_SExt(self):
        self._run_Resize(self.de.sext)

    def _run_Noop(self):
        opnd = self.data(self.inst.opnd)
        result = opnd

        self.write_back(result)
        self.inc_pc()

    def run_FPTrunc(self):
        self._run_Noop()    # NO-OP. Python only has Double.

    def run_FPExt(self):
        self._run_Noop()    # NO-OP. Python only has Double.

    def run_FPToUI(self):
        opnd, to_len = self.data(self.inst.opnd), self.inst.to_ty.length
        result = self.de.fptoui(opnd, to_len)

        self.write_back(result)
        self.inc_pc()

    def run_FPToSI(self):
        opnd, to_len = self.data(self.inst.opnd), self.inst.to_ty.length
        result = self.de.fptosi(opnd, to_len)

        self.write_back(result)
        self.inc_pc()

    def run_UIToFP(self):
        opnd, from_len = self.data(self.inst.opnd), self.inst.from_ty.length
        result = self.de.uitofp(opnd, from_len)

        self.write_back(result)
        self.inc_pc()

    def run_SIToFP(self):
        opnd, from_len = self.data(self.inst.opnd), self.inst.from_ty.length
        result = self.de.sitofp(opnd, from_len)

        self.write_back(result)
        self.inc_pc()

    def run_BitCast(self):
        # NOTE: Python does not support the 32-bit float type.
        # Only support conversion between Int(64) and Double.

        from_ty, to_ty = self.inst.from_ty, self.inst.to_ty
        opnd = self.data(self.inst.opnd)

        if (t.type_equals(from_ty, int64_t) and
                t.type_equals(to_ty, double_t)):
            result = self.de.bitcast_i64tof(opnd)
        elif (t.type_equals(from_ty, double_t) and
                t.type_equals(to_ty, int64_t)):
            result = self.de.bitcast_ftoi64(opnd)
        else:
            raise self._error_with_status(NotImplementedError, lambda sm:
                    "BitCast between {} and {} is not supported. "
                    "Only support conversion between Int(64) and Double)"
                    .format(from_ty, to_ty))

        self.write_back(result)
        self.inc_pc()
    
    def run_RefCast(self):
        self._run_Noop()    # NO-OP. RefCast is static

    def _run_TagRef64TypeCheck(self, defunc):
        opnd = self.data(self.inst.opnd)
        result = defunc(opnd)

        self.write_back(result)
        self.inc_pc()

    def run_IsInt(self):
        self._run_TagRef64TypeCheck(self.de.is_int)

    def run_IsFP(self):
        self._run_TagRef64TypeCheck(self.de.is_fp)

    def run_IsRef(self):
        self._run_TagRef64TypeCheck(self.de.is_ref)

    def _run_TagRef64Wrapping(self, defunc):
        opnd = self.data(self.inst.opnd)
        result = defunc(opnd)

        self.write_back(result)
        self.inc_pc()

    def run_IntToTR64(self):
        self._run_TagRef64Wrapping(self.de.int_to_tr64)

    def run_FPToTR64(self):
        self._run_TagRef64Wrapping(self.de.fp_to_tr64)

    def run_RefToTR64(self):
        opnd = self.data(self.inst.opnd)
        tag = self.data(self.inst.tag) if self.inst.tag != None else 0
        result = self.de.ref_to_tr64(opnd, tag)

        self.write_back(result)
        self.inc_pc()

    def _run_TagRef64Unwrapping(self, defunc):
        opnd = self.data(self.inst.opnd)
        result = defunc(opnd)

        self.write_back(result)
        self.inc_pc()

    def run_TR64ToInt(self):
        self._run_TagRef64Unwrapping(self.de.tr64_to_int)

    def run_TR64ToFP(self):
        self._run_TagRef64Unwrapping(self.de.tr64_to_fp)

    def run_TR64ToRef(self):
        self._run_TagRef64Unwrapping(self.de.tr64_to_ref)

    def run_TR64ToTag(self):
        self._run_TagRef64Unwrapping(self.de.tr64_to_tag)

    def run_Branch(self):
        self.branch_and_phis(self.inst.label)
        
    def run_Branch2(self):
        cond = self.data(self.inst.cond)
        next_bb = self.inst.if_true if cond == 1 else self.inst.if_false
        self.branch_and_phis(next_bb)

    def run_Switch(self):
        val_data = self.data(self.inst.val)
        for k,v in self.inst.cases.items():
            k_data = self.data(k)
            if val_data == k_data:
                dst = v
                break
        else:
            dst = self.inst.def_target

        self.branch_and_phis(dst)
        
    def run_Phi(self):
        raise self._error_with_status(NotImplementedError, lambda sm:
                "Phi nodes are not supposed to be reached in "
                "normal execution flow.")

    def run_Call(self):
        new_snp_id = self.data(self.inst.target)
        new_snp = self.itpr.id_to_snippet(new_snp_id, lazy_load=True)
        args_data = [self.data(v) for v in self.inst.args]
        new_frame = Frame.for_snippet(new_snp, args_data, self.stack)
        new_frame.prev = self.top
        self.push_pc()
        self.top = new_frame
        self.jump(PC(new_snp.entry, 0))

    def run_Invoke(self):
        new_snp_id = self.data(self.inst.target)
        new_snp = self.itpr.id_to_snippet(new_snp_id, lazy_load=True)
        args_data = [self.data(v) for v in self.inst.args]
        new_frame = Frame.for_snippet(new_snp, args_data, self.stack)
        new_frame.prev = self.top
        self.push_pc()
        self.top = new_frame
        self.jump(PC(new_snp.entry, 0))

    def run_TailCall(self):
        new_snp_id = self.data(self.inst.target)
        new_snp = self.itpr.id_to_snippet(new_snp_id, lazy_load=True)
        args_data = [self.data(v) for v in self.inst.args]
        new_frame = Frame.for_snippet(new_snp, args_data, self.stack)
        new_frame.prev = self.top.prev
        self.top = new_frame
        self.jump(PC(new_snp.entry, 0))

    def run_Ret(self):
        ret_val = self.data(self.inst.val)

        if self.top.prev == None:
            raise self._error_with_status(InterpreterRuntimeException,
                    lambda sm: "Returning from the bottom frame {}, "
                    "which is an undefined behaviour of the MicroVM."
                    .format(self.top))
       
        self.top = self.top.prev
        self.pop_pc()
        self.write_back(ret_val)

        if isinstance(self.inst, s.Call):
            self.inc_pc()
        elif isinstance(self.inst, s.Invoke):
            self.branch_and_phis(self.inst.normal)
        else:
            raise self._error_with_status(InterpreterBug,
                    lambda sm: "Unexpected calling instruction {}."
                    .format(type(self.inst).__name__))

    def run_Throw(self):
        exc_data = self.data(self.inst.val)

        while True:
            if self.top.prev == None:
                raise self._error_with_status(InterpreterRuntimeException,
                        lambda sm: "Exception thrown from the bottom frame {}, "
                        "which is an undefined behaviour of the MicroVM."
                        .format(frame))
            self.top = self.top.prev
            self.pop_pc()

            if isinstance(self.inst, s.Invoke):
                break

        landing_bb = self.inst.exceptional

        self.branch_and_phis(landing_bb, exc_data)

    def run_SwapStack(self):
        raise NotImplementedError("SwapStack")

    def run_ExtractValue(self):
        agg = self.data(self.inst.agg)
        result = self.de.extract_value(self.inst.field_indices, agg)

        self.write_back(result)
        self.inc_pc()

    def run_InsertValue(self):
        agg = self.data(self.inst.agg)
        val = self.data(self.inst.val)
        result = self.de.insert_value(self.inst.field_indices, agg, val)

        self.write_back(result)
        self.inc_pc()

    def run_Alloc(self):
        addr = self.de.alloc(self.inst.td, self.inst.aid)
        self.write_back(addr)
        self.inc_pc()

    def run_AllocHybridArray(self):
        length = self.data(self.inst.length)
        addr = self.de.allochybridarray(self.inst.td, length, self.inst.aid)
        self.write_back(addr)
        self.inc_pc()

    def run_GetField(self):
        addr = self.data(self.inst.obj)
        index = (self.data(self.inst.index) if self.inst.index is not None
                else None)

        result = self.de.getfield(self.inst.obj_ty, self.inst.field_ty,
                self.inst.field_indices, self.inst.part, index, addr)

        self.write_back(result)
        self.inc_pc()

    def run_SetField(self):
        addr = self.data(self.inst.obj)
        data = self.data(self.inst.val)
        index = (self.data(self.inst.index) if self.inst.index is not None
                else None)

        self.de.setfield(self.inst.obj_ty, self.inst.field_ty,
                self.inst.field_indices, self.inst.part, index, addr, data)

        self.inc_pc()

    def run_GetLength(self):
        obj_ty = self.inst.obj_ty
        addr = self.data(self.inst.obj)
        result = self.de.getlength(obj_ty, addr)

        self.write_back(result)
        self.inc_pc()

    def run_Trap(self):
        args = [self.data(v) for v in self.inst.args]
        self.inst.handler(self.thread, *args)

    def run_TrapCall(self):
        args = [self.data(v) for v in self.inst.args]
        result = self.inst.handler(self.thread, *args)

        self.write_back(result)
        self.inc_pc()

    def run_IntrinsicCall(self):
        args = [self.data(v) for v in self.inst.args]
        funcname = self.inst.funcname
        handler = getattr(self.de, "intrinsic_"+funcname, None)

        if handler == None:
            raise self._error_with_status(InterpreterBug, lambda sm:
                    "Unsupported intrinsic function '{}'".format(
                        funcname))

        result = handler(*args)

        self.write_back(result)
        self.inc_pc()

    def branch_and_phis(self, dst:s.BasicBlock, cur_exc:"addr:exception"=0):
        """
        Jump to a basic block and evaluate phi-nodes in the beginning of a
        basic block.
        """

        src = self.pc.bb
        self.jump(PC(dst, 0))

        while (isinstance(self.inst, s.Phi) or
                isinstance(self.inst, s.LandingPad)):
            if isinstance(self.inst, s.Phi):
                phi_value = self.inst.cases[src]
                phi_data = self.data(phi_value)
                self.write_back(phi_data)
            else:
                self.write_back(cur_exc)

            self.inc_pc()

    def _error_with_status(self, err_ty:Exception, msg_gen:"SnippetMarker->str"
            ) -> Exception :
        bb = self.pc.bb
        snp = bb.snippet
        inst = self.inst
        sm = s.SnippetMarker(snp)
        msg = "Snippet {}, BasicBlock {}, instruction {}: {}".format(
                snp.name or "(unnamed)", sm.bb_numbered(bb),
                sm.inst_numbered(inst), msg_gen(sm))
        return err_ty(msg)

class FunctionRecord(object):
    def __init__(self, sig:t.Signature, snp:s.Snippet=None):
        self.sig = sig
        self.snp = snp

class Interpreter(object):
    def __init__(self, mvm:mvm.MicroVM=None, de:de.DirectExecutor=None):
        if mvm is None and de is None:
            raise ValueError("At least one of mvm and de is required")

        self.mvm = mvm if mvm is not None else de.mvm
        self.de = (de if de is not None
                else directexecutor.DirectExecutor(self.mvm))

        self.recs = []

        self.undefined_snippet_call_handler = None

    def declare(self, sig:t.Signature) -> int:
        """
        Allocate a function handle (a.k.a. snippet ID) given a signature.
        """
        sid = len(self.recs)
        new_rec = FunctionRecord(sig)
        self.recs.append(new_rec)

        return sid

    def define(self, sid:int, snp:s.Snippet):
        """
        Bind a snippet ID to a snippet.

        This emulates the "compile" or "re-compile" step of compilers. A SID can
        be re-bound many times to different snippets.
        """

        rec = self.recs[sid]
        
        if not t.type_equals(rec.sig, snp.sig):
            raise InterpreterException("Signature mismatch. Expected {}, got {}"
                    .format(rec.sig, snp.sig))

        rec.snp = snp

    def id_to_snippet(self, sid:int, lazy_load:bool=False) -> "s.Snippet|None":
        """
        Return the snippet bound to the give Snippet ID, or None if the SID is
        unbound.

        If lazy_load is True, the undefined_snippet_call_handler will be called.

        Raise IndexError if this SID is invalid.
        """
        
        snp = self.recs[sid].snp

        if snp == None and lazy_load == True:
            self.on_undefined_snippet_call(sid, None)
            snp = self.recs[sid].snp
        
        return snp

    def on_undefined_snippet_call(self, sid:int, thread:Thread):
        """
        Called by the InterpreterThread when an undefine snippet is called. This
        method forwards the call to self.undefined_snippet_call_handler.

        If self.undefined_snippet_call_handler is None, an exception is raised.
        """

        if self.undefined_snippet_call_handler is None:
            raise InterpreterRuntimeException("An undefined SID {} is called, "
                    "but undefined_snippet_call_handler is not provided."
                    .format(sid))

        self.undefined_snippet_call_handler(sid, thread)

    def set_undefined_snippet_call_handler(self, handler:"(int,Thread)->None"):
        """
        Set the Undefined Snippet Call Handler, which will be called when an
        undefined snippet ID is called.

        The handler takes two parameters. The first is the SID. The second is
        the Thread object, or None if the thread does not yet exist because the
        snippet is looked up by the MicroVM.
        
        After handling, the MicroVM will assume the sid is bound and resume the
        thread and re-try calling the snippet.  If it is not desired, the
        handler should set the Thread's running flag to False to stop
        interpretation or raise a Python exception.

        TODO: If a non-None value is returned from the handler, it represents an
        exception (object address) to the user code.
        """
        self.undefined_snippet_call_handler = handler

    def run_snippet(self, sid:int, args:list) -> Thread:
        """
        Run the snippet given by its SID in a new thread and a new stack.

        Return the thread object created for this run.
        """
        snp = self.id_to_snippet(sid, lazy_load=True)

        stack = Stack.for_snippet(snp, args)
        thread = Thread(stack)
        thread.pc = PC(snp.entry, 0)
        self.run_thread(thread)

        return thread

    def run_thread(self, thread:Thread):
        """
        Continue running on the given thread.
        """
        interpreter_thread = InterpreterThread(self, thread)
        interpreter_thread.run()
