from . import (mtypes as t)
from .commontypes import *

INTRINSIC_FUNCS = {
        "pow": t.Signature([double_t, double_t], double_t),
        "floor": t.Signature([double_t], double_t),
        }
