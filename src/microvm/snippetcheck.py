from . import (mtypes as t, snippet as s, intrinsicfuncs)

from .utils.supertuple import SuperTuple
from .mconstants import *
from .commontypes import *

import itertools

BEGINNER_INSTS = [s.Phi, s.LandingPad]
ENDER_INSTS = [s.Branch, s.Branch2, s.Switch, s.Ret,
        s.Invoke, s.TailCall, s.Throw, s.Trap]
NONMID_INSTS = BEGINNER_INSTS + ENDER_INSTS

def check_snippet(snp):
    """
    Perform static checks over a snippet.

    It is recommended to run check_snippet after constructing a snippet to
    find static errors.
    """
    for bb in snp.basic_blocks:
        check_basic_block(snp, bb)

    check_entry_block(snp)

    for bb in snp.basic_blocks:
        for inst in bb.instructions:
            check_instruction(snp, bb, inst)


def check_basic_block(snp, bb):
    """
    Statically check the validity of a basic block
    """
    insts = bb.instructions
    if len(insts)==0:
        raise SnippetCheckError(snp, bb, msg="Basicblock must not be empty")

    ix = 0

    # A basic block should begin with many LandingPad and/or many phi-nodes.
    while ix < len(insts) and any(isinstance(insts[ix], ity)
            for ity in BEGINNER_INSTS):
        ix += 1

    while ix < len(insts) and not any(isinstance(insts[ix], ity)
            for ity in NONMID_INSTS):
        ix += 1

    if ix == len(insts):
        raise SnippetCheckError(snp, bb, msg=
                "Basicblock must end with one of {}".format(
                    ", ".join(ity.__name__ for ity in ENDER_INSTS)))
    elif any(isinstance(insts[ix], ity) for ity in BEGINNER_INSTS):
        raise SnippetCheckError(snp, bb, insts[ix], msg=
                "Instruction {} must appear at the beginning of a "
                "basicblock.".format(type(insts[ix]).__name__))
    elif any(isinstance(insts[ix], ity) for ity in ENDER_INSTS):
        if ix != len(insts)-1:
            raise SnippetCheckError(snp, bb, insts[ix], msg=
                    "Instruction {} must appear at the end of a "
                    "basicblock.".format(type(insts[ix]).__name__))

    last_inst = bb.instructions[-1]
    if isinstance(last_inst, s.Branch):
        check_phi_completeness(snp, bb, last_inst.label)
    elif isinstance(last_inst, s.Branch2):
        check_phi_completeness(snp, bb, last_inst.if_true)
        check_phi_completeness(snp, bb, last_inst.if_false)
    elif isinstance(last_inst, s.Invoke):
        check_phi_completeness(snp, bb, last_inst.normal)
        check_phi_completeness(snp, bb, last_inst.exceptional)

def check_entry_block(snp):
    bb = snp.entry
    for inst in bb.instructions:
        if any(isinstance(inst, ity) for ity in BEGINNER_INSTS):
            raise SnippetCheckError(snp, bb, inst, msg=
                    "Instruction {} must not appear in the entry block."
                    .format(type(inst).__name__))

def check_instruction(snp, bb, inst):
    auto_check_instruction(snp, bb, inst)

    if isinstance(inst, s.Constant):
        raise SnippetCheckError(snp, bb, inst, msg=
                "Constant should not be in a basic block")
    elif isinstance(inst, s.Parameter):
        raise SnippetCheckError(snp, bb, inst, msg=
                "Parameter should not be in a basic block")

    elif isinstance(inst, s.BinOp):
        if isinstance(inst.ty, t.Int):
            if inst.op not in s.INT_OPS:
                raise SnippetCheckError(snp, bb, inst, "op", msg=
                        "Integer operand type {} requires a "
                        "integer operation. Got op={}.".format(
                            inst.ty, inst.op))
        elif isinstance(inst.ty, t.FloatingPointType):
            if inst.op not in s.FP_OPS:
                raise SnippetCheckError(snp, bb, inst, "op", msg=
                        "Floating point operand type {} requires a "
                        "floating point operation. Got op={}.".format(
                            inst.ty, inst.op))
        else:
            raise SnippetCheckError(snp, bb, inst, field_name="ty", msg=
                    "Binary operation of type {} is not supported".format(
                        inst.ty))

    elif isinstance(inst, s.Cmp):
        if isinstance(inst.ty, t.Int):
            if inst.cond not in s.INT_CONDS:
                raise SnippetCheckError(snp, bb, inst, "cond", msg=
                        "Integer operand type {} requires a "
                        "integer condition. Got cond={}.".format(
                            inst.ty, inst.cond))
        elif isinstance(inst.ty, t.FloatingPointType):
            if inst.cond not in s.FP_CONDS:
                raise SnippetCheckError(snp, bb, inst, "cond", msg=
                        "Floating point operand type {} requires a "
                        "floating point condition. Got cond={}.".format(
                            inst.ty, inst.cond))
        elif isinstance(inst.ty, t.Ref):
            if inst.cond not in s.REF_CONDS:
                raise SnippetCheckError(snp, bb, inst, "cond", msg=
                        "Reference type {} requires a "
                        "reference condition. Got cond={}.".format(
                            inst.ty, inst.cond))
        elif isinstance(inst.ty, t.TagRef64):
            if inst.cond not in s.TAGREF64_CONDS:
                raise SnippetCheckError(snp, bb, inst, "cond", msg=
                        "TagRef64 type {} requires a "
                        "tagged reference condition. Got cond={}.".format(
                            inst.ty, inst.cond))
        else:
            raise SnippetCheckError(snp, bb, inst, field_name="ty", msg=
                    "Comparison of type {} is not supported".format(
                        inst.ty))


    elif isinstance(inst, s.Select):
        pass # Automatic checking is sufficient

    elif isinstance(inst, s.Trunc):
        if not (inst.to_len < inst.from_len):
            raise SnippetCheckError(snp, bb, inst, msg=
                    "to_len ({}) should be less than from_len ({})"
                    .format(inst.to_len, inst.from_len))

    elif isinstance(inst, s.ZExt) or isinstance(inst, s.SExt):
        if not (inst.to_len > inst.from_len):
            raise SnippetCheckError(snp, bb, inst, msg=
                    "to_len ({}) should be greater than from_len ({})"
                    .format(inst.to_len, inst.from_len))

    elif isinstance(inst, s.FPTrunc):
        if not (inst.to_ty.bitlength < inst.from_ty.bitlength):
            raise SnippetCheckError(snp, bb, inst, msg=
                    "to_ty ({}) should be shorter than from_ty ({})"
                    .format(inst.to_ty, inst.from_ty))

    elif isinstance(inst, s.FPExt):
        if not (inst.to_ty.bitlength > inst.from_ty.bitlength):
            raise SnippetCheckError(snp, bb, inst, msg=
                    "to_ty ({}) should be longer than from_ty ({})"
                    .format(inst.to_ty, inst.from_ty))

    elif (isinstance(inst, s.FPToUI) or isinstance(inst, s.FPToSI) or
            isinstance(inst, s.UIToFP) or isinstance(inst, s.SIToFP)):
        pass # Automatic checking is sufficient

    elif isinstance(inst, s.BitCast):
        if not inst.from_ty.bitlength == inst.to_ty.bitlength:
            raise SnippetCheckError(snp, bb, inst, msg=
                    "to_ty ({}) should have the same length as from_ty ({})"
                    .format(inst.to_ty, inst.from_ty))
                    
    elif isinstance(inst, s.RefCast):
        if not (t.is_prefix(inst.from_ty, inst.to_ty) or
                t.is_prefix(inst.to_ty, inst.from_ty)):
            raise SnippetCheckError(snp, bb, inst, msg=
                    "either from_ty {} or to_ty {} must be a prefix of the "
                    "other.".format(inst.from_ty, inst.to_ty))

    elif (isinstance(inst, s.TagRef64TypeCheck) or
            isinstance(inst, s.TagRef64Wrapping) or
            isinstance(inst, s.TagRef64Unwrapping)):
        pass # Automatic checking is sufficient

    elif isinstance(inst, s.Branch):
        pass # Automatic checking is sufficient

    elif isinstance(inst, s.Branch2):
        pass # Automatic checking is sufficient

    elif isinstance(inst, s.Switch):
        pass # Automatic checking is sufficient

    elif isinstance(inst, s.Phi):
        pass # Automatic checking is sufficient

    elif (isinstance(inst, s.AbstractCall)
            or isinstance(inst, s.Trap)
            or isinstance(inst, s.TrapCall)
            or isinstance(inst, s.IntrinsicCall)):

        # Check intrinsic function name validity
        if isinstance(inst, s.IntrinsicCall):
            if not inst.funcname in intrinsicfuncs.INTRINSIC_FUNCS:
                raise ValueError("Unknown intrinsic function '{}'".format(
                    inst.funcname))

        # Get parameter types
        if isinstance(inst, s.IntrinsicCall):
            param_ty = intrinsicfuncs.INTRINSIC_FUNCS[inst.funcname].param_ty
        else:
            param_ty = inst.sig.param_ty

        args_ty = [a.value_type for a in inst.args]

        # Check argument list length
        if len(param_ty) != len(args_ty):
            raise SnippetCheckError(snp, bb, inst, "len(args)", msg=
                    "Argument number mismatch. Expected {}, got {}"
                    .format(len(param_ty), len(args_ty)))

        # Check argument types
        for ix,(pty,aty) in enumerate(zip(param_ty, args_ty)):
            if not t.type_equals(aty, pty):
                field_name = "args[{}]".format(ix)
                raise SnippetCheckError(snp, bb, inst, field_name, msg=
                        "Argument type {} does match the parameter type {}"
                        .format(aty, pty))

        # Check TailCall return value type
        if isinstance(inst, s.TailCall):
            if not t.type_equals(inst.sig.ret_ty, snp.sig.ret_ty):
                raise SnippetCheckError(snp, bb, inst, "ty.ret_ty", msg=
                        "Callee's return type {} does not match the "
                        "current snippet's return type {}.".format(
                        inst.sig.ret_ty, snp.sig.ret_ty))

    elif isinstance(inst, s.Ret):
        if not t.type_equals(inst.ty, snp.sig.ret_ty):
            raise SnippetCheckError(snp, bb, inst, "ty", msg=
                    "Return type ty={} does not match the current "
                    "snippet's return type {}".format(inst.ty, snp.sig.ret_ty))

    elif isinstance(inst, s.Throw):
        pass # Automatic checking is sufficient
    elif isinstance(inst, s.LandingPad):
        pass # LandingPad has no parameters and is always valid

    elif isinstance(inst, s.SwapStack):
        pass # Not implemented

    elif isinstance(inst, s.Alloc):
        if isinstance(inst.td.ty, t.HybridArray):
            raise SnippetCheckError(snp, bb, inst, "td.ty", msg=
                    "The Alloc instruction cannot allocate HybridArray. Use "
                    "AllocHybridArray, instead.")

    elif isinstance(inst, s.AllocHybridArray):
        if not isinstance(inst.td.ty, t.HybridArray):
            raise SnippetCheckError(snp, bb, inst, "td.ty", msg=
                    "The AllocHybridArray instruction can only allocate "
                    "HybridArray. Got {}. Use Alloc for fixed types."
                    .format(inst.td.ty))
                
    elif (isinstance(inst, s.AggregateOperation) or
            isinstance(inst, s.FieldOperation)):

        # Check "part" and "index"
        if isinstance(inst, s.FieldOperation):
            if isinstance(inst.obj_ty, t.HybridArray):
                if inst.part not in (t.FIXED_PART, t.VAR_PART):
                    raise SnippetCheckError(snp, bb, inst, "part", msg=
                            "When getting/setting a field of a HybridArray, "
                            "the 'part' parameter must be t.FIXED_PART or "
                            "t.VAR_PART. Got {}.".format(inst.part))
                if inst.part == t.VAR_PART and inst.index is None:
                    raise SnippetCheckError(snp, bb, inst, "index", msg=
                            "When getting/setting the VAR_PART of a "
                            "HybridArray, the 'index' parameter must not be "
                            "None.")

        if isinstance(inst, s.AggregateOperation):
            target_ty = inst.agg_ty
        else:
            if isinstance(inst.obj_ty, t.HybridArray):
                if inst.part == t.FIXED_PART:
                    target_ty = inst.obj_ty.fixed_ty
                else:
                    target_ty = inst.obj_ty.var_ty
            else:
                target_ty = inst.obj_ty

        # Check field expression
        field_actual_ty = check_field_indices(snp, bb, inst,
                "field_indices", inst.field_indices, target_ty)

        if not t.type_equals(field_actual_ty, inst.field_ty):
            raise SnippetCheckError(snp, bb, inst, "field_indices", msg=
                    "field's actual type {} does not match field_ty {}"
                    .format(field_actual_ty, inst.field_ty))


    elif isinstance(inst, s.GetLength):
        pass # Automatic checking is sufficient

    elif isinstance(inst, s.GetInternalRef):
        pass # Not implemented
    elif isinstance(inst, s.SetInternalRef):
        pass # Not implemented
    elif isinstance(inst, s.Fence):
        pass # Not implemented
    elif isinstance(inst, s.CmpXchg):
        pass # Not implemented
    elif isinstance(inst, s.AtomicRMW):
        pass # Not implemented
    else:
        raise TypeError("Unknown instruction " + type(inst).__name__)

def check_phi_completeness(snp, from_bb, to_bb):
    for inst in to_bb.instructions:
        if isinstance(inst, s.Phi):
            if from_bb not in inst.cases:
                raise SnippetCheckError(snp, to_bb, inst, "cases", msg=
                        "Phi node {} does not contain a case from "
                        "basicblock {}".format(inst.name, from_bb.name))

def check_field_indices(snp, bb, inst,
        field_name:str, field_indices:[int], obj_ty:t.Type):
    """
    Check if a list of field indices is valid for a type (Struct or atom).
    Return the actual type of the field.
    """

    cur_ty = obj_ty
    for ixix, ix in enumerate(field_indices):
        if isinstance(cur_ty, t.Struct):
            if not (0<= ix < len(cur_ty.fields)):
                raise SnippetCheckError(snp, bb, inst, field_name, msg=
                        "{} has type {}, which has only {} fields. "
                        "The next ({}-th) element of the field_indices "
                        "is {}, which is out of the range."
                        .format(field_name, cur_ty,
                            len(cur_ty.fields), ixix, ix))

            cur_ty = cur_ty.fields[ix]
            field_name = "{}[{}]".format(field_name, ix)

        else:
            raise SnippetCheckError(snp, bb, inst, field_name, msg=
                    "{} has type {}, which does not have any fields or "
                    "elements. The next ({}-th) element of the "
                    "field_indices is {}, which is redundant."
                    .format(field_name, cur_ty, ixix, ix))

    return cur_ty

def auto_check_instruction(snp, bb, inst):
    inst_type = type(inst)
    fields = inst.__fields__

    for field_name, field_obj in fields.items():
        match_lhs = getattr(inst, field_name)
        match_rhs = field_obj.ty

        _match(snp, bb, inst, field_name, match_lhs, match_rhs)

def _match(snp, bb, inst, field_name, match_lhs, match_rhs):
    if isinstance(match_rhs, type):
        if not isinstance(match_lhs, match_rhs):
            raise FieldMatchError(snp, bb, inst, field_name,
                    "Expected instance of {}. Got {}.".format(
                        match_rhs.__name__, type(match_lhs).__name__))

    elif isinstance(match_rhs, s.EnumFrom):
        if not match_lhs in match_rhs.choices:
            raise FieldMatchError(snp, bb, inst, field_name,
                    "Expected one of {}. Got {}.".format(
                        ", ".join(match_rhs.choices), match_lhs))

    elif isinstance(match_rhs, s.ListOf):
        if not isinstance(match_lhs, list):
            raise FieldMatchError(snp, bb, inst, field_name,
                    "Expected a list. Got {}.".format(
                        type(match_lhs).__name__))

        match_rhs_inner = match_rhs.ty

        for ix, match_lhs_inner in enumerate(match_lhs):
            new_field_name = "{}[{}]".format(field_name, ix)
            _match(snp, bb, inst, new_field_name, match_lhs_inner,
                    match_rhs_inner)

    elif isinstance(match_rhs, s.DictOf):
        if not isinstance(match_lhs, dict):
            raise FieldMatchError(snp, bb, inst, field_name,
                    "Expected a dict. Got {}.".format(
                        type(match_lhs).__name__))

        match_rhs_key = match_rhs.key_ty
        match_rhs_value = match_rhs.value_ty

        for match_lhs_key, match_lhs_value in match_lhs.items():
            new_field_name_k = "{}{{{}: _}}".format(field_name, 
                    match_lhs_key)
            _match(snp, bb, inst, new_field_name_k, match_lhs_key,
                    match_rhs_key)

            new_field_name_v = "{}{{_: {}}}".format(field_name,
                    match_lhs_value)
            _match(snp, bb, inst, new_field_name_v, match_lhs_value,
                    match_rhs_value)

    elif isinstance(match_rhs, s.Optional):
        if match_lhs is not None:
            match_rhs_new = match_rhs.ty
            _match(snp, bb, inst, field_name, match_lhs, match_rhs_new)

    elif isinstance(match_rhs, s.ValueOf):
        if not isinstance(match_lhs, s.Value):
            raise FieldMatchError(snp, bb, inst, field_name,
                    "Expected SSA Value. Got {}.".format(
                        type(match_lhs).__name__))

        match_lhs_inner = match_lhs.value_type
        match_rhs_inner = match_rhs.ty
        new_field_name = "ValueOf({})".format(field_name)
        _match(snp, bb, inst, new_field_name, match_lhs_inner,
                match_rhs_inner)

    elif isinstance(match_rhs, str):
        if not hasattr(inst, match_rhs):
            raise FieldMatchError(snp, bb, inst, field_name,
                    "Referring to non-existing field {}".format(match_rhs))

        match_rhs_new = getattr(inst, match_rhs)
        _match(snp, bb, inst, field_name, match_lhs, match_rhs_new)

    elif isinstance(match_rhs, SuperTuple):
        match_rhs_type = type(match_rhs)
        if not isinstance(match_lhs, type(match_rhs)):
            raise FieldMatchError(snp, bb, inst, field_name,
                    "Expected instance of {}. Got {}.".format(
                        match_rhs_type.__name__, type(match_lhs).__name__))

        for rhs_field_name in match_rhs_type.__fields__:
            rhs_field_value = getattr(match_rhs, rhs_field_name)
            if rhs_field_value is not None:
                lhs_field_value = getattr(match_lhs, rhs_field_name)
                field_name_new = "{}.{}".format(field_name, rhs_field_name)
                _match(snp, bb, inst, field_name_new, lhs_field_value,
                        rhs_field_value)

    elif match_rhs == callable:
        if not callable(match_lhs):
            raise FieldMatchError(snp, bb, inst, field_name,
                    "Expected callable object. Got {}.".format(
                        type(match_lhs).__name__))

    else:
        if match_lhs != match_rhs:
            raise FieldMatchError(snp, bb, inst, field_name,
                    "Expected {}. Got {}.".format(match_rhs, match_lhs))
    
class SnippetCheckError(Exception):
    """ Thrown if a snippet is not statically well-formed. """

    def __init__(self, snp, bb=None, inst=None, field_name=None, msg=None):
        sm = s.SnippetMarker(snp)
        parts = [
                "snippet `{}'".format(snp.name or "(noname)"),
                "basicblock `{}'".format(
                    sm.bb_numbered(bb)) if bb!=None else None,
                "instruction `{}:{}'".format(
                    sm.inst_numbered(inst), type(inst).__name__)
                    if inst!=None else None,
                "field `{}'".format(field_name) if field_name!=None else None,
                ]

        parts_str = ", ".join([p for p in parts if p is not None])

        _msg = ("In {}: {}".format(parts_str, msg))
        Exception.__init__(self, _msg)

class FieldMatchError(SnippetCheckError):
    """ Thrown during automatic instruction checking. """

