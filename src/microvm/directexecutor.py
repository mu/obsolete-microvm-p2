"""
This module executes primitive MicroVM actions on a microvm instance.

====

About the Python Data Representation

Represent MicroVM data in Python data structures. This module is used for
encoding the values of MicroVM primitive types in Python and encoding such
values in the memory (bytearray or memory map).

In the DirectExecutor class, such Python values are used as operands and
return values of various methods including binop_*, alloc, getlength, etc..

In the interpreter module, such representation is used to store constant and
register values. It is often called "data" instead of "value" in order not to
be confused with SSA Values.

Mapping rules:
    MicroVM types       Python representation       Binary representation
    Int(N)          <-> int (limited to N bits) <-> 8,16,32,64-bit int
    Float           <-> float                   <-> IEEE754 float
    Double          <-> float                   <-> IEEE754 double
    Ref             <-> int (the address)       <-> address-size int
    WeakRef                             (not implemented)
    InternalRef     <->                 (not implemented)
    TagRef64        <-> int (64 bits)           <-> 64-bit int
    Struct          <-> [field0, field1, ...]   <-> read/written field-wise
    HybridArray         (not SSA)                   read/written element-wise
    Void            <-> None                    <-> (nothing)
    FuncHandle      <-> int (func id)           <-> word-size int (func id)
    Thread          <->                 (not implemented)
    Stack           <->                 (not implemented)
    Frame           <->                 (not implemented)

All binary data are in the native byte order.
"""

from . import (mtypes as t, microvm, layout)
import struct
from .mconstants import *
from .commontypes import *

import math

class DirectExecutorException(Exception):
    pass

# Masking functions

def _mask(num:int, nbits:int):
    " Mask an int to its lowset few bits. "
    return num & ((1<<nbits)-1)

def _uunmask(num:int, nbits:int):
    " Unmask an unsigned int to get its Python value. "
    return num

def _sunmask(num:int, nbits:int):
    " Unmask a signed int to get its Python value. "
    return num | ~(((num&(1<<(nbits-1)))<<1)-1)

# Binary operation wrapper
# Map the func to the category of n-bit integers.

def _iubinop(lhs:int, rhs:int, nbits:int, func:"(int,int)->int") -> int:
    " Integer unsigned binary operation. "
    return _mask(func(_uunmask(lhs,nbits),_uunmask(rhs,nbits)), nbits)

def _isbinop(lhs:int, rhs:int, nbits:int, func:"(int,int)->int") -> int:
    " Integer signed binary operation. "
    return _mask(func(_sunmask(lhs,nbits),_sunmask(rhs,nbits)), nbits)

def _iucmp(lhs:int, rhs:int, nbits:int, func:"(int,int)->bool") -> "0|1":
    " Integer unsigned comparison. "
    return 1 if func(_uunmask(lhs,nbits),_uunmask(rhs,nbits)) else 0

def _iscmp(lhs:int, rhs:int, nbits:int, func:"(int,int)->bool") -> "0|1":
    " Integer signed comparison. "
    return 1 if func(_sunmask(lhs,nbits),_sunmask(rhs,nbits)) else 0

# Memory buffer reader/writer

INT_FORMATS={8:"=B", 16:"=H", 32:"=L", 64:"=Q"}

def _int_format(nbits:int) -> str:
    try:
        return INT_FORMATS[nbits]
    except KeyError:
        raise DirectExecutorException("Memory read or write of {}-bit integer "
                "is not supported.".format(nbits))

def _read_int(buf:bytearray, off:int, nbits:int) -> int:
    " Read an n-bit integer from a buffer. "
    return struct.unpack_from(_int_format(nbits), buf, off)[0]
    
def _read_float(buf:bytearray, off:int) -> float:
    """
    Read a IEEE754 single precision FP number from a buffer.
    NOTE: Python does not have single-precision numbers. There may be potential
    information loss.
    """
    return struct.unpack_from("=f", buf, off)[0]

def _read_double(buf:bytearray, off:int) -> float:
    " Read a IEEE754 double precision FP number from a buffer. "
    return struct.unpack_from("=d", buf, off)[0]

def _write_int(buf:bytearray, off:int, val:int, nbits:int):
    " Write an n-bit integer into a buffer. "
    struct.pack_into(_int_format(nbits), buf, off, val)
    
def _write_float(buf:bytearray, off:int, val:float):
    """
    Write a IEEE754 single precision FP number into a buffer.
    NOTE: Python does not have single-precision numbers. There may be potential
    information loss.
    """
    struct.pack_into("=f", buf, off, val)

def _write_double(buf:bytearray, off:int, val:float):
    " Write a IEEE754 double precision FP number into a buffer. "
    struct.pack_into("=d", buf, off, val)

# Aggregate write

def _deep_copy_list(lst:list):
    """ Deep copy a nested list of lists. """
    if isinstance(lst, list):
        return [_deep_copy_list(x) for x in lst]
    else:
        return lst

def _insert_value_rec(sfe:"[int]", agg:object, val:object, level:int) -> object:
    """
    Create a new aggregagte data which is identical to agg except the field
    indicated by sfe which is replaced by val. level is the current level of
    recursion
    """
    if level == len(sfe):
        return val
    else:
        return [_insert_value_rec(sfe, elem, val, level+1)
                if ix == sfe[level] else _deep_copy_list(elem)
                for ix,elem in enumerate(agg)]

# Memory read-write

def _read_atom(buf:bytearray, off:int, field_ty:t.Type):
    " Read an atom type from the memory. The offset is already calculated. "
    if isinstance(field_ty, t.Int):
        return _read_int(buf, off, field_ty.length)
    elif isinstance(field_ty, t.Float):
        return _read_float(buf, off)
    elif isinstance(field_ty, t.Double):
        return _read_double(buf, off)
    elif isinstance(field_ty, t.Ref):
        return _read_int(buf, off, ADDRESS_SIZE_BITS)
    elif isinstance(field_ty, t.TagRef64):
        return _read_int(buf, off, 64)
    elif isinstance(field_ty, t.FuncHandle):
        return _read_int(buf, off, ADDRESS_SIZE_BITS)
    else:
        raise DirectExecutorException("Reading of type {} is not allowed."
                .format(field_ty))

def _write_field(buf:bytearray, off:int, field_ty:t.Type, val:object):
    " Write an atom type from the memory. The offset is already calculated. "
    if isinstance(field_ty, t.Int):
        _write_int(buf, off, val, field_ty.length)
    elif isinstance(field_ty, t.Float):
        _write_float(buf, off, val)
    elif isinstance(field_ty, t.Double):
        _write_double(buf, off, val)
    elif isinstance(field_ty, t.Ref):
        _write_int(buf, off, val, ADDRESS_SIZE_BITS)
    elif isinstance(field_ty, t.TagRef64):
        _write_int(buf, off, val, 64)
    elif isinstance(field_ty, t.FuncHandle):
        _write_int(buf, off, val, ADDRESS_SIZE_BITS)
    else:
        raise DirectExecutorException("Writing of type {} is not allowed."
                .format(field_ty))

# The DirectExecutor class

class DirectExecutor(object):
    def __init__(self, mvm):
        self.mvm = mvm

    # Binary operations

    @classmethod
    def binop_add(cls, lhs:int, rhs:int, nbits:int) -> int:
        return _iubinop(lhs, rhs, nbits, lambda x,y: x+y)
    @classmethod
    def binop_sub(cls, lhs:int, rhs:int, nbits:int) -> int:
        return _iubinop(lhs, rhs, nbits, lambda x,y: x-y)
    @classmethod
    def binop_mul(cls, lhs:int, rhs:int, nbits:int) -> int:
        return _iubinop(lhs, rhs, nbits, lambda x,y: x*y)
    @classmethod
    def binop_udiv(cls, lhs:int, rhs:int, nbits:int) -> int:
        return _iubinop(lhs, rhs, nbits, lambda x,y: x//y)
    @classmethod
    def binop_sdiv(cls, lhs:int, rhs:int, nbits:int) -> int:
        return _isbinop(lhs, rhs, nbits, lambda x,y: x//y)
    @classmethod
    def binop_urem(cls, lhs:int, rhs:int, nbits:int) -> int:
        return _iubinop(lhs, rhs, nbits, lambda x,y: x%y)
    @classmethod
    def binop_srem(cls, lhs:int, rhs:int, nbits:int) -> int:
        return _isbinop(lhs, rhs, nbits, lambda x,y: x%y)
    @classmethod
    def binop_shl(cls, lhs:int, rhs:int, nbits:int) -> int:
        return _iubinop(lhs, rhs, nbits, lambda x,y: x<<y)
    @classmethod
    def binop_lshr(cls, lhs:int, rhs:int, nbits:int) -> int:
        return _iubinop(lhs, rhs, nbits, lambda x,y: x>>y)
    @classmethod
    def binop_ashr(cls, lhs:int, rhs:int, nbits:int) -> int:
        return _isbinop(lhs, rhs, nbits, lambda x,y: x>>y)
    @classmethod
    def binop_and(cls, lhs:int, rhs:int, nbits:int) -> int:
        return _iubinop(lhs, rhs, nbits, lambda x,y: x&y)
    @classmethod
    def binop_or(cls, lhs:int, rhs:int, nbits:int) -> int:
        return _iubinop(lhs, rhs, nbits, lambda x,y: x|y)
    @classmethod
    def binop_xor(cls, lhs:int, rhs:int, nbits:int) -> int:
        return _iubinop(lhs, rhs, nbits, lambda x,y: x^y)

    @classmethod
    def binop_fadd(cls, lhs:float, rhs:float) -> float:
        return lhs+rhs
    @classmethod
    def binop_fsub(cls, lhs:float, rhs:float) -> float:
        return lhs-rhs
    @classmethod
    def binop_fmul(cls, lhs:float, rhs:float) -> float:
        return lhs*rhs
    @classmethod
    def binop_fdiv(cls, lhs:float, rhs:float) -> float:
        return lhs/rhs
    @classmethod
    def binop_frem(cls, lhs:float, rhs:float) -> float:
        return lhs%rhs

    # Comparison

    @classmethod
    def cmp_ieq(cls, lhs:int, rhs:int, nbits:int) -> "0|1":
        return _iucmp(lhs, rhs, nbits, lambda x,y: x==y)
    @classmethod
    def cmp_ine(cls, lhs:int, rhs:int, nbits:int) -> "0|1":
        return _iucmp(lhs, rhs, nbits, lambda x,y: x!=y)
    @classmethod
    def cmp_iugt(cls, lhs:int, rhs:int, nbits:int) -> "0|1":
        return _iucmp(lhs, rhs, nbits, lambda x,y: x>y)
    @classmethod
    def cmp_iuge(cls, lhs:int, rhs:int, nbits:int) -> "0|1":
        return _iucmp(lhs, rhs, nbits, lambda x,y: x>=y)
    @classmethod
    def cmp_iult(cls, lhs:int, rhs:int, nbits:int) -> "0|1":
        return _iucmp(lhs, rhs, nbits, lambda x,y: x<y)
    @classmethod
    def cmp_iule(cls, lhs:int, rhs:int, nbits:int) -> "0|1":
        return _iucmp(lhs, rhs, nbits, lambda x,y: x<=y)
    @classmethod
    def cmp_isgt(cls, lhs:int, rhs:int, nbits:int) -> "0|1":
        return _iscmp(lhs, rhs, nbits, lambda x,y: x>y)
    @classmethod
    def cmp_isge(cls, lhs:int, rhs:int, nbits:int) -> "0|1":
        return _iscmp(lhs, rhs, nbits, lambda x,y: x>=y)
    @classmethod
    def cmp_islt(cls, lhs:int, rhs:int, nbits:int) -> "0|1":
        return _iscmp(lhs, rhs, nbits, lambda x,y: x<y)
    @classmethod
    def cmp_isle(cls, lhs:int, rhs:int, nbits:int) -> "0|1":
        return _iscmp(lhs, rhs, nbits, lambda x,y: x<=y)

    # NOTE: we can't emulate native FP operations in Python

    @classmethod
    def cmp_ffalse(cls, lhs:float, rhs:float) -> "0|1":
        return 0
    @classmethod
    def cmp_foeq(cls, lhs:float, rhs:float) -> "0|1":
        return 1 if lhs==rhs else 0
    @classmethod
    def cmp_fogt(cls, lhs:float, rhs:float) -> "0|1":
        return 1 if lhs>rhs else 0
    @classmethod
    def cmp_foge(cls, lhs:float, rhs:float) -> "0|1":
        return 1 if lhs>=rhs else 0
    @classmethod
    def cmp_folt(cls, lhs:float, rhs:float) -> "0|1":
        return 1 if lhs<rhs else 0
    @classmethod
    def cmp_fole(cls, lhs:float, rhs:float) -> "0|1":
        return 1 if lhs<=rhs else 0
    @classmethod
    def cmp_fone(cls, lhs:float, rhs:float) -> "0|1":
        return 1 if lhs!=rhs else 0
    @classmethod
    def cmp_fueq(cls, lhs:float, rhs:float) -> "0|1":
        return 1 if lhs==rhs else 0
    @classmethod
    def cmp_fugt(cls, lhs:float, rhs:float) -> "0|1":
        return 1 if lhs>rhs else 0
    @classmethod
    def cmp_fuge(cls, lhs:float, rhs:float) -> "0|1":
        return 1 if lhs>=rhs else 0
    @classmethod
    def cmp_fult(cls, lhs:float, rhs:float) -> "0|1":
        return 1 if lhs<rhs else 0
    @classmethod
    def cmp_fule(cls, lhs:float, rhs:float) -> "0|1":
        return 1 if lhs<=rhs else 0
    @classmethod
    def cmp_fune(cls, lhs:float, rhs:float) -> "0|1":
        return 1 if lhs!=rhs else 0
    @classmethod
    def cmp_ftrue(cls, lhs:float, rhs:float) -> "0|1":
        return 1

    @classmethod
    def cmp_req(cls, lhs:int, rhs:int) -> "0|1":
        return 1 if lhs==rhs else 0
    @classmethod
    def cmp_rne(cls, lhs:int, rhs:int) -> "0|1":
        return 1 if lhs!=rhs else 0

    @classmethod
    def cmp_treq(cls, lhs:int, rhs:int) -> "0|1":
        return 1 if lhs==rhs else 0
    @classmethod
    def cmp_trne(cls, lhs:int, rhs:int) -> "0|1":
        return 1 if lhs!=rhs else 0

    # Conditional value

    @classmethod
    def select(cls, cond:"0|1", iftrue, iffalse):
        return iftrue if cond else iffalse

    # Bit conversion

    @classmethod
    def trunc(cls, opnd:int, from_len:int, to_len:int) -> int:
        return _mask(opnd, to_len)
    @classmethod
    def zext(cls, opnd:int, from_len:int, to_len:int) -> int:
        return _mask(_uunmask(opnd, from_len), to_len)
    @classmethod
    def sext(cls, opnd:int, from_len:int, to_len:int) -> int:
        return _mask(_sunmask(opnd, from_len), to_len)

    ## NOTE: FPTrunc and FPExt are no-ops

    @classmethod
    def fptoui(cls, opnd:float, to_len:int) -> int:
        return _mask(int(opnd), to_len)
    @classmethod
    def fptosi(cls, opnd:float, to_len:int) -> int:
        return _mask(int(opnd), to_len)
    @classmethod
    def uitofp(cls, opnd:int, from_len:int) -> float:
        return float(_uunmask(opnd, from_len))
    @classmethod
    def sitofp(cls, opnd:int, from_len:int) -> float:
        return float(_sunmask(opnd, from_len))

    @classmethod
    def bitcast_i64tof(cls, opnd:int) -> float:
        bit_repr = struct.pack("=Q", opnd)
        result = struct.unpack("=d", bit_repr)[0]
        return result

    @classmethod
    def bitcast_ftoi64(cls, opnd:float) -> int:
        bit_repr = struct.pack("=d", opnd)
        result = struct.unpack("=Q", bit_repr)[0]
        return result

    ## NOTE: RefCast is no-op

    # TagRef64 operations

    @classmethod
    def is_int(cls, opnd:int) -> "0|1":
        return opnd & 0x7ff0000000000001 == 0x7ff0000000000001
    @classmethod
    def is_fp(cls, opnd:int) -> "0|1":
        return (opnd & 0x7ff0000000000001 != 0x7ff0000000000001
                and opnd & 0x7ff0000000000003 != 0x7ff0000000000002)
    @classmethod
    def is_ref(cls, opnd:int) -> "0|1":
        return opnd & 0x7ff0000000000003 == 0x7ff0000000000002

    @classmethod
    def int_to_tr64(cls, opnd:int) -> int:
        """
        NOTE: opnd is a 52-bit integer.
        """
        return (0x7ff0000000000001 |            # exponent and last two bits
                ((opnd&0x7ffffffffffff)<<1) |   # last 51 bits, shifted
                ((opnd&0x8000000000000)<<12))   # the 51st bit

    @classmethod
    def fp_to_tr64(cls, opnd:float) -> int:
        bits = cls.bitcast_ftoi64(opnd)
        if (bits & 0x7ff0000000000000 == 0x7ff0000000000000 and
                bits & 0x000fffffffffffff != 0x0000000000000000):   # real NaN
            # use this "canonical" NaN to avoid conflicting with Ref and inf
            bits = bits & 0xfff8000000000000 | 0x0000000000000008
        return bits

    @classmethod
    def ref_to_tr64(cls, opnd:int, tag:int) -> int:
        return (0x7ff0000000000002 |            # exponent and last two bits
                (opnd&0x7ffffffffff8) |         # 44 bits (no last 3 bits)
                ((opnd&0x800000000000)<<16) |   # the 47th bit
                ((tag&0x3e)<<46) |              # first 5 bits of tag
                ((tag&0x1)<<2))                 # the last bit of tag

    @classmethod
    def tr64_to_int(cls, opnd:int) -> int:
        return (((opnd&0xffffffffffffe)>>1) |   # last 51 bits, shifted
                ((opnd&0x8000000000000000)>>12))# the 51st bit

    @classmethod
    def tr64_to_fp(cls, opnd:float) -> int:
        return cls.bitcast_i64tof(opnd)        # all valid Doubles are "as-is"

    @classmethod
    def tr64_to_ref(cls, opnd:int) -> int:
        return ((opnd&0x7ffffffffff8) |         # 44 bits (no last 3 bits)
                (((~(((opnd&0x8000000000000000)<<1)-1))>>17)
                    &0xffff800000000000))       # Mask the high bits

    @classmethod
    def tr64_to_tag(cls, opnd:int) -> int:
        return (((opnd&0x000f800000000000)>>46)|# high 5 bits
                ((opnd&0x4)>>2))                # the last bit

    # NOTE: Control flow instructions are not "executable"

    # Aggregate operations

    @classmethod
    def extract_value(cls, field_indices:"[int]", agg:object) -> object:
        try:
            cur_agg = agg
            for ix in field_indices:
                cur_agg = cur_agg[ix]
            return cur_agg
        except:
            raise DirectExecutorException("Attempting to extract {} from {}"
                    .format(sfe, agg))

    @classmethod
    def insert_value(cls, field_indices:"[int]", agg:object, val:object) -> object:
        try:
            return _insert_value_rec(field_indices, agg, val, 0)
        except:
            raise DirectExecutorException("Attempting to insert at {} into {}"
                    .format(sfe, agg))

    # Object operations

    def alloc(self, td:t.TypeDescriptor, aid:int=0) -> int:
        ty = td.ty
        size = layout.fixed_obj_size(ty)
        begin = self.mvm.obj_space.allocate(size)
        objref = begin + layout.OBJREF_FROM_BEGIN

        return objref
        
    def allochybridarray(self, td:t.TypeDescriptor, length, aid:int=0) -> int:
        ty = td.ty

        size = layout.hybridarray_obj_size(ty, length)
        length_offset = layout.length_offset(ty)

        begin = self.mvm.obj_space.allocate(size)
        objref = begin + layout.OBJREF_FROM_BEGIN

        _write_int(self.mvm.mem, objref+length_offset, length, WORD_SIZE_BITS)

        return objref

    def getfield(self, obj_ty:t.Type, field_ty:t.Type, field_indices:"[int]",
            part:"PART", index:int, obj:int):
        if isinstance(field_ty, t.Struct):
            return [self.getfield(obj_ty, subfield_ty, field_indices+[ix], part,
                        index, obj)
                for ix, subfield_ty in enumerate(field_ty.fields)]
        else:
            field_offset = layout.field_offset(obj_ty, field_indices, part,
                    index)
            return _read_atom(self.mvm.mem, obj+field_offset, field_ty)

    def setfield(self, obj_ty:t.Type, field_ty:t.Type, field_indices:"[int]",
            part:"PART", index:int, obj:int, val: object):
        if isinstance(field_ty, t.Struct):
            for ix, subfield_ty in enumerate(field_ty.fields):
                self.setfield(obj_ty, subfield_ty, field_indices+[ix], part,
                        index, obj, val[ix])
        else:
            field_offset = layout.field_offset(obj_ty, field_indices, part,
                    index)
            return _write_field(self.mvm.mem, obj+field_offset, field_ty, val)

    def getlength(self, obj_ty:t.Type, obj:int):
        return _read_int(self.mvm.mem, obj+layout.length_offset(obj_ty),
                WORD_SIZE_BITS)

    @classmethod
    def intrinsic_pow(self, x, y):
        return math.pow(x,y)

    @classmethod
    def intrinsic_floor(self, x):
        return math.floor(x)
