from .utils.supertuple import Field, MixIn, SuperTuple
from . import (mtypes as t, intrinsicfuncs)
from .mconstants import *
from .commontypes import *

from abc import abstractmethod

# If you change the instruction set, please also change snippetcheck

class Snippet():
    def __init__(self, sig:t.Signature, name:str=None):
        """
        sig     Snippet signature.
        name    Name of the snippet. For debug only. Not for name-based
                snippet/function resolution.
        """
        self.sig = sig
        self.params = [Parameter(ty=x) for i,x in enumerate(self.sig.param_ty)]
        self.entry = BasicBlock(self, "entry")
        self.basic_blocks = [self.entry]
        self.name = name

    def __repr__(self):
        return "<Snippet '{}' at 0x{:x}>".format(self.name, id(self))

class BasicBlock():
    def __init__(self, snp:Snippet, name:str=None):
        """
        snp     The snippet it belongs to.
        name    Name of the basicblock. For debug only.
        """
        self.snippet = snp
        self.instructions = []
        self.name = name

    def __repr__(self):
        return "<BasicBlock '{}' at 0x{:x}>".format(self.name, id(self))

# ==== About field annotations ====
# SSA values are instances of the Value class. It is a "SuperTuple" and its
# fields can be annotated to facilitate automatic static type checking.
#
# Fields are written in the form:
#   <field_name> = Field(<field_type>, <description>)
# 
# , where <field_type> is for automatic type checking
# and <description> is a human-readable description
#
# See doc/supertuple-and-field-grammar.rst for more information.
#
# The checking algorithm is in snippetinfo.SnippetInfo.auto_check_instruction
# =================================

# Type-checking symbols

class ValueOf(object):
    """
    Denote that the field has type Value (the Value type defined later in this
    module.  It is an SSA Value instead of a Python value.) which evaluates to
    a MicroVM type denoted by the ty parameter.
    """
    def __init__(self, ty):
        self.ty = ty

class EnumFrom(object):
    """ Choose from a list of values. """
    def __init__(self, choices):
        self.choices = choices

class Optional(object):
    """ An optional field. May be None. """
    def __init__(self, ty):
        self.ty = ty

class ListOf(object):
    """ A Python list in which each element is ty. """
    def __init__(self, ty):
        self.ty = ty

class DictOf(object):
    """ A Python dict in which each key is key_ty and each value is value_ty."""
    def __init__(self, key_ty, value_ty):
        self.key_ty = key_ty
        self.value_ty = value_ty

# Basic SSA Values

class Value(SuperTuple):
    __annotations__ = ["name"]  # A descriptive name. For debug.

    @property
    @abstractmethod
    def value_type(self):
        return

class Constant(Value):
    """
    A constant value. Usually literal.

    See the pydatarepr module for mapping rules between Python values and
    MicroVM types.
    """

    ty = Field(t.Type)
    data = Field(object, "Python value. Coerced to ty.")

    @property
    def value_type(self):
        return self.ty

class Parameter(Value):
    ty = Field(t.Type)

    @property
    def value_type(self):
        return self.ty

# Instructions

class Instruction(Value):
    pass

# Binary operations

INT_OPS = ["add", "sub", "mul", "udiv", "sdiv", "urem", "srem",
        "shl", "ashr", "lshr", "and", "or", "xor"]

FP_OPS = ["fadd", "fsub", "fmul", "fdiv", "frem"]

class BinOp(Instruction):
    ty = Field(t.Type)                      # operand and result type
    op = Field(EnumFrom(INT_OPS+FP_OPS))    # operator
    lhs = Field(ValueOf("ty"))              # left-hand-side
    rhs = Field(ValueOf("ty"))              # right-hand-side

    @property
    def value_type(self):
        return self.ty

INT_CONDS = ["eq", "ne", "ugt", "uge", "ult", "ule",
        "sgt", "sge", "slt", "sle"]

FP_CONDS = ["false", "oeq", "ogt", "oge", "olt", "ole", "one", "ord",
        "ueq", "ugt", "uge", "ult", "ule", "une", "uno", "true"]

REF_CONDS = ["req", "rne"]

TAGREF64_CONDS = ["treq", "trne"]

# Comparing

class Cmp(Instruction):
    ty = Field(t.Type)                      # operand type
    cond = Field(EnumFrom(INT_CONDS+
        FP_CONDS+REF_CONDS+TAGREF64_CONDS)) # condition
    lhs = Field(ValueOf("ty"))              # left-hand-side
    rhs = Field(ValueOf("ty"))              # right-hand-side

    @property
    def value_type(self):
        return int1_t
    
class Select(Instruction):
    ty = Field(t.Type)                      # result type
    cond = Field(ValueOf(int1_t))           # previous comparing result
    if_true = Field(ValueOf("ty"))          # value if cond is 1
    if_false = Field(ValueOf("ty"))         # value if cond is 0

    @property
    def value_type(self):
        return self.ty

# Simple type conversion

class Resize(Instruction):
    from_len = Field(int)                   # operand length
    to_len = Field(int)                     # result length
    opnd = Field(ValueOf(t.Int("from_len")))    # operand

    @property
    def value_type(self):
        return t.Int(self.to_len)

class Retype(Instruction):
    from_ty = Field(t.Type)                 # operand type
    to_ty = Field(t.Type)                   # result type
    opnd = Field(ValueOf("from_ty"))        # operand

    @property
    def value_type(self):
        return self.to_ty

class Trunc(Resize):
    pass
class ZExt(Resize):
    pass
class SExt(Resize):
    pass

class FPTrunc(Retype):
    pass
class FPExt(Retype):
    pass

class FPToUI(Retype):
    pass
class UIToFP(Retype):
    pass
class FPToSI(Retype):
    pass
class SIToFP(Retype):
    pass

class BitCast(Retype):
    pass

class RefCast(Instruction):
    from_ty = Field(t.Type)                     # operand referred type
    to_ty = Field(t.Type)                       # result referred type
    opnd = Field(ValueOf(t.Ref("from_ty")))

    @property
    def value_type(self):
        return t.Ref(self.to_ty)

# TagRef64 operations

class TagRef64TypeCheck(Instruction):
    opnd = Field(ValueOf(t.TagRef64))

    @property
    def value_type(self):
        return int1_t

class IsInt(TagRef64TypeCheck):
    pass

class IsFP(TagRef64TypeCheck):
    pass

class IsRef(TagRef64TypeCheck):
    pass

class TagRef64Wrapping(Instruction):
    ty = Field(t.Type)      # The referred type of the generated TagRef64

    @property
    def value_type(self):
        return t.TagRef64(self.ty)

class IntToTR64(TagRef64Wrapping):
    opnd = Field(ValueOf(int52_t))

class FPToTR64(TagRef64Wrapping):
    opnd = Field(ValueOf(double_t))

class RefToTR64(TagRef64Wrapping):
    opnd = Field(ValueOf(t.Ref("ty")))
    tag = Field(ValueOf(int6_t))

class TagRef64Unwrapping(Instruction):
    ty = Field(t.Type)      # The referred type of the generated TagRef64
    opnd = Field(ValueOf(t.TagRef64("ty")))     # The source TagRef64

class TR64ToInt(TagRef64Unwrapping):
    @property
    def value_type(self):
        return int52_t

class TR64ToFP(TagRef64Unwrapping):
    @property
    def value_type(self):
        return double_t

class TR64ToRef(TagRef64Unwrapping):
    @property
    def value_type(self):
        return t.Ref(self.ty)

class TR64ToTag(TagRef64Unwrapping):
    @property
    def value_type(self):
        return int6_t

# Intra-snippet flow control

class Branch(Instruction):
    """ Unconditional branching """
    label = Field(BasicBlock)                   # destination

    @property
    def value_type(self):
        return None

class Branch2(Instruction):
    """ Two-way conditional branching """
    cond = Field(ValueOf(int1_t))               # condition
    if_true = Field(BasicBlock)                 # destination for cond==1
    if_false = Field(BasicBlock)                # destination for cond==0

    @property
    def value_type(self):
        return None

class Switch(Instruction):
    ty = Field(t.Int)                           # operand type
    val = Field(ValueOf("ty"))                  # operand
    def_target = Field(BasicBlock)              # default target
    cases = Field(DictOf(Constant("ty"), BasicBlock))   # cases

    @property
    def value_type(self):
        return None

class Phi(Instruction):
    ty = Field(t.Type)                          # result type
    cases = Field(DictOf(BasicBlock, ValueOf("ty")))    # cases

    @property
    def value_type(self):
        return self.ty
    
# Inter-snippet flow control
class AbstractCall(Instruction):
    """
    Super class for both Call and TailCall
    """
    sig = Field(t.Signature)
    target = Field(ValueOf(t.FuncHandle("sig")))
    args = Field(ListOf(Value), "match each param_ty")

    @property
    def value_type(self):
        return self.sig.ret_ty

class Call(AbstractCall):
    """ Normal call. Not capturing exceptions. Not terminating instruction. """

class Invoke(AbstractCall):
    """ Normal call. Must capture exceptions. Terminating instruction. """
    normal = Field(BasicBlock)
    exceptional = Field(BasicBlock)

class TailCall(AbstractCall):
    """ Tail call. Must not capture exceptions. """

class Ret(Instruction):
    ty = Field(t.Type)
    val = Field(ValueOf("ty"))

    @property
    def value_type(self):
        return None

class Throw(Instruction):
    val = Field(ValueOf(t.Ref))

    @property
    def value_type(self):
        return None

class LandingPad(Instruction):
    @property
    def value_type(self):
        return void_ref_t

class SwapStack(Instruction):
    stack = Field(ValueOf(t.Stack))

    @property
    def value_type(self):
        raise NotImplementedError()

# Aggregate operations
class AggregateOperation(Instruction):
    agg_ty = Field(t.Type)
    field_ty = Field(t.Type)
    field_indices = Field(ListOf(int))
    agg = Field(ValueOf("agg_ty"))

class ExtractValue(AggregateOperation):
    @property
    def value_type(self):
        return self.field_ty

class InsertValue(AggregateOperation):
    val = Field(ValueOf("field_ty"))

    @property
    def value_type(self):
        return self.agg_ty

# Object operations

## Some memory-access criteria
ORDERING = ["unordered", "monotonic", "acquire", "release", "acq_rel",
        "seq_cst"]

class OrderingMixin(SuperTuple):
    ordering = Field(Optional(EnumFrom(ORDERING)))
    singlethread = Field(Optional(bool))

class VolatilityMixin(SuperTuple):
    volatile = Field(Optional(bool))

class Alloc(Instruction):
    td = Field(t.TypeDescriptor)
    aid = Field(int)

    @property
    def value_type(self):
        return t.Ref(self.td.ty)

class AllocHybridArray(Instruction):
    td = Field(t.TypeDescriptor)
    length = Field(ValueOf(word_t))
    aid = Field(int)

    @property
    def value_type(self):
        return t.Ref(self.td.ty)

class FieldOperation(Instruction):
    obj_ty = Field(t.Type)
    field_ty = Field(t.Type)
    field_indices = Field(ListOf(int))
    part = Field(Optional(EnumFrom([t.SCALAR, t.FIXED_PART, t.VAR_PART])))
    index = Field(Optional(ValueOf(word_t)))
    obj = Field(ValueOf(t.Ref("obj_ty")))

class GetField(FieldOperation):
    ordering_mixin = MixIn(OrderingMixin)
    volatility_mixin = MixIn(VolatilityMixin)

    @property
    def value_type(self):
        return self.field_ty

class SetField(FieldOperation):
    val = Field(ValueOf("field_ty"))

    ordering_mixin = MixIn(OrderingMixin)
    volatility_mixin = MixIn(VolatilityMixin)

    @property
    def value_type(self):
        return None

class GetLength(Instruction):
    obj_ty = Field(t.HybridArray)
    obj = Field(ValueOf(t.Ref("obj_ty")))

    ordering_mixin = MixIn(OrderingMixin)
    volatility_mixin = MixIn(VolatilityMixin)

    @property
    def value_type(self):
        return word_t

class GetInternalRef(Instruction):
    obj_ty = Field(t.Type)
    field_ty = Field(t.Type)
    field_expr = Field(ListOf(int))
    obj = Field(ValueOf(t.Ref("obj_ty")))

    ordering_mixin = MixIn(OrderingMixin)
    volatility_mixin = MixIn(VolatilityMixin)

    @property
    def value_type(self):
        raise NotImplementedError()

class SetInternalRef(Instruction):
    ty = Field(t.Type)
    int_ref = Field(ValueOf(t.InternalRef("ty")))
    val = Field(ValueOf("ty"))

    ordering_mixin = MixIn(OrderingMixin)
    volatility_mixin = MixIn(VolatilityMixin)

    @property
    def value_type(self):
        return NotImplementedError()

class Fence(Instruction):
    ordering_mixin = MixIn(OrderingMixin)

    @property
    def value_type(self):
        return NotImplementedError()

class CmpXchg(FieldOperation):
    old_val = Field(ValueOf("field_ty"))
    new_val = Field(ValueOf("field_ty"))

    ordering_mixin = MixIn(OrderingMixin)
    volatility_mixin = MixIn(VolatilityMixin)

    @property
    def value_type(self):
        return self.field_ty

ATOMICRMW_OPS = ["xchg", "add", "sub", "and", "nand", "or", "xor", "max", "min", "umax", "umin"]

class AtomicRMW(FieldOperation):
    op = Field(EnumFrom(ATOMICRMW_OPS))
    val = Field(ValueOf("field_ty"))

    ordering_mixin = MixIn(OrderingMixin)
    volatility_mixin = MixIn(VolatilityMixin)

    @property
    def value_type(self):
        return self.field_ty

class Trap(Instruction):
    sig = Field(t.Signature)
    handler = Field(callable)
    args = Field(ListOf(Value), "match each of ty.param_ty")

    @property
    def value_type(self):
        return None

class TrapCall(Instruction):
    sig = Field(t.Signature)
    handler = Field(callable)
    args = Field(ListOf(Value), "match each of ty.param_ty")

    @property
    def value_type(self):
        return self.sig.ret_ty

class IntrinsicCall(Instruction):
    funcname = Field(str)
    args = Field(ListOf(Value), "match the intrinsic function's param_ty")

    @property
    def value_type(self):
        return intrinsicfuncs.INTRINSIC_FUNCS[self.funcname].ret_ty

# Unified value field visiting and replacing
def itervalfields(inst:Instruction):
    """
    inst    The instruction to introspect.
    yields: For each field in inst which refers to another Value, yield a pair
            of (v, f) where v is the current value and f is a function which,
            when called with a new value, will replace the current value with
            the new value.

    This function provides a way to uniformly manipulate all kinds of SSA
    Values. It is useful when one instruction (or other values) is replaced by
    another because of various optimisation.
    """

    def isvaluefield(fty):
        return (isinstance(fty, type) and issubclass(fty, Value)
                or isinstance(fty, Value)
                or isinstance(fty, ValueOf))

    for field_name, field_obj in inst.__fields__.items():
        fty = field_obj.ty

        if isinstance(fty, Optional):
            fty = fty.ty

        if isvaluefield(fty):
            def replacer(newv):
                setattr(inst, field_name, newv)

            yield (getattr(inst, field_name), replacer)

        elif isinstance(fty, ListOf):
            ity = fty.ty

            if isvaluefield(ity):
                lst = getattr(inst, field_name)
                for ix,val in enumerate(lst):
                    def replacer(newv):
                        lst[ix] = newv
                    yield (val, replacer)
            
        elif isinstance(fty, DictOf):
            # keys are not replacable.
            # Neither Switch nor Phi has keys as SSA Values.

            vty = fty.value_ty

            if isvaluefield(vty):
                dic = getattr(inst, field_name)

                for key,val in dic.items():
                    def replacer(newv):
                        dic[key] = newv
                    yield (val, replacer)

# Pretty printing
class SnippetMarker():
    """
    Label a snippet and its component for pretty printing.
    """
    def __init__(self, snp):

        self.all_bbs = snp.basic_blocks
        self.bb2index = self._number(self.all_bbs)

        self.all_insts = [inst for bb in self.all_bbs
                for inst in bb.instructions]
        self.inst2index = self._number(self.all_insts)

        self.all_params = snp.params
        self.param2index = self._number(self.all_params)

    @staticmethod
    def _optname(n):
        if n is not None:
            return "({})".format(n)
        else:
            return ""

    @staticmethod
    def _number(seq):
        return {x:i for i,x in enumerate(seq)}

    def bb_numbered(self, bb):
        return "bb{}{}".format(self.bb2index[bb], self._optname(bb.name))

    def inst_numbered(self, inst):
        return "inst{}{}".format(self.inst2index[inst],
                self._optname(inst.name))

    def param_numbered(self, param):
        return "param{}{}".format(self.param2index[param],
                self._optname(param.name))

    def value_numbered(self, value):
        if isinstance(value, Constant):
            return "Const[{}]({})".format(pptype(fv.ty), data)
        elif isinstance(value, Parameter):
            return self.param_numbered(value)
        else:
            return self.inst_numbered(value)

def ppsnippet(snp, pptype_custom=None, ppconst_custom=None):
    """
    Pretty print a snippet.

    pptype and ppconst are called on types and constants. They are customisable
    so that specific programming languages clients can override them to print
    more language-aware strings.
    """

    def pptype(ty):
        return t.pptype(ty, pptype_custom)

    def ppconst(fv):
        if ppconst_custom is not None:
            custom_result = ppconst_custom(fv)
            if custom_result is not None:
                return custom_result

        if isinstance(fv.ty, t.FuncHandle):
            data = "Snippet({})".format(fv)
        elif isinstance(fv.ty, t.TagRef64):
            from . import directexecutor
            if directexecutor.DirectExecutor.is_int(fv.data):
                data = "TagRef64[Int]({})".format(
                        directexecutor.DirectExecutor.tr64_to_int(fv.data))
            elif directexecutor.DirectExecutor.is_fp(fv.data):
                data = "TagRef64[FP]({})".format(
                        directexecutor.DirectExecutor.tr64_to_fp(fv.data))
            else:
                data = "TagRef64[Ref]({}, {})".format(
                        directexecutor.DirectExecutor.tr64_to_ref(fv.data),
                        directexecutor.DirectExecutor.tr64_to_tag(fv.data))

        else:
            data = fv.data
        const_str = "Const[{}]({})".format(pptype(fv.ty), data)
        return const_str

    def ppfieldvalue(fv):
        if isinstance(fv, t.Type):
            return pptype(fv)
        elif isinstance(fv, Constant):
            return ppconst(fv)
        elif isinstance(fv, Parameter):
            return sm.param_numbered(fv)
        elif isinstance(fv, Instruction):
            return sm.inst_numbered(fv)
        elif isinstance(fv, BasicBlock):
            return sm.bb_numbered(fv)
        elif isinstance(fv, t.TypeDescriptor):
            return "TD({})".format(fv.ty)
        elif isinstance(fv, list):
            return "[{}]".format(", ".join(map(ppfieldvalue, fv)))
        elif isinstance(fv, dict):
            return "{{{}}}".format(", ".join(
                "{}: {}".format(ppfieldvalue(k), ppfieldvalue(v))
                for k,v in fv.items()))
        else:
            return str(fv)

    def ppfields(inst):
        return ", ".join("{}={}".format(fn, ppfieldvalue(getattr(inst, fn)))
            for fn in inst.__fields__
            if fn != "name" and getattr(inst, fn) != None)

    sm = SnippetMarker(snp)

    lines = []

    lines.append("Snippet `{}' :: {}".format(snp.name or "(noname)", snp.sig))
    lines.append("")

    for bb in sm.all_bbs:
        lines.append("  {}:".format(sm.bb_numbered(bb)))

        for inst in bb.instructions:
            lines.append("    {} = {}({})".format(sm.inst_numbered(inst),
                type(inst).__name__, ppfields(inst)))

        lines.append("")

    return "\n".join(lines)

