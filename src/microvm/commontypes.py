"""
Pre-defined common types. It is supposed to save typing (both noun and verb).
"""
from . import mtypes as t
from .mconstants import *

void_t = t.Void()
int1_t = t.Int(1)
int8_t = t.Int(8)
int16_t = t.Int(16)
int32_t = t.Int(32)
int64_t = t.Int(64)
float_t = t.Float()
double_t = t.Double()

word_t = t.Int(WORD_SIZE_BITS)
void_ref_t = t.Ref(void_t)
void_tagref64_t = t.TagRef64(void_t)

int52_t = t.Int(52) # Int payload of TagRef64
int6_t = t.Int(6)   # Int tag with Ref payload of TagRef64

