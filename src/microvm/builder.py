from . import snippet as s

class Builder(object):
    """
    Snippet builder - convenient tools to build snippets

    Stateful object. Do not share.
    """

    def __init__(self, sig, name):
        self.snp = s.Snippet(sig, name)
        self.cur_bb = self.snp.entry

    def new_bb(self, name):
        """
        Create a new basic block and add it to the current snippet.
        """

        bb = s.BasicBlock(self.snp, name)
        self.snp.basic_blocks.append(bb)
        return bb

    def use(self, bb):
        """
        Move the cursor to a new basic-block.

        This method cannot move to another snippet. You should create a new
        Builder for each snippet to build.
        """
        self.cur_bb = bb

    def inst(self, inst_ty, *args, **kwargs):
        """
        Create an SSA value and append it to the current basic block.
        """
        new_inst = inst_ty(*args, **kwargs)
        self.cur_bb.instructions.append(new_inst)
        return new_inst

    def __getattr__(self, name):
        if hasattr(s, name):
            inst_ty = getattr(s, name)
            def wrapped_inst(*args, **kwargs):
                return self.inst(inst_ty, *args, **kwargs)
            return wrapped_inst
        else:
            raise AttributeError("Builder does not have attribute '{}'"
                    .format(name))

