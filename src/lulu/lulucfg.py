"""
The Control-Flow Graph.

This is a high-level abstraction over a Lua program. It describes every action
the program takes (e.g. adding, subtracting, jumping, loading/storing local or
global variables, loading constants, etc.), but knows nothing about types (not
even constants which are just "number", "string" or "constant (nil, false and
true).

The CFG is in SSA form, but variables are loaded and stored.

The types will be inferred by the annotator. The type of values may not be LUA
values, but may be primitive "magic" MicroVM values as well.
"""

from collections import namedtuple

class CFG(object):
    """
    A CFG object represents the CFG of a Lua Function.

    A lvar (or local variable) is the storage location of a local variable. It
    is local to a Lua function. They are operated using LoadLocal and
    StoreLocal instructions. Before translating to real SSA form, all LoadLocal
    and StoreLocal instructions and any usage of lvars need to be eliminated
    and replaced with phi-nodes.
    """
    def __init__(self, name):
        self.name = name
        self.params = []

        # Informative only
        self.lvarnames = []

        self.entry = BasicBlock(self, "entry")
        self.basicblocks = [self.entry] # Entry is always the first basicblock

        self.nested_cfgs = []   # Nested functions defined in the current func

        self.constants = set()  # A set of all constants used in this CFG

    @property
    def nparams(self):
        return len(self.params)

    @property
    def nlvars(self):
        return len(self.lvarnames)

    def add_param(self):
        param_id = self.nparams
        param = Parameter(param_id)
        self.params.append(param)
        return param

    def add_lvar(self, name):
        lvarid = self.nlvars
        self.lvarnames.append(name)
        return lvarid

class BasicBlock(object):
    """
    A Basic Block of the CFG
    """
    def __init__(self, cfg, name):
        self.cfg = cfg
        self.name = name
        self.instructions=[]

# "Instructions".

class Value(object):
    __fields__=[]
    __annotations__=[
            "type_anno",    # Set in RLua type annotating phase.
            "value_anno",   # Set in RLua and Lua annotating phase.
            ]

    def __init__(self, *args, **kwargs):
        if len(args) != len(self.__fields__):
            raise ValueError("Value {} takes {} parameters, but {} supplied"
                    .format(type(self).__name__, len(self.__fields__),
                        len(args)))
        for f,a in zip(self.__fields__, args):
            setattr(self, f, a)

        for a in self.__annotations__:
            setattr(self, a, None)

        for k,v in kwargs.items():
            if k not in self.__annotations__:
                raise ValueError("Value {} does not have annotation {}"
                        .format(type(self).__name__, k))
            setattr(self, k, v)

## Constants

class Constant(Value): pass

class NilConst(Constant):       __fields__=[]
class BooleanConst(Constant):   __fields__=["val"]  # val is bool
class NumberConst(Constant):    __fields__=["val"]  # val is float
class StringConst(Constant):    __fields__=["val"]  # val is str
class FunctionConst(Constant):  __fields__=["val"]  # val is CFG

## Parameters

class Parameter(Value):         __fields__=["index"]    # index is Python int

## Instructions

class Instruction(Value): pass

## BinOP/UnOp

class BinOp(Instruction):       __fields__=["lhs", "rhs"] # both Value

class Add(BinOp): pass
class Sub(BinOp): pass
class Mul(BinOp): pass
class Div(BinOp): pass
class Mod(BinOp): pass
class Pow(BinOp): pass
class Concat(BinOp): pass
class Eq(BinOp): pass
class Ne(BinOp): pass
class Lt(BinOp): pass
class Le(BinOp): pass

class UnOp(Instruction):        __fields__=["opnd"] # opnd is Value

class Unm(UnOp): pass
class Len(UnOp): pass
class Not(UnOp): pass

## Jumps

class Br(Instruction):          __fields__=["dst"]  # dst is BasicBlock
class Br2(Instruction):         __fields__=["cond", "dstt", "dstf"]
                                            # cond is Value
                                            # dstt and dstf are BasicBlock
class Phi(Instruction):        
                                __fields__=["cases"]
                                            # cases is {BasicBlock: Value}
                                __annotations__=["lvar"]
                                # lvar is the lvar this phi-node is placed for
                                # NOTE: all __annotations__ are informative only

## Calls

class Call(Instruction):        __fields__=["func", "args"]
                                            # func is Value
                                            # args is list of Value

class Ret(Instruction):         __fields__=["vals"]
                                            # vals is list of Value

class ExtractVA(Instruction):   __fields__=["va", "index"]
                                            # va is Call instruction
                                            # index is Python int

## Load/Store

# lvar is Python int; name, obj, key and val are Value.
class LoadLocal(Instruction):   __fields__=["lvar"]
class StoreLocal(Instruction):  __fields__=["lvar", "val"]

class LoadGlobal(Instruction):  __fields__=["name"]
class StoreGlobal(Instruction): __fields__=["name", "val"]

class LoadSubscr(Instruction):  __fields__=["obj", "key"]
class StoreSubscr(Instruction): __fields__=["obj", "key", "val"]

# Pretty printing

def ppcfg(cfg):
    lines = []
    
    lines.append("CFG `{}':".format(cfg.name))

    all_bbs = cfg.basicblocks
    bb_nums = {bb:ix for ix,bb in enumerate(all_bbs)}

    def bb_numbered(bb):
        return "bb{}({})".format(bb_nums[bb], bb.name)

    all_insts = [inst for bb in cfg.basicblocks
            for inst in bb.instructions]
    inst_nums = {inst:ix for ix, inst in enumerate(all_insts)}

    def inst_numbered(inst):
        return "inst{}".format(inst_nums[inst])

    def ppvalue(val):
        if isinstance(val, Constant):
            if isinstance(val, NilConst): return "nil"
            elif isinstance(val, BooleanConst):
                return "true" if val.val else "false"
            elif isinstance(val, NumberConst):
                return "Num({})".format(val.val)
            elif isinstance(val, StringConst):
                return "Str({})".format(val.val)
            elif isinstance(val, FunctionConst):
                return "Func({})".format(val.val.name)
        elif isinstance(val, Parameter):
            return "Param({})".format(val.index)
        elif isinstance(val, BasicBlock):
            return bb_numbered(val)
        elif isinstance(val, Instruction):
            return inst_numbered(val)
        elif isinstance(val, list):
            return "[{}]".format(", ".join(ppvalue(x) for x in val))
        elif isinstance(val, dict):
            return "{{{}}}".format(", ".join(
                "{}:{}".format(ppvalue(k),ppvalue(v))
                for k,v in val.items()))
        else:
            return str(val)

    def ppfields(inst):
        return ", ".join(
                ["{}={}".format(fn, ppvalue(getattr(inst, fn)))
                for fn in inst.__fields__] + 
                    ["{}={}".format(an, ppvalue(getattr(inst, an)))
                    for an in inst.__annotations__ if getattr(inst, an) != None]
                    )

    lines.append("  constants:")
    for const in cfg.constants:
        lines.append("    {}".format(ppvalue(const)))

    lines.append("  lvars:")
    for ix, lvarname in enumerate(cfg.lvarnames):
        lines.append("    {}: {}".format(ix, lvarname))

    for bb in cfg.basicblocks:
        lines.append("  {}:".format(bb_numbered(bb)))

        for inst in bb.instructions:
            lines.append("    {} = {}({})".format(inst_numbered(inst),
                type(inst).__name__, ppfields(inst)))

    return "\n".join(lines)

# Helper functions

def get_cfgs_recursive(cfg):
    """
    cfg     The CFG
    yields: The cfg and its nested CFGs.
    """
    yield cfg
    for cfg2 in cfg.nested_cfgs:
        yield from get_cfgs_recursive(cfg2)

def itervalfields(inst):
    """
    inst    The instruction to introspect.
    yields: For each reference in any field of this instruction (nested or not),
            yield the value along with a "replacer" function which, when called,
            replaces the current value with a new value.

    Iterate over all fields which may refer to other objects so that the values
    may be replaced regardless of the instruction "format" (whether a reference
    to a Value is nested in a nested field (like Phi) or is just a field).
    """
    if isinstance(inst, Phi):
        for k,v in inst.cases.items():
            def replacer(newv):
                phi.cases[k] = newv
            yield (v, replacer)
    else:
        for k in inst.__fields__:
            v = getattr(inst, k)
            if isinstance(v, list):
                for i,vv in enumerate(v):
                    def replacer(newv):
                        v[i] = newv
                    yield (vv, replacer)
                    
            else:
                def replacer(newv):
                    setattr(inst, k, newv)
                yield (v, replacer)

