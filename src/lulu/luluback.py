"""
LuLu backend.

This translates LuLu CFG to MicroVM IR. It is required that the LuLu CFG is
already in the SSA form and is type-annotated.
"""

import os
_DEBUG = (os.environ.get("LULUBACK_DEBUG", None) in ("on", "yes", "1"))

from . import lulucfg as c
from .luluconstants import *

from microvm import builder

class PlaceHolder(object):
    def __init__(self, luluinst):
        self.luluinst = luluinst

class LuLuBack(object):
    def __init__(self, lc):
        self.lc = lc
        self.de = lc.de
        self.itpr = lc.itpr
        self.mvm = lc.mvm

        self.cfg_to_sid = {}
        self.cfg_to_funcgr = {} # CFG -> GlobalReference(f_r)

    def translate_recursive(self, cfg) -> "int:addr":
        """
        cfg     The CFG
        return: The address to the function_t object, which contains the SID of
                the CFG.

        Translate the cfg and its nested cfgs to MicroVM IR.
        """
        build_list = []

        for cur_cfg in c.get_cfgs_recursive(cfg):
            name = cur_cfg.name
            sid = self.itpr.declare(luafunc_sig)
            self.cfg_to_sid[cur_cfg] = sid
            build_list.append((cur_cfg, name, sid))

            func_addr = self.de.alloc(self.lc.function_td)
            self.lc._lua_function_set_mvm_func(func_addr, sid)
            func_gr = self.mvm.make_global_ref(func_addr)
            self.cfg_to_funcgr[cur_cfg] = func_gr
    
        for cur_cfg, name, sid in build_list:
            b = builder.Builder(luafunc_sig, name)
            self._translate(cur_cfg, b)
            self.itpr.define(sid, b.snp)

        return self.cfg_to_funcgr[cfg].obj_addr

    def _translate(self, cfg, b:builder.Builder):
        """
        cfg     The CFG
        b       The MicroVM Builder for the CFG

        Translate one CFG to MicroVM IR
        """
        
        bbmap = {}      # lulucfg.BasicBlock -> snippet.BasicBlock
        valmap = {}   # lulucfg.Value -> snippet.Value

        # Map constants

        for lconst in cfg.constants:
            if isinstance(lconst, c.NilConst):
                mconst = s.Constant(lv_r, self.lc.make_nil())
            elif isinstance(lconst, c.BooleanConst):
                mconst = s.Constant(lv_r, self.lc.make_boolean(lconst.val))
            elif isinstance(lconst, c.NumberConst):
                mconst = s.Constant(lv_r, self.lc.make_number(lconst.val))
            elif isinstance(lconst, c.StringConst):
                mconst = s.Constant(lv_r, self.lc.make_string(lconst.val))
            elif isinstance(lconst, c.FunctionConst):
                func_addr = self.cfg_to_funcgr[lconst.val].obj_addr
                func_lv = self.de.ref_to_tr64(func_addr, LUA_TFUNCTION)
                mconst = s.Constant(lv_r, func_lv)

            valmap[lconst] = mconst

        # Map basic blocks

        bbmap[cfg.entry] = b.snp.entry
        for lbb in cfg.basicblocks[1:]:
            mbb = b.new_bb(lbb.name)
            bbmap[lbb] = mbb

        # Map parameters

        b.use(b.snp.entry)
        [mparams] = b.snp.params
        for ix, lparam in enumerate(cfg.params):
            mparam = self.call_prim(b, "params_get", mparams, constiw(ix))
            valmap[lparam] = mparam

        # Map instructions

        for lbb in cfg.basicblocks:
            mbb = bbmap[lbb]
            b.use(mbb)

            for linst in lbb.instructions:
                if isinstance(linst, c.BinOp):
                    lhs = PlaceHolder(linst.lhs)
                    rhs = PlaceHolder(linst.rhs)
                    minst = self.call_prim(b, self.OP_MAP[type(linst)], lhs,rhs)
                    valmap[linst] = minst
                elif isinstance(linst, c.UnOp):
                    opnd = PlaceHolder(linst.opnd)
                    minst = self.call_prim(b, self.OP_MAP[type(linst)], opnd)
                    valmap[linst] = minst
                elif isinstance(linst, c.Br):
                    b.Branch(bbmap[linst.dst])
                elif isinstance(linst, c.Br2):
                    cond = self.call_prim(b, "is_lua_true",
                            PlaceHolder(linst.cond))
                    b.Branch2(cond, bbmap[linst.dstt], bbmap[linst.dstf])
                elif isinstance(linst, c.Phi):
                    minst = b.Phi(lv_r, {
                        bbmap[lbb2]: PlaceHolder(lv)
                        for lbb2,lv in linst.cases.items()
                        })
                    valmap[linst] = minst
                elif isinstance(linst, c.Call):
                    params = self.params_pack(b, linst.args)
                    func = PlaceHolder(linst.func)
                    mvmfunc = self.call_prim(b, "lua_prepare_call", func)
                    minst = b.Call(luafunc_sig, mvmfunc, [params])
                    valmap[linst] = minst
                elif isinstance(linst, c.Ret):
                    params = self.params_pack(b, linst.vals)
                    b.Ret(p_r, params)
                elif isinstance(linst, c.ExtractVA):
                    minst = self.call_prim(b, "params_get",
                            PlaceHolder(linst.va), constiw(linst.index))
                    valmap[linst] = minst
                elif isinstance(linst, c.LoadLocal):
                    raise ValueError("CFG is not SSA")
                elif isinstance(linst, c.StoreLocal):
                    raise ValueError("CFG is not SSA")
                elif isinstance(linst, c.LoadGlobal):
                    minst = self.call_prim(b, "lua_index",
                            s.Constant(lv_r, self.lc.get_env()),
                            s.Constant(lv_r, self.lc.make_string(linst.name)))
                    valmap[linst] = minst
                elif isinstance(linst, c.StoreGlobal):
                    self.call_prim(b, "lua_newindex",
                            s.Constant(lv_r, self.lc.get_env()),
                            s.Constant(lv_r, self.lc.make_string(linst.name)),
                            PlaceHolder(linst.val))
                elif isinstance(linst, c.LoadSubscr):
                    minst = self.call_prim(b, "lua_index",
                            PlaceHolder(linst.obj),
                            PlaceHolder(linst.key))
                    valmap[linst] = minst
                elif isinstance(linst, c.StoreSubscr):
                    self.call_prim(b, "lua_newindex",
                            PlaceHolder(linst.obj),
                            PlaceHolder(linst.key),
                            PlaceHolder(linst.val))
        
        # Replace all place-holders

        for mbb in b.snp.basic_blocks:
            for minst in mbb.instructions:
                for v,f in s.itervalfields(minst):
                    if isinstance(v, PlaceHolder):
                        try:
                            f(valmap[v.luluinst])
                        except:
                            if _DEBUG:
                                sm = s.SnippetMarker(b.snp)
                                from luaclient import luabuilder
                                lspp = luabuilder.LuaSnippetPrettyPrinter(self.lc)
                                print(lspp.lua_aware_ppsnippet(b.snp))

                                print("inst: {}".format(sm.inst_numbered(minst)))
                                print("v={}".format(v))
                                print("v.luluinst={}".format(v.luluinst))
                            raise

        if _DEBUG:
            from luaclient import luabuilder
            lspp = luabuilder.LuaSnippetPrettyPrinter(self.lc)
            print(lspp.lua_aware_ppsnippet(b.snp))
        

    OP_MAP = {
            c.Add:      "lua_add",
            c.Sub:      "lua_sub",
            c.Mul:      "lua_mul",
            c.Div:      "lua_div",
            c.Mod:      "lua_mod",
            c.Pow:      "lua_pow",
            c.Concat:   "lua_concat",
            c.Eq:       "lua_eq",
            c.Ne:       "lua_ne",
            c.Lt:       "lua_lt",
            c.Le:       "lua_le",
            c.Unm:      "lua_unm",
            c.Len:      "lua_len",
            c.Not:      "lua_not",
            }
    
    def call_prim(self, b, name, *args) -> 's.Call':
        """
        name    The name of the primitive function
        args    The arguments to the primitive function
        return: A MicroVM Call instruction to the primitive function

        Call a primitive function.
        """
        sid, func_type = self.lc.prim_funcs[name]
        func_const = s.Constant(func_type, sid)
        return b.Call(func_type, func_const, list(args))

    def params_pack(self, b, largs:'[c.Value]') -> 's.Value of p_r':
        """
        largs   A list of Lua values.
        return: A param_r which contains the packed Lua values.

        Pack a list of Lua values into a param_r. Generates MicroVM code on the
        fly.
        """
        if len(largs) == 0:
            params = s.Constant(p_r, self.lc.make_params())
        else:
            last = largs[-1]
            if isinstance(last, c.Call):
                valen = b.GetLength(p_r, PlaceHolder(last))
                selems = len(largs) - 1
                nelems = b.binop("add", constiw(selems), valen)
                has_va = True
            else:
                selems = len(largs)
                nelems = constiw(selems)
                has_va = False

            params = b.AllocHybridArray(self.lc.params_td, nelems)

            for ix in range(selems):
                b.SetField(p_t, lv_r, [], t.VAR_PART, constiw(ix), params,
                        PlaceHolder(largs[ix]))
                
            if has_va:
                self.call_prim(b, "params_append", params, PlaceHolder(last),
                        constiw(selems))

        return params


