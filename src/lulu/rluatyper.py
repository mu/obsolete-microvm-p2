"""
The RLua CFG Typer

This module annotates the CFG with the type of each SSA Values.

The CFG is constructed to be typeless, but the types are inferred in a separate
pass, which is done by this module.

This module only works with RLua code. RLua is a static programming language and
it is easy to infer the types of variables. 
"""

from .luluconstants import *

from . import lulucfg as c, tossa

import os

_DEBUG = (
        os.environ.get("LULU_RLUA_TYPER_DEBUG", None)
        in ("on", "yes", "1"))

TYPE_ANNOS = {
        "nil": lv_r,
        "i1": int1_t,
        "i6": int6_t,
        "i52": int52_t,
        "word": word_t,
        "double": double_t,
        "string": string_r,
        "table": table_r,
        "hash_table": hash_table_r,
        "function": function_r,
        "luaval": lv_r,
        }
class ValueAnno(object):
    pass

class FixedValue(ValueAnno):
    def __init__(self, val):
        self.val = val

class VariableValue(ValueAnno):
    pass

class ForwardValue(ValueAnno):
    def __init__(self, inst):
        self.inst = inst

def annotate_rlua(cfg:c.CFG):
    """
    Annotate an RLua function.
    """

    # Preprocess. Annotate all used constants and parameters.

    for bb in cfg.basicblocks:
        for inst in bb.instructions:
            for v,_ in c.itervalfields(inst):
                if isinstance(inst, c.Constant):
                    if isinstance(inst, c.NilConst):
                        inst.type_anno  = "nil"
                        inst.value_anno = FixedValue(None)
                    elif isinstance(inst, c.FalseConst):
                        inst.type_anno  = "i52"
                        inst.value_anno = FixedValue(0)
                    elif isinstance(inst, c.TrueConst):
                        inst.type_anno  = "i52"
                        inst.value_anno = FixedValue(1)
                    elif isinstance(inst, c.NumberConst):
                        inst.type_anno  = "double"
                        inst.value_anno = FixedValue(inst.val)
                    elif isinstance(inst, c.StringConst):
                        inst.type_anno  = "string"
                        inst.value_anno = FixedValue(inst.val)
                    elif isinstance(inst, c.FunctionConst):
                        inst.type_anno  = "function"
                        inst.value_anno = FixedValue(inst.val)
                elif isinstance(inst, c.Parameter):
                    inst.type_anno  = "luaval"
                    inst.value_anno = VariableValue()

    # Annotate the CFG. Start from the entry.

    succ = tossa.cfg_to_succ(cfg)

    seq, _ = _dfs(succ)
    dfsorder = [0] * len(succ)
    for v,n in enumerate(seq):
        dfsorder[n] = v

    for bbnum in dfsorder:
        bb = seq[bbnum]

        for inst in bb.instructions:
            pass




    

        



    




