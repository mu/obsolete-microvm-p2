"""
LuLu frontend.

This translates text-formatted Lua program into LuLu CFG.
"""

import os
_DEBUG = (os.environ.get("LULUFRONT_DEBUG", None) in ("on", "yes", "1"))

from luaclient.thirdparty import lua52grammar, lua52precompiled, lrparsing

grammar = lua52grammar.Lua52Grammar
grammar.pre_compile_grammar(lua52precompiled.LUA52_PRECOMPILED)

from . import lulucfg as c

def is_token(pt, *ms):
    return isinstance(pt[0], lrparsing.Token) and pt[1] in ms

class Scope(object):
    def __init__(self, parent=None, breakto=None):
        self.var2lvar = {}       # str -> int: var name to lvar id
        self.parent=parent
        self.breakto = (breakto if breakto!=None else
                        parent.breakto if parent!=None else
                        None)
        self.level = 0 if parent == None else parent.level+1
    
    def search(self, var_name:str) -> 'int:lvar | None':
        cur_scope = self

        while cur_scope != None:
            if var_name in cur_scope.var2lvar:
                return cur_scope.var2lvar[var_name]
            else:
                cur_scope = cur_scope.parent

        return None

class CFGBuilder(object):
    def __init__(self, func_name):
        self.cfg = c.CFG(func_name)
        self.cur_bb = self.cfg.entry
    
    def new_bb(self, name):
        bb = c.BasicBlock(self.cfg, name)
        self.cfg.basicblocks.append(bb)
        return bb

    def use(self, bb):
        self.cur_bb = bb

    def const(self, const_ty, *args):
        the_const = const_ty(*args)
        self.cfg.constants.add(the_const)
        return the_const

    def inst(self, inst_ty, *args):
        the_inst = inst_ty(*args)
        self.cur_bb.instructions.append(the_inst)
        return the_inst

    def __getattr__(self, name):
        if hasattr(c, name):
            value_ty = getattr(c, name)
            if issubclass(value_ty, c.Constant):
                def gen(*args):
                    return self.const(value_ty, *args)
            else:
                def gen(*args):
                    return self.inst(value_ty, *args)
            return gen
        else:
            raise AttributeError("CFGBuilder does not have attribute {}".format(
                name))

class CompileError(Exception):
    " Thrown if the Lua program contains a static error. "

class LuLuBug(Exception):
    " Indicate unreachable code in this program. For debugging purposes. "

def pthandler(ptnode):
    """
    An annotation on a LuLuFront method. It will check the "pt" parameter and
    verify it is passed the correct parse tree node.
    """
    def decl(f):
        def wrapper(self, pt, b, scope, *args, **kwargs):
            if not isinstance(b, CFGBuilder):
                raise ValueError("Argument b has type {}, but CFGBuilder is "
                        "expected.".format(type(b).__name__))
            if not isinstance(scope, Scope):
                raise ValueError("Argument scope has type {}, but Scope is "
                        "expected.".format(type(scope).__name__))
            if pt[0] != ptnode:
                raise ValueError("Method {} handles parse tree node {}, but "
                        "node {} is supplied.".format(f.__name__, ptnode,
                            pt[0]))
            return f(self, pt, b, scope, *args, **kwargs)
        wrapper.__name__ = f.__name__
        return wrapper
    return decl

class LuLuFront(object):
    def __init__(self):
        pass

    # Public interface

    def build_from_code(self, code):
        lines = code.splitlines()

        if len(lines)>0 and lines[0].startswith("#!"):
            skip = len(lines[0])+1
        else:
            skip = 0

        lua_prog = "\n".join(lines[skip:])

        parse_tree = grammar.parse(lua_prog)

        if _DEBUG:
            print(grammar.repr_parse_tree(parse_tree))

        cfg = self.chunk(parse_tree)

        return cfg

    # Parse tree walking

    def dispatch(self, pt, b, scope):
        if isinstance(pt[0], lrparsing.Token):
            method_name = pt[0].name.split(".")[-1]
        else:
            method_name = pt[0].name

        method = getattr(self, method_name)
        return method(pt, b, scope)

    ## Tokens

    @pthandler(grammar.T.short_string)
    def short_string(self, pt, b, scope) -> c.StringConst:
        RE = lua52grammar.LUA_SHORT_STRING_RE
        ESC = lua52grammar.LUA_SHORT_STRING_ESC
        def lua_sub(match):
            data = match.group()
            c = data[1]
            result = ESC.get(c, c)
            if not callable(result):
                return result
            return result(data[1:])
        str_content = RE.sub(lua_sub, pt[1])[1:-1]
        return b.StringConst(str_content)

    @pthandler(grammar.T.long_string)
    def long_string(self, pt, b, scope) -> c.StringConst:
        marker_len = pt[1].find('[', 1) + 1
        if pt[1][marker_len] == '\n':
            str_content = pt[1][marker_len + 1:-marker_len]
        else:
            str_content = pt[1][marker_len:-marker_len]
        return b.StringConst(str_content)

    @pthandler(grammar.T.decimal_number)
    def decimal_number(self, pt, b, scope) -> c.NumberConst:
        num = float(pt[1])
        return b.NumberConst(num)

    @pthandler(grammar.T.hex_number)
    def hex_number(self, pt, b, scope) -> c.NumberConst:
        toks = pt[1].split("p")
        if len(toks) == 1:
            mult = 1.0
        else:
            mult = 2.0 ** int(toks[1])
        toks = toks[0].split(".")
        if len(toks) == 1:
            num = float(int(toks[0], 16)) * mult
        else:
            num = int(toks[0] + toks[1], 16) * mult / 16.0 ** len(toks[1])
        return b.NumberConst(num)

    @pthandler(grammar.T.var_args)
    def var_args(self, pt, b, scope):
        raise NotImplementedError(pt)

    ## Non-terminals

    @pthandler(grammar.string)
    def string(self, pt, b, scope) -> c.StringConst:
        return self.dispatch(pt[1], b, scope)

    @pthandler(grammar.number)
    def number(self, pt, b, scope) -> c.StringConst:
        return self.dispatch(pt[1], b, scope)

    @pthandler(grammar.variable_ref)
    def variable_ref(self, pt, b, scope) -> "LoadXxxxx":
        var_name = pt[1][1]

        var_lvar = scope.search(var_name)

        if var_lvar == None:
            var_val = b.LoadGlobal(var_name)
        else:
            var_val = b.LoadLocal(var_lvar)

        for next_vr in pt[2:]:
            if not is_token(next_vr, "."):
                next_var_name = next_vr[1]
                next_var_name_const = b.StringConst(next_var_name)
                var_val = b.LoadSubscr(var_val, next_var_name_const)

        return var_val

    @pthandler(grammar.subscript_exp)
    def subscript_exp(self, pt, b, scope) -> c.LoadSubscr:
        obj = self.prefix_exp(pt[1], b, scope, single_value=True)
        key = self.exp(pt[3], b, scope, single_value=True)
        return b.LoadSubscr(obj, key)

    @pthandler(grammar.var)
    def var(self, pt, b, scope) -> "LoadXxxxxx":
        return self.dispatch(pt[1], b, scope)

    # LValue is one of:
    #    | ("local", int)           A local variable
    #    | ("global", str)          A global variable
    #    | ("subscr", Value, Value) A subscription

    @pthandler(grammar.variable_ref)
    def variable_ref_lval(self, pt, b, scope) -> 'LValue':
        valid_vrs = [vr for vr in pt[1:] if not is_token(vr, ".")]

        if len(valid_vrs) == 1:
            var_name = valid_vrs[0][1]
            var_lvar = scope.search(var_name)

            if var_lvar == None:
                return ("global", var_name)
            else:
                return ("local", var_lvar)
        else:
            var_name = valid_vrs[0][1]

            var_lvar = scope.search(var_name)

            if var_lvar == None:
                var_val = b.LoadGlobal(var_name)
            else:
                var_val = b.LoadLocal(var_lvar)

            for next_vr in valid_vrs[1:-1]:
                next_var_name = next_vr[1]
                next_var_name_const = b.StringConst(next_var_name)
                var_val = b.LoadSubscr(var_val, next_var_name_const)

            last_name_const = b.StringConst(pt[-1][1])

            return ("subscr", var_val, last_name_const)

    @pthandler(grammar.subscript_exp)
    def subscript_exp_lval(self, pt, b, scope) -> 'LValue':
        obj = self.prefix_exp(pt[1], b, scope, single_value=True)
        sub = self.exp(pt[3], b, scope, single_value=True)
        return ("subscr", obj, sub)

    @pthandler(grammar.var)
    def var_lval(self, pt, b, scope) -> 'LValue':
        if pt[1][0] == grammar.variable_ref:
            return self.variable_ref_lval(pt[1], b, scope)
        elif pt[1][0] == grammar.subscript_exp:
            return self.subscript_exp_lval(pt[1], b, scope)
        else:
            raise LuLuBug("LValue: {}".format(repr(pt)))

    @pthandler(grammar.var_list)
    def var_list(self, pt, b, scope) -> '[LValue]':
        lvals = []
        for var in pt[1:]:
            if not is_token(var, ","):
                lval = self.var_lval(var, b, scope)
                lvals.append(lval)
        return lvals

    @pthandler(grammar.exp_list)
    def exp_list(self, pt, b, scope) -> '[c.Value]':
        vals = []
        valid_exps = [exp for exp in pt[1:] if not is_token(exp, ",")]
        for ix, exp in enumerate(valid_exps):
            val = self.exp(exp, b, scope, single_value=(ix!=len(exp)-1))
            vals.append(val)
        return vals

    @pthandler(grammar.field)
    def field(self, pt, b, scope):
        raise NotImplementedError(pt)

    @pthandler(grammar.table_constructor)
    def table_constructor(self, pt, b, scope):
        raise NotImplementedError(pt)

    @pthandler(grammar.function_args)
    def function_args(self, pt, b, scope) -> '[c.Value]':
        if is_token(pt[1], "("):
            if pt[2][0] == grammar.exp_list:
                return self.exp_list(pt[2], b, scope)
            else:
                return []
        else:
            return [self.dispatch(pt[1], b, scope)]

    @pthandler(grammar.function_call)
    def function_call(self, pt, b, scope, single_value:bool) ->'c.Call':
        """
        Make a function call. If single_value==True, it is instantly converted
        using an ExtractVA instruction.
        """
        if is_token(pt[2], ":"):
            raise NotImplementedError("Method call")
        else:
            func = self.prefix_exp(pt[1], b, scope, single_value=True)
            args = self.function_args(pt[2], b, scope)
            call = b.Call(func, args)

            if single_value:
                result = b.ExtractVA(call, 0)
            else:
                result = call

            return result

    @pthandler(grammar.block)
    def block(self, pt, b, scope) -> bool:
        "returns: True if it has a last_statement. False otherwise"
        for x in pt[1:]:
            if x[0] == grammar.statement:
                self.statement(x, b, scope)
            elif x[0] == grammar.last_statement:
                self.last_statement(x, b, scope)
                new_bb = b.new_bb("unreachable bb after block")
                b.use(new_bb)
                return True

        return False

    @pthandler(grammar.name_list)
    def name_list(self, pt, b, scope) -> "[str]":
        return [name[1] for name in pt[1:] if not is_token(name, ",")]

    @pthandler(grammar.parameter_list)
    def parameter_list(self, pt, b, scope) -> "[str]":
        params = []
        for p in pt[1:]:
            if p[0] == grammar.T.name:
                params.append(p[1])
            elif p[0] == grammar.T.var_args:
                raise NotImplementedError(p)

        return params

    @pthandler(grammar.function_body)
    def function_body(self, pt, b, scope, name=None) -> c.FunctionConst:
        if name == None:
            name = "<anon_lua_func>"

        params = self.parameter_list(pt[3], b, scope)

        new_builder = CFGBuilder(name)

        new_scope = Scope()
        for param_name in params:
            param_val = new_builder.cfg.add_param()
            param_lvar = new_builder.cfg.add_lvar(param_name)
            new_scope.var2lvar[param_name] = param_lvar
            param_lval = ("local", param_lvar)
            self.assign(param_lval, param_val, new_builder, new_scope)

        self.block(pt[5], new_builder, new_scope)

        # Guard in case the body does not return.
        new_builder.Ret([])

        cfg = new_builder.cfg
        if _DEBUG:
            print(c.ppcfg(cfg))

        b.cfg.nested_cfgs.append(cfg)

        return b.FunctionConst(cfg)

    @pthandler(grammar.anon_function)
    def anon_function(self, pt, b, scope) -> c.FunctionConst:
        return self.function_body(pt[2], b, scope)

    @pthandler(grammar.constant)
    def constant(self, pt, b, scope) -> "XxxxConst":
        if is_token(pt[1], "nil"):
            return b.NilConst()
        elif is_token(pt[1], "false"):
            return b.BooleanConst(False)
        elif is_token(pt[1], "true"):
            return b.BooleanConst(True)
        else:
            raise Bug("Unexpected token {}".format(pt))

    @pthandler(grammar.adjusted_exp)
    def adjusted_exp(self, pt, b, scope) -> c.Value:
        return self.exp(pt[2], b, scope, single_value=True)

    @pthandler(grammar.prefix_exp)
    def prefix_exp(self, pt, b, scope, single_value) -> c.Value:
        if pt[1][0] == grammar.function_call:
            return self.function_call(pt[1], b, scope, single_value)
        else:
            return self.dispatch(pt[1], b, scope)

    @pthandler(grammar.exp)
    def exp(self, pt, b, scope, single_value) -> c.Value:
        if len(pt) == 2:
            if pt[1][0] == grammar.prefix_exp:
                return self.prefix_exp(pt[1], b, scope, single_value)
            else:
                return self.dispatch(pt[1], b, scope)
        elif (len(pt) == 4 and pt[1][0] == grammar.exp and
                pt[3][0] == grammar.exp and
                isinstance(pt[2][0], lrparsing.Token)):
            op = pt[2][1]
            if op in ["and", "or"]:
                eval_rhs = b.new_bb("{}_eval_rhs".format(op))
                cont = b.new_bb("{}_cont".format(op))

                lhs = self.exp(pt[1], b, scope, single_value=True)
                lhs_from = b.cur_bb
                if op == "and":
                    b.Br2(lhs, eval_rhs, cont)
                else:
                    b.Br2(lhs, cont, eval_rhs)

                b.use(eval_rhs)
                rhs = self.exp(pt[3], b, scope, single_value=True)
                rhs_from = b.cur_bb
                b.Br(cont)

                b.use(cont)
                result = b.Phi({
                    lhs_from: lhs,
                    rhs_from: rhs,
                    })

                return result

            else:
                lhs = self.exp(pt[1], b, scope, single_value=True)
                rhs = self.exp(pt[3], b, scope, single_value=True)
                if op == "+":       return b.Add(lhs, rhs)
                elif op == "-":     return b.Sub(lhs, rhs)
                elif op == "*":     return b.Mul(lhs, rhs)
                elif op == "/":     return b.Div(lhs, rhs)
                elif op == "%":     return b.Mod(lhs, rhs)
                elif op == "^":     return b.Pow(lhs, rhs)
                elif op == "..":    return b.Concat(rhs, lhs)
                elif op == "==":    return b.Eq(lhs, rhs)
                elif op == "~=":    return b.Ne(lhs, rhs)
                elif op == "<":     return b.Lt(lhs, rhs)
                elif op == "<=":    return b.Le(lhs, rhs)
                elif op == ">":     return b.Lt(rhs, lhs)
                elif op == ">=":    return b.Le(rhs, lhs)
                else:
                    raise LuLuBug("Unexpected binop {}".format(op))
        elif (len(pt) == 3 and isinstance(pt[1][0], lrparsing.Token) and
                pt[2][0] == grammar.exp):
            opnd = self.exp(pt[2], b, scope, single_value=True)
            op = pt[1][1]
            if op == "-":       return b.Unm(opnd)
            elif op == "#":     return b.Len(opnd)
            elif op == "not":   return b.Not(opnd)
            else:
                raise LuLuBug("Unexpected unop {}".format(op))
        else:
            raise LuLuBug("Unexpected exp {}".format(pt))

    @pthandler(grammar.function_name)
    def function_name(self, pt, b, scope) -> 'LValue':
        lval = self.variable_ref_lval(pt[1], b, scope)

        if len(pt)==4:
            raise NotImplementedError("Method definition")

        return lval

    @pthandler(grammar.assign_st)
    def assign_st(self, pt, b, scope) -> None:
        lvals = self.var_list(pt[1], b, scope)
        rvals = self.exp_list(pt[3], b, scope)
        self.assign_many(lvals, rvals, b, scope)

    @pthandler(grammar.do_st)
    def do_st(self, pt, b, scope) -> None:
        nested_scope = Scope(scope)
        self.block(pt[2][2], b, nested_scope)

    @pthandler(grammar.while_st)
    def while_st(self, pt, b, scope) -> None:
        cond_pt, body_pt = pt[2], pt[4][2]
        
        while_head = b.new_bb("while_head")
        while_body = b.new_bb("while_body")
        while_exit = b.new_bb("while_exit")

        b.Br(while_head)

        b.use(while_head)

        cond = self.exp(cond_pt, b, scope, single_value=True)
        b.Br2(cond, while_body, while_exit)

        b.use(while_body)
        new_scope = Scope(scope, while_exit)
        self.block(body_pt, b, new_scope)
        b.Br(while_head)

        b.use(while_exit)
        return None

    @pthandler(grammar.repeat_st)
    def repeat_st(self, pt, b, scope) -> None:
        body_pt, cond_pt = pt[2][2], pt[4]

        repeat_body = b.new_bb("repeat_body")
        repeat_tail = b.new_bb("repeat_tail")
        repeat_exit = b.new_bb("repeat_exit")

        b.Br(repeat_body)

        b.use(repeat_body)
        new_scope = Scope(scope, repeat_exit)
        self.block(body_pt, b, new_scope)
        b.Br(repeat_tail)

        b.use(repeat_tail)
        cond = self.exp(cond_pt, b, scope, single_value=True)
        b.Br2(cond, repeat_exit, repeat_body)
        
        b.use(repeat_exit)
        return None

    @pthandler(grammar.if_st)
    def if_st(self, pt, b, scope) -> None:
        cond_body_pairs = [(pt[2], pt[4][2])]
        body_else = None

        for ix in range(5, len(pt), 4):
            if is_token(pt[ix], "elseif"):
                cond_body_pairs.append((pt[ix+1], pt[ix+3][2]))
            elif is_token(pt[ix], "else"):
                body_else = pt[ix+1][2]
                break
            elif is_token(pt[ix], "end"):
                break
            else:
                raise LuLuBug("Unexpected token {}".format(pt[ix]))

        branch_hbs = [(
            b.new_bb("if_case{}_head".format(ix)),
            b.new_bb("if_case{}_body".format(ix)),
            Scope(scope),
            ) for ix in range(len(cond_body_pairs))]

        if body_else != None:
            bb_else = b.new_bb("bb_else")
            else_scope = Scope(scope)

        if_exit = b.new_bb("if_exit")

        b.Br(branch_hbs[0][0])

        for ix, (cond_pt, body_pt) in enumerate(cond_body_pairs):
            bb_head, bb_body, nested_scope = branch_hbs[ix]

            b.use(bb_head)
            cond = self.exp(cond_pt, b, scope, single_value=True)

            if ix == len(cond_body_pairs)-1:
                if body_else != None:
                    fail_to = bb_else
                else:
                    fail_to = if_exit
            else:
                fail_to = branch_hbs[ix+1][0]

            b.Br2(cond, bb_body, fail_to)

            b.use(bb_body)
            self.block(body_pt, b, nested_scope)
            b.Br(if_exit)

        if body_else != None:
            b.use(bb_else)
            self.block(body_else, b, else_scope)
            b.Br(if_exit)

        b.use(if_exit)
        return None

    @pthandler(grammar.for_steps)
    def for_steps(self, pt, b, scope) -> "(str) + (c.Value)*3":
        name = pt[1][1]

        var = self.exp(pt[3], b, scope, single_value=True)
        limit = self.exp(pt[5], b, scope, single_value=True)
        if len(pt)==8:
            step = self.exp(pt[7], b, scope, single_value=True)
        else:
            step = b.NumberConst(1.0)

        return (name, var, limit, step)

    @pthandler(grammar.for_step_st)
    def for_step_st(self, pt, b, scope) -> None:
        for_steps_pt, body_pt = pt[3], pt[5]

        v_name, var, limit, step = self.for_steps(for_steps_pt, b, scope)
        
        for_head = b.new_bb("for_head")
        for_check_le = b.new_bb("for_check_le")
        for_check_ge = b.new_bb("for_check_ge")
        for_body = b.new_bb("for_body")
        for_next = b.new_bb("for_next")
        for_exit = b.new_bb("for_exit")

        prev_bb = b.cur_bb
        b.Br(for_head)

        b.use(for_head)
        var_phi = b.Phi({ prev_bb: var })

        step_is_pos = b.Lt(b.NumberConst(0.0), step)
        b.Br2(step_is_pos, for_check_le, for_check_ge)

        b.use(for_check_le)
        le_limit = b.Le(var_phi, limit)
        b.Br2(le_limit, for_body, for_exit)

        b.use(for_check_ge)
        ge_limit = b.Le(limit, var_phi)
        b.Br2(ge_limit, for_body, for_exit)

        b.use(for_body)
        new_scope = Scope(scope, for_exit)
        v_lvar = self.define_local(v_name, b, new_scope)
        self.assign(("local", v_lvar), var_phi, b, new_scope)
        self.block(body_pt, b, new_scope)
        b.Br(for_next)
        
        b.use(for_next)
        new_var = b.Add(var_phi, step)
        var_phi.cases[for_next] = new_var
        b.Br(for_head)

        b.use(for_exit)
        return None

    @pthandler(grammar.for_name_list)
    def for_name_list(self, pt, b, scope):
        raise NotImplementedError(pt)

    @pthandler(grammar.for_in_st)
    def for_in_st(self, pt, b, scope):
        raise NotImplementedError(pt)

    @pthandler(grammar.function_call_st)
    def function_call_st(self, pt, b, scope) -> None:
        self.function_call(pt[1], b, scope, single_value=False)

    @pthandler(grammar.function_decl_st)
    def function_decl_st(self, pt, b, scope) -> None:
        func = self.function_body(pt[3], b, scope)
        lval = self.function_name(pt[2], b, scope)
        self.assign(lval, func, b, scope)

    @pthandler(grammar.local_function_decl_st)
    def local_function_decl_st(self, pt, b, scope) -> None:
        func_name = pt[3][1]
        func = self.function_body(pt[4], b, scope, name="<lua_func>"+func_name)
        var_lvar = self.define_local(func_name, b, scope)
        lval = ("local", var_lvar)
        self.assign(lval, func, b, scope)

    @pthandler(grammar.local_assign_st)
    def local_assign_st(self, pt, b, scope) -> None:
        var_names = self.name_list(pt[2], b, scope)
        lvals = []
        for var_name in var_names:
            var_lvar = self.define_local(var_name, b, scope)
            lvals.append(("local", var_lvar))
        if len(pt)==5:
            rvals = self.exp_list(pt[4], b, scope)
        else:
            rvals = []
        self.assign_many(lvals, rvals, b, scope)

    @pthandler(grammar.statement)
    def statement(self, pt, b, scope) -> None:
        self.dispatch(pt[1], b, scope)

    @pthandler(grammar.return_st)
    def return_st(self, pt, b, scope) -> None:
        exps = self.exp_list(pt[2], b, scope)
        b.Ret(exps)

    @pthandler(grammar.break_st)
    def break_st(self, pt, b, scope) -> None:
        target = scope.breakto
        if target==None:
            raise CompileError("<break> at line {} not inside a loop".format(
                pt[1][3]))
        b.Br(target)

    @pthandler(grammar.last_statement)
    def last_statement(self, pt, b, scope) -> None:
        self.dispatch(pt[1], b, scope)

    def chunk(self, pt:"ParseTree") -> c.CFG:
        """
        Chunk is the top-level structure of a Lua program.
        """

        b = CFGBuilder("main")

        scope = Scope()
        self.block(pt[2], b, scope)

        # Guard in case the body does not return.
        b.Ret([])

        cfg = b.cfg

        if _DEBUG:
            print(c.ppcfg(cfg))

        return cfg

    # Helpers

    def assign_many(self, lvals:'[LValue]', rvals:'[Value]', b, scope) -> None:
        if len(lvals) < len(rvals):
            lvals_f = lvals
            rvals_f = rvals[:len(lvals)]
            va = False
        else:
            if len(rvals)==0 or not isinstance(rvals[-1], c.Call):
                lvals_f = lvals
                rvals_f = rvals[:len(lvals)]
                if len(lvals) > len(rvals):
                    rvals_f += [b.NilConst()]*(len(lvals)-len(rvals))
                va = False
            else:
                last = rvals[-1]
                nf = len(rvals)-1
                lvals_f = lvals[:nf]
                rvals_f = rvals[:nf]
                lvals_v = lvals[nf:]
                va = True

        for lval, rval in zip(lvals_f, rvals_f):
            self.assign(lval, rval, b, scope)
                
        if va:
            for ix, lval in enumerate(lvals_v):
                rval = b.ExtractVA(last, ix)
                self.assign(lval, rval, b, scope)

    def assign(self, lval:'LValue', rval:c.Value, b, scope) -> None:
        if lval[0] == "local":
            b.StoreLocal(lval[1], rval)
        elif lval[0] == "global":
            b.StoreGlobal(lval[1], rval)
        elif lval[0] == "subscr":
            b.StoreSubscr(lval[1], lval[2], rval)
        else:
            raise ValueError("Unexpected LValue {}".format(lval[0]))

    def define_local(self, var_name:str, b, scope) -> 'int:lvar':
        var_lvar = b.cfg.add_lvar(var_name)
        scope.var2lvar[var_name] = var_lvar
        return var_lvar
    
if __name__=='__main__':
    import sys
    if len(sys.argv) != 2:
        print(
"""USAGE: python -m lulu.lulufront <filename.lua>"
If <filename.lua> is -, then it is read from STDIN"
Use the Lua test suite for testing."
Available at http://www.lua.org/tests/5.2/""")
    else:
        lua_prog_fn = sys.argv[1]
        if lua_prog_fn == "-":
            lua_prog_raw = sys.stdin.read()
        else:
            with open(lua_prog_fn) as f:
                lua_prog_raw = f.read()

        lulufront = LuLuFront()
        cfg = lulufront.build_from_code(lua_prog_raw)
