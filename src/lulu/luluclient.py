"""
The LuLu Lua Client.

This is the MicroVM-facing part of the LuLu and the main module of the LuLu
project.
"""

import os
_DEBUG = (os.environ.get("LULUCLIENT_DEBUG", None) in ("on", "yes", "1"))

from .luluconstants import *

from . import lulucfg as c, lulufront, tossa, luluback

from luaclient import luaprimitives, luabuilder, luaclient

import collections

PrimFuncRec = collections.namedtuple("PrimFuncRec", ["sid", "sig"])
BuiltinFuncRec = collections.namedtuple("BuiltinFuncRec", ["sid", "func_gr"])

class LuLuUnhandledError(Exception):
    """
    Thrown if an unhandled error is thrown from a Lua program.
    """

class LuLuClient(luaclient.AbstractLuaClient):
    def __init__(self):
        super().__init__()

        # A LuaCompiler object to compile lua code
        self.front = lulufront.LuLuFront()
        self.back = luluback.LuLuBack(self)

    def build_lua(self, prog:str) -> "addr:f_r":
        """ Build a lua program to a snippet.  """

        cfg = self.front.build_from_code(prog)

        for cur_cfg in c.get_cfgs_recursive(cfg):
            if _DEBUG:
                print("BEFORE:")
                print(c.ppcfg(cur_cfg))

            tossa.cfg_to_ssa(cur_cfg)
            
            if _DEBUG:
                print("AFTER:")
                print(c.ppcfg(cur_cfg))

        func_addr = self.back.translate_recursive(cfg)

        return func_addr
    
    # Lua code execution
    def run_lua(self, prog:str):
        """ Load a Lua program from its text form and execute it.  """

        func = self.build_lua(prog)
        bottom_frame_func_sid = self.prim_funcs["bottom_frame_func"].sid

        th = self.itpr.run_snippet(bottom_frame_func_sid, [func])
        
if __name__=='__main__':
    import sys
    if len(sys.argv) != 2:
        print(
"""USAGE: python -m luaclient.luaclient <filename.lua>
If <filename.lua> is -, then it is read from STDIN
Use the Lua test suite for testing.
Available at http://www.lua.org/tests/5.2/""")
    else:
        lua_prog_fn = sys.argv[1]
        if lua_prog_fn == "-":
            lua_prog_raw = sys.stdin.read()
        else:
            with open(lua_prog_fn) as f:
                lua_prog_raw = f.read()

        #print("This is the program:", lua_prog_raw)

        lulu_client = LuLuClient()
        lulu_client.run_lua(lua_prog_raw)


