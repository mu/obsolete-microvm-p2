"""
This module is used for type annotation.
"""

from .luluconstants import *

class LuLuType(object):
    def mvmtype(self):
        raise NotImplementedError()

# Real Lua types

class LuaType(LuLuType):
    def mvmtype(self):
        return luaval_r

class NilType(LuLuType):
    def mvmtype(self):
        return 

class Int1Type(LuLuType):
    def mvmtype(self):
        return int1_t
    
class Int52Type(LuLuType):
    def mvmtype(self):
        return int52_t

class WordType(LuLuType):
    def mvmtype(self):
        return word_t

class DoubleType(LuLuType):
    def mvmtype(self):
        return double_t

class StringRef(LuLuType):
    def mvmtype(self):
        return string_r

class TableRef(LuLuType):
    def mvmtype(self):
        return table_r

class HashTableRef(LuLuType):
    def mvmtype(self):
        return hash_table_r

class FunctionRef(LuLuType):
    def mvmtype(self):
        return function_r

class FieldRefType(LuLuType):
    def __init__(self, basetype:LuLuType, fieldname:str):
        self.basetype=basetype
        self.fieldname=fieldname

    def mvmtype(self):
        return hash_table_r

