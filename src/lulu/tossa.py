"""
This module converts LoadLocal and StoreLocal to real SSA form.

The Load-Store to SSA algorithm is based on [1].

The dominance tree generation algorithm is based on [2]. A naive but simple
alternative [3] is also implemented to test its correctness.

Reference:

[1] Ron Cytron, Jeanne Ferrante, Barry K. Rosen, Mark N. Wegman, and F.
    Kenneth Zadeck. 1991. Efficiently computing static single assignment form
    and the control dependence graph. ACM Trans. Program. Lang. Syst. 13, 4
    (October 1991), 451-490. DOI=10.1145/115372.115320
    http://doi.acm.org/10.1145/115372.115320

[2] Thomas Lengauer and Robert Endre Tarjan. 1979. A fast algorithm for
    finding dominators in a flowgraph. ACM Trans. Program. Lang. Syst. 1, 1
    (January 1979), 121-141. DOI=10.1145/357062.357071
    http://doi.acm.org/10.1145/357062.357071 

[3] Alfred V. Aho and Jeffrey D. Ullman. 1972. The Theory of Parsing,
    Translation, and Compiling. Prentice-Hall, Inc., Upper Saddle River, NJ,
    USA.
    http://dl.acm.org/citation.cfm?id=SERIES11430.578789

"""

from . import lulucfg as c

import os

_DEBUG = (
        os.environ.get("LULU_TOSSA_DEBUG", None)
        in ("on", "yes", "1"))

# Dominance tree finding

## The Aho and Ullman method

def _mask_search(succ:'[[int]]', avoid:int) -> '[int]':
    """
    succ    The successors of each node.
    avoid   The node to avoid.
    return: A list of nodes other than `avoid` which are unreachable.

    Search a graph from the root, avoiding the node designated by `avoid`.

    * Assume the 0-th node is the root.
    """

    reachable = [False] * len(succ)
    q = [0]     # Use stack as the job queue. Implies depth-first search.
                # Actually any kind of search (BFS, DFS, ...) is okay.
    reachable[0] = True

    while len(q) > 0:
        cur_node = q.pop()
    
        for next_node in succ[cur_node]:
            if next_node != avoid and reachable[next_node] == False:
                reachable[next_node] = True
                q.append(next_node)

    return [i for i,r in enumerate(reachable) if i != avoid and r == False]

def au_get_idoms(succ:'[[int]]') -> '[int]':
    """
    succ    The successors of each node.
    return: The immediate dominator of each node. 0 for the root.

    The Aho and Ullman's dominance tree algorithm

    This algorithm iteratively finds the nodes dominated by each node and
    updates the "current dominator" array.

    * Assume the 0-th node is the root.
    """
    dom = [0] * len(succ)

    for i in range(1, len(succ)):
        js = _mask_search(succ, i)
        for j in js:    # i dominates j
            if dom[j] == dom[i]:
                dom[j] = i

    return dom

## The Lengauer and Tarjan method

def _dfs(succ:'[[int]]') -> '([int], [int])':
    """
    succ    The successors of each node.
    return: The sequence number each node is visited and the parent of each node
            (0 for the root).

    Traverse a graph in the depth-first order.

    * Assume the 0-th node is the root.
    """

    indices = [-1] * len(succ)  # -1: unvisited, 0+: visited
    parent = [0] * len(succ)
    q = [0]     # Use stack as the job queue.

    next_index = 0
    while len(q) > 0:
        cur_node = q.pop()

        if indices[cur_node] != -1:
            # NOTE: Since we push all children before visiting (on popping),
            # some nodes may be pushed more than once.
            continue

        indices[cur_node] = next_index
        next_index += 1
    
        # NOTE: Push the children backward on the stack so the first child will
        # be popped first. This dictates the visiting order.
        for next_node in reversed(succ[cur_node]):
            if indices[next_node] == -1:
                parent[next_node] = cur_node
                q.append(next_node)

    return indices, parent

def _get_preds(succ:'[[int]]') -> '[[int]]':
    """
    succ    The successors of each node.
    return: The predecessors of each node.


    * Assume the 0-th node is the root.
    """

    pred = [[] for n in succ]
    for i,js in enumerate(succ):
        for j in js:
            pred[j].append(i)

    return pred

def lt_get_idoms(succ:'[[int]]') -> '[int]':
    """
    succ    The successors of each node.
    return: The immediate dominator of each node. 0 for the root.

    The Lengauer and Tarjan's dominance tree algorithm

    This algorithm uses a depth-first search to construct a spanning tree of the
    CFG and traverse the tree backwards to determine the semi-dominator and
    immediate dominator of each node.

    This algorithm has a lower time complexity than the Aho&Ullman's algorithm
    and returns exactly the same result.

    * Assume the 0-th node is the root.
    """

    # Step 1: DFS and prepare
    semi, parent = _dfs(succ)
    pred = _get_preds(succ)

    vertex = [0] * len(succ)
    for v,n in enumerate(semi):
        vertex[n] = v

    ancestor = [0] * len(succ)
    label = list(range(len(succ)))

    bucket = [[] for n in succ]

    dom = [0] * len(succ)

    def compress(v:int):
        " Compress the path from vertex v. "
        assert(ancestor[v] != 0)    # Should be tested in ieval()
        if ancestor[ancestor[v]] != 0:
            compress(ancestor[v])
            if semi[label[ancestor[v]]] < semi[label[v]]:
                label[v] = label[ancestor[v]]
            ancestor[v] = ancestor[ancestor[v]]

    def link(v:int, w:int):
        " Link v as the ancestor of w. "
        ancestor[w] = v

    def ieval(v:int) -> int:
        " Find the node n with the smallest semi[n] for n in v to sdom(v). "

        if ancestor[v] == 0:
            return v
        else:
            compress(v)
            return label[v]

    for i in range(len(succ)-1, 0, -1):
        w = vertex[i]

        # Step 2: Calculate sdom(w)
        for v in pred[w]:
            u = ieval(v)
            if semi[u] < semi[w]:
                semi[w] = semi[u]
            bucket[vertex[semi[w]]].append(w)
            link(parent[w], w)

        # Step 3: Implicitly determine idom of notes whose sdom = parent[w]
        for v in bucket[parent[w]]:
            u = ieval(v)
            dom[v] = u if semi[u] < semi[v] else parent[w]

        bucket[parent[w]][:] = []   # Clear bucket

    # Step 4: Explicitly determine idoms
    for i in range(1, len(succ)):
        w = vertex[i]
        if dom[w] != vertex[semi[w]]:
            dom[w] = dom[dom[w]]

    return dom

# Dominance frontier finding

def _traverse_tree_bottom_up(parent:'[int]') -> 'gen(int)':
    """
    parent  The parent of each node in a tree. The root points to itself.
    yield:  Each node in the bottom-up order.

    Traverse a tree in the bottom-up order.  A node is visited after all of its
    children are visited.
    """
    incount = [0] * len(parent)
    for n in parent:
        incount[n] += 1

    q = [i for i,c in enumerate(incount) if c==0]
    while len(q) > 0:
        x = q.pop()

        yield x

        incount[parent[x]] -= 1
        if incount[parent[x]] == 0:
            q.append(parent[x])

def _get_children(parent:'[int]') -> '[[int]]':
    """
    parent  The parent of each node in a tree. The root points to itself.
    return: The children of each node.

    Compute the children of each node of a tree.
    """
    children = [[] for n in parent]
    for c,p in enumerate(parent):
        if c == 0: continue     # skip root
        children[p].append(c)

    return children

def get_df(succ:'[[int]]', idom:'[int]') -> '[[int]]':
    """
    succ    The successors of each node.
    idom    The immediate dominator of each node. The root points to itself.
    return: The dominance frontier of each node.

    Calculate the dominance frontier of each node.

    * Assume the 0-th node is the root.
    """

    #print(succ, idom)

    #children = _get_children(idom)
    #print (children)

    df = [set() for n in idom]

    for x in _traverse_tree_bottom_up(idom):
        if x == 0: continue     # Ignore the root since it dominates all nodes

        # Local
        for y in succ[x]:
            if idom[y] != x:
                df[x].add(y)

        # Up
        #for z in children[x]:
        #    for y in df[z]:
        #        if idom[y] != x:
        #            df[x].add(y)

        # Up simplified
        for y in df[x]:
            if idom[y] != idom[x]:
                df[idom[x]].add(y)
    
    return list(map(sorted, df))

# Phi node placement

def get_phi_placement(df:'[[int]]', init:'[int]') -> '[int]':
    """
    df      The dominance frontier of each node.
    init    The initial node set.
    return: The nodes where Phi-functions should be placed.
    """
    has_already = [False] * len(df)
    work = [False] * len(df)
    for x in init:
        work[x] = True

    q = list(init)

    while len(q) > 0:
        x = q.pop()
        for y in df[x]:
            if has_already[y] == False:
                has_already[y] = True
                if work[y] == False:
                    work[y] = True
                    q.append(y)

    return [i for i,h in enumerate(has_already) if h==True]

# CFG transformation

def cfg_to_succ(cfg:c.CFG) -> "[[int]]":
    """
    cfg     The Control Flow Graph
    return: The successors of each basic block.

    Convert a cfg to a list of successors.
    """

    succ = [[] for x in cfg.basicblocks]

    bb2index = {bb:ix for ix,bb in enumerate(cfg.basicblocks)}
    
    for bb in cfg.basicblocks:
        ix = bb2index[bb]

        last = bb.instructions[-1]
        if isinstance(last, c.Br):
            ix1 = bb2index[last.dst]
            succ[ix].append(ix1)
        elif isinstance(last, c.Br2):
            ix1 = bb2index[last.dstt]
            ix2 = bb2index[last.dstf]
            succ[ix].append(ix1)
            succ[ix].append(ix2)

    return succ

def remove_unreachable_blocks(cfg:c.CFG):
    """
    cfg     The control flow graph.

    Remove unreachable basic blocks from a cfg.
    """

    succ = cfg_to_succ(cfg)

    reachable = [False for bb in succ]
    q = [0]
    reachable[0] = True

    while len(q) > 0:
        cur_node = q.pop()

        for next_node in succ[cur_node]:
            if not reachable[next_node]:
                reachable[next_node] = True
                q.append(next_node)

    cfg.basicblocks[:] = [bb for bb,r in zip(cfg.basicblocks, reachable) if r]

def cfg_to_ssa(cfg:c.CFG):
    """
    cfg     The control flow graph.

    Transform a CFG to the SSA form in place.
    """

    # Assign all lvars to nil in the entry block.
    the_nil = c.NilConst()
    cfg.constants.add(the_nil)
    cfg.entry.instructions[:0] = [c.StoreLocal(lvar, the_nil)
            for lvar in range(cfg.nlvars)]

    # Calcluate the immediate dominators and dominance frontiers

    remove_unreachable_blocks(cfg)

    succ = cfg_to_succ(cfg)
    idom = lt_get_idoms(succ)
    df = get_df(succ, idom)

    if _DEBUG:
        print("After removing unreachable blocks:")
        print(c.ppcfg(cfg))

        for i,ss in enumerate(succ):
            print("{} -> {}".format(i, ", ".join(map(str,ss))))

        for i,dm in enumerate(idom):
            print("{} => {}".format(dm, i))

        for i,dfs in enumerate(df):
            print("{} ----: [{}]".format(i, ", ".join(map(str,dfs))))

    # Insert Phi-nodes where needed.
    
    ## Find the appearance of all StoreLocal's
    assigned_in = [set() for x in range(cfg.nlvars)]
    for i,bb in enumerate(cfg.basicblocks):
        for inst in bb.instructions:
            if isinstance(inst, c.StoreLocal):
                assigned_in[inst.lvar].add(i)

    if _DEBUG:
        for i,ai in enumerate(assigned_in):
            print("{} is assigned in: [{}]".format(i, ", ".join(map(str,ai))))

    ## For each basic block, find the lvars which need phi-nodes.
    bb_newphis = [[] for bb in cfg.basicblocks] # bbnum -> [lvar]
    for lvar,ai in enumerate(assigned_in):
        phi_places = get_phi_placement(df, ai)
        if _DEBUG:
            print("place Phi[{}] in {}".format(
                lvar, ", ".join(map(str,phi_places))))
        for bbi in phi_places:
            bb_newphis[bbi].append(lvar)

    if _DEBUG:
        for bbnum,lvars in enumerate(bb_newphis):
            print("BB[{}] has phi for {}".format(
                bbnum, ", ".join(map(str,lvars))))

    ## Insert Phi-nodes
    phi2lvar = {}   # c.Phi -> lvar
    lvarbb2phi = {} # (lvar, bbnum) -> c.Phi
    for bbnum,lvars in enumerate(bb_newphis):
        new_phis = []
        for lvar in lvars:
            phi = c.Phi({}, lvar=lvar)
            phi2lvar[phi] = lvar
            lvarbb2phi[(lvar,bbnum)] = phi
            new_phis.append(phi)

        cfg.basicblocks[bbnum].instructions[:0] = new_phis

    if _DEBUG:
        print(c.ppcfg(cfg))

    # Load/Store replacing

    ## Forward every LoadLocal to its current SSA Value

    forward = {}    # c.LoadLocal -> SSA Value
    children = _get_children(idom)

    varstack = [[] for lvar in range(cfg.nlvars)]

    def do_bb(bbnum:int):
        """ Process a basic block. """
        if _DEBUG:
            print ("BB: {}".format(bbnum))
        bb = cfg.basicblocks[bbnum]
        bps = map(len, varstack)    # Record the initial lengths for fast pop

        for inst in bb.instructions:
            if isinstance(inst, c.StoreLocal):
                varstack[inst.lvar].append(inst.val)
            elif inst in phi2lvar:  # The old CFG already contains Phi-nodes
                varstack[phi2lvar[inst]].append(inst)
            elif isinstance(inst, c.LoadLocal):
                # Since we force assigned all lvars to nil, there is always
                # a StoreLocal for every lvar before loading.
                forward[inst] = varstack[inst.lvar][-1]

        for bbnum2 in succ[bbnum]:
            bb2 = cfg.basicblocks[bbnum2]
            for inst2 in bb2.instructions:
                if isinstance(inst2, c.Phi):
                    if inst2 in phi2lvar:
                        inst2.cases[bb] = varstack[phi2lvar[inst2]][-1]
                else:   # All phis are in the beginning
                    break

        for child_bbnum in children[bbnum]:
            do_bb(child_bbnum)

        for st,oldsize in zip(varstack, bps):
            st[oldsize:] = []

    do_bb(0)

    if _DEBUG:
        print("After replacement")
        print(c.ppcfg(cfg))

    ## Remove LoadLocal and StoreLocal

    for bb in cfg.basicblocks:
        new_insts = []
        for inst in bb.instructions:
            if isinstance(inst, c.StoreLocal) or isinstance(inst, c.LoadLocal):
                pass
            else:
                for fieldvar, replacer in c.itervalfields(inst):
                    if fieldvar in forward:
                        replacer(forward[fieldvar])
                new_insts.append(inst)
        bb.instructions[:] = new_insts

    if _DEBUG:
        print("After removal")
        print(c.ppcfg(cfg))

if __name__=='__main__':
    from . import lulufront, lulucfg as c
    import sys

    if len(sys.argv) != 2:
        print(
"""USAGE: python -m lulu.tossa <filename.lua>"
If <filename.lua> is -, then it is read from STDIN"
Use the Lua test suite for testing."
Available at http://www.lua.org/tests/5.2/""")
    else:
        lua_prog_fn = sys.argv[1]
        if lua_prog_fn == "-":
            lua_prog_raw = sys.stdin.read()
        else:
            with open(lua_prog_fn) as f:
                lua_prog_raw = f.read()

        lf = lulufront.LuLuFront()
        cfg = lf.build_from_code(lua_prog_raw)

        for cur_cfg in c.get_cfgs_recursive(cfg):
            print("before:")
            print(c.ppcfg(cur_cfg))
            cfg_to_ssa(cur_cfg)
            print("after:")
            print(c.ppcfg(cur_cfg))

