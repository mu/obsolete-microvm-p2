--[==[
This lua source file is "magic". It follows a different set of rules from
real-world Lua programs.

- Global variables are not allowed. Only local variables are.
- The main chunk does not make sense. It will be ignored, but is compiled like
  other functions. All nested functions in the main chunk will be recognised by
  the LuLu magical translator.
- Magic functions are allowed. They are accessible through the _MAG "global"
  variable.
- Variables may hold non-lua types, e.g. Integers.
  * Integer literals and types can be introduced by calling _MAG.int(num_lit)
    where num_lit is a number literal. It will be converted to a word-size int.
  * Char literals and types can be introduced by calling _MAG.char(num_lit)
    where num_lit is a number literal. It will be converted to a char-size int.
- A variable can hold exactly one type, e.g. Integer, Char or LuaVal. All
  "traditional" Lua values have type "LuaVal".
]==]

function raw_equals(x, y)
    if _MAG.is_int(x) and _MAG.is_int(y) then
        return _MAG.to_int(x) == _MAG.to_int(y)
    elseif _MAG.is_fp(x) and _MAG.is_fp(y) then
        return _MAG.to_fp(x) == _MAG.to_fp(y)
    elseif _MAG.is_ref(x) and _MAG.is_ref() then
        local xt, yt = _MAG.to_tag(x), _MAG.to_tag(y)
        if xt ~= yt then
            return _MAG.i1(0)
        else
            local xa, ya = _MAG.to_ref(x), _MAG.to_ref(y)
            return xa == ya
        end
    else
        return _MAG.int1(0)
    end
end
