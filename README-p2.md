# MicroVM Prototype-2

This is the second prototype of the MicroVM.

This update is only a clean-up of the previous MicroVM Prototype.

# Changes

## Python3

The program is written in Python3 instead of Python2. It allows a more succinct
grammar of "SuperTuple" and many detailed improvements.

## Tagged reference

Tagged reference types and corresponding instructions are added.

## Compound types as SSA values

Struct and FixedArray are now allowed in the SSA form. ExtractValue and
InsertValue are two instructions to handle these types.

## Intrinsic functions

Intrinsic function is a kind of instructions which, like functions, has pre-
determined parameters and return values. These instructions are unified as a
IntrinsicCall instruction and is easier to extend.

## New object model and no TIBs

The MicroVM will no longer help the Client to keep a TIB pointer inside the
object header. As for no-GC garbage collection strategy, there will be no header
before objects except VarArray, which has an immutable field for the length of
the array.

## Exact type equality

It is now able to test exact type equality between types.

