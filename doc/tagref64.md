# About the `TagRef64`  data type.

A TagRef64 is a tagged reference type. It is a union of Int(51), Double and Ref.

It borrows several special values from a IEEE754 double precision floating point
number to represent integers and references.

## Background

An IEEE754 double precision floating point number (called "Double" for short in
the following passage) consists of a 1-bit sign bit, a 11-bit exponent and a
52-bit fraction, which sum up to a total of 64 bits.

The following is a diagram of a Double. `s`, `e` and `f` represent sign,
exponent and fraction, respectively.

    seee eeee eeee ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff

- When the exponent is all zeroes, then
  - if the fraction is also all zeroes, then the number is zero. NOTE that there
    are positive zero `+0.0` and negative zero `-0.0` according to the sign bit.
  - otherwise the number is a subnormal number.
- When the exponent is all ones (0x7ff or 0b11111111111), then
  - if the fraction is also all zeroes, then the number is infinity NOTE that
    there are positive infinity `+inf` and negative infinity `-inf` accorcing to
    the sign bit.
  - otherwise the number is not a number, or `NaN`. 

Since there are `2**53-2` different kinds of NaNs, we borrow some of them to
represents other data types, namely references and integers.

## Bit Patterns

We borrow some NaN cases to represent integers or references.

When the exponent is not all ones, it is a normal Double number.

When the exponent is all ones, consider the following cases:

    s111 1111 1111 .... .... .... .... .... .... .... .... .... .... .... .... ..00

The above pattern (when the last two bits are 00) represents normal Double NaN
or infinity. IEEE754 rules still apply. i.e. all zeros means infinity and not
all zeroes means NaN. There are still signalling NaN and quiet NaN.

    s111 1111 1111 tttt taaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa at10

The above pattern (when the last two bits are 10) represents a reference. The
46th bit to the 3rd bit (the `a` part) represents the 46th bit to the 3rd bit
of the address. The sign bit (`s`) will be duplicated to cover the highest 17
bits.  Other bits (the `t` part) form a 6-bit integer, which is available for
the client to store additional information (e.g. type ID).

NOTE: The current implementation of x86-64 only has a 48-bit address space. So
only the lower 48 bits of the 64-bit word are significant and the higher 16
bits must be the copy of the 47th bit. So the 52-bit fraction part of the
Double number is sufficient to store a 48-bit address. Furthermore, in the
MicroVM on a 64-bit architecture, all objects are 8-byte aligned, so the lowest
3 bits are always zero. So only 45 bits are encoded in this data type.

    s111 1111 1111 nnnn nnnn nnnn nnnn nnnn nnnn nnnn nnnn nnnn nnnn nnnn nnnn nnn1

The above pattern (when the last bit is 1) represents an integer. The sign
bit (`s`) and the highest 51 bits of the fraction part (`n`) together forms a
52-bit integer. The signedness of integers in the MicroVM is determined by
operations rather than type.

NOTE: A TagRef64 is not a Double if and only if the exponent is all zeroes and
the last bit is one.

## Operations

Several instructions are provided to work with this tagged reference type.

For testing: 

- `IsInt(opnd: ValueOf(TagRef64)) -> 0|1`: tests if a TagRef64 represents an
  integer.
- `IsFP(opnd: ValueOf(TagRef64)) -> 0|1`: tests if a TagRef64 represents a
  Double.
- `IsRef(opnd: ValueOf(TagRef64)) -> 0|1`: tests if a TagRef64 represents a
  reference.

Wrappers:

- `IntToTR64(ref_ty: Type, opnd: ValueOf(Int(52))) -> TagRef64(ref_ty)`:
  This instruction wraps an integer into a TagRef64 of a specific type.
- `FPToTR64(ref_ty: Type, opnd: ValueOf(Double())) -> TagRef64(ref_ty)`:
  This instruction wraps a Double into a TagRef64 of a specific type.
- `RefToTR64(ref_ty: Type, opnd: ValueOf(Ref(ref_ty)),
             tag: Optional(ValueOf(Int(6)))) -> TagRef64(ref_ty)`:
  This instruction wraps a Ref into a TagRef64 of the same reference type.
  The "tag" parameter is for storing an extra 6-bit integer tag alongside the
  reference. It is optional and defaults to 0.

Unwrappers:

- `TR64ToInt(ref_ty: Type, opnd: TagRef64(ref_ty)) -> Int(52)`:
  This instruction unwraps an integer out of a TagRef64.
- `TR64ToFP(ref_ty: Type, opnd: TagRef64(ref_ty)) -> Double()`:
  This instruction unwraps a Double out of a TagRef64.
- `TR64ToRef(ref_ty: Type, opnd: TagRef64(ref_ty)) -> Ref(ref_ty)`:
  This instruction unwraps a Ref out of a TagRef64 of the same reference type.
- `TR64ToTag(ref_ty: Type, opnd: TagRef64(ref_ty)) -> Int(6)`:
  This instruction unwraps the tag which accompanies a Ref.

NOTE: None of the above instructions are "potential excepting instructions". The
client must check the type a TagRef64 actually contains before unwrapping. i.e.:

- `TR64ToInt` must work on a TagRef64 which encodes an `Int(52)`.
- `TR64ToFP` must work on a TagRef64 which encodes a `Double()`.
- `TR64ToRef` and `TR64ToTag` must work on a TagRef64 which encodes a `Ref`.

Unwrapping a TagRef64 using the wrong instruction is an undefined behaviour.

## Python representation

In Python, the value of this type is represented as a 64-bit integer. They are
written and read into the memory as 64-bit integers. The above wrapper and
unwrapper instructions are implemented as bit operations over 64-bit integers.

