import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), "..", "src"))

import unittest

from lulu import (luluclient)
from lulu.luluconstants import *

class TestLuLuCode(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.lc = luluclient.LuLuClient()
        cls.de = cls.lc.de
        cls.mvm = cls.lc.mvm
        cls.itpr = cls.lc.itpr

    def setUp(self):
        self.run_lua("""
        assert_equals = function(a,b)
            if a ~= b then
                print("Assert failed.", a, "~=", b);
                error("Assert failed.", a, "~=", b);
            end
        end
        """)

    def run_lua(self, prog):
        self.lc.run_lua(prog)

    def test_hello_world(self):
        print("test_hello_world")
        self.run_lua("""
            print "Hello world!"
        """)

    def test_values(self):
        print("test_values")
        self.run_lua("""
            print(nil, false, true, 3.14, "Hello")
        """)

    def test_ops(self):
        print("test_ops")
        self.run_lua("""
            assert_equals(1.0 + 2.0, 3.0)
            assert_equals(1.0 - 2.0, -1.0)
            assert_equals(1.0 * 2.0, 2.0)
            assert_equals(1.0 / 2.0, 0.5)
            assert_equals(1.0 % 2.0, 1.0)
            assert_equals(1.0 ^ 2.0, 1.0)
            assert_equals(1.0 < 2.0, true)
            assert_equals(1.0 > 2.0, false)
            assert_equals(1.0 <= 2.0, true)
            assert_equals(1.0 >= 2.0, false)
            assert_equals(1.0 == 2.0, false)
            assert_equals(1.0 ~= 2.0, true)
            assert_equals(1.0 and 2.0, 2.0)
            assert_equals(nil and 2.0, nil)
            assert_equals(1.0 or 2.0, 1.0)
            assert_equals(false or 2.0, 2.0)
            assert_equals(not 1.0, false)
            assert_equals(#"hello", 5)
            assert_equals(-1, -1)
        """)

    def test_vars(self):
        print("test_vars")
        self.run_lua("""
            local a,b,c,d = 1,2,3,4
            a,b,c,d = 5,6
            assert_equals(a, 5)
            assert_equals(b, 6)
            assert_equals(c, nil)
            assert_equals(d, nil)

            e = 9
            assert_equals(e, 9)
            assert_equals(f, nil)
        """)
    
    def test_do(self):
        print("test_do")
        self.run_lua("""
            local a, b = 42, "Hello"
            do
                local a = 84
                assert_equals(a, 84)
                assert_equals(b, "Hello")
            end
            assert_equals(a, 42)
            assert_equals(b, "Hello")
        """)

    def test_while(self):
        print("test_while")
        self.run_lua("""
            local a=1
            while a < 3 do
                print('testing while', a)
                a = a + 1
            end
            assert_equals(a, 3)
        """)

    def test_deep_while(self):
        print("test_deep_while")
        self.run_lua("""
            local a=1
            while a < 3 do
                local b = -3;
                print("a,b=",a, b)
                do
                    assert_equals(b, -3)
                    local c = -5;
                    assert_equals(c, -5)
                end
                assert_equals(c, nil)
                a = a + 1
            end
            assert_equals(b, nil)
        """)

    def test_repeat(self):
        print("test_repeat")
        self.run_lua("""
            local a=1
            repeat
                print("testing_repeat", a)
                a = a + 1
            until a >= 3 
            assert_equals(a, 3)
        """)

    def test_if(self):
        print("test_if")
        self.run_lua("""
            local a,b=2,nil
            if a==1 then
                b="one"
            elseif a==2 then
                b="two"
            elseif a==3 then
                b="three"
            end
            assert_equals(b, "two")

            local c="change me"
            if a==4 then
                error("a is not 4")
            else
                c=42
            end
            assert_equals(c, 42)

            local d=84
            if a==4 then
                error("a is not 4")
            end
            assert_equals(d, 84)
        """)

    def test_for_step(self):
        print("test_for_step")
        self.run_lua("""
            local i=42
            for i = 1, 3 do
                print("Hello", i)
            end
            for j = 0, 6, 2 do
                print("Aloha", j)
            end
            for j = 100, 0, -30 do
                print("Yay!", j)
            end
            print("Goodbye", i)
            assert_equals(i, 42)
        """)

    def test_anon_function(self):
        print("test_anon_function")
        self.run_lua("""
            local f = function() return 42; end
            local x = f()
            assert_equals(x, 42)
        """)

    def test_named_function(self):
        print("test_named_function")
        self.run_lua("""
            function named_func() return 42; end
            local x = named_func()
            assert_equals(x, 42)
        """)

    def test_local_function(self):
        print("test_local_function")
        self.run_lua("""
            local function named_func() return 84; end
            local x = named_func()
            assert_equals(x, 84)
        """)

    def test_recursive_call(self):
        print("test_recursive_call")
        self.run_lua("""
            factorial = function(x)
                if x==0 then return 1
                else return factorial(x-1)*x
                end
            end
            local x = factorial(10)
            assert_equals(x, 3628800)
        """)
