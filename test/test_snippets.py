import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), "..", "src"))
MICROVM_TEST_PRETTY_PRINT_SNIPPET = (
        os.environ.get("MICROVM_TEST_PRETTY_PRINT_SNIPPET", None)
        in ("on", "yes", "1"))

import unittest

from microvm import (mtypes as t, snippet as s, builder, snippetcheck as sc)
from microvm.commontypes import *

class TestSnippets(unittest.TestCase):
    @unittest.skipIf(not MICROVM_TEST_PRETTY_PRINT_SNIPPET,
            "Don't pollute the output unless having to")
    def test_pretty_print(self):
        struct_t = t.Struct([int64_t, int32_t, double_t])
        struct_r = t.Ref(struct_t)

        fac_sig = t.Signature([int64_t], int64_t)
        b2 = builder.Builder(fac_sig, name="factorial")
        snp2 = b2.snp

        eq0 = b2.Cmp(int64_t, "eq", snp2.params[0], s.Constant(int64_t, 0))
        b2exit = b2.new_bb("exit")
        b2rec = b2.new_bb("rec")
        b2.Branch2(eq0, b2exit, b2rec)

        b2.use(b2exit)
        b2.Ret(int64_t, s.Constant(int64_t, 1))

        b2.use(b2rec)
        pm1 = b2.BinOp(int64_t, "sub", snp2.params[0], s.Constant(int64_t, 1))
        rec_call = b2.Call(fac_sig, s.Constant(t.FuncHandle(fac_sig), snp2), [pm1])
        b2.Ret(int64_t, b2.BinOp(int64_t, "mul", snp2.params[0], rec_call))

        b = builder.Builder(t.Signature([int64_t, int64_t], int64_t), "unnamed")
        snp = b.snp
        sum1 = b.BinOp(int64_t, "add",
                s.Constant(int64_t, 1), s.Constant(int64_t, 2))
        sum2 = b.BinOp(int64_t, "sub", snp.params[0], snp.params[1])
        prod = b.BinOp(int64_t, "mul", sum1, sum2)

        pos = b.Cmp(int64_t, "sgt", prod, s.Constant(int64_t, 0), name="prod>0")

        is_pos = b.new_bb("is_pos")
        is_neg = b.new_bb("is_neg")
        exit = b.new_bb("exit")

        b.Branch2(pos, is_pos, is_neg)

        b.use(is_pos)
        b.Branch(exit)

        b.use(is_neg)
        nprod = b.BinOp(int64_t, "sub", s.Constant(int64_t, 0), prod,
                name="-prod")
        b.Branch(exit)

        b.use(exit)
        res = b.Phi(int64_t, {
            is_pos: prod,
            is_neg: nprod,
            }, name="result")
        fac = b.Call(fac_sig, s.Constant(t.FuncHandle(fac_sig), snp2), [res],
                name="call_factorial")

        #obj = b.Alloc(struct_t, aid=0)
        #b.SetField(struct_t, int64_t, s.FE(0), obj, fac)
        #old_f1 = b.GetField(struct_t, int32_t, s.FE(1), obj)

        b.Ret(int64_t, fac)

        print(s.ppsnippet(snp2))
        print(s.ppsnippet(snp))

        sc.check_snippet(snp)

