import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), "..", "src"))

import unittest

from luaclient import luaclient
from luaclient import luabuilder
from luaclient.luaconstants import *

from pot import Pot

def stop_thread(thread):
    thread.running = False

class TestLuaClient(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.lc = luaclient.LuaClient()
        cls.de = cls.lc.de
        cls.mvm = cls.lc.mvm
        cls.itpr = cls.lc.itpr

    def ppconst_itpr_aware(self, fv):
        if isinstance(fv.ty, t.FuncHandler):
            snp = self.itpr.id_to_snippet(fv.data)
            return "Snippet({})".format(snp.name if snp != None else "(noname)")
        else:
            return None

    def ppsnippet(self, snp):
        pp = luabuilder.LuaSnippetPrettyPrinter(self.lc)
        return pp.lua_aware_ppsnippet(snp)


    def helpful_check_snippet(self, snp):
        try:
            sc.check_snippet(snp)
        except:
            print(self.ppsnippet(snp))
            raise

    # LuaClient tests

    def test_value_creation(self):
        nil_tr    = self.lc.make_nil()
        true_tr   = self.lc.make_boolean(True)
        false_tr  = self.lc.make_boolean(False)
        number_tr = self.lc.make_number(42.0)
        string_tr = self.lc.make_string("Hello world!")
        table_tr  = self.lc.make_table()

        self.assertEqual(self.lc.lua_get_typeid(nil_tr   ), LUA_TNIL)
        self.assertEqual(self.lc.lua_get_typeid(true_tr  ), LUA_TBOOLEAN)
        self.assertEqual(self.lc.lua_get_typeid(false_tr ), LUA_TBOOLEAN)
        self.assertEqual(self.lc.lua_get_typeid(number_tr), LUA_TNUMBER)
        self.assertEqual(self.lc.lua_get_typeid(string_tr), LUA_TSTRING)
        self.assertEqual(self.lc.lua_get_typeid(table_tr ), LUA_TTABLE)

        self.assertEqual(self.lc.from_lua(nil_tr   ), None)
        self.assertEqual(self.lc.from_lua(true_tr  ), True)
        self.assertEqual(self.lc.from_lua(false_tr ), False)
        self.assertEqual(self.lc.from_lua(number_tr), 42.0)
        self.assertEqual(self.lc.from_lua(string_tr), "Hello world!")
        self.assertEqual(self.lc.from_lua(table_tr ), [])

        self.assertTrue(self.lc.lua_raw_equals(nil_tr,    nil_tr))
        self.assertTrue(self.lc.lua_raw_equals(true_tr,   true_tr))
        self.assertTrue(self.lc.lua_raw_equals(false_tr,  false_tr))
        self.assertTrue(self.lc.lua_raw_equals(number_tr, number_tr))
        self.assertTrue(self.lc.lua_raw_equals(string_tr, string_tr))
        self.assertTrue(self.lc.lua_raw_equals(table_tr,  table_tr))

        self.assertFalse(self.lc.lua_raw_equals(nil_tr,    false_tr))
        self.assertFalse(self.lc.lua_raw_equals(true_tr,   number_tr))

        true_tr2   = self.lc.make_boolean(True)
        number_tr2 = self.lc.make_number(42.0)
        string_tr2 = self.lc.make_string("Hello world!")
        table_tr2  = self.lc.make_table()

        self.assertTrue (self.lc.lua_raw_equals(true_tr  , true_tr2))
        self.assertTrue (self.lc.lua_raw_equals(number_tr, number_tr2))
        self.assertTrue (self.lc.lua_raw_equals(string_tr, string_tr2))
        self.assertFalse(self.lc.lua_raw_equals(table_tr , table_tr2))

    def test_get_set_table_field(self):
        table = self.lc.make_table()
        for k in range(26):
            knum = self.lc.make_number(float(k))
            vstr = self.lc.make_string(chr(k+ord("A")))
            self.lc.lua_table_set(table, knum, vstr)

        for k in range(26):
            knum = self.lc.make_number(float(k))
            vstr = self.lc.lua_table_get(table, knum)
            v = self.lc.from_lua(vstr)

            self.assertEqual(v, chr(int(k)+ord("A")))

        nil = self.lc.make_nil()

        for k in range(26):
            knum = self.lc.make_number(float(k))
            self.lc.lua_table_set(table, knum, nil)
            vstr = self.lc.lua_table_get(table, knum)

            self.assertTrue(self.lc.lua_raw_equals(vstr, nil))

    # LuaBuilder and LuaPrimitives tests

    def test_mvmir_raw_equals(self):
        sig = t.Signature([], void_t)
        sid = self.itpr.declare(sig)
        b = luabuilder.LuaBuilder(self.lc, sig)
        snp = b.snp

        pot = Pot()

        nil_tr     = b.nil
        true_tr    = b.true
        false_tr   = b.false
        number_tr  = b.number(42.0)
        number_tr2 = b.number(43.0)
        string_tr  = b.string("Hello world!")
        string_tr2 = b.string("Goodbye world!")
        table_tr   = b.t2lv(b.table_new())
        table_tr2  = b.t2lv(b.table_new())

        nil_eq     = pot << b.raw_equals(nil_tr   , nil_tr)
        true_eq    = pot << b.raw_equals(true_tr  , true_tr)
        false_eq   = pot << b.raw_equals(false_tr , false_tr)
        number_eq  = pot << b.raw_equals(number_tr, number_tr)
        string_eq  = pot << b.raw_equals(string_tr, string_tr)
        table_eq   = pot << b.raw_equals(table_tr , table_tr)
        bool_eq2   = pot << b.raw_equals(true_tr  , false_tr)
        number_eq2 = pot << b.raw_equals(number_tr, number_tr2)
        string_eq2 = pot << b.raw_equals(string_tr, string_tr2)
        table_eq2  = pot << b.raw_equals(table_tr , table_tr2)

        pot.make_trap(b)

        self.helpful_check_snippet(snp)
        self.itpr.define(sid, snp)

        th = self.itpr.run_snippet(sid, [])

        self.assertEqual(pot[nil_eq    ], 1)
        self.assertEqual(pot[true_eq   ], 1)
        self.assertEqual(pot[false_eq  ], 1)
        self.assertEqual(pot[number_eq ], 1)
        self.assertEqual(pot[string_eq ], 1)
        self.assertEqual(pot[table_eq  ], 1)
        self.assertEqual(pot[bool_eq2  ], 0)
        self.assertEqual(pot[number_eq2], 0)
        self.assertEqual(pot[string_eq2], 0)
        self.assertEqual(pot[table_eq2 ], 0)

    def test_mvmir_table_get_set(self):
        sig = t.Signature([], void_t)
        sid = self.itpr.declare(sig)
        b = luabuilder.LuaBuilder(self.lc, sig)
        snp = b.snp

        pot = Pot()

        key1 = b.true
        key2 = b.false
        key3 = b.number(42.0)
        key4 = b.string("hello")

        value1 = b.string("true")
        value2 = b.string("false")
        value3 = b.string("forty-two")
        value4 = b.number(999999.0)

        table = b.table_new()

        b.table_set(table, key1, value1)
        b.table_set(table, key2, value2)
        b.table_set(table, key3, value3)
        b.table_set(table, key4, value4)

        g1 = pot << b.table_get(table, key1)
        g2 = pot << b.table_get(table, key2)
        g3 = pot << b.table_get(table, key3)
        g4 = pot << b.table_get(table, key4)

        b.table_set(table, key1, value4)
        b.table_set(table, key2, value3)
        b.table_set(table, key3, b.nil)
        b.table_set(table, key4, value1)

        g12 = pot << b.table_get(table, key1)
        g22 = pot << b.table_get(table, key2)
        g32 = pot << b.table_get(table, key3)
        g42 = pot << b.table_get(table, key4)

        pot.make_trap(b)

        self.helpful_check_snippet(snp)
        self.itpr.define(sid, snp)

        th = self.itpr.run_snippet(sid, [])

        self.assertEqual(pot[g1], value1.data)
        self.assertEqual(pot[g2], value2.data)
        self.assertEqual(pot[g3], value3.data)
        self.assertEqual(pot[g4], value4.data)
    
        self.assertEqual(pot[g12], value4.data)
        self.assertEqual(pot[g22], value3.data)
        self.assertEqual(pot[g32], b.nil.data)
        self.assertEqual(pot[g42], value1.data)

    @unittest.skip("too slow")
    def test_mvmir_table_get_set_big(self):
        sig = t.Signature([], void_t)
        sid = self.itpr.declare(sig)
        b = luabuilder.LuaBuilder(self.lc, sig)
        snp = b.snp

        pot = Pot()

        table = b.table_new()

        s_ix, bb_sb, bb_sn, bb_se = b.for_loop(constiw(0), constiw(26), constiw(1),
                "set")

        b.use(bb_sb)

        number = b.n2lv(b.sitofp(s_ix, n_t))
        code = b.binop("add", s_ix, constiw(65))    #0 -> 'A', ..., 25 -> 'Z'
        string = b.string_new(constiw(1))
        b.string_set_char(string, constiw(0), b.trunc(code, c_t))
        b.table_set(table, number, b.s2lv(string))
        b.br(bb_sn)

        b.use(bb_se)    # get and check elements
        g_ix, bb_gb, bb_gn, bb_ge = b.for_loop(constiw(0), constiw(26), constiw(1),
                name="get")

        b.use(bb_gb)
        number2 = b.n2lv(b.sitofp(g_ix, n_t))

        code2 = b.binop("add", g_ix, constiw(65))    #0 -> 'A', ..., 25 -> 'Z'
        string2 = b.string_new(constiw(1))
        b.string_set_char(string2, constiw(0), b.trunc(code2, c_t))
        string_actual = b.table_get(table, number2)

        equals = b.raw_equals(b.s2lv(string2), string_actual)

        _, assert_fail = b.br2(equals, bb_gn, "assert_fail")

        b.use(assert_fail)
        
        def trap_assert_fail(thread):
            string2_lv = self.de.ref_to_tr64(pot[string2], LUA_TSTRING)
            string_actual_lv = pot[string_actual]
            self.fail("Not equal: string2={}, string_actual={}".format(
                self.lc.from_lua(string2_lv),
                self.lc.from_lua(string_actual_lv)))
            stop_thread(thread)

        b.trap(trap_assert_fail, [])

        b.use(bb_ge)

        r_ix, bb_rb, bb_rn, bb_re = b.for_loop(constiw(0), constiw(26),
                constiw(1), name="remove")

        b.use(bb_rb)
        number3 = b.n2lv(b.sitofp(g_ix, n_t))
        b.table_set(table, number3, b.nil)
        string_actual3 = b.table_get(table, number3)

        equals3 = b.raw_equals(string_actual3, b.nil)

        _, assert_fail3 = b.br2(equals3, bb_rn, "assert_fail3")

        b.use(assert_fail3)
        
        def trap_assert_fail3(thread):
            string_actual3_lv = pot[string_actual3]
            self.fail("Not nil: string_actual3={}".format(
                self.lc.from_lua(string_actual3_lv)))
            stop_thread(thread)

        b.trap(trap_assert_fail3, [])
        
        b.use(bb_re)

        pot.make_trap(b)
        
        self.helpful_check_snippet(snp)
        self.itpr.define(sid, snp)

        th = self.itpr.run_snippet(sid, [])

    def test_mvmir_lua_function_call(self):
        sig = t.Signature([], void_t)
        sid = self.itpr.declare(sig)
        b = luabuilder.LuaBuilder(self.lc, sig)
        snp = b.snp

        pot = Pot()

        lua_args = b.params_new(constiw(5))

        b.params_set_item(lua_args, constiw(0), b.nil)
        b.params_set_item(lua_args, constiw(1), b.false)
        b.params_set_item(lua_args, constiw(2), b.true)
        b.params_set_item(lua_args, constiw(3), b.number(42))
        b.params_set_item(lua_args, constiw(4), b.string("Hello world!"))

        rv = b.builtin_print(lua_args)

        pot.make_trap(b)

        self.helpful_check_snippet(snp)
        self.itpr.define(sid, snp)
        th = self.itpr.run_snippet(sid, [])

    def test_mvmir_ops(self):
        sig = t.Signature([], void_t)
        sid = self.itpr.declare(sig)
        b = luabuilder.LuaBuilder(self.lc, sig)
        snp = b.snp

        pot = Pot()

        x = b.number(40.0)
        y = b.number(3.0)
        u = b.string("hello")
        v = b.string("world")

        tab = b.table_new()
        b.table_set(tab, x, y)
        b.table_set(tab, u, v)

        add = pot << b.lua_add(x, y)
        sub = pot << b.lua_sub(x, y)
        mul = pot << b.lua_mul(x, y)
        div = pot << b.lua_div(x, y)
        mod = pot << b.lua_mod(x, y)
        pow = pot << b.lua_pow(x, y)
        
        eq_n = pot << b.lua_eq(x, y)
        ne_n = pot << b.lua_ne(x, y)
        lt_n = pot << b.lua_lt(x, y)
        le_n = pot << b.lua_le(x, y)

        eq_s = pot << b.lua_eq(u, v)
        ne_s = pot << b.lua_ne(u, v)
        lt_s = pot << b.lua_lt(u, v)
        le_s = pot << b.lua_le(u, v)

        concat = pot << b.lua_concat(u, v)

        ulen = pot << b.lua_len(u)
        tlen = pot << b.lua_len(b.t2lv(tab))

        unm = pot << b.lua_unm(x)

        pot.make_trap(b)

        self.helpful_check_snippet(snp)
        self.itpr.define(sid, snp)
        th = self.itpr.run_snippet(sid, [])

        self.assertAlmostEqual(self.lc.from_lua(pot[add]), 40.0 + 3.0)
        self.assertAlmostEqual(self.lc.from_lua(pot[sub]), 40.0 - 3.0)
        self.assertAlmostEqual(self.lc.from_lua(pot[mul]), 40.0 * 3.0)
        self.assertAlmostEqual(self.lc.from_lua(pot[div]), 40.0 / 3.0)
        self.assertAlmostEqual(self.lc.from_lua(pot[mod]), 40.0 % 3.0)
        self.assertAlmostEqual(self.lc.from_lua(pot[pow]), 40.0 ** 3.0)

        self.assertEqual(self.lc.from_lua(pot[eq_n]), 0)
        self.assertEqual(self.lc.from_lua(pot[ne_n]), 1)
        self.assertEqual(self.lc.from_lua(pot[lt_n]), 0)
        self.assertEqual(self.lc.from_lua(pot[le_n]), 0)

        self.assertEqual(self.lc.from_lua(pot[eq_s]), 0)
        self.assertEqual(self.lc.from_lua(pot[ne_s]), 1)
        self.assertEqual(self.lc.from_lua(pot[lt_s]), 1)
        self.assertEqual(self.lc.from_lua(pot[le_s]), 1)

        self.assertEqual(self.lc.from_lua(pot[concat]), "helloworld")

        self.assertEqual(self.lc.from_lua(pot[unm]), -40.0)

        self.assertAlmostEqual(self.lc.from_lua(pot[ulen]), 5.0)
        self.assertAlmostEqual(self.lc.from_lua(pot[tlen]), 2.0)
    
    def test_mvmir_cond(self):
        sig = t.Signature([], void_t)
        sid = self.itpr.declare(sig)
        b = luabuilder.LuaBuilder(self.lc, sig)
        snp = b.snp

        pot = Pot()

        tst_nil   = pot << b.is_lua_true(b.nil)
        tst_false = pot << b.is_lua_true(b.false)
        tst_true  = pot << b.is_lua_true(b.true)
        tst_zero  = pot << b.is_lua_true(b.number(0.0))
        tst_nz    = pot << b.is_lua_true(b.number(3.14))
        tst_str   = pot << b.is_lua_true(b.string("Hello"))

        not_nil   = pot << b.lua_not(b.nil)
        not_false = pot << b.lua_not(b.false)
        not_true  = pot << b.lua_not(b.true)
        not_zero  = pot << b.lua_not(b.number(0.0))
        not_nz    = pot << b.lua_not(b.number(3.14))
        not_str   = pot << b.lua_not(b.string("Hello"))
        
        pot.make_trap(b)

        self.helpful_check_snippet(snp)
        self.itpr.define(sid, snp)
        th = self.itpr.run_snippet(sid, [])

        self.assertEqual(pot[tst_nil  ], 0)
        self.assertEqual(pot[tst_false], 0)
        self.assertEqual(pot[tst_true ], 1)
        self.assertEqual(pot[tst_zero ], 1)
        self.assertEqual(pot[tst_nz   ], 1)
        self.assertEqual(pot[tst_str  ], 1)

        self.assertEqual(pot[not_nil  ], self.lc.make_boolean(True ))
        self.assertEqual(pot[not_false], self.lc.make_boolean(True ))
        self.assertEqual(pot[not_true ], self.lc.make_boolean(False))
        self.assertEqual(pot[not_zero ], self.lc.make_boolean(False))
        self.assertEqual(pot[not_nz   ], self.lc.make_boolean(False))
        self.assertEqual(pot[not_str  ], self.lc.make_boolean(False))

    def test_mvmir_params_pack_unpack(self):
        sig = t.Signature([], void_t)
        sid = self.itpr.declare(sig)
        b = luabuilder.LuaBuilder(self.lc, sig)
        snp = b.snp
        
        pot = Pot()

        fa_params = pot << b.params_pack([b.nil, b.false, b.true, b.number(42.0)])
        fa0, fa1, fa2 = b.params_unpack(fa_params, 3)

        pot.add(fa0, fa1, fa2)

        va_params = pot << b.params_pack([b.number(101.0), b.number(102.0), fa_params])
        [va0, va1, va2, va3], vas = b.params_unpack(va_params, 4, va=True)

        pot.add(va0, va1, va2, va3, vas)
        
        pot.make_trap(b)

        self.helpful_check_snippet(snp)
        self.itpr.define(sid, snp)
        th = self.itpr.run_snippet(sid, [])

        fapvs = self.lc.params_dump(pot[fa_params])
        vapvs = self.lc.params_dump(pot[va_params])
        vasvs = self.lc.params_dump(pot[vas])

        self.assertEqual(len(fapvs), 4)
        self.assertEqual(len(vapvs), 6)
        self.assertEqual(len(vasvs), 2)

        self.assertEqual      (self.lc.from_lua(fapvs[0]), None)
        self.assertEqual      (self.lc.from_lua(fapvs[1]), False)
        self.assertEqual      (self.lc.from_lua(fapvs[2]), True)
        self.assertAlmostEqual(self.lc.from_lua(fapvs[3]), 42.0)

        self.assertEqual      (self.lc.from_lua(pot[fa0]), None)
        self.assertEqual      (self.lc.from_lua(pot[fa1]), False)
        self.assertEqual      (self.lc.from_lua(pot[fa2]), True)


        self.assertAlmostEqual(self.lc.from_lua(vapvs[0]), 101.0)
        self.assertAlmostEqual(self.lc.from_lua(vapvs[1]), 102.0)
        self.assertEqual      (self.lc.from_lua(vapvs[2]), None)
        self.assertEqual      (self.lc.from_lua(vapvs[3]), False)
        self.assertEqual      (self.lc.from_lua(vapvs[4]), True)
        self.assertAlmostEqual(self.lc.from_lua(vapvs[5]), 42.0)

        self.assertAlmostEqual(self.lc.from_lua(pot[va0]), 101.0)
        self.assertAlmostEqual(self.lc.from_lua(pot[va1]), 102.0)
        self.assertEqual      (self.lc.from_lua(pot[va2]), None)
        self.assertEqual      (self.lc.from_lua(pot[va3]), False)

        self.assertEqual      (self.lc.from_lua(vasvs[0]), True)
        self.assertAlmostEqual(self.lc.from_lua(vasvs[1]), 42.0)

    def test_mvmir_table_index_newindex(self):
        sig = t.Signature([], void_t)
        sid = self.itpr.declare(sig)
        b = luabuilder.LuaBuilder(self.lc, sig)
        snp = b.snp

        pot = Pot()

        key1 = b.true
        key2 = b.false
        key3 = b.number(42.0)
        key4 = b.string("hello")

        value1 = b.string("true")
        value2 = b.string("false")
        value3 = b.string("forty-two")
        value4 = b.number(999999.0)

        table = b.t2lv(b.table_new())

        b.lua_newindex(table, key1, value1)
        b.lua_newindex(table, key2, value2)
        b.lua_newindex(table, key3, value3)
        b.lua_newindex(table, key4, value4)

        g1 = pot << b.lua_index(table, key1)
        g2 = pot << b.lua_index(table, key2)
        g3 = pot << b.lua_index(table, key3)
        g4 = pot << b.lua_index(table, key4)

        pot.make_trap(b)

        self.helpful_check_snippet(snp)
        self.itpr.define(sid, snp)

        th = self.itpr.run_snippet(sid, [])

        self.assertEqual(pot[g1], value1.data)
        self.assertEqual(pot[g2], value2.data)
        self.assertEqual(pot[g3], value3.data)
        self.assertEqual(pot[g4], value4.data)
    
