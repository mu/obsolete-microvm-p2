import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), "..", "src"))
TEST_MAGIC_COMPILE_VERBOSE = (
        os.environ.get("TEST_MAGIC_COMPILE_VERBOSE", None)
        in ("on", "yes", "1"))

import unittest

from lulu import lulucfg as c, lulufront, tossa, rluatyper, luluback

@unittest.skip
class TestMagicCompile(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.lulufront = lulufront.LuLuFront()

    def test_magic_source(self):
        cfg = self.lulufront.build_from_code(r"""
            local x = _MAG.int64(42)
            local y = _MAG.int64(84)
            while x <= y do
                x = x + _MAG.int64(1)
            end
            return x
        """)

        print(c.ppcfg(cfg))

        tossa.cfg_to_ssa(cfg)

        print(c.ppcfg(cfg))

