import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), "..", "src"))
MICROVM_TEST_PRETTY_PRINT_SNIPPET = (
        os.environ.get("MICROVM_TEST_PRETTY_PRINT_SNIPPET", None)
        in ("on", "yes", "1"))

import unittest

from microvm import (mtypes as t, layout as l)
from microvm.mconstants import *
from microvm.commontypes import *

class TestLayout(unittest.TestCase):
    def test_align(self):
        self.assertEqual(l.align_up( 0,8),  0)
        self.assertEqual(l.align_up( 1,8),  8)
        self.assertEqual(l.align_up( 2,8),  8)
        self.assertEqual(l.align_up( 7,8),  8)
        self.assertEqual(l.align_up( 8,8),  8)
        self.assertEqual(l.align_up( 9,8), 16)
        self.assertEqual(l.align_up(15,8), 16)
        self.assertEqual(l.align_up(16,8), 16)
        self.assertEqual(l.align_up(17,8), 24)

        self.assertEqual(l.align_down( 0,8),  0)
        self.assertEqual(l.align_down( 1,8),  0)
        self.assertEqual(l.align_down( 2,8),  0)
        self.assertEqual(l.align_down( 7,8),  0)
        self.assertEqual(l.align_down( 8,8),  8)
        self.assertEqual(l.align_down( 9,8),  8)
        self.assertEqual(l.align_down(15,8),  8)
        self.assertEqual(l.align_down(16,8), 16)
        self.assertEqual(l.align_down(17,8), 16)

        self.assertEqual(l.next_pow_of_two( 1),   1)
        self.assertEqual(l.next_pow_of_two( 2),   2)
        self.assertEqual(l.next_pow_of_two( 3),   4)
        self.assertEqual(l.next_pow_of_two( 4),   4)
        self.assertEqual(l.next_pow_of_two( 5),   8)
        self.assertEqual(l.next_pow_of_two( 7),   8)
        self.assertEqual(l.next_pow_of_two( 8),   8)
        self.assertEqual(l.next_pow_of_two( 9),  16)
        self.assertEqual(l.next_pow_of_two(15),  16)
        self.assertEqual(l.next_pow_of_two(16),  16)
        self.assertEqual(l.next_pow_of_two(17),  32)
        self.assertEqual(l.next_pow_of_two(31),  32)
        self.assertEqual(l.next_pow_of_two(32),  32)
        self.assertEqual(l.next_pow_of_two(33),  64)

        self.assertEqual(l.bytes_size(  1),   1)
        self.assertEqual(l.bytes_size(  2),   2)
        self.assertEqual(l.bytes_size(  3),   4)
        self.assertEqual(l.bytes_size(  4),   4)
        self.assertEqual(l.bytes_size(  5),   8)
        self.assertEqual(l.bytes_size(  7),   8)
        self.assertEqual(l.bytes_size(  8),   8)
        self.assertEqual(l.bytes_size(  9),  16)
        self.assertEqual(l.bytes_size( 15),  16)
        self.assertEqual(l.bytes_size( 16),  16)
        self.assertEqual(l.bytes_size( 17),  24)

        self.assertEqual(l.sizeof(t.Int(  1)),   1)
        self.assertEqual(l.sizeof(t.Int(  8)),   1)
        self.assertEqual(l.sizeof(t.Int(  9)),   2)
        self.assertEqual(l.sizeof(t.Int( 15)),   2)
        self.assertEqual(l.sizeof(t.Int( 16)),   2)
        self.assertEqual(l.sizeof(t.Int( 17)),   4)
        self.assertEqual(l.sizeof(t.Int( 31)),   4)
        self.assertEqual(l.sizeof(t.Int( 32)),   4)
        self.assertEqual(l.sizeof(t.Int( 33)),   8)
        self.assertEqual(l.sizeof(t.Int( 63)),   8)
        self.assertEqual(l.sizeof(t.Int( 64)),   8)
        self.assertEqual(l.sizeof(t.Int( 65)),  16)
        self.assertEqual(l.sizeof(t.Int(127)),  16)
        self.assertEqual(l.sizeof(t.Int(128)),  16)
        self.assertEqual(l.sizeof(t.Int(129)),  24)

        self.assertEqual(l.sizeof(void_t),       0)
        self.assertEqual(l.sizeof(word_t),       8)
        self.assertEqual(l.sizeof(float_t),      4)
        self.assertEqual(l.sizeof(double_t),     8)
        self.assertEqual(l.sizeof(void_ref_t),   8)
        self.assertEqual(l.sizeof(t.TagRef64(void_t)),  8)

        self.assertEqual(l.alignof(t.Int(  1)),   1)
        self.assertEqual(l.alignof(t.Int(  8)),   1)
        self.assertEqual(l.alignof(t.Int(  9)),   2)
        self.assertEqual(l.alignof(t.Int( 15)),   2)
        self.assertEqual(l.alignof(t.Int( 16)),   2)
        self.assertEqual(l.alignof(t.Int( 17)),   4)
        self.assertEqual(l.alignof(t.Int( 31)),   4)
        self.assertEqual(l.alignof(t.Int( 32)),   4)
        self.assertEqual(l.alignof(t.Int( 33)),   8)
        self.assertEqual(l.alignof(t.Int( 63)),   8)
        self.assertEqual(l.alignof(t.Int( 64)),   8)
        self.assertEqual(l.alignof(t.Int( 65)),   8)
        self.assertEqual(l.alignof(t.Int(127)),   8)
        self.assertEqual(l.alignof(t.Int(128)),   8)
        self.assertEqual(l.alignof(t.Int(129)),   8)

        self.assertEqual(l.alignof(void_t),       1)
        self.assertEqual(l.alignof(word_t),       8)
        self.assertEqual(l.alignof(float_t),      4)
        self.assertEqual(l.alignof(double_t),     8)
        self.assertEqual(l.alignof(void_ref_t),   8)
        self.assertEqual(l.alignof(t.TagRef64(void_t)),  8)

        self.assertEqual(l.type_seq_size([]),                                 0)
        self.assertEqual(l.type_seq_size([void_t, void_t, void_t]),           0)
        self.assertEqual(l.type_seq_size([t.Struct([])]),                     0)
        self.assertEqual(l.type_seq_size([int8_t]),                           1)
        self.assertEqual(l.type_seq_size([int8_t]*5),                         5)
        self.assertEqual(l.type_seq_size([int8_t, int16_t]),                  4)
        self.assertEqual(l.type_seq_size([int8_t, int16_t, int16_t]),         6)
        self.assertEqual(l.type_seq_size([int16_t, int8_t, int8_t, int16_t]), 6)
        self.assertEqual(l.type_seq_size([int16_t, int32_t, int64_t]),       16)
        self.assertEqual(l.type_seq_size([int16_t, int32_t, int64_t]),       16)
        self.assertEqual(l.type_seq_size([int64_t, void_t, int32_t]),        12)
        self.assertEqual(l.type_seq_size([t.Struct([int8_t])]),               8)
        self.assertEqual(l.type_seq_size([t.Struct([int8_t]), int8_t]),       9)

        self.assertEqual(l.sizeof(t.Struct([int8_t])),                        8)
        self.assertEqual(l.sizeof(t.Struct([int8_t, int16_t, int32_t])),      8)
        self.assertEqual(l.sizeof(t.Struct([int64_t, int64_t, float_t])),    24)

        s1 = t.Struct([int8_t, int8_t, int32_t, int8_t])

        self.assertEqual(l.field_offset(s1, [0]), 0+l.FIELDS_OFFSET)
        self.assertEqual(l.field_offset(s1, [1]), 1+l.FIELDS_OFFSET)
        self.assertEqual(l.field_offset(s1, [2]), 4+l.FIELDS_OFFSET)
        self.assertEqual(l.field_offset(s1, [3]), 8+l.FIELDS_OFFSET)

        s2 = t.Struct([int8_t, t.Struct([double_t, int32_t, int32_t, int64_t]),
            void_ref_t])

        self.assertEqual(l.field_offset(s2, [0]),    0+l.FIELDS_OFFSET)
        self.assertEqual(l.field_offset(s2, [1,0]),  8+l.FIELDS_OFFSET)
        self.assertEqual(l.field_offset(s2, [1,1]), 16+l.FIELDS_OFFSET)
        self.assertEqual(l.field_offset(s2, [1,2]), 20+l.FIELDS_OFFSET)
        self.assertEqual(l.field_offset(s2, [1,3]), 24+l.FIELDS_OFFSET)
        self.assertEqual(l.field_offset(s2, [2]),   32+l.FIELDS_OFFSET)

        self.assertEqual(l.fixed_obj_size(int8_t), 8)
        self.assertEqual(l.fixed_obj_size(s1), 16)
        self.assertEqual(l.fixed_obj_size(s2), 40)

        self.assertEqual(l.alignof(s2), WORD_SIZE_BYTES)

        h1 = t.HybridArray(s2, s1)
        self.assertEqual(l.field_offset(h1, [], part=t.FIXED_PART), l.HYBRIDARRAY_FIELDS_OFFSET)
        self.assertEqual(l.field_offset(h1, [], part=t.VAR_PART, index=0), l.elements_offset(h1))

        self.assertEqual(l.field_offset(h1, [1],   part=t.FIXED_PART), 8+l.HYBRIDARRAY_FIELDS_OFFSET)
        self.assertEqual(l.field_offset(h1, [1,2], part=t.FIXED_PART), 20+l.HYBRIDARRAY_FIELDS_OFFSET)

        self.assertEqual(l.field_offset(h1, [], part=t.VAR_PART, index=0), l.elements_offset(h1))
        self.assertEqual(l.field_offset(h1, [], part=t.VAR_PART, index=4),
                l.align_up(l.sizeof(s1),WORD_SIZE_BYTES)*4+l.elements_offset(h1))

        self.assertEqual(l.hybridarray_obj_size(h1, 3),
                l.elements_offset(h1) + 3 * l.align_up(l.sizeof(s1), l.alignof(s1)))

        self.assertEqual(l.length_offset(h1), l.elements_offset(h1) - WORD_SIZE_BYTES)
