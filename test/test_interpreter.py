import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), "..", "src"))

import unittest

from microvm import (microvm as mvm ,mtypes as t ,snippet as s,
        snippetcheck as sc, interpreter as i, builder, directexecutor as de)
from microvm.mconstants import *
from microvm.commontypes import *

from pot import Pot

import math

def stop_thread(thread):
    thread.running = False

def mask64(n):
    return de._mask(n, 64)

def consti8(n): return s.Constant(int8_t, n)
def consti16(n): return s.Constant(int16_t, n)
def consti32(n): return s.Constant(int32_t, n)
def consti64(n): return s.Constant(int64_t, n)
def constf(n): return s.Constant(float_t, n)
def constd(n): return s.Constant(double_t, n)

class TestInterpreter(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.mvm = mvm.MicroVM()
        self.itpr = i.Interpreter(self.mvm)
        self.de = self.itpr.de

    def helpful_check_snippet(self, snp):
        try:
            sc.check_snippet(snp)
        except:
            print(s.ppsnippet(snp))
            raise

    def test_bin_op(self):
        sig = t.Signature([int64_t, int64_t, double_t, double_t], int64_t)
        b = builder.Builder(sig, "test_bin_op")
        snp = b.snp
        sid = self.itpr.declare(sig)

        pot = Pot()

        add_inst  = pot << b.BinOp(int64_t, "add",  snp.params[0], snp.params[1])
        sub_inst  = pot << b.BinOp(int64_t, "sub",  snp.params[0], snp.params[1])
        mul_inst  = pot << b.BinOp(int64_t, "mul",  snp.params[0], snp.params[1])
        udiv_inst = pot << b.BinOp(int64_t, "udiv", snp.params[0], snp.params[1])
        sdiv_inst = pot << b.BinOp(int64_t, "sdiv", snp.params[0], snp.params[1])
        urem_inst = pot << b.BinOp(int64_t, "urem", snp.params[0], snp.params[1])
        srem_inst = pot << b.BinOp(int64_t, "srem", snp.params[0], snp.params[1])
        shl_inst  = pot << b.BinOp(int64_t, "shl",  snp.params[0], snp.params[1])
        lshr_inst = pot << b.BinOp(int64_t, "lshr", snp.params[0], snp.params[1])
        ashr_inst = pot << b.BinOp(int64_t, "ashr", snp.params[0], snp.params[1])
        and_inst  = pot << b.BinOp(int64_t, "and",  snp.params[0], snp.params[1])
        or_inst   = pot << b.BinOp(int64_t, "or",   snp.params[0], snp.params[1])
        xor_inst  = pot << b.BinOp(int64_t, "xor",  snp.params[0], snp.params[1])

        fadd_inst = pot << b.BinOp(double_t, "fadd", snp.params[2], snp.params[3])
        fsub_inst = pot << b.BinOp(double_t, "fsub", snp.params[2], snp.params[3])
        fmul_inst = pot << b.BinOp(double_t, "fmul", snp.params[2], snp.params[3])
        fdiv_inst = pot << b.BinOp(double_t, "fdiv", snp.params[2], snp.params[3])
        frem_inst = pot << b.BinOp(double_t, "frem", snp.params[2], snp.params[3])

        trap_inst = pot.make_trap(b)

        self.helpful_check_snippet(snp)
        self.itpr.define(sid, snp)

        th = self.itpr.run_snippet(sid, [14, 3, 14.0, 3.0])

        self.assertEqual(pot[add_inst ],  17)
        self.assertEqual(pot[sub_inst ],  11)
        self.assertEqual(pot[mul_inst ],  42)
        self.assertEqual(pot[udiv_inst],   4)
        self.assertEqual(pot[urem_inst],   2)
        self.assertEqual(pot[sdiv_inst],   4)
        self.assertEqual(pot[srem_inst],   2)
        self.assertEqual(pot[shl_inst ], 112)
        self.assertEqual(pot[lshr_inst],   1)
        self.assertEqual(pot[ashr_inst],   1)
        self.assertEqual(pot[and_inst ],   2)
        self.assertEqual(pot[or_inst  ],  15)
        self.assertEqual(pot[xor_inst ],  13)

        self.assertAlmostEqual(pot[fadd_inst], 17.0)
        self.assertAlmostEqual(pot[fsub_inst], 11.0)
        self.assertAlmostEqual(pot[fmul_inst], 42.0)
        self.assertAlmostEqual(pot[fdiv_inst], 14.0/3.0)
        self.assertAlmostEqual(pot[frem_inst], 14.0%3.0)

        th = self.itpr.run_snippet(sid, [-14, 3, 14.0, 3.0])

        self.assertEqual(pot[add_inst ], mask64(-11 ))
        self.assertEqual(pot[sub_inst ], mask64(-17 ))
        self.assertEqual(pot[mul_inst ], mask64(-42 ))
        self.assertEqual(pot[sdiv_inst], mask64(-5  ))
        self.assertEqual(pot[srem_inst], mask64(1   ))
        self.assertEqual(pot[shl_inst ], mask64(-112))
        self.assertEqual(pot[ashr_inst], mask64(-2  ))
        self.assertEqual(pot[and_inst ], mask64(2   ))
        self.assertEqual(pot[or_inst  ], mask64(-13 ))
        self.assertEqual(pot[xor_inst ], mask64(-15 ))

    def test_cmp(self):
        sig = t.Signature([int64_t, int64_t, double_t, double_t,
            void_ref_t, void_ref_t, void_tagref64_t, void_tagref64_t], int64_t)
        b = builder.Builder(sig, "test_cmp")
        snp = b.snp
        sid = self.itpr.declare(sig)

        pot = Pot()

        eq_inst  = pot << b.Cmp(int64_t, "eq",  snp.params[0], snp.params[1])
        ne_inst  = pot << b.Cmp(int64_t, "ne",  snp.params[0], snp.params[1])
        ult_inst = pot << b.Cmp(int64_t, "ult", snp.params[0], snp.params[1])
        ule_inst = pot << b.Cmp(int64_t, "ule", snp.params[0], snp.params[1])
        ugt_inst = pot << b.Cmp(int64_t, "ugt", snp.params[0], snp.params[1])
        uge_inst = pot << b.Cmp(int64_t, "uge", snp.params[0], snp.params[1])
        slt_inst = pot << b.Cmp(int64_t, "slt", snp.params[0], snp.params[1])
        sle_inst = pot << b.Cmp(int64_t, "sle", snp.params[0], snp.params[1])
        sgt_inst = pot << b.Cmp(int64_t, "sgt", snp.params[0], snp.params[1])
        sge_inst = pot << b.Cmp(int64_t, "sge", snp.params[0], snp.params[1])

        oeq_inst = pot << b.Cmp(double_t, "oeq", snp.params[2], snp.params[3])
        one_inst = pot << b.Cmp(double_t, "one", snp.params[2], snp.params[3])
        olt_inst = pot << b.Cmp(double_t, "olt", snp.params[2], snp.params[3])
        ole_inst = pot << b.Cmp(double_t, "ole", snp.params[2], snp.params[3])
        ogt_inst = pot << b.Cmp(double_t, "ogt", snp.params[2], snp.params[3])
        oge_inst = pot << b.Cmp(double_t, "oge", snp.params[2], snp.params[3])

        req_inst = pot << b.Cmp(void_ref_t, "req", snp.params[4], snp.params[5])
        rne_inst = pot << b.Cmp(void_ref_t, "rne", snp.params[4], snp.params[5])

        treq_inst  = pot << b.Cmp(void_tagref64_t, "treq", snp.params[6], snp.params[6])
        trne_inst  = pot << b.Cmp(void_tagref64_t, "trne", snp.params[7], snp.params[7])
        treq_inst2 = pot << b.Cmp(void_tagref64_t, "treq", snp.params[6], snp.params[7])
        trne_inst2 = pot << b.Cmp(void_tagref64_t, "trne", snp.params[7], snp.params[6])

        trap_inst = pot.make_trap(b)
        
        self.helpful_check_snippet(snp)
        self.itpr.define(sid, snp)
        
        th = self.itpr.run_snippet(sid, [14, 3, 14.0, 3.0, 256, 256,
            self.de.ref_to_tr64(256, 0), self.de.ref_to_tr64(256, 1)])

        self.assertEqual(pot[eq_inst   ], 0)
        self.assertEqual(pot[ne_inst   ], 1)
        self.assertEqual(pot[ult_inst  ], 0)
        self.assertEqual(pot[ule_inst  ], 0)
        self.assertEqual(pot[ugt_inst  ], 1)
        self.assertEqual(pot[uge_inst  ], 1)
        self.assertEqual(pot[slt_inst  ], 0)
        self.assertEqual(pot[sle_inst  ], 0)
        self.assertEqual(pot[sgt_inst  ], 1)
        self.assertEqual(pot[sge_inst  ], 1)
        self.assertEqual(pot[olt_inst  ], 0)
        self.assertEqual(pot[ole_inst  ], 0)
        self.assertEqual(pot[ogt_inst  ], 1)
        self.assertEqual(pot[oge_inst  ], 1)
        self.assertEqual(pot[req_inst  ], 1)
        self.assertEqual(pot[rne_inst  ], 0)
        self.assertEqual(pot[treq_inst ], 1)
        self.assertEqual(pot[trne_inst ], 0)
        self.assertEqual(pot[treq_inst2], 0)
        self.assertEqual(pot[trne_inst2], 1)

    def test_select(self):
        sig = t.Signature([int64_t], double_t)
        b = builder.Builder(sig, "test_select")
        snp = b.snp
        sid = self.itpr.declare(sig)

        pot = Pot()

        cmp_inst = pot << b.Cmp(int64_t, "slt", snp.params[0], consti64(42))
        select_inst = pot << b.Select(double_t, cmp_inst, constd(3.14), constd(2.72))

        trap_inst = pot.make_trap(b)
        
        self.helpful_check_snippet(snp)
        self.itpr.define(sid, snp)
        
        th = self.itpr.run_snippet(sid, [42])

        self.assertEqual(pot[cmp_inst], 0)
        self.assertAlmostEqual(pot[select_inst], 2.72)

        th = self.itpr.run_snippet(sid, [41])

        self.assertEqual(pot[cmp_inst], 1)
        self.assertAlmostEqual(pot[select_inst], 3.14)

    def test_int_resize(self):
        sig = t.Signature([int32_t], void_t)
        b = builder.Builder(sig, "test_int_resize")
        snp = b.snp
        sid = self.itpr.declare(sig)

        pot = Pot()

        trunc_inst = pot << b.Trunc(32, 16, snp.params[0])
        zext_inst  = pot << b.ZExt (32, 64, snp.params[0])
        sext_inst  = pot << b.SExt (32, 64, snp.params[0])

        trap_inst = pot.make_trap(b)
        
        self.helpful_check_snippet(snp)
        self.itpr.define(sid, snp)
        
        th = self.itpr.run_snippet(sid, [0x12345678])

        self.assertEqual(pot[trunc_inst], 0x5678)
        self.assertEqual(pot[zext_inst ], 0x12345678)
        self.assertEqual(pot[sext_inst ], 0x12345678)

        th = self.itpr.run_snippet(sid, [-0x12345678])

        self.assertEqual(pot[sext_inst], mask64(-0x12345678))

    def test_fp_resize(self):
        sig = t.Signature([float_t, double_t], void_t)
        b = builder.Builder(sig, "test_fp_resize")
        snp = b.snp
        sid = self.itpr.declare(sig)

        pot = Pot()

        fptrunc_inst = pot << b.FPTrunc(double_t, float_t, snp.params[1])
        fpext_inst   = pot << b.FPExt  (float_t, double_t, snp.params[0])

        trap_inst = pot.make_trap(b)
        
        self.helpful_check_snippet(snp)
        self.itpr.define(sid, snp)
        
        th = self.itpr.run_snippet(sid, [3.14, 2.72])

        self.assertAlmostEqual(pot[fptrunc_inst], 2.72)
        self.assertAlmostEqual(pot[fpext_inst  ], 3.14)

    def test_int_fp_conversion(self):
        sig = t.Signature([int32_t, int32_t, double_t, double_t], void_t)
        b = builder.Builder(sig, "test_int_fp_conversion")
        snp = b.snp
        sid = self.itpr.declare(sig)

        pot = Pot()

        fptoui_inst  = pot << b.FPToUI(double_t, int32_t,  snp.params[2])
        fptosi_inst  = pot << b.FPToSI(double_t, int32_t,  snp.params[2])
        fptosi_inst2 = pot << b.FPToSI(double_t, int32_t,  snp.params[3])
        uitofp_inst  = pot << b.UIToFP(int32_t,  double_t, snp.params[0])
        sitofp_inst  = pot << b.SIToFP(int32_t,  double_t, snp.params[0])
        sitofp_inst2 = pot << b.SIToFP(int32_t,  double_t, snp.params[1])

        trap_inst = pot.make_trap(b)
        
        self.helpful_check_snippet(snp)
        self.itpr.define(sid, snp)
        
        th = self.itpr.run_snippet(sid, [42, -42, 3.14, -3.14])

        self.assertEqual(pot[fptoui_inst], 3)
        self.assertEqual(pot[fptosi_inst], 3)
        self.assertEqual(pot[fptosi_inst2], de._mask(-3, 32))
        self.assertAlmostEqual(pot[uitofp_inst], 42.0)
        self.assertAlmostEqual(pot[sitofp_inst], 42.0)
        self.assertAlmostEqual(pot[sitofp_inst2], -42.0)

    def test_bitcast(self):
        sig = t.Signature([int64_t, double_t], void_t) 
        b = builder.Builder(sig, "test_bitcast")
        snp = b.snp
        sid = self.itpr.declare(sig)

        pot = Pot()

        bitcast_inst1 = pot << b.BitCast(int64_t, double_t, snp.params[0])
        bitcast_inst2 = pot << b.BitCast(double_t, int64_t, snp.params[1])

        trap_inst = pot.make_trap(b)
        
        self.helpful_check_snippet(snp)
        self.itpr.define(sid, snp)
        
        th = self.itpr.run_snippet(sid, [0xbff4000000000000, 100.0])
        self.assertEqual(pot[bitcast_inst1], -1.25)
        self.assertEqual(pot[bitcast_inst2], 0x4059000000000000)

    def test_tagref64(self):
        ha_t = t.HybridArray(void_t, int8_t)
        ha_r = t.Ref(ha_t)
        ha_td = self.mvm.define_type(ha_t)

        trha_t = t.TagRef64(ha_t)

        # show = lambda x: x if x_is_int else
        #                  int(x) if x_is_fp else
        #                  int(x.var_part[0]) + tag
        show_sig = t.Signature([trha_t], int64_t)
        bs = builder.Builder(show_sig, "show")
        show_snp = bs.snp
        show_sid = self.itpr.declare(show_sig)

        [x] = show_snp.params

        bb_is_int   = bs.new_bb("bb_is_int")
        bb_test_fp  = bs.new_bb("bb_test_fp")
        bb_is_fp    = bs.new_bb("bb_is_fp")
        bb_test_ref = bs.new_bb("bb_test_ref")
        bb_is_ref   = bs.new_bb("bb_is_ref")
        bb_unreachable = bs.new_bb("bb_unreachable")

        is_int = bs.IsInt(x)
        bs.Branch2(is_int, bb_is_int, bb_test_fp)

        bs.use(bb_test_fp)
        is_fp = bs.IsFP(x)
        bs.Branch2(is_fp, bb_is_fp, bb_test_ref)

        bs.use(bb_test_ref)
        is_ref = bs.IsRef(x)
        bs.Branch2(is_ref, bb_is_ref, bb_unreachable)

        bs.use(bb_is_int)
        x_fromi_52 = bs.TR64ToInt(ha_t, x)
        x_fromi_64 = bs.ZExt(52, 64, x_fromi_52)
        bs.Ret(int64_t, x_fromi_64)

        bs.use(bb_is_fp)
        x_fromf_fp = bs.TR64ToFP(ha_t, x)
        x_fromf_64 = bs.FPToUI(double_t, int64_t, x_fromf_fp)
        bs.Ret(int64_t, x_fromf_64)

        bs.use(bb_is_ref)
        x_fromr_ref = bs.TR64ToRef(ha_t, x)
        x_fromr_8   = bs.GetField(ha_t, int8_t, [], t.VAR_PART, consti64(0), x_fromr_ref)
        x_fromr_64  = bs.ZExt(8, 64, x_fromr_8)
        x_fromr_tag = bs.TR64ToTag(ha_t, x)
        x_fromr_t64 = bs.ZExt(6, 64, x_fromr_tag)

        sumxs = bs.BinOp(int64_t, "add", x_fromr_64, x_fromr_t64)
        bs.Ret(int64_t, sumxs)

        bs.use(bb_unreachable)

        def supposed_unreached(thread):
            print(bin(pot[thread, x]))
            self.fail("This is not reachable")
            stop_thread(thread)

        bs.Trap(t.Signature([]), supposed_unreached, [])
        
        sig = t.Signature([], void_t) 
        b = builder.Builder(sig, "test_tagref64")
        snp = b.snp
        sid = self.itpr.declare(sig)

        show_const = s.Constant(t.FuncHandle(show_sig), show_sid)

        const1 = s.Constant(int52_t, 0xa123456789abc)
        const2 = constd(2.0)
        const3 = b.AllocHybridArray(ha_td, consti64(8), 0)
        constt = s.Constant(int6_t, 48)

        b.SetField(ha_t, int8_t, [], t.VAR_PART, consti64(0), const3, consti8(3))

        tr1 = b.IntToTR64(ha_t, const1)
        tr2 = b.FPToTR64 (ha_t, const2)
        tr3 = b.RefToTR64(ha_t, const3, constt)

        pot = Pot()

        call1 = pot << b.Call(show_sig, show_const, [tr1])
        call2 = pot << b.Call(show_sig, show_const, [tr2])
        call3 = pot << b.Call(show_sig, show_const, [tr3])

        trap_inst = pot.make_trap(b)
        
        self.helpful_check_snippet(show_snp)
        self.helpful_check_snippet(snp)
        
        self.itpr.define(show_sid, show_snp)
        self.itpr.define(sid, snp)

        collector = []
        th = self.itpr.run_snippet(sid, [])
        self.assertEqual(pot[call1], 0xa123456789abc)
        self.assertEqual(pot[call2], 2)
        self.assertEqual(pot[call3], 51)


    def test_branch(self):
        sig = t.Signature([], void_t)
        b = builder.Builder(sig, "test_branch")
        snp = b.snp
        sid = self.itpr.declare(sig)

        addzero_inst = b.BinOp(int64_t, "add", consti64(0), consti64(0))

        bb2 = b.new_bb("bb2")
        br_inst = b.Branch(bb2)

        b.use(bb2)
        addone_inst = b.BinOp(int64_t, "add", consti64(0), consti64(1))

        trap_reached = [False]

        def trap_check_stop(thread):
            trap_reached[0] = True
            stop_thread(thread)

        trap_inst = b.Trap(t.Signature([]), trap_check_stop, [])
        
        self.helpful_check_snippet(snp)
        self.itpr.define(sid, snp)
        
        th = self.itpr.run_snippet(sid, [])

        self.assertEqual(trap_reached[0], True)

    def test_branch2(self):
        sig = t.Signature([int64_t], void_t)
        b = builder.Builder(sig, "test_branch2")
        snp = b.snp
        sid = self.itpr.declare(sig)

        cmp_inst = b.Cmp(int64_t, "eq", snp.params[0], consti64(42))

        bb_eq = b.new_bb("bb_eq")
        bb_ne = b.new_bb("bb_ne")
        br_inst = b.Branch2(cmp_inst, bb_eq, bb_ne)

        def trap_with_num(thread, num):
            trap_reached[num] = True
            stop_thread(thread)

        b.use(bb_eq)
        trap0 = b.Trap(t.Signature([]), lambda th: trap_with_num(th, 0), [])

        b.use(bb_ne)
        trap1 = b.Trap(t.Signature([]), lambda th: trap_with_num(th, 1), [])
        
        self.helpful_check_snippet(snp)
        self.itpr.define(sid, snp)
        
        trap_reached = [False, False]
        th = self.itpr.run_snippet(sid, [42])
        
        self.assertEqual(trap_reached[0], True)
        self.assertEqual(trap_reached[1], False)

        trap_reached = [False, False]
        th = self.itpr.run_snippet(sid, [43])
        
        self.assertEqual(trap_reached[0], False)
        self.assertEqual(trap_reached[1], True)

    def test_phi(self):
        sig = t.Signature([int64_t], void_t)
        b = builder.Builder(sig, "test_phi")
        snp = b.snp
        sid = self.itpr.declare(sig)

        pot = Pot()

        cmp_inst = b.Cmp(int64_t, "eq", snp.params[0], consti64(42))

        bb_eq = b.new_bb("bb_eq")
        bb_ne = b.new_bb("bb_ne")
        bb_merge = b.new_bb("bb_merge")
        br_inst = b.Branch2(cmp_inst, bb_eq, bb_ne)

        b.use(bb_eq)
        br_inst_eq = b.Branch(bb_merge)

        b.use(bb_ne)
        br_inst_ne = b.Branch(bb_merge)

        b.use(bb_merge)
        phi_inst = pot << b.Phi(int64_t, {
            bb_eq: consti64(1),
            bb_ne: consti64(2),
            })

        trap_inst = pot.make_trap(b)
        
        self.helpful_check_snippet(snp)
        self.itpr.define(sid, snp)
        
        th = self.itpr.run_snippet(sid, [42])

        self.assertEqual(pot[phi_inst], 1)

        th = self.itpr.run_snippet(sid, [43])

        self.assertEqual(pot[phi_inst], 2)

    def test_gcd(self):
        sig = t.Signature([int64_t, int64_t], int64_t)
        b = builder.Builder(sig, "test_gcd")
        snp = b.snp
        sid = self.itpr.declare(sig)
        
        pot = Pot()

        loop_head = b.new_bb("loop_head")
        loop_body = b.new_bb("loop_body")
        loop_exit = b.new_bb("loop_exit")
        br_init = b.Branch(loop_head)

        b.use(loop_head)
        val_x = pot << b.Phi(int64_t)
        val_y = b.Phi(int64_t)

        cond = b.Cmp(int64_t, "eq", val_y, consti64(0))
        br_head = b.Branch2(cond, loop_exit, loop_body)

        b.use(loop_body)
        val_y2 = b.BinOp(int64_t, "srem", val_x, val_y)
        br_body = b.Branch(loop_head)

        b.use(loop_exit)

        trap_inst = pot.make_trap(b)

        val_x.cases = {
                snp.entry: snp.params[0],
                loop_body: val_y,
                }

        val_y.cases = {
                snp.entry: snp.params[1],
                loop_body: val_y2,
                }

        self.helpful_check_snippet(snp)
        self.itpr.define(sid, snp)
        
        th = self.itpr.run_snippet(sid, [34,55])
        
        self.assertEqual(pot[val_x], 1)

        th = self.itpr.run_snippet(sid, [72,30])
        
        self.assertEqual(pot[val_x], 6)

    def test_switch(self):
        sig = t.Signature([int64_t], void_t)
        b = builder.Builder(sig, "test_switch")
        snp = b.snp
        sid = self.itpr.declare(sig)

        bb_def = b.new_bb("bb_def")
        bb_42 = b.new_bb("bb_42")
        bb_56 = b.new_bb("bb_56")
        bb_80 = b.new_bb("bb_80")

        switch = b.Switch(int64_t, snp.params[0], bb_def, {
            consti64(42): bb_42,
            consti64(56): bb_56,
            consti64(80): bb_80,
            })

        def trap_with_num(thread, num):
            trap_reached[num] = True
            stop_thread(thread)

        b.use(bb_def)
        trap_def = b.Trap(t.Signature([]), lambda th: trap_with_num(th, 0), [])
        b.use(bb_42)
        trap_42 = b.Trap(t.Signature([]), lambda th: trap_with_num(th, 1), [])
        b.use(bb_56)
        trap_56 = b.Trap(t.Signature([]), lambda th: trap_with_num(th, 2), [])
        b.use(bb_80)
        trap_80 = b.Trap(t.Signature([]), lambda th: trap_with_num(th, 3), [])
        
        self.helpful_check_snippet(snp)
        self.itpr.define(sid, snp)
        
        trap_reached = [False, False, False, False]
        th = self.itpr.run_snippet(sid, [42])
        self.assertEqual(trap_reached, [False, True, False, False])

        trap_reached = [False, False, False, False]
        th = self.itpr.run_snippet(sid, [56])
        self.assertEqual(trap_reached, [False, False, True, False])

        trap_reached = [False, False, False, False]
        th = self.itpr.run_snippet(sid, [80])
        self.assertEqual(trap_reached, [False, False, False, True])

        trap_reached = [False, False, False, False]
        th = self.itpr.run_snippet(sid, [9])
        self.assertEqual(trap_reached, [True, False, False, False])

    def test_call(self):
        # square_sum_snp is lambda x,y: x*x + y*y
        square_sum_sig = t.Signature([int64_t, int64_t], int64_t)
        b = builder.Builder(square_sum_sig, "square_sum")
        square_sum_snp = b.snp

        x, y = square_sum_snp.params

        x2 = b.BinOp(int64_t, "mul", x, x)
        y2 = b.BinOp(int64_t, "mul", y, y)
        x2py2 = b.BinOp(int64_t, "add", x2, y2)
        b.Ret(int64_t, x2py2)

        square_sum_sid = self.itpr.declare(square_sum_sig)
        
        # the main driver function
        sig = t.Signature([int64_t, int64_t], void_t)
        b = builder.Builder(sig, "test_call")
        snp = b.snp
        sid = self.itpr.declare(sig)

        pot = Pot()

        call_inst = pot << b.Call(square_sum_sig,
            s.Constant(t.FuncHandle(square_sum_sig), square_sum_sid),
            [snp.params[0], snp.params[1]])

        trap_inst = pot.make_trap(b)

        self.helpful_check_snippet(square_sum_snp)
        self.helpful_check_snippet(snp)

        self.itpr.define(square_sum_sid, square_sum_snp)
        self.itpr.define(sid, snp)

        th = self.itpr.run_snippet(sid, [3,4])
        
        self.assertEqual(pot[call_inst], 25)

        th = self.itpr.run_snippet(sid, [100,100])
        
        self.assertEqual(pot[call_inst], 20000)

    def test_factorial(self):
        # fac = lambda x: 1 if x==0 else x * fac(x-1)
        fac_sig = t.Signature([int64_t], int64_t)
        b = builder.Builder(fac_sig, "fac")
        fac_snp = b.snp
        fac_sid = self.itpr.declare(fac_sig)

        [x] = fac_snp.params

        xeq0 = b.Cmp(int64_t, "eq", x, consti64(0))

        if_z = b.new_bb("if_z")
        if_nz = b.new_bb("if_nz")
        b.Branch2(xeq0, if_z, if_nz)

        b.use(if_z)
        b.Ret(int64_t, consti64(1))

        b.use(if_nz)
        xm1 = b.BinOp(int64_t, "sub", x, consti64(1))
        fac_xm1 = b.Call(fac_sig, s.Constant(t.FuncHandle(fac_sig), fac_sid), [xm1])
        prod = b.BinOp(int64_t, "mul", fac_xm1, x)
        b.Ret(int64_t, prod)
        
        # the main driver function
        sig = t.Signature([int64_t], void_t)
        b = builder.Builder(sig, "test_factorial")
        snp = b.snp
        sid = self.itpr.declare(sig)
        
        pot = Pot()

        call_inst = pot << b.Call(fac_sig, s.Constant(t.FuncHandle(fac_sig), fac_sid), [snp.params[0]])

        trap_inst = pot.make_trap(b)

        self.helpful_check_snippet(fac_snp)
        self.helpful_check_snippet(snp)

        self.itpr.define(fac_sid, fac_snp)
        self.itpr.define(sid, snp)

        th = self.itpr.run_snippet(sid, [0])
        
        self.assertEqual(pot[call_inst], 1)

        th = self.itpr.run_snippet(sid, [10])
        
        self.assertEqual(pot[call_inst], 3628800)

    def test_tailcall(self):
        sig = t.Signature([int64_t, int64_t], int64_t)
        f1_sig = t.Signature([int64_t, int64_t], int64_t)
        f2_sig = t.Signature([int64_t, int64_t], int64_t)

        b = builder.Builder(sig, "test_tailcall")
        snp = b.snp
        sid = self.itpr.declare(sig)

        pot = Pot()

        # f1 = lambda a,b: f2(x*x, y*y)
        # f2 = lambda x,y: x+y

        b1 = builder.Builder(f1_sig, "f1")
        f1_snp = b1.snp

        b2 = builder.Builder(f2_sig, "f2")
        f2_snp = b2.snp

        f1_sid = self.itpr.declare(f1_sig)
        f2_sid = self.itpr.declare(f2_sig)

        x1, y1 = f1_snp.params
        x12 = b1.BinOp(int64_t, "mul", x1, x1)
        y12 = b1.BinOp(int64_t, "mul", y1, y1)
        f1_call_f2 = b1.TailCall(f2_sig, s.Constant(t.FuncHandle(f2_sig), f2_sid), [x12, y12])

        x2, y2 = f2_snp.params
        x2py2 = b2.BinOp(int64_t, "add", x2, y2)
        f2_ret = b2.Ret(int64_t, x2py2)

        call_inst = pot << b.Call(f1_sig, s.Constant(t.FuncHandle(f1_sig), f1_sid), snp.params)

        trap_inst = pot.make_trap(b)

        self.helpful_check_snippet(snp)
        self.helpful_check_snippet(f1_snp)
        self.helpful_check_snippet(f2_snp)

        self.itpr.define(sid, snp)
        self.itpr.define(f1_sid, f1_snp)
        self.itpr.define(f2_sid, f2_snp)

        th = self.itpr.run_snippet(sid, [3,4])
        
        self.assertEqual(pot[call_inst], 25)

    def test_factorial_tailrec(self):
        # fac = lambda x,acc: acc if x==0 else fac(x-1, acc*x)
        # Call with fac(x, 1)
        fac_sig = t.Signature([int64_t, int64_t], int64_t)
        b = builder.Builder(fac_sig, "fac")
        fac_snp = b.snp
        fac_sid = self.itpr.declare(fac_sig)

        x, acc = fac_snp.params

        xeq0 = b.Cmp(int64_t, "eq", x, consti64(0))

        if_z = b.new_bb("if_z")
        if_nz = b.new_bb("if_nz")

        br2_inst = b.Branch2(xeq0, if_z, if_nz)

        b.use(if_z)
        ret_inst = b.Ret(int64_t, acc)

        b.use(if_nz)
        xm1 = b.BinOp(int64_t, "sub", x, consti64(1))
        prod = b.BinOp(int64_t, "mul", acc, x)
        tail_call_inst = b.TailCall(fac_sig, s.Constant(t.FuncHandle(fac_sig), fac_sid),
                [xm1, prod])
        
        # the main driver function
        sig = t.Signature([int64_t], void_t)
        b = builder.Builder(sig, "test_factorial_tailrec")
        snp = b.snp
        sid = self.itpr.declare(sig)

        pot = Pot()

        call_inst = pot << b.Call(fac_sig, s.Constant(t.FuncHandle(fac_sig), fac_sid),
            [snp.params[0], consti64(1)])

        trap_inst = pot.make_trap(b)

        self.helpful_check_snippet(fac_snp)
        self.helpful_check_snippet(snp)

        self.itpr.define(fac_sid, fac_snp)
        self.itpr.define(sid, snp)

        th = self.itpr.run_snippet(sid, [0])
        
        self.assertEqual(pot[call_inst], 1)

        th = self.itpr.run_snippet(sid, [10])
        
        self.assertEqual(pot[call_inst], 3628800)

    def test_aggregate_operations(self):
        s2_t = t.Struct([int16_t, int32_t, float_t, double_t])
        s1_t = t.Struct([int64_t, t.Struct([int64_t, int64_t, s2_t])])

        sig = t.Signature([int64_t, s1_t], void_t)
        b = builder.Builder(sig, "test_aggregate_operations")
        snp = b.snp
        sid = self.itpr.declare(sig)

        pot = Pot()

        x,y = snp.params

        e1 = pot << b.ExtractValue(int64_t, int64_t , []     , x)
        e2 = pot << b.ExtractValue(s1_t   , double_t, [1,2,3], y)

        i1 = pot << b.InsertValue(int64_t, int64_t , []     , x, consti64(84))
        i2 = pot << b.InsertValue(s1_t   , double_t, [1,2,3], y, constd(3.14))
        i3 = pot << b.InsertValue(s1_t   , int64_t , [1,1]  , y, consti64(9))
        i4 = pot << b.InsertValue(s1_t   , s2_t    , [1,2]  , y,
                s.Constant(s2_t, [32767, 40000000, 2.71, 6.28]))

        pot.make_trap(b)

        self.helpful_check_snippet(snp)
        self.itpr.define(sid, snp)

        th = self.itpr.run_snippet(sid, [42, [1,[2,3,[4,5,6.0,7.0]]]])

        self.assertEqual(pot[e1], 42)
        self.assertEqual(pot[e2], 7.0)
        self.assertEqual(pot[i1], 84)
        self.assertEqual(pot[i2], [1,[2,3,[4,5,6.0,3.14]]])
        self.assertEqual(pot[i3], [1,[2,9,[4,5,6.0,7.0]]])
        self.assertEqual(pot[i4], [1,[2,3,[32767, 40000000, 2.71, 6.28]]])

    def test_object_operations(self):
        s2_t = t.Struct([int64_t, double_t, int64_t])
        s1_t = t.Struct([int64_t, s2_t, double_t])

        s1_td = self.mvm.define_type(s1_t)

        sig = t.Signature([], void_t)
        b = builder.Builder(sig, "test_object_operations")
        snp = b.snp
        sid = self.itpr.declare(sig)

        pot = Pot()

        addr = b.Alloc(s1_td, 0)

        b.SetField(s1_t, int64_t , [0]  , obj=addr, val=consti64(42))
        b.SetField(s1_t, int64_t , [1,0], obj=addr, val=consti64(43))
        b.SetField(s1_t, double_t, [1,1], obj=addr, val=constd(3.14))
        b.SetField(s1_t, int64_t , [1,2], obj=addr, val=consti64(44))
        b.SetField(s1_t, double_t, [2]  , obj=addr, val=constd(2.72))

        gf0  = pot << b.GetField(s1_t, int64_t , [0]  , obj=addr)
        gf10 = pot << b.GetField(s1_t, int64_t , [1,0], obj=addr)
        gf11 = pot << b.GetField(s1_t, double_t, [1,1], obj=addr)
        gf12 = pot << b.GetField(s1_t, int64_t , [1,2], obj=addr)
        gf2  = pot << b.GetField(s1_t, double_t, [2]  , obj=addr)

        trap_inst = pot.make_trap(b)

        self.helpful_check_snippet(snp)
        self.itpr.define(sid, snp)

        th = self.itpr.run_snippet(sid, [])

        self.assertEqual      (pot[gf0 ], 42)
        self.assertEqual      (pot[gf10], 43)
        self.assertAlmostEqual(pot[gf11], 3.14)
        self.assertEqual      (pot[gf12], 44)
        self.assertAlmostEqual(pot[gf2 ], 2.72)

    def test_hybridarray(self):
        hh_t = t.Struct([int64_t, double_t])
        ha_t = t.HybridArray(hh_t, int8_t)
        ha_td = self.mvm.define_type(ha_t)

        sig = t.Signature([int64_t, int64_t], void_t)
        b = builder.Builder(sig, "test_hybridarray")
        snp = b.snp
        sid = self.itpr.declare(sig)

        pot = Pot()

        ha = b.AllocHybridArray(ha_td, snp.params[0], 0)

        b.SetField(ha_t, int64_t , [0], t.FIXED_PART, None     , ha, snp.params[1])
        b.SetField(ha_t, double_t, [1], t.FIXED_PART, None     , ha, constd(3.14))
        b.SetField(ha_t, int8_t  , [] , t.VAR_PART, consti64(0), ha, consti8(ord('h')))
        b.SetField(ha_t, int8_t  , [] , t.VAR_PART, consti64(1), ha, consti8(ord('e')))
        b.SetField(ha_t, int8_t  , [] , t.VAR_PART, consti64(2), ha, consti8(ord('l')))
        b.SetField(ha_t, int8_t  , [] , t.VAR_PART, consti64(3), ha, consti8(ord('l')))
        b.SetField(ha_t, int8_t  , [] , t.VAR_PART, consti64(4), ha, consti8(ord('o')))

        gf0 = pot << b.GetField(ha_t, int64_t , [0], t.FIXED_PART, None     , ha)
        gf1 = pot << b.GetField(ha_t, double_t, [1], t.FIXED_PART, None     , ha)
        gv0 = pot << b.GetField(ha_t, int8_t  , [] , t.VAR_PART, consti64(0), ha)
        gv1 = pot << b.GetField(ha_t, int8_t  , [] , t.VAR_PART, consti64(1), ha)
        gv2 = pot << b.GetField(ha_t, int8_t  , [] , t.VAR_PART, consti64(2), ha)
        gv3 = pot << b.GetField(ha_t, int8_t  , [] , t.VAR_PART, consti64(3), ha)
        gv4 = pot << b.GetField(ha_t, int8_t  , [] , t.VAR_PART, consti64(4), ha)
        glen = pot << b.GetLength(ha_t, ha)

        trap_inst = pot.make_trap(b)

        self.helpful_check_snippet(snp)
        self.itpr.define(sid, snp)

        th = self.itpr.run_snippet(sid, [30, 42])

        self.assertEqual      (pot[gf0], 42)
        self.assertAlmostEqual(pot[gf1], 3.14)
        self.assertEqual      (pot[gv0], ord('h'))
        self.assertEqual      (pot[gv1], ord('e'))
        self.assertEqual      (pot[gv2], ord('l'))
        self.assertEqual      (pot[gv3], ord('l'))
        self.assertEqual      (pot[gv4], ord('o'))
        self.assertEqual      (pot[glen], 30)

    def test_exception_handling(self):
        exc_t = t.Struct([int64_t])
        exc_td = self.mvm.define_type(exc_t)

        f1_sig = t.Signature([int64_t], int64_t)
        b1 = builder.Builder(f1_sig, "f1")
        f1_snp = b1.snp
        f1_sid = self.itpr.declare(f1_sig)

        alloc_inst = b1.Alloc(exc_td, 0)

        setfield_inst = b1.SetField(exc_t, int64_t, [0], obj=alloc_inst,
                val=f1_snp.params[0])

        throw_inst = b1.Throw(alloc_inst)

        sig = t.Signature([int64_t], void_t) 
        b = builder.Builder(sig, "test_exception_handling")
        snp = b.snp
        sid = self.itpr.declare(sig)

        pot = Pot()

        bb_cont = b.new_bb("bb_cont")
        bb_landing = b.new_bb("bb_landing")

        call_inst = b.Invoke(f1_sig, s.Constant(t.FuncHandle(f1_sig), f1_sid), [consti64(42)],
                bb_cont, bb_landing)

        b.use(bb_cont)

        def supposed_unreached(thread):
            self.fail("This is not reachable")
            stop_thread(thread)

        b.Trap(t.Signature([]), supposed_unreached, [])

        b.use(bb_landing)

        landingpad_inst = b.LandingPad()

        refcast_inst = b.RefCast(void_t, exc_t, landingpad_inst)

        getfield_inst = pot << b.GetField(exc_t, int64_t, [0], obj=refcast_inst)

        trap_inst = pot.make_trap(b)

        self.helpful_check_snippet(f1_snp)
        self.helpful_check_snippet(snp)

        self.itpr.define(f1_sid, f1_snp)
        self.itpr.define(sid, snp)

        th = self.itpr.run_snippet(sid, [42])

        self.assertEqual(pot[getfield_inst], 42)

    def test_exception_phi(self):
        exc_t = t.Struct([int64_t])
        exc_td = self.mvm.define_type(exc_t)

        f1_sig = t.Signature([int64_t], int64_t)
        b1 = builder.Builder(f1_sig, "f1")
        f1_snp = b1.snp
        f1_sid = self.itpr.declare(f1_sig)

        pot = Pot()

        alloc_inst = b1.Alloc(exc_td, 0)

        setfield_inst = b1.SetField(exc_t, int64_t, [0], obj=alloc_inst,
                val=f1_snp.params[0])

        throw_inst = b1.Throw(alloc_inst)

        sig = t.Signature([int64_t], void_t) 
        b = builder.Builder(sig, "test_exception_phi")
        snp = b.snp
        sid = self.itpr.declare(sig)

        [param0] = snp.params

        bb_cont0 = b.new_bb("bb_cont0")
        bb_cont1 = b.new_bb("bb_cont1")
        bb_cont2 = b.new_bb("bb_cont2")
        bb_landing = b.new_bb("bb_landing")

        lt0 = b.Cmp(int64_t, "slt", param0, consti64(0))
        b.Branch2(lt0, bb_cont1, bb_cont0)

        b.use(bb_cont0)
        
        call_inst = b.Invoke(f1_sig, s.Constant(t.FuncHandle(f1_sig), f1_sid), [consti64(42)],
                bb_cont2, bb_landing)

        b.use(bb_cont1)
        call_inst = b.Invoke(f1_sig, s.Constant(t.FuncHandle(f1_sig), f1_sid), [consti64(42)],
                bb_cont2, bb_landing)

        b.use(bb_landing)

        landingpad_inst = b.LandingPad()
        phi = pot << b.Phi(int64_t, {
            bb_cont0: consti64(101),
            bb_cont1: consti64(102),
            })

        b.Branch(bb_cont2)

        b.use(bb_cont2)

        pot.make_trap(b)

        self.helpful_check_snippet(f1_snp)
        self.helpful_check_snippet(snp)

        self.itpr.define(f1_sid, f1_snp)
        self.itpr.define(sid, snp)

        th = self.itpr.run_snippet(sid, [42])
        self.assertEqual(pot[phi], 101)
        th = self.itpr.run_snippet(sid, [-42])
        self.assertEqual(pot[phi], 102)

    def test_trap_call(self):
        trap_func_sig = t.Signature([int64_t], int64_t)

        sig = t.Signature([], void_t)
        b = builder.Builder(sig, "test_trap_call")
        snp = b.snp
        sid = self.itpr.declare(sig)

        pot = Pot()

        def add_and_add(thread, num):
            result_pool.append(num)
            return num+1

        trap_call_inst = pot << b.TrapCall(trap_func_sig, add_and_add, [consti64(42)])

        trap_inst = pot.make_trap(b)

        self.helpful_check_snippet(snp)
        self.itpr.define(sid, snp)

        result_pool = []
        th = self.itpr.run_snippet(sid, [])

        self.assertEqual(result_pool, [42])
        self.assertEqual(pot[trap_call_inst], 43)

    def test_intrinsic(self):
        sig = t.Signature([], void_t)
        b = builder.Builder(sig, "test_intrinsic")
        snp = b.snp
        sid = self.itpr.declare(sig)
        
        pot = Pot()

        intrinsic_pow   = pot << b.IntrinsicCall("pow"  , [constd(2.0), constd(4.0)])
        intrinsic_floor = pot << b.IntrinsicCall("floor", [constd(3.14)])

        pot.make_trap(b)

        self.helpful_check_snippet(snp)
        self.itpr.define(sid, snp)

        th = self.itpr.run_snippet(sid, [])

        self.assertEqual(pot[intrinsic_pow], math.pow(2.0, 4.0))
        self.assertEqual(pot[intrinsic_floor], math.floor(3.14))

    def test_bottom_return(self):
        sig = t.Signature([], void_t)
        b = builder.Builder(sig, "test_bottom_return")
        snp = b.snp
        sid = self.itpr.declare(sig)

        b.Ret(void_t, s.Constant(void_t, None))

        self.helpful_check_snippet(snp)
        self.itpr.define(sid, snp)

        with self.assertRaises(i.InterpreterRuntimeException):
            th = self.itpr.run_snippet(sid, [])

    def test_lazy_definition_by_interpreter(self):
        itpr = i.Interpreter(self.mvm)
        
        sig = t.Signature([], void_t)

        redef_check = [False]
        trap_check = [False]

        def redef(sid, thread):
            redef_check[0] = True

            b = builder.Builder(sig, "lazily_defined")
            snp = b.snp

            def trap_func(thread):
                trap_check[0] = True
                thread.running = False

            b.Trap(t.Signature([]), trap_func, [])

            self.helpful_check_snippet(snp)
            itpr.define(sid, snp)

        itpr.set_undefined_snippet_call_handler(redef)

        sid = itpr.declare(sig)

        itpr.run_snippet(sid, [])

        self.assertTrue(redef_check[0])
        self.assertTrue(trap_check[0])

    def test_lazy_definition_in_execution(self):
        itpr = i.Interpreter(self.mvm)
        
        sig = t.Signature([], void_t)
        sqsum_sig = t.Signature([int64_t, int64_t], int64_t)

        sid = itpr.declare(sig)
        sqsum_sid = itpr.declare(sqsum_sig)
        sqsum_const = s.Constant(t.FuncHandle(sqsum_sig), sqsum_sid)

        b = builder.Builder(sig, "old")
        snp = b.snp

        pot = Pot()

        ss1 = pot << b.Call(sqsum_sig, sqsum_const, [consti64(1), consti64(2)])
        ss2 = pot << b.Call(sqsum_sig, sqsum_const, [consti64(3), consti64(4)])
        ss3 = pot << b.Call(sqsum_sig, sqsum_const, [consti64(5), consti64(6)])
        pot.make_trap(b)

        self.helpful_check_snippet(snp)
        itpr.define(sid, snp)

        redef_check = [False]

        def redef(sid, thread):
            if sid == sqsum_sid:
                redef_check[0] = True

                b = builder.Builder(sqsum_sig, "new")
                sqsum_snp = b.snp

                x,y = sqsum_snp.params
                x2 = b.BinOp(int64_t, "mul", x, x)
                y2 = b.BinOp(int64_t, "mul", y, y)
                res = b.BinOp(int64_t, "add", x2, y2)
                b.Ret(int64_t, res)

                self.helpful_check_snippet(sqsum_snp)
                itpr.define(sqsum_sid, sqsum_snp)
            else:
                self.fail("Unexpected SID redefining")


        itpr.set_undefined_snippet_call_handler(redef)

        th = itpr.run_snippet(sid, [])

        self.assertTrue(redef_check[0])

        self.assertEqual(pot[ss1], 5)
        self.assertEqual(pot[ss2], 25)
        self.assertEqual(pot[ss3], 61)

    def test_osr(self):
        itpr = i.Interpreter(self.mvm)

        sig = t.Signature([], void_t)
        sqsum_sig = t.Signature([int64_t, int64_t], int64_t)

        sid = itpr.declare(sig)
        sqsum_sid = itpr.declare(sqsum_sig)
        sqsum_const = s.Constant(t.FuncHandle(sqsum_sig), sqsum_sid)

        b = builder.Builder(sig, "test_osr")
        snp = b.snp

        pot = Pot()

        ss1 = pot << b.Call(sqsum_sig, sqsum_const, [consti64(1), consti64(2)])
        ss2 = pot << b.Call(sqsum_sig, sqsum_const, [consti64(3), consti64(4)])
        ss3 = pot << b.Call(sqsum_sig, sqsum_const, [consti64(5), consti64(6)])
        pot.make_trap(b)

        self.helpful_check_snippet(snp)
        itpr.define(sid, snp)

        redef_check = [False]

        def redef_sqsum(thread, cur_x, cur_y):
            redef_check[0] = True

            b = builder.Builder(sqsum_sig, "sqsum")
            new_sqsum_snp = b.snp

            x,y = new_sqsum_snp.params
            x2 = b.BinOp(int64_t, "mul", x, x)
            y2 = b.BinOp(int64_t, "mul", y, y)
            res = b.BinOp(int64_t, "add", x2, y2)
            b.Ret(int64_t, res)

            self.helpful_check_snippet(new_sqsum_snp)
            itpr.define(sqsum_sid, new_sqsum_snp)

            stack = thread.stack
            top = stack.top
            top_prev = top.prev

            new_top = i.Frame.for_snippet(new_sqsum_snp, [cur_x, cur_y], stack)
            new_top.prev = top_prev
            stack.top = new_top

            thread.pc = i.PC(new_sqsum_snp.entry, 0)

        b = builder.Builder(sqsum_sig, "sqsum_stub")
        sqsum_snp = b.snp
        b.Trap(t.Signature([int64_t, int64_t]), redef_sqsum, b.snp.params)

        self.helpful_check_snippet(sqsum_snp)
        itpr.define(sqsum_sid, sqsum_snp)

        th = itpr.run_snippet(sid, [])

        self.assertTrue(redef_check[0])

        self.assertEqual(pot[ss1], 5)
        self.assertEqual(pot[ss2], 25)
        self.assertEqual(pot[ss3], 61)

