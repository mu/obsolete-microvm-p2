zparseopts c=o_cov f=o_flags
if [[ "$o_cov" != "" ]] then
    RUNNER=(python3 -m coverage run -m)
    HTML=1
else
    RUNNER=(python3 -m)
    HTML=0
fi
$RUNNER unittest $o_flags test_*.py(:s/.py//) 2>&1
if [[ HTML -eq 1 ]] then
    echo "Generating HTML..."
    python3 -m coverage html
fi
