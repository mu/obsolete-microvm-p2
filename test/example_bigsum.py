import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), "..", "src"))

import unittest

from microvm import (microvm as mvm ,mtypes as t ,snippet as s,
        snippetcheck as sc, interpreter as i, builder, directexecutor as de)
from microvm.mconstants import *
from microvm.commontypes import *

from pot import Pot

import math

def helpful_check_snippet(snp):
    try:
        sc.check_snippet(snp)
    except:
        print(s.ppsnippet(snp))
        raise

def stop_thread(thread):
    thread.running = False

def mask64(n):
    return de._mask(n, 64)

def consti8(n): return s.Constant(int8_t, n)
def consti16(n): return s.Constant(int16_t, n)
def consti32(n): return s.Constant(int32_t, n)
def consti64(n): return s.Constant(int64_t, n)
def constf(n): return s.Constant(float_t, n)
def constd(n): return s.Constant(double_t, n)

the_mvm = mvm.MicroVM()
the_itpr = i.Interpreter(the_mvm)
the_de = the_itpr.de

def mkfunc():
    sig = t.Signature([int64_t, int64_t], void_t)
    b = builder.Builder(sig, "summing")
    snp = b.snp
    sid = the_itpr.declare(sig)

    fro, to = snp.params

    pot = Pot()

    entry = snp.entry

    head = b.new_bb("head")
    b.Branch(head)

    b.use(head)

    v_num = b.Phi(int64_t, name="v_num")
    v_sum = pot << b.Phi(int64_t, name="v_sum")
    v_le  = b.Cmp(int64_t, "sle", v_num, to)

    body = b.new_bb("body")
    exit = b.new_bb("exit")
    b.Branch2(v_le, body, exit)

    b.use(body)
    v_sum2 = b.BinOp(int64_t, "add", v_sum, v_num)
    v_num2 = b.BinOp(int64_t, "add", v_num, consti64(1))
    b.Branch(head)

    v_num.cases = {
            entry: fro,
            body: v_num2,
            }

    v_sum.cases = {
            entry: consti64(0),
            body: v_sum2,
            }

    b.use(exit)
    trap_inst = pot.make_trap(b)

    helpful_check_snippet(snp)
    the_itpr.define(sid, snp)

    return sid, lambda:pot[v_sum]

the_sid, get_result = mkfunc()

the_itpr.run_snippet(the_sid, [1, 1000000])

print(get_result())

