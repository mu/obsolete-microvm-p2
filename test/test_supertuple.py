import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), "..", "src"))

import unittest

from microvm.utils.supertuple import Field, MixIn, SuperTuple

class TestSuperTuple(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        class Something(SuperTuple):
            __annotations__=["anno1", "anno2"]
            foo = Field(int)
            bar = Field(str, "This is dangerous")
            baz = Field(float)

        class SomethingElse(Something):
            __annotations__=["anno3"]
            a = Field(int)
            b = Field(float)

            OPS = ["+", "-", "*", "/"]

        class MixedDown(SuperTuple):
            pass

        class MixedUp(MixedDown):
            x = Field(int)
            s_mixin = MixIn(Something)
            y = Field(float)

        cls.Something = Something
        cls.SomethingElse = SomethingElse
        cls.MixedDown = MixedDown
        cls.MixedUp = MixedUp

    def test_basic(self):

        x = self.Something(foo=1, bar="Abc", baz=3.14)

        self.assertEqual(x.foo, 1)
        self.assertEqual(x.bar, "Abc")
        self.assertEqual(x.baz, 3.14)

        y = self.Something(1, "Abc", 3.14)

        self.assertEqual(y.foo, 1)
        self.assertEqual(y.bar, "Abc")
        self.assertEqual(y.baz, 3.14)

        z = self.Something(1, "Abc", 3.14, bar="Def")

        self.assertEqual(z.foo, 1)
        self.assertEqual(z.bar, "Def")
        self.assertEqual(z.baz, 3.14)

    def test_inheritence(self):
        x = self.SomethingElse(foo=2, bar="Def", b=2.718)

        self.assertEqual(x.foo, 2)
        self.assertEqual(x.bar, "Def")
        self.assertEqual(x.baz, None)
        self.assertEqual(x.a, None)
        self.assertEqual(x.b, 2.718)

        y = self.SomethingElse(2, "Def", 12.56, True, False)

        self.assertEqual(y.foo, 2)
        self.assertEqual(y.bar, "Def")
        self.assertEqual(y.baz, 12.56)
        self.assertEqual(y.a, True)
        self.assertEqual(y.b, False)

    def test_key_error(self):
        with self.assertRaises(AttributeError):
            z = self.Something(no_such_key=42)

    def test_field_assignment(self):
        x = self.Something(foo=1, bar="Abc", baz=3.14)
        x.baz = 6.28

        self.assertEqual(x.baz, 6.28)

    def test_unknown_fields_error(self):
        with self.assertRaises(AttributeError):
            x = self.Something(foo=1, bar="Abc", baz=3.14)
            x.no_such_key = 6.28

        with self.assertRaises(AttributeError):
            x = self.Something(1,2,3,4)

    def test_class_attributes(self):
        self.assertEqual(self.SomethingElse.OPS, ["+", "-", "*", "/"])

    def test_introspect(self):
        self.assertEqual(self.Something.__fields__["foo"].ty, int)

    def test_annotation(self):
        x = self.Something(anno1="yay")
        self.assertEqual(x.anno1, "yay")

        y = self.SomethingElse(anno2="woohoo", anno3="Louder!")
        self.assertEqual(y.anno2, "woohoo")
        self.assertEqual(y.anno3, "Louder!")

    def test_mixin(self):
        z = self.MixedUp(1,2,3,4,5)
        self.assertEqual(z.x, 1)
        self.assertEqual(z.foo, 2)
        self.assertEqual(z.bar, 3)
        self.assertEqual(z.baz, 4)
        self.assertEqual(z.y, 5)
        self.assertTrue(isinstance(z, self.Something))
        
