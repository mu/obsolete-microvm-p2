import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), "..", "src"))
TEST_TOSSA_VERBOSE = (
        os.environ.get("TEST_TOSSA_VERBOSE", None)
        in ("on", "yes", "1"))

import unittest
from lulu import tossa, lulufront

class TestToSSA(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.lulufront = lulufront.LuLuFront()

    def test_mask_search(self):
        self.assertEqual(tossa._mask_search([[1],[2],[3],[4],[]], 2), [3,4])
        self.assertEqual(tossa._mask_search([[1],[2,3],[4],[4],[]], 2), [])
        self.assertEqual(tossa._mask_search([[1],[2,3],[4],[5],[6],[6],[]], 2), [4])
        self.assertEqual(tossa._mask_search([[1],[2,3],[4],[5],[6],[6],[]], 1), [2,3,4,5,6])
        self.assertEqual(tossa._mask_search([[1],[2,4],[3,6],[2],[5],[6],[]], 2), [3])
        self.assertEqual(tossa._mask_search([[1],[2,4],[3,6],[2],[5],[6],[]], 4), [5])
        self.assertEqual(tossa._mask_search([[1],[2,4],[3,6],[2],[5],[6],[]], 3), [])

        self.assertEqual(tossa._mask_search([[1],[2],[3,4],[5,6],[8],[7],[7],[9],[9],[]], 2), [3,4,5,6,7,8,9])
        self.assertEqual(tossa._mask_search([[1],[2],[3,4],[5,6],[8],[7],[7],[9],[9],[]], 3), [5,6,7])
        self.assertEqual(tossa._mask_search([[1],[2],[3,4],[5,6],[8],[7],[7],[9],[9],[]], 5), [])
        self.assertEqual(tossa._mask_search([[1],[2],[3,4],[5,6],[8],[7],[7],[9],[9],[]], 4), [8])

    IDOM_CASES = [
        ([[1],[2,4],[3,6],[2],[5],[6],[]], [0,0,1,2,1,4,1]),
        ([[1],[2],[3,4],[5,6],[8],[7],[7],[9],[9],[]], [0,0,1,2,2,3,3,3,4,2]),
        ([[1,13],[2],[3,7],[4,5],[6],[6],[8],[8],[9],[10,11],[11],[9,12],[2,13],[]],
            [0,0,1,2,3,3,3,2,2,8,9,9,11,0]), # example from [1] in tossa.py
            ]

    def test_au_get_idoms(self):
        for succ, idoms in self.IDOM_CASES:
            self.assertEqual(tossa.au_get_idoms(succ), idoms)

    def test_dfs(self):
        self.assertEqual(tossa._dfs([[1],[2,4],[3,6],[2],[5],[6],[]]),
                ([0,1,2,3,5,6,4], [0,0,1,2,1,4,2]))
        self.assertEqual(tossa._dfs([[1],[2],[3,4],[5,6],[8],[7],[7],[9],[9],[]]),
                ([0,1,2,3,8,4,7,5,9,6],[0,0,1,2,2,3,3,5,4,7]))
        self.assertEqual(tossa._dfs([[1,13],[2],[3,7],[4,5],[6],[6],[8],[8],[9],[10,11],[11],[9,12],[2,13],[]]),
                ([0,1,2,3,4,12,5,13,6,7,8,9,10,11],[0,0,1,2,3,3,4,2,6,8,9,10,11,12]))

    def test_get_pred(self):
        self.assertEqual(tossa._get_preds([[1],[2,4],[3,6],[2],[5],[6],[]]),
                [[],[0],[1,3],[2],[1],[4],[2,5]])
        self.assertEqual(tossa._get_preds([[1],[2],[3,4],[5,6],[8],[7],[7],[9],[9],[]]),
                [[],[0],[1],[2],[2],[3],[3],[5,6],[4],[7,8]])
        self.assertEqual(tossa._get_preds([[1,13],[2],[3,7],[4,5],[6],[6],[8],[8],[9],[10,11],[11],[9,12],[2,13],[]]),
                [[],[0],[1,12],[2],[3],[3],[4,5],[2],[6,7],[8,11],[9],[9,10],[11],[0,12]])

    def test_lt_get_idoms(self):
        for succ, idoms in self.IDOM_CASES:
            self.assertEqual(tossa.lt_get_idoms(succ), idoms)

    def test_get_df(self):
        self.assertEqual(tossa.get_df(*self.IDOM_CASES[0]),
                [[],[],[2,6],[2],[6],[6],[]])
        self.assertEqual(tossa.get_df(*self.IDOM_CASES[1]),
                [[],[],[],[9],[9],[7],[7],[9],[9],[]])
        self.assertEqual(tossa.get_df(*self.IDOM_CASES[2]),
                [[],[13],[2,13],[8],[6],[6],[8],[8],[2,13],[2,9,13],[11],[2,9,13],[2,13],[]])

    def assertPhiPlacement(self, succ, init, expected):
        idom = tossa.lt_get_idoms(succ)
        df = tossa.get_df(succ, idom)
        self.assertEqual(tossa.get_phi_placement(df, init), expected)

    def test_get_phi_placement(self):
        # Branching structure
        self.assertPhiPlacement([[1],[2,3],[4],[4],[]], [0,2,3], [4])
        self.assertPhiPlacement([[1],[2,3],[4],[4],[]], [0,1], [])
        self.assertPhiPlacement([[1],[2,3],[4],[4],[]], [0,2], [4])
        self.assertPhiPlacement([[1],[2,3],[4],[4],[]], [0,1,2], [4])

        # Loop structure
        self.assertPhiPlacement([[1],[2,3],[1],[]], [0,2], [1])

        # Loop with break
        self.assertPhiPlacement([[1],[2,4],[3,4],[1],[]], [0,2], [1,4])
        self.assertPhiPlacement([[1],[2,4],[3,4],[1],[]], [0,3], [1])

        # Loop local variable
        # NOTE: This algorithm cannot handle nested local scope well and it
        # will insist on assuming all variables are local to the whole CFG.
        # Anyway, those redundant phi-nodes will be removed via dead code
        # elimination.
        self.assertPhiPlacement([[1],[2,6],[3,4],[5],[5],[1],[]], [0,2], [1])
        self.assertPhiPlacement([[1],[2,6],[3,4],[5],[5],[1],[]], [0,2,3], [1,5])

    def test_cfg_to_ssa(self):
        cfg = self.lulufront.build_from_code("""
            local x,y,z = 1
            for i = 1,10 do
                if y == nil then
                    z = 2
                else
                    z = 3
                end
                print(z)
                if i == 7 then
                    break
                end
            end
        """)

        tossa.cfg_to_ssa(cfg)


