import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), "..", "src"))
MICROVM_TEST_PRETTY_PRINT_SNIPPET = (
        os.environ.get("MICROVM_TEST_PRETTY_PRINT_SNIPPET", None)
        in ("on", "yes", "1"))

import unittest

from microvm import (mtypes as t, microvm, directexecutor as de, layout)
from microvm.mconstants import *
from microvm.commontypes import *

import math

def i64(num):
    return de._mask(num, 64)

class TestDirectExecutor(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.mvm = microvm.MicroVM()
        cls.de = de.DirectExecutor(cls.mvm)
    
    def test_mask(self):
        self.assertEqual(de._mask(0x55aa55aa55aa55aa, 32), 0x55aa55aa)
        self.assertEqual(de._uunmask(0x55aa55aa, 32), 0x55aa55aa)
        self.assertEqual(de._uunmask(0xaa55aa55, 32), 0xaa55aa55)
        self.assertEqual(de._sunmask(0x55aa55aa, 32), 0x55aa55aa)
        self.assertEqual(de._sunmask(0xaa55aa55, 32), -0x55aa55aa-1)

        self.assertEqual(de._uunmask(de._mask(0x55aa, 16), 16), 0x55aa)
        self.assertEqual(de._sunmask(de._mask(0x55aa, 16), 16), 0x55aa)
        self.assertEqual(de._sunmask(de._mask(-0x55aa, 16), 16), -0x55aa)

    def test_binop(self):
        self.assertEqual(self.de.binop_add (i64( 100), i64(30), 64), i64( 100+30))
        self.assertEqual(self.de.binop_sub (i64( 100), i64(30), 64), i64( 100-30))
        self.assertEqual(self.de.binop_mul (i64( 100), i64(30), 64), i64( 100*30))
        self.assertEqual(self.de.binop_udiv(i64( 100), i64(30), 64), i64( 100//30))
        self.assertEqual(self.de.binop_sdiv(i64( 100), i64(30), 64), i64( 100//30))
        self.assertEqual(self.de.binop_sdiv(i64(-100), i64(30), 64), i64(-100//30))
        self.assertEqual(self.de.binop_urem(i64( 100), i64(30), 64), i64( 100%30))
        self.assertEqual(self.de.binop_srem(i64( 100), i64(30), 64), i64( 100%30))
        self.assertEqual(self.de.binop_srem(i64(-100), i64(30), 64), i64(-100%30))
        self.assertEqual(self.de.binop_shl (i64( 0x1234), i64(4), 64), i64(0x1234<<4))
        self.assertEqual(self.de.binop_lshr(i64( 0x1234), i64(4), 64), i64(0x1234>>4))
        self.assertEqual(self.de.binop_ashr(i64( 0x1234), i64(4), 64), i64(0x1234>>4))
        self.assertEqual(self.de.binop_ashr(i64(-0x1234), i64(4), 64), i64((-0x1234)>>4))
        self.assertEqual(self.de.binop_and (i64(0x1234), i64(0x9876), 64), i64(0x1234&0x9876))
        self.assertEqual(self.de.binop_or  (i64(0x1234), i64(0x9876), 64), i64(0x1234|0x9876))
        self.assertEqual(self.de.binop_xor (i64(0x1234), i64(0x9876), 64), i64(0x1234^0x9876))

        self.assertEqual(self.de.binop_fadd(100.0, 30.0), 100.0+30.0)
        self.assertEqual(self.de.binop_fsub(100.0, 30.0), 100.0-30.0)
        self.assertEqual(self.de.binop_fmul(100.0, 30.0), 100.0*30.0)
        self.assertEqual(self.de.binop_fdiv(100.0, 30.0), 100.0/30.0)
        self.assertEqual(self.de.binop_frem(100.0, 30.0), 100.0%30.0)

    def test_cmp(self):
        self.assertEqual(self.de.cmp_ieq (i64( 3), i64( 4), 64), 3==4)
        self.assertEqual(self.de.cmp_ieq (i64( 4), i64( 4), 64), 4==4)
        self.assertEqual(self.de.cmp_ine (i64( 3), i64( 4), 64), 3!=4)
        self.assertEqual(self.de.cmp_ine (i64( 4), i64( 4), 64), 4!=4)
        self.assertEqual(self.de.cmp_iugt(i64( 3), i64( 4), 64), 3> 4)
        self.assertEqual(self.de.cmp_iugt(i64( 4), i64( 4), 64), 4> 4)
        self.assertEqual(self.de.cmp_iuge(i64( 3), i64( 4), 64), 3>=4)
        self.assertEqual(self.de.cmp_iuge(i64( 4), i64( 4), 64), 4>=4)
        self.assertEqual(self.de.cmp_iult(i64( 3), i64( 4), 64), 3< 4)
        self.assertEqual(self.de.cmp_iult(i64( 4), i64( 4), 64), 4< 4)
        self.assertEqual(self.de.cmp_iule(i64( 3), i64( 4), 64), 3<=4)
        self.assertEqual(self.de.cmp_iule(i64( 4), i64( 4), 64), 4<=4)
        self.assertEqual(self.de.cmp_isgt(i64( 3), i64( 4), 64), 3> 4)
        self.assertEqual(self.de.cmp_isgt(i64( 4), i64( 4), 64), 4> 4)
        self.assertEqual(self.de.cmp_isge(i64( 3), i64( 4), 64), 3>=4)
        self.assertEqual(self.de.cmp_isge(i64( 4), i64( 4), 64), 4>=4)
        self.assertEqual(self.de.cmp_islt(i64( 3), i64( 4), 64), 3< 4)
        self.assertEqual(self.de.cmp_islt(i64( 4), i64( 4), 64), 4< 4)
        self.assertEqual(self.de.cmp_isle(i64( 3), i64( 4), 64), 3<=4)
        self.assertEqual(self.de.cmp_isle(i64( 4), i64( 4), 64), 4<=4)
        self.assertEqual(self.de.cmp_isgt(i64(-3), i64( 4), 64),-3> 4)
        self.assertEqual(self.de.cmp_isgt(i64(-4), i64( 4), 64),-4> 4)
        self.assertEqual(self.de.cmp_isge(i64(-3), i64( 4), 64),-3>=4)
        self.assertEqual(self.de.cmp_isge(i64(-4), i64( 4), 64),-4>=4)
        self.assertEqual(self.de.cmp_islt(i64(-3), i64( 4), 64),-3< 4)
        self.assertEqual(self.de.cmp_islt(i64(-4), i64( 4), 64),-4< 4)
        self.assertEqual(self.de.cmp_isle(i64(-3), i64( 4), 64),-3<=4)
        self.assertEqual(self.de.cmp_isle(i64(-4), i64( 4), 64),-4<=4)

        oz = lambda x: 1 if x else 0

        self.assertEqual(self.de.cmp_ftrue (3.0, 4.0), 1)
        self.assertEqual(self.de.cmp_ffalse(3.0, 4.0), 0) 
        self.assertEqual(self.de.cmp_foeq(3.0, 4.0), oz(3.0==4.0))
        self.assertEqual(self.de.cmp_foeq(4.0, 4.0), oz(4.0==4.0))
        self.assertEqual(self.de.cmp_fone(3.0, 4.0), oz(3.0!=4.0))
        self.assertEqual(self.de.cmp_fone(4.0, 4.0), oz(4.0!=4.0))
        self.assertEqual(self.de.cmp_fogt(3.0, 4.0), oz(3.0> 4.0))
        self.assertEqual(self.de.cmp_fogt(4.0, 4.0), oz(4.0> 4.0))
        self.assertEqual(self.de.cmp_foge(3.0, 4.0), oz(3.0>=4.0))
        self.assertEqual(self.de.cmp_foge(4.0, 4.0), oz(4.0>=4.0))
        self.assertEqual(self.de.cmp_folt(3.0, 4.0), oz(3.0< 4.0))
        self.assertEqual(self.de.cmp_folt(4.0, 4.0), oz(4.0< 4.0))
        self.assertEqual(self.de.cmp_fole(3.0, 4.0), oz(3.0<=4.0))
        self.assertEqual(self.de.cmp_fole(4.0, 4.0), oz(4.0<=4.0))
        self.assertEqual(self.de.cmp_fueq(3.0, 4.0), oz(3.0==4.0))
        self.assertEqual(self.de.cmp_fueq(4.0, 4.0), oz(4.0==4.0))
        self.assertEqual(self.de.cmp_fune(3.0, 4.0), oz(3.0!=4.0))
        self.assertEqual(self.de.cmp_fune(4.0, 4.0), oz(4.0!=4.0))
        self.assertEqual(self.de.cmp_fugt(3.0, 4.0), oz(3.0> 4.0))
        self.assertEqual(self.de.cmp_fugt(4.0, 4.0), oz(4.0> 4.0))
        self.assertEqual(self.de.cmp_fuge(3.0, 4.0), oz(3.0>=4.0))
        self.assertEqual(self.de.cmp_fuge(4.0, 4.0), oz(4.0>=4.0))
        self.assertEqual(self.de.cmp_fult(3.0, 4.0), oz(3.0< 4.0))
        self.assertEqual(self.de.cmp_fult(4.0, 4.0), oz(4.0< 4.0))
        self.assertEqual(self.de.cmp_fule(3.0, 4.0), oz(3.0<=4.0))
        self.assertEqual(self.de.cmp_fule(4.0, 4.0), oz(4.0<=4.0))

    def test_select(self):
        self.assertEqual(self.de.select(1, 42, 84), 42)
        self.assertEqual(self.de.select(0, 42, 84), 84)

    def test_conv_cast(self):
        self.assertEqual(self.de.trunc( 0x55aa55aa, 32, 16), 0x55aa)
        self.assertEqual(self.de.zext ( 0x55aa, 16, 32), 0x55aa)
        self.assertEqual(self.de.zext ( 0xaa55, 16, 32), 0xaa55)
        self.assertEqual(self.de.sext ( 0x55aa, 16, 32), 0x55aa)
        self.assertEqual(self.de.sext ( 0xaa55, 16, 32), 0xffffaa55)

        self.assertEqual(self.de.fptoui(12.34, 64), i64(12))
        self.assertEqual(self.de.fptosi(12.34, 64), i64(12))
        self.assertEqual(self.de.uitofp(30000, 16), 30000.0)
        self.assertEqual(self.de.uitofp(40000, 16), 40000.0)
        self.assertEqual(self.de.sitofp(30000, 16), 30000.0)
        self.assertEqual(self.de.sitofp(-30000, 16),-30000.0)

        self.assertEqual(self.de.bitcast_i64tof(0), 0.0)
        self.assertEqual(self.de.bitcast_i64tof(0x8000000000000000), -0.0)

        # 0100 0000 0101 1001 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000
        # +|<-  1029 ->| |<-    (1.)1001                                              ->|
        # +0b1.1001 * 2**(1029-1023) = 100.0
        self.assertEqual(self.de.bitcast_i64tof(0x4059000000000000), 100.0)

        # 1100 0000 0101 1001 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000
        # -|<-  1029 ->| |<-    (1.)1001                                              ->|
        # -0b1.1001 * 2**(1029-1023) = -100.0
        self.assertEqual(self.de.bitcast_i64tof(0xc059000000000000), -100.0)

        self.assertEqual(self.de.bitcast_ftoi64(0.0), 0)

        # 1011 1111 1111 0100 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000
        # -|<-  1023 ->| |<-    (1.)0000                                              ->|
        # -0b1.0100 * 2**(1023-1023) = -1.25
        self.assertEqual(self.de.bitcast_ftoi64(-1.25), 0xbff4000000000000)

    def test_aggregate_ops(self):
        self.assertEqual(self.de.extract_value([], 42), 42)
        self.assertEqual(self.de.extract_value([1,2,3], [1,[2,3,[4,5,6,7]]]), 7)

        self.assertEqual(self.de.insert_value([], 42, 84), 84)
        self.assertEqual(self.de.insert_value([1,2,3], [1,[2,3,[4,5,6,7]]], 8),
                [1,[2,3,[4,5,6,8]]])
        self.assertEqual(self.de.insert_value([1,1], [1,[2,3,[4,5,6,7]]], 9),
                [1,[2,9,[4,5,6,7]]])
        self.assertEqual(self.de.insert_value([1,2], [1,[2,3,[4,5,6,7]]], 
            [40,50,60,70]), [1,[2,3,[40,50,60,70]]])

    def test_object_ops(self):
        i64td = self.mvm.define_type(int64_t)

        i64_1 = self.de.alloc(i64td)
        i64_2 = self.de.alloc(i64td)
        i64_3 = self.de.alloc(i64td)

        self.de.setfield(int64_t, int64_t, [], t.SCALAR, None, i64_1, 0x123456789abcdef0)
        self.de.setfield(int64_t, int64_t, [], t.SCALAR, None, i64_2, 0x23456789abcdef01)
        self.de.setfield(int64_t, int64_t, [], t.SCALAR, None, i64_3, 0x3456789abcdef012)
        i64_1r = self.de.getfield(int64_t, int64_t, [], t.SCALAR, None, i64_1)
        i64_2r = self.de.getfield(int64_t, int64_t, [], t.SCALAR, None, i64_2)
        i64_3r = self.de.getfield(int64_t, int64_t, [], t.SCALAR, None, i64_3)
        self.assertEqual(i64_1r, 0x123456789abcdef0)
        self.assertEqual(i64_2r, 0x23456789abcdef01)
        self.assertEqual(i64_3r, 0x3456789abcdef012)

        ss = t.Struct([int32_t, double_t])
        s = t.Struct([int64_t, ss, int64_t, float_t])
        s_td = self.mvm.define_type(s)

        s1_a = self.de.alloc(s_td)

        self.de.setfield(s, int64_t,  [0],   t.SCALAR, None, s1_a, 101)
        self.de.setfield(s, int32_t,  [1,0], t.SCALAR, None, s1_a, 102)
        self.de.setfield(s, double_t, [1,1], t.SCALAR, None, s1_a, 103.0)
        self.de.setfield(s, int64_t,  [2],   t.SCALAR, None, s1_a, 104)
        self.de.setfield(s, float_t,  [3],   t.SCALAR, None, s1_a, 105.0)

        s1_f0 = self.de.getfield(s, int64_t,  [0],   t.SCALAR, None, s1_a)
        s1_f10= self.de.getfield(s, int32_t,  [1,0], t.SCALAR, None, s1_a)
        s1_f11= self.de.getfield(s, double_t, [1,1], t.SCALAR, None, s1_a)
        s1_f2 = self.de.getfield(s, int64_t,  [2],   t.SCALAR, None, s1_a)
        s1_f4 = self.de.getfield(s, float_t,  [3],   t.SCALAR, None, s1_a)

        self.assertEqual(s1_f0 , 101)
        self.assertEqual(s1_f10, 102)
        self.assertEqual(s1_f11, 103.0)
        self.assertEqual(s1_f2 , 104)
        self.assertEqual(s1_f4 , 105.0)

        s1_f1 = self.de.getfield(s, ss, [1], t.SCALAR, None, s1_a)
        self.assertEqual(s1_f1 , [102, 103.0])

        self.de.setfield(s, ss, [1], t.SCALAR, None, s1_a, [99, 88.0])
        s1_f10 = self.de.getfield(s, int32_t,  [1,0], t.SCALAR, None, s1_a)
        s1_f11 = self.de.getfield(s, double_t, [1,1], t.SCALAR, None, s1_a)
        self.assertEqual(s1_f10, 99)
        self.assertEqual(s1_f11, 88.0)

        hh_t = t.Struct([int64_t, double_t])
        ha_t = t.HybridArray(hh_t, int8_t)
        ha_td = self.mvm.define_type(ha_t)
        ha = self.de.allochybridarray(ha_td, 30)

        self.de.setfield(ha_t, int64_t , [0], t.FIXED_PART, None, ha, 42  )
        self.de.setfield(ha_t, double_t, [1], t.FIXED_PART, None, ha, 3.14)
        self.de.setfield(ha_t, int8_t  , [], t.VAR_PART, 0, ha, ord('h'))
        self.de.setfield(ha_t, int8_t  , [], t.VAR_PART, 1, ha, ord('e'))
        self.de.setfield(ha_t, int8_t  , [], t.VAR_PART, 2, ha, ord('l'))
        self.de.setfield(ha_t, int8_t  , [], t.VAR_PART, 3, ha, ord('l'))
        self.de.setfield(ha_t, int8_t  , [], t.VAR_PART, 4, ha, ord('o'))

        g00 = self.de.getfield(ha_t, int64_t , [0], t.FIXED_PART, None, ha)
        g01 = self.de.getfield(ha_t, double_t, [1], t.FIXED_PART, None, ha)
        g10 = self.de.getfield(ha_t, int8_t  , [], t.VAR_PART, 0, ha)
        g11 = self.de.getfield(ha_t, int8_t  , [], t.VAR_PART, 1, ha)
        g12 = self.de.getfield(ha_t, int8_t  , [], t.VAR_PART, 2, ha)
        g13 = self.de.getfield(ha_t, int8_t  , [], t.VAR_PART, 3, ha)
        g14 = self.de.getfield(ha_t, int8_t  , [], t.VAR_PART, 4, ha)
        glen = self.de.getlength(ha_t, ha)

        self.assertEqual(g00, 42      )
        self.assertEqual(g01, 3.14    )
        self.assertEqual(g10, ord('h'))
        self.assertEqual(g11, ord('e'))
        self.assertEqual(g12, ord('l'))
        self.assertEqual(g13, ord('l'))
        self.assertEqual(g14, ord('o'))
        self.assertEqual(glen, 30)


    def test_tr64(self):
        fp = 1234567890.0
        fp_tr64 = self.de.fp_to_tr64(fp)
        #fp_bitcast = self.de.bitcast_ftoi64(fp)
        #print("\norig={:064b}\ntr64={:064b}\nback={:064b}\n".format(fp_bitcast, fp_tr64, 0))
        self.assertEqual(self.de.is_fp (fp_tr64), 1)
        self.assertEqual(self.de.is_int(fp_tr64), 0)
        self.assertEqual(self.de.is_ref(fp_tr64), 0)
        fp_back = self.de.tr64_to_fp(fp_tr64)
        self.assertEqual(fp_back, fp)

        int49 = 0x123456789abcd
        int49_tr64 = self.de.int_to_tr64(int49)
        #print("\norig={:064b}\ntr64={:064b}\nback={:064b}\n".format(int49, int49_tr64, 0))
        self.assertEqual(self.de.is_fp (int49_tr64), 0)
        self.assertEqual(self.de.is_int(int49_tr64), 1)
        self.assertEqual(self.de.is_ref(int49_tr64), 0)
        int49_back = self.de.tr64_to_int(int49_tr64)
        self.assertEqual(int49_back, int49)

        int52 = 0xa23456789abcd
        int52_tr64 = self.de.int_to_tr64(int52)
        self.assertEqual(self.de.is_fp (int52_tr64), 0)
        self.assertEqual(self.de.is_int(int52_tr64), 1)
        self.assertEqual(self.de.is_ref(int52_tr64), 0)
        int52_back = self.de.tr64_to_int(int52_tr64)
        self.assertEqual(int52_back, int52)

        ref48_hi = 0xffff8edcba987658
        ref48_hi_tr64 = self.de.ref_to_tr64(ref48_hi, 0)
        self.assertEqual(self.de.is_fp (ref48_hi_tr64), 0)
        self.assertEqual(self.de.is_int(ref48_hi_tr64), 0)
        self.assertEqual(self.de.is_ref(ref48_hi_tr64), 1)
        ref48_hi_back = self.de.tr64_to_ref(ref48_hi_tr64)
        #print("\norig={:064b}\ntr64={:064b}\nback={:064b}\n".format(ref48_hi, ref48_hi_tr64, ref48_hi_back))
        self.assertEqual(ref48_hi_back, ref48_hi)

        ref0     = 0x0
        ref0_tag = 0x35
        ref0_tr64 = self.de.ref_to_tr64(ref0, ref0_tag)
        self.assertEqual(self.de.is_fp (ref0_tr64), 0)
        self.assertEqual(self.de.is_int(ref0_tr64), 0)
        self.assertEqual(self.de.is_ref(ref0_tr64), 1)
        ref0_back     = self.de.tr64_to_ref(ref0_tr64)
        ref0_tag_back = self.de.tr64_to_tag(ref0_tr64)
        #print("\norig={:064b}\ntag ={:064b}\ntr64={:064b}\nback={:064b}\ntagb={:064b}\n".format(ref0, ref0_tag, ref0_tr64, ref0_back, ref0_tag_back))
        self.assertEqual(ref0_back , ref0)
        self.assertEqual(ref0_tag_back, ref0_tag)


        ref48_lo = 0x00007edcba987658
        ref48_tag = 0x35
        ref48_lo_tr64 = self.de.ref_to_tr64(ref48_lo, ref48_tag)
        self.assertEqual(self.de.is_fp (ref48_lo_tr64), 0)
        self.assertEqual(self.de.is_int(ref48_lo_tr64), 0)
        self.assertEqual(self.de.is_ref(ref48_lo_tr64), 1)
        ref48_lo_back  = self.de.tr64_to_ref(ref48_lo_tr64)
        ref48_tag_back = self.de.tr64_to_tag(ref48_lo_tr64)
        #print("\norig={:064b}\ntag ={:064b}\ntr64={:064b}\nback={:064b}\ntagb={:064b}\n".format(ref48_lo, ref48_tag, ref48_lo_tr64, ref48_lo_back, ref48_tag_back))
        self.assertEqual(ref48_lo_back , ref48_lo )
        self.assertEqual(ref48_tag_back, ref48_tag)

    def test_intrinsic(self):
        self.assertEqual(self.de.intrinsic_pow(2.0, 8.0), math.pow(2.0, 8.0))
        self.assertEqual(self.de.intrinsic_floor(3.5), math.floor(3.5))
