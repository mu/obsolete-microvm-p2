from microvm import mtypes as t

def stop_thread(thread):
    thread.running = False

class Pot(object):
    """
    A convenient object to record the values of instructions.
    """

    def __init__(self):
        self.records = None
        self.watch_insts = []

    def add(self, *insts):
        for inst in insts:
            self.watch_insts.append(inst)

    def __lshift__(self, other:"instruction"):
        self.watch_insts.append(other)
        return other

    def make_trap(self, b:"builder"):
        types = [v.value_type for v in self.watch_insts]
        pot = {}

        def the_trap(thread, *args):
            self.records = {}
            for inst, arg in zip(self.watch_insts, args):
                self.records[inst] = arg
            stop_thread(thread)

        return b.Trap(t.Signature(types), the_trap, self.watch_insts)
        
    def __getitem__(self, key:"instruction"):
        try:
            return self.records[key]
        except KeyError:
            raise KeyError("Instruction not recorded: {}".format(key))

