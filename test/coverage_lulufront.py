import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), "..", "src"))

import unittest

import lulu.lulufront

class CoverageLuLuFront(unittest.TestCase):
    def setUp(self):
        self.lulufront = lulu.lulufront.LuLuFront()

    def buildLua(self, code):
        self.lulufront.build_from_code(code)

    def test_shebang(self):
        self.buildLua(r"""#!/usr/bin/env lua
            print("Hello world")
        """)

    def test_literals(self):
        self.buildLua(r"""
            print(nil, false, true, 3.14, 0x100, 0x3.14, 0x3.14p3, "hello",
            "hello\tworld\n", "hello\010world\x0d\z      yeah!\n")
        """)

    def test_vars(self):
        self.buildLua(r"""
            local a,b,c
            a = 42
            b,c = 1,2
            d = 100
            local e,f = 1,2,3,4,5
            local g,h,i = 6,7
            local j,k,l = m,e,n.foo.bar[1]["2"]
            e.foo.bar = "Hello"
            o.foo = "world"
            p["foo"] = "world"
            local q,r,s = some_func(1)
            local t = (some_func(1))
            """)
        
    def test_function_call(self):
        self.buildLua(r"""
            print()
            print("Hello world",1,2,3)
            print"Hello world"
            print(4,5,6)
            local x = print(4,5,6)+1
        """)

    def test_function_def(self):
        self.buildLua(r"""
            function gf()
            end
            local function lf(a,b,c)
                return c
            end
            local af = function (d)
                local x = 42
                return 42
            end
        """)

    def test_do_st(self):
        self.buildLua(r"""
            local a,b = 42,84
            do
                local b,c = 126,168
            end
            print(a,b,c)
        """)

    def test_while_st(self):
        self.buildLua(r"""
            local a = 1
            while a < 10 do
                print(a)
                a = a + 1
            end
        """)

    def test_repeat_st(self):
        self.buildLua(r"""
            local a = 1
            repeat
                print(a)
                a = a + 1
            until a >= 10
        """)

    def test_if_st(self):
        self.buildLua(r"""
            local a = 10
            if a > 0 then
                print("Yay!")
            end
        """)
        self.buildLua(r"""
            local a = 10
            if a > 9 then
                print("Yay!")
            elseif a > 5 then
                print("Oo-hoo!")
            elseif a > 2 then
                print("Ahhhh!")
            end
        """)
        self.buildLua(r"""
            local a = 10
            if a > 9 then
                print("Yay!")
            elseif a > 5 then
                print("Oo-hoo!")
            else
                print("It still needs to be 20% cooler!")
            end
        """)

    def test_for_step_st(self):
        self.buildLua(r"""
            for i = 1,10 do
                print(i)
            end
        """)
        self.buildLua(r"""
            for i = 1,10,3 do
                print(i)
            end
        """)
        self.buildLua(r"""
            for i = 10,1,-2 do
                print(i)
            end
        """)

    def test_break_st(self):
        self.buildLua(r"""
            for i = 10,0,-1 do
                print(i)
                if i == 7 then
                    print("Counting down aborted!")
                    break
                end
            end
        """)

        with self.assertRaises(lulu.lulufront.CompileError):
            self.buildLua(r"""
                do
                    break
                end
            """)

    def test_exp(self):
        self.buildLua(r"""
            local a,b
            local add = a + b
            local sub = a - b
            local mul = a * b
            local div = a / b
            local rem = a % b
            local pow = a ^ b
            local cat = a .. b
            local eq = a == b
            local ne = a ~= b
            local lt = a < b
            local le = a <= b
            local gt = a > b
            local ge = a >= b
            local neg = -a
            local len = #a
            local n = not a
        """)

        self.buildLua(r"""
            local a = 42 and foo()
        """)

        self.buildLua(r"""
            local d = false or yay()
        """)

        

