import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), "..", "src"))
MICROVM_TEST_PRETTY_PRINT_TYPE = (
        os.environ.get("MICROVM_TEST_PRETTY_PRINT_TYPE", None)
        in ("on", "yes", "1"))

import unittest

import microvm.mtypes as t
from microvm.commontypes import *

class TestTypes(unittest.TestCase):
    @unittest.skipIf(not MICROVM_TEST_PRETTY_PRINT_TYPE,
            "Don't pollute the output unless having to")
    def test_pretty_print(self):
        print(void_t)
        print(int1_t)
        print(int8_t)
        print(int16_t)
        print(int32_t)
        print(int64_t)
        print(float_t)
        print(double_t)

        s1 = t.Struct([int8_t, int64_t, float_t, double_t])
        print(s1)

        xnode = t.Struct([int64_t])
        ynode = t.Struct([int64_t])
        xnode.fields.append(t.Ref(ynode))
        ynode.fields.append(t.Ref(xnode))

        print(xnode)

        print(t.HybridArray(int8_t , int64_t))

        print(t.Signature([int8_t, int16_t], int32_t))

        sig_t = t.mkfunchandle([int32_t], void_t)
        signal = t.mkfunchandle([int32_t, sig_t], sig_t)
        print(signal)

        print(t.Ref(s1))

    def test_equality(self):
        self.assertTrue(t.type_equals(void_t, void_t))
        self.assertTrue(t.type_equals(int8_t, int8_t))
        self.assertTrue(t.type_equals(int64_t, int64_t))
        self.assertTrue(t.type_equals(float_t, float_t))
        self.assertTrue(t.type_equals(double_t, double_t))

        s1 = t.Struct([int8_t, int64_t, float_t, double_t])
        s2 = t.Struct([int8_t, int64_t, float_t, double_t])
        s3 = t.Struct([int8_t, int64_t, float_t])
        s4 = t.Struct([int8_t, float_t, double_t])

        self.assertTrue(t.type_equals(s1, s2))
        self.assertFalse(t.type_equals(s1, s3))
        self.assertFalse(t.type_equals(s1, s4))
        self.assertFalse(t.type_equals(s3, s4))

        llnode = t.Struct([int64_t])
        llnode.fields.append(t.Ref(llnode))

        self.assertTrue(t.type_equals(llnode, llnode))

        anode = t.Struct([int64_t])
        bnode = t.Struct([int64_t])
        cnode = t.Struct([int64_t])
        anode.fields.append(t.Ref(bnode))
        bnode.fields.append(t.Ref(cnode))
        cnode.fields.append(t.Ref(anode))

        xnode = t.Struct([int64_t])
        ynode = t.Struct([int64_t])
        xnode.fields.append(t.Ref(ynode))
        ynode.fields.append(t.Ref(xnode))

        self.assertTrue(t.type_equals(anode, xnode))
        self.assertTrue(t.type_equals(anode, llnode))

        ha1 = t.HybridArray(int8_t , int64_t)
        ha2 = t.HybridArray(int8_t , int64_t)
        ha3 = t.HybridArray(int8_t , int32_t)
        ha4 = t.HybridArray(int16_t, int64_t)
        self.assertTrue(t.type_equals(ha1, ha2))
        self.assertFalse(t.type_equals(ha1, ha3))
        self.assertFalse(t.type_equals(ha1, ha4))

        func1 = t.mkfunchandle([int64_t, double_t], float_t)
        func2 = t.mkfunchandle([int64_t, double_t], float_t)
        func3 = t.mkfunchandle([int64_t, float_t], float_t)
        func4 = t.mkfunchandle([int64_t], float_t)
        func5 = t.mkfunchandle([int64_t, double_t], void_t)

        self.assertTrue(t.type_equals(func1, func2))
        self.assertFalse(t.type_equals(func1, func3))
        self.assertFalse(t.type_equals(func1, func4))
        self.assertFalse(t.type_equals(func1, func5))

        # Invoke "man 3 signal" for more information about these types.

        sig_t = t.mkfunchandle([int32_t], void_t)
        signal = t.mkfunchandle([int32_t, sig_t], sig_t)
        big_signal = t.mkfunchandle([int32_t, t.mkfunchandle([int32_t], void_t)],
                t.mkfunchandle([int32_t], void_t))

        self.assertTrue(t.type_equals(signal, big_signal))

    def test_prefix(self):
        s1 = t.Struct([int8_t, int64_t, float_t, double_t])
        s2 = t.Struct([int8_t, int64_t, float_t, double_t])
        s3 = t.Struct([int8_t, int64_t, float_t])
        s4 = t.Struct([int8_t, float_t, double_t])

        self.assertTrue (t.is_prefix(void_t, s1))
        self.assertTrue (t.is_prefix(void_t, s2))
        self.assertTrue (t.is_prefix(void_t, s3))
        self.assertTrue (t.is_prefix(void_t, s4))
        self.assertTrue (t.is_prefix(s1, s2))
        self.assertTrue (t.is_prefix(s2, s1))
        self.assertFalse(t.is_prefix(s1, s3))
        self.assertTrue (t.is_prefix(s3, s1))
        self.assertFalse(t.is_prefix(s1, s4))
        self.assertFalse(t.is_prefix(s4, s1))
        self.assertFalse(t.is_prefix(s3, s4))
        self.assertFalse(t.is_prefix(s4, s3))
        
        h1 = t.HybridArray(s1, int64_t)
        h2 = t.HybridArray(s1, int32_t)
        h3 = t.HybridArray(s2, int64_t)

        # hybrid array cannot be prefixed
        self.assertFalse(t.is_prefix(int64_t, h1))
        self.assertFalse(t.is_prefix(int64_t, h2))
        self.assertFalse(t.is_prefix(int64_t, h3))

    def test_field_type(self):
        s1 = t.Struct([int8_t, int64_t, float_t, double_t])

        self.assertTrue (t.type_equals(t.field_type(s1, [0]), int8_t  ))
        self.assertTrue (t.type_equals(t.field_type(s1, [1]), int64_t ))
        self.assertTrue (t.type_equals(t.field_type(s1, [2]), float_t ))
        self.assertTrue (t.type_equals(t.field_type(s1, [3]), double_t))

        s2 = t.Struct([int16_t, s1, int32_t])
        
        self.assertTrue (t.type_equals(t.field_type(s2, [0]), int16_t  ))
        self.assertTrue (t.type_equals(t.field_type(s2, [1]), s1 ))
        self.assertTrue (t.type_equals(t.field_type(s2, [1,2]), float_t ))
        self.assertTrue (t.type_equals(t.field_type(s2, [1,3]), double_t))
